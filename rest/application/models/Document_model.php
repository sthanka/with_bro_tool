<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
        $this->load->model('Mcommon');
    }

    public function addBulkDocuments($data)
    {
        $this->db->insert_batch('document',$data);
        return 1;
    }

    public function getDocList($data){
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/
        $this->db->select('m.id_module,u.first_name,concat(u.first_name," ",u.last_name) updated_name,concat(u1.first_name," ",u1.last_name) updated_by_name,ml.module_name,ql.question_text,tl.topic_name,d.*,uploaded_on,concat(u.first_name," ",u.last_name) as uploaded_user_name,u.user_role_id,c.contract_owner_id,c.delegate_id');
        $this->db->from('module m');
        $this->db->join('module_language ml','m.id_module = ml.module_id','');
        $this->db->join('contract_review crv','crv.id_contract_review = m.contract_review_id','LEFT');
        $this->db->join('contract c','c.id_contract = crv.contract_id and c.is_deleted=0','LEFT');

        if(isset($data['reference_type']) &&  $data['reference_type']== 'question' ){

            $this->db->join('topic t','t.module_id = m.id_module','');
            $this->db->join('topic_language tl','tl.topic_id = t.id_topic','');
            $this->db->join('question q','q.topic_id = t.id_topic','');
            $this->db->join('question_language ql','q.id_question = ql.question_id','');
            $this->db->join('document d','d.reference_id = q.id_question and d.reference_type = "question"','');
            $this->db->join('user u','u.id_user = d.uploaded_by','');
            $this->db->join('user u1','u1.id_user=d.updated_by','LEFT');
            if(isset($data['page_type']) && $data['page_type']='contract_review'){
                $this->db->where('d.reference_id IN (select q_sub.id_question from question q_sub LEFT JOIN question q2_sub on q2_sub.parent_question_id=q_sub.parent_question_id LEFT JOIN topic t2_sub on t2_sub.id_topic=q2_sub.topic_id LEFT JOIN module m2_sub on m2_sub.id_module=t2_sub.module_id LEFT JOIN contract_review cr2_sub on cr2_sub.id_contract_review=m2_sub.contract_review_id
LEFT JOIN contract c2_sub on c2_sub.id_contract=cr2_sub.contract_id and c2_sub.is_deleted=0 LEFT JOIN topic t1_sub on t1_sub.id_topic=q_sub.topic_id LEFT JOIN module m1_sub on m1_sub.id_module=t1_sub.module_id LEFT JOIN contract_review cr1_sub on cr1_sub.id_contract_review=m1_sub.contract_review_id where q2_sub.id_question='.$this->db->escape($data['reference_id']).' and cr1_sub.contract_id=cr2_sub.contract_id)',false,false);
            }
            else {
                $this->db->where('d.reference_id', $data['reference_id']);
            }
        }
        else  if(isset($data['reference_type']) && $data['reference_type'] == 'topic') {
            $this->db->join('topic t', 't.module_id = m.id_module','');
            $this->db->join('topic_language tl','tl.topic_id = t.id_topic','');
            $this->db->join('question q', 'q.topic_id = t.id_topic', '');
            $this->db->join('question_language ql','q.id_question = ql.question_id','');
            $this->db->join('document d', 'd.reference_id = q.id_question and d.reference_type = "question"', '');
            $this->db->join('user u', 'u.id_user = d.uploaded_by', '');
            $this->db->join('user u1','u1.id_user=d.updated_by','LEFT');
            if(isset($data['page_type']) && $data['page_type']='contract_review'){
                $this->db->where('t.id_topic IN (select t_sub.id_topic from contract_review cr_sub  JOIN module m_sub on m_sub.contract_review_id=cr_sub.id_contract_review JOIN topic t_sub on t_sub.module_id=m_sub.id_module JOIN topic t2_sub on t2_sub.parent_topic_id=t_sub.parent_topic_id LEFT JOIN module m2_sub on m2_sub.id_module=t2_sub.module_id LEFT JOIN contract_review cr2_sub on cr2_sub.id_contract_review=m2_sub.contract_review_id where cr_sub.contract_id=cr2_sub.contract_id and t2_sub.id_topic='.$this->db->escape($data['reference_id']).')');
            }else {
                $this->db->where('t.id_topic', $data['reference_id']);
            }
        }
        else if(isset($data['reference_type']) && $data['reference_type'] == 'module' || isset($data['module_id'])){
            $this->db->join('topic t','m.id_module = t.module_id','');
            $this->db->join('topic_language tl','tl.topic_id = t.id_topic','');
            $this->db->join('question q','q.topic_id = t.id_topic','');
            $this->db->join('question_language ql','q.id_question = ql.question_id','');
            $this->db->join('document d','d.reference_id = q.id_question and d.reference_type = "question"','');
            $this->db->join('user u','u.id_user = d.uploaded_by','');
            $this->db->join('user u1','u1.id_user=d.updated_by','LEFT');
            if(isset($data['contract_user'])){
                $this->db->join('contract_user cu','m.id_module=cu.module_id and cu.status=1','');
                $this->db->where('cu.user_id',$data['contract_user']);
            }
            if(isset($data['page_type']) && $data['page_type']='contract_overview' && isset($data['contract_id'])){
                $this->db->where('c.id_contract', $data['contract_id']);
            }
            else {
                if (isset($data['module_id']))
                    $this->db->where('m.contract_review_id', $data['module_id']);
                else
                    $this->db->where('d.module_id', $data['reference_type']);
            }
        }

        if(isset($data['document_status']))
            $this->db->where('d.document_status',$data['document_status']);
        if(isset($data['updated_by']))
            $this->db->where('d.updated_by>0');
        /* results count start */
        $all_clients_db = clone $this->db;
        $all_clients_count = $all_clients_db->count_all_results();
        /* results count end */
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('d.document_name', $data['search'], 'both');
            $this->db->or_like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.last_name', $data['search'], 'both');
            $this->db->or_like('d.uploaded_on', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(d.document_name like "%'.$data['search'].'%"
            or u.first_name like "%'.$data['search'].'%"
            or u.last_name like "%'.$data['search'].'%"
            or d.uploaded_on like "%'.$data['search'].'%")');*/
        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('d.id_document','DESC');
        $query = $this->db->get();
       // echo $this->db->last_query();exit;
        $result = $query->result_array();
        foreach ($result as $k => $v) {
            $view_access = 'annus';
            $edit_access = 'annus';
            $delete_access = 'annus';
            if(isset($data['id_user']) && isset($data['user_role_id'])) {
                if ($data['user_role_id'] == 6 || $data['user_role_id'] == 5) {
                    $delete_access = "annus";
                    if ($v['uploaded_by'] == $data['id_user']) {
                        $view_access = $edit_access = $delete_access = 'itako';
                    }
                } else if ($data['user_role_id'] == 4 || $data['user_role_id'] == 3 || $data['user_role_id'] == 2 || $data['user_role_id'] == 1) {
                    if ($v['uploaded_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $view_access = $edit_access = $delete_access = 'itako';
                    }
                }
                if($data['id_user']==$v['contract_owner_id'] || $data['id_user']==$v['delegate_id']){
                    $delete_access = "itako";
                }
            }
            else{
                $view_access = $edit_access = $delete_access = "itako";
            }
            $result[$k]['vaav']=$view_access;
            $result[$k]['eaae']=$edit_access;
            $result[$k]['daad']=$delete_access;
        }
        return array('total_records' => $all_clients_count,'data' => $result);

    }
    public function getDocLogList($data){
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/
        $this->db->select('m.id_module,u.first_name,concat(u.first_name," ",u.last_name) updated_name,concat(u1.first_name," ",u1.last_name) updated_by_name,ml.module_name,ql.question_text,tl.topic_name,d.*,uploaded_on,concat(u.first_name," ",u.last_name) as uploaded_user_name,u.user_role_id,c.contract_owner_id,c.delegate_id');
        $this->db->from('module m');
        $this->db->join('module_language ml','m.id_module = ml.module_id','');
        $this->db->join('contract_review crv','crv.id_contract_review = m.contract_review_id','LEFT');
        $this->db->join('contract c','c.id_contract = crv.contract_id and c.is_deleted=0','LEFT');

        if(isset($data['reference_type']) &&  $data['reference_type']== 'question' ){

            $this->db->join('topic t','t.module_id = m.id_module','');
            $this->db->join('topic_language tl','tl.topic_id = t.id_topic','');
            $this->db->join('question q','q.topic_id = t.id_topic','');
            $this->db->join('question_language ql','q.id_question = ql.question_id','');
            $this->db->join('document d','d.reference_id = q.id_question and d.reference_type = "question"','');
            $this->db->join('user u','u.id_user = d.uploaded_by','');
            $this->db->join('user u1','u1.id_user=d.updated_by','LEFT');
            if(isset($data['page_type']) && $data['page_type']='contract_review'){
                $this->db->where('d.reference_id IN (select q_sub.id_question from question q_sub LEFT JOIN question q2_sub on q2_sub.parent_question_id=q_sub.parent_question_id LEFT JOIN topic t2_sub on t2_sub.id_topic=q2_sub.topic_id LEFT JOIN module m2_sub on m2_sub.id_module=t2_sub.module_id LEFT JOIN contract_review cr2_sub on cr2_sub.id_contract_review=m2_sub.contract_review_id
LEFT JOIN contract c2_sub on c2_sub.id_contract=cr2_sub.contract_id and c2_sub.is_deleted=0 LEFT JOIN topic t1_sub on t1_sub.id_topic=q_sub.topic_id LEFT JOIN module m1_sub on m1_sub.id_module=t1_sub.module_id LEFT JOIN contract_review cr1_sub on cr1_sub.id_contract_review=m1_sub.contract_review_id where q2_sub.id_question='.$this->db->escape($data['reference_id']).' and cr1_sub.contract_id=cr2_sub.contract_id)',false,false);
            }
            else {
                $this->db->where('d.reference_id', $data['reference_id']);
            }
        }
        else  if(isset($data['reference_type']) && $data['reference_type'] == 'topic') {
            $this->db->join('topic t', 't.module_id = m.id_module','');
            $this->db->join('topic_language tl','tl.topic_id = t.id_topic','');
            $this->db->join('question q', 'q.topic_id = t.id_topic', '');
            $this->db->join('question_language ql','q.id_question = ql.question_id','');
            $this->db->join('document d', 'd.reference_id = q.id_question and d.reference_type = "question"', '');
            $this->db->join('user u', 'u.id_user = d.uploaded_by', '');
            $this->db->join('user u1','u1.id_user=d.updated_by','LEFT');
            if(isset($data['page_type']) && $data['page_type']='contract_review'){
                $this->db->where('t.id_topic IN (select t_sub.id_topic from contract_review cr_sub  JOIN module m_sub on m_sub.contract_review_id=cr_sub.id_contract_review JOIN topic t_sub on t_sub.module_id=m_sub.id_module JOIN topic t2_sub on t2_sub.parent_topic_id=t_sub.parent_topic_id LEFT JOIN module m2_sub on m2_sub.id_module=t2_sub.module_id LEFT JOIN contract_review cr2_sub on cr2_sub.id_contract_review=m2_sub.contract_review_id where cr_sub.contract_id=cr2_sub.contract_id and t2_sub.id_topic='.$this->db->escape($data['reference_id']).')');
            }else {
                $this->db->where('t.id_topic', $data['reference_id']);
            }
        }
        else if(isset($data['reference_type']) && $data['reference_type'] == 'module' || isset($data['module_id'])){
            $this->db->join('topic t','m.id_module = t.module_id','');
            $this->db->join('topic_language tl','tl.topic_id = t.id_topic','');
            $this->db->join('question q','q.topic_id = t.id_topic','');
            $this->db->join('question_language ql','q.id_question = ql.question_id','');
            $this->db->join('document d','d.reference_id = q.id_question and d.reference_type = "question"','');
            $this->db->join('user u','u.id_user = d.uploaded_by','');
            $this->db->join('user u1','u1.id_user=d.updated_by','LEFT');
            if(isset($data['contract_user'])){
                $this->db->join('contract_user cu','m.id_module=cu.module_id and cu.status=1','');
                $this->db->where('cu.user_id',$data['contract_user']);
            }
            if(isset($data['page_type']) && $data['page_type']='contract_overview' && isset($data['contract_id'])){
                $this->db->where('c.id_contract', $data['contract_id']);
            }
            else {
                if (isset($data['module_id']))
                    $this->db->where('m.contract_review_id', $data['module_id']);
                else
                    $this->db->where('d.module_id', $data['reference_type']);
            }
        }

        if(isset($data['document_status']))
            $this->db->where('d.document_status',$data['document_status']);
        if(isset($data['updated_by']))
            $this->db->where('d.updated_by>0');
        /* results count start */
        $all_clients_db = clone $this->db;
        $all_clients_count = $all_clients_db->count_all_results();
        /* results count end */
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('d.document_name', $data['search'], 'both');
            $this->db->or_like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.last_name', $data['search'], 'both');
            $this->db->or_like('d.uploaded_on', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(d.document_name like "%'.$data['search'].'%"
            or u.first_name like "%'.$data['search'].'%"
            or u.last_name like "%'.$data['search'].'%"
            or d.uploaded_on like "%'.$data['search'].'%")');*/
        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        $this->db->order_by('d.updated_on DESC');
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        $result = $query->result_array();
        foreach ($result as $k => $v) {
            $view_access = 'annus';
            $edit_access = 'annus';
            $delete_access = 'annus';
            if(isset($data['id_user']) && isset($data['user_role_id'])) {
                if ($data['user_role_id'] == 6 || $data['user_role_id'] == 5) {
                    $delete_access = "annus";
                    if ($v['uploaded_by'] == $data['id_user']) {
                        $view_access = $edit_access = $delete_access = 'itako';
                    }
                } else if ($data['user_role_id'] == 4 || $data['user_role_id'] == 3 || $data['user_role_id'] == 2 || $data['user_role_id'] == 1) {
                    if ($v['uploaded_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $view_access = $edit_access = $delete_access = 'itako';
                    }
                }
                if($data['id_user']==$v['contract_owner_id'] || $data['id_user']==$v['delegate_id']){
                    $delete_access = "itako";
                }
            }
            else{
                $view_access = $edit_access = $delete_access = "itako";
            }
            $result[$k]['vaav']=$view_access;
            $result[$k]['eaae']=$edit_access;
            $result[$k]['daad']=$delete_access;
        }
        return array('total_records' => $all_clients_count,'data' => $result);

    }

    /*public function getAllDoccumentList($data)
    {        ///// Brings documents from all reviews and sends to contract review page.
        $this->db->select('m.id_module,ml.module_name,d.*,date_format(d.uploaded_on,\'%Y-%m-%d\') as uploaded_on,concat(u.first_name," ",u.last_name) as uploaded_user_name,u.user_role_id,c.contract_owner_id,c.delegate_id');
        $this->db->from('document d');
        $this->db->join('module m','m.id_module = d.module_id','');
        $this->db->join('module_language ml','m.id_module = ml.module_id','');
        $this->db->join('contract_review crv','crv.id_contract_review = m.contract_review_id','LEFT');
        $this->db->join('contract c','c.id_contract = crv.contract_id','LEFT');
        $this->db->join('user u','u.id_user=d.uploaded_by','LEFT');
        $this->db->where('d.module_id IN (select id_contract_review from contract_review where contract_id='.$data['id_contract'].')');
        $query = $this->db->get();
        $result = $query->result_array();

        foreach ($result as $k => $v) {
            $view_access = 'annus;
            $edit_access = 'annus;
            $delete_access = 'annus;
            if(isset($data['id_user']) && isset($data['user_role_id'])) {
                if ($data['user_role_id'] == 6 || $data['user_role_id'] == 5) {
                    $delete_access = "itako;
                    if ($v['uploaded_by'] == $data['id_user']) {
                        $view_access = $edit_access = $delete_access = "itako;
                    }
                } else if ($data['user_role_id'] == 4 || $data['user_role_id'] == 3 || $data['user_role_id'] == 2 || $data['user_role_id'] == 1) {
                    if ($v['uploaded_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $view_access = $edit_access = $delete_access = "itako;
                    }
                }
                if($data['id_user']==$v['contract_owner_id'] || $data['id_user']==$v['delegate_id']){
                    $delete_access = "itako;
                }
            }
            else{
                $view_access = $edit_access = $delete_access = "itako;
            }
            $result[$k]['vaav']=$view_access;
            $result[$k]['eaae']=$edit_access;
            $result[$k]['daad']=$delete_access;
        }

        return $result;


    }*/

    public function getDocumentsList($data)
    {
        $this->db->select('d.*,concat(u.first_name," ",u.last_name) updated_name,concat(u1.first_name," ",u1.last_name) updated_by_name,u.user_role_id,CONCAT(u.first_name," ",u.last_name) as uploaded_user,DATE_FORMAT(d.uploaded_on,"%Y-%m-%d") as updated_date');
        $this->db->from('document d');
        $this->db->join('user u','u.id_user=d.uploaded_by','LEFT');
        $this->db->join('user u1','u1.id_user=d.updated_by','LEFT');
        if(isset($data['module_id']))
            $this->db->where('d.module_id',$data['module_id']);
        if(isset($data['module_type']))
            $this->db->where('d.module_type',$data['module_type']);
        if(isset($data['reference_id']))
            $this->db->where('d.reference_id',$data['reference_id']);
        if(isset($data['reference_type']))
            $this->db->where('d.reference_type',$data['reference_type']);
        if(isset($data['document_status']))
            $this->db->where('d.document_status',$data['document_status']);
        if(isset($data['id_document']))
            $this->db->where('d.id_document',$data['id_document']);
        if(isset($data['updated_by']))
            $this->db->where('d.updated_by>0');
        $query = $this->db->get();
        $result=$query->result_array();
        foreach ($result as $k => $v) {
            $view_access = 'annus';
            $edit_access = 'annus';
            $delete_access = 'annus';
            if(isset($data['id_user']) && isset($data['user_role_id'])) {
                if ($data['user_role_id'] == 6 || $data['user_role_id'] == 5) {
                    if ($v['uploaded_by'] == $data['id_user']) {
                        $view_access = $edit_access = $delete_access = "itako";
                    }
                } else if ($data['user_role_id'] == 4 || $data['user_role_id'] == 3 || $data['user_role_id'] == 2 || $data['user_role_id'] == 1) {
                    if ($v['uploaded_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $view_access = $edit_access = $delete_access = "itako";
                    }
                }
            }
            else{
                $view_access = $edit_access = $delete_access = "itako";
            }
            if($v['reference_type']=='contract'){
                if(isset($data['id_user']) && isset($data['user_role_id'])) {
                    if($data['user_role_id']==2){

                    }
                    else {
                        if ((isset($data['contract_owner_id']) && $data['id_user'] == $data['contract_owner_id']) || (isset($data['delegate_id']) && $data['id_user'] == $data['delegate_id'])) {
                            $delete_access = "itako";
                        }
                    }
                }
            }
            $result[$k]['vaav']=$view_access;
            $result[$k]['eaae']=$edit_access;
            $result[$k]['daad']=$delete_access;
        }

        return $result;
    }
    public function getDocumentsListPagination($data)
    {
        $this->db->select('d.*,CONCAT_WS(\' \',u.first_name,u.last_name) as uploaded_user_name,u.user_role_id');
        $this->db->from('document d');
        $this->db->join('user u','u.id_user=d.uploaded_by','LEFT');
        if(isset($data['module_id']))
            $this->db->where('d.module_id',$data['module_id']);
        if(isset($data['module_type']))
            $this->db->where('d.module_type',$data['module_type']);
        if(isset($data['reference_id']))
            $this->db->where('d.reference_id',$data['reference_id']);
        if(isset($data['reference_type']))
            $this->db->where('d.reference_type',$data['reference_type']);
        if(isset($data['document_status']))
            $this->db->where('d.document_status',$data['document_status']);
        if(isset($data['id_document']))
            $this->db->where('d.id_document',$data['id_document']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('d.document_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(d.document_name like "%'.$data['search'].'%")');*/

        /* results count start */
        $all_clients_db = clone $this->db;
        $all_clients_count = $all_clients_db->count_all_results();
        /* results count end */

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('d.id_document','DESC');
        $query = $this->db->get();
        $result=$query->result_array();
        foreach ($result as $k => $v) {
            $view_access = 'annus';
            $edit_access = 'annus';
            $delete_access = 'annus';
            if(isset($data['id_user']) && isset($data['user_role_id'])) {
                if ($data['user_role_id'] == 6 || $data['user_role_id'] == 5) {
                    if ($v['uploaded_by'] == $data['id_user']) {
                        $view_access = $edit_access = $delete_access = "itako";
                    }
                } else if ($data['user_role_id'] == 4 || $data['user_role_id'] == 3 || $data['user_role_id'] == 2 || $data['user_role_id'] == 1) {
                    if ($v['uploaded_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $view_access = $edit_access = $delete_access = "itako";
                    }
                }
            }
            else{
                $view_access = $edit_access = $delete_access = "itako";
            }
            $result[$k]['vaav']=$view_access;
            $result[$k]['eaae']=$edit_access;
            $result[$k]['daad']=$delete_access;
        }
        return array('total_records' => $all_clients_count,'data' => $result);
    }
    public function addDocument($data)
    {
        $this->db->insert_batch('document', $data);
        return 1;
    }
    public function updateDocument($data)
    {
        $this->db->where('id_document', $data['id_document']);
        $this->db->update('document', $data);
        return 1;
    }

    public function deleteDocument($user,$data,$source)
    {
        $this->db->where('id_document',$data['id_document']);
        /*$this->db->delete('document');*/
        $this->db->update('document', array('document_status'=>0,'updated_by'=>$user,'updated_on'=>currentDate(),'document_source'=>str_replace('/', '/deleted/', $source)));
        return 1;
    }
    public function getDocument($data)
    {
        $this->db->select('*');
        $this->db->from('document');
        $this->db->where('id_document',$data['id_document']);
        $query = $this->db->get();
        $result=$query->result_array();
        return $result;
    }

}