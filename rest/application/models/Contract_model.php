<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contract_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
        $this->load->model('Mcommon');
    }
    

    public function getContractList($data)
    {
        if(isset($data['contract_status']))
            $data['contract_status']=explode(',',$data['contract_status']);
        $this->db->select('c.*,rcl.relationship_category_name,max(`crv`.`id_contract_review`) as id_contract_review,IF(IFNULL(c.parent_contract_id,0)>0,\'sub_agreement\',IF((select count(cpa.id_contract) from contract cpa where cpa.parent_contract_id=c.id_contract)>0,\'parent_agreement\',\'agreement\')) as agreement_type,contract_progress(c.id_contract) as contract_progress_percentage');
        $this->db->from('contract c');
        $this->db->join('business_unit bu','bu.id_business_unit=c.business_unit_id','left');
        $this->db->join('currency cu','c.currency_id=cu.id_currency','left');
        $this->db->join('relationship_category_language rcl','c.relationship_category_id=rcl.relationship_category_id and language_id=1','left');
        $this->db->join('contract_review crv','crv.contract_id=c.id_contract','left');
        if(isset($data['customer_user'])) {
            $this->db->join('contract_user cur', 'c.id_contract=cur.contract_id and cur.status=1', '');
            $this->db->join('module m', 'm.id_module=cur.module_id', '');
            $this->db->where('cur.user_id',$data['customer_user']);
            $this->db->where('cur.status','1');
            $this->db->where('m.contract_review_id=(select max(id_contract_review) from contract_review where contract_id=c.id_contract)');
        }
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('c.contract_name', $data['search'], 'both');
            $this->db->or_like('rcl.relationship_category_name', $data['search'], 'both');
            $this->db->or_like('c.provider_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(c.contract_name like "%'.($data['search']).'%"
            or rcl.relationship_category_name like "%'.($data['search']).'%"
            or c.provider_name like "%'.($data['search']).'%")');*/
        if(isset($data['business_unit_id']) && !is_array($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all')
            $this->db->where('c.business_unit_id',$data['business_unit_id']);
        if(isset($data['id_business_unit']) && !is_array($data['id_business_unit']) && strtolower($data['id_business_unit'])!='all')
            $this->db->where('c.business_unit_id',$data['id_business_unit']);
        if(isset($data['customer_id']))
            $this->db->where('bu.customer_id',$data['customer_id']);
        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        if(isset($data['contract_owner_id']))
            $this->db->where('c.contract_owner_id',$data['contract_owner_id']);
        if(isset($data['created_by']))
            $this->db->where('c.created_by',$data['created_by']);
        if(isset($data['contract_status']) && !is_array($data['contract_status']))
            $this->db->where('c.contract_status',$data['contract_status']);
        if(isset($data['contract_status']) && is_array($data['contract_status']))
            $this->db->where_in('c.contract_status',$data['contract_status']);
        if(isset($data['provider_name']) && strtolower($data['provider_name'])!='all')
            $this->db->where('c.provider_name',$data['provider_name']);
        if(isset($data['parent_contract_id']) && isset($data['parent_contract_id'])>0)
            $this->db->where('c.parent_contract_id',$data['parent_contract_id']);
        else
            $this->db->where('c.parent_contract_id',0);
        if(isset($data['deleted'])){

        }
        else
            $this->db->where('c.is_deleted','0');
        $this->db->group_by('c.id_contract');
        $this->db->order_by('c.contract_name','asc');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $all_clients_count = $query->num_rows();

        /* results count end */

        $this->db->select('c.*,cu.currency_name,rc.classification_name,rcl.relationship_category_name,max(`crv`.`id_contract_review`) as id_contract_review,IF(IFNULL(c.parent_contract_id,0)>0,\'sub_agreement\',IF((select count(cpa.id_contract) from contract cpa where cpa.parent_contract_id=c.id_contract)>0,\'parent_agreement\',\'agreement\')) as agreement_type,contract_progress(c.id_contract) as contract_progress_percentage');
        $this->db->from('contract c');
        $this->db->join('business_unit bu','bu.id_business_unit=c.business_unit_id','left');
        $this->db->join('currency cu','c.currency_id=cu.id_currency','left');
        $this->db->join('relationship_category_language rcl','c.relationship_category_id=rcl.relationship_category_id and language_id=1','left');
        $this->db->join('contract_review crv','crv.contract_id=c.id_contract','left');
        $this->db->join('relationship_classification_language rc','rc.relationship_classification_id=c.classification_id','left');
        if(isset($data['customer_user'])) {
            $this->db->join('contract_user cur', 'c.id_contract=cur.contract_id and cur.status=1', '');
            $this->db->join('module m', 'm.id_module=cur.module_id', '');
            $this->db->where('cur.user_id',$data['customer_user']);
            $this->db->where('m.contract_review_id=(select max(id_contract_review) from contract_review where contract_id=c.id_contract)');
        }
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('c.contract_name', $data['search'], 'both');
            $this->db->or_like('rcl.relationship_category_name', $data['search'], 'both');
            $this->db->or_like('c.provider_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(c.contract_name like "%'.($data['search']).'%"
            or rcl.relationship_category_name like "%'.($data['search']).'%"
            or c.provider_name like "%'.($data['search']).'%")');*/
        if(isset($data['business_unit_id'])  && !is_array($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all')
            $this->db->where('c.business_unit_id',$data['business_unit_id']);
        if(isset($data['id_business_unit']) && !is_array($data['id_business_unit']) && strtolower($data['id_business_unit'])!='all')
            $this->db->where('c.business_unit_id',$data['id_business_unit']);
        if(isset($data['customer_id']))
            $this->db->where('bu.customer_id',$data['customer_id']);
        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        if(isset($data['created_by']))
            $this->db->where('c.created_by',$data['created_by']);
        if(isset($data['contract_status']) && !is_array($data['contract_status']))
            $this->db->where('c.contract_status',$data['contract_status']);
        if(isset($data['contract_status']) && is_array($data['contract_status']))
            $this->db->where_in('c.contract_status',$data['contract_status']);
        if(isset($data['provider_name']) && strtolower($data['provider_name'])!='all')
            $this->db->where('c.provider_name',$data['provider_name']);
        if(isset($data['parent_contract_id']) && isset($data['parent_contract_id'])>0)
            $this->db->where('c.parent_contract_id',$data['parent_contract_id']);
        else
            $this->db->where('c.parent_contract_id',0);
        if(isset($data['deleted'])){

        }
        else
            $this->db->where('c.is_deleted','0');
        $this->db->group_by('c.id_contract');

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('c.provider_name,c.contract_name','asc');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return array('total_records' => $all_clients_count,'data' => $query->result_array());
    }

    public function getDeletedContractList($data)
    {
        if(isset($data['contract_status']))
            $data['contract_status']=explode(',',$data['contract_status']);
        $this->db->select('c.*,concat(u.first_name," ",u.last_name) deleted_by,rcl.relationship_category_name,max(`crv`.`id_contract_review`) as id_contract_review,IF(IFNULL(c.parent_contract_id,0)>0,\'sub_agreement\',IF((select count(cpa.id_contract) from contract cpa where cpa.parent_contract_id=c.id_contract)>0,\'parent_agreement\',\'agreement\')) as agreement_type,contract_progress(c.id_contract) as contract_progress_percentage');
        $this->db->from('contract c');
        $this->db->join('business_unit bu','bu.id_business_unit=c.business_unit_id','left');
        $this->db->join('currency cu','c.currency_id=cu.id_currency','left');
        $this->db->join('relationship_category_language rcl','c.relationship_category_id=rcl.relationship_category_id and language_id=1','left');
        $this->db->join('contract_review crv','crv.contract_id=c.id_contract','left');
        $this->db->join('user u','c.updated_by = u.id_user','left');
        if(isset($data['customer_user'])) {
            $this->db->join('contract_user cur', 'c.id_contract=cur.contract_id and cur.status=1', '');
            $this->db->join('module m', 'm.id_module=cur.module_id', '');
            $this->db->where('cur.user_id',$data['customer_user']);
            $this->db->where('m.contract_review_id=(select max(id_contract_review) from contract_review where contract_id=c.id_contract)');
        }
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('c.contract_name', $data['search'], 'both');
            $this->db->or_like('rcl.relationship_category_name', $data['search'], 'both');
            $this->db->or_like('c.provider_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(c.contract_name like "%'.($data['search']).'%"
            or rcl.relationship_category_name like "%'.($data['search']).'%"
            or c.provider_name like "%'.($data['search']).'%")');*/
        if(isset($data['business_unit_id']) && !is_array($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all')
            $this->db->where('c.business_unit_id',$data['business_unit_id']);
        if(isset($data['id_business_unit']) && !is_array($data['id_business_unit']) && strtolower($data['id_business_unit'])!='all')
            $this->db->where('c.business_unit_id',$data['id_business_unit']);
        if(isset($data['customer_id']))
            $this->db->where('bu.customer_id',$data['customer_id']);
        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        if(isset($data['contract_owner_id']))
            $this->db->where('c.contract_owner_id',$data['contract_owner_id']);
        if(isset($data['contract_status']) && !is_array($data['contract_status']))
            $this->db->where('c.contract_status',$data['contract_status']);
        if(isset($data['contract_status']) && is_array($data['contract_status']))
            $this->db->where_in('c.contract_status',$data['contract_status']);
        if(isset($data['provider_name']) && strtolower($data['provider_name'])!='all')
            $this->db->where('c.provider_name',$data['provider_name']);
        if(isset($data['parent_contract_id'])  && isset($data['parent_contract_id'])>0)
            $this->db->where('c.parent_contract_id',$data['parent_contract_id']);
        $this->db->where('c.is_deleted','1');
        $this->db->group_by('c.id_contract');
        $this->db->order_by('c.contract_name','asc');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $all_clients_count = $query->num_rows();

        /* results count end */

        $this->db->select('c.*,concat(u.first_name," ",u.last_name) deleted_by,cu.currency_name,rc.classification_name,rcl.relationship_category_name,max(`crv`.`id_contract_review`) as id_contract_review,IF(IFNULL(c.parent_contract_id,0)>0,\'sub_agreement\',IF((select count(cpa.id_contract) from contract cpa where cpa.parent_contract_id=c.id_contract)>0,\'parent_agreement\',\'agreement\')) as agreement_type,contract_progress(c.id_contract) as contract_progress_percentage');
        $this->db->from('contract c');
        $this->db->join('business_unit bu','bu.id_business_unit=c.business_unit_id','left');
        $this->db->join('currency cu','c.currency_id=cu.id_currency','left');
        $this->db->join('relationship_category_language rcl','c.relationship_category_id=rcl.relationship_category_id and language_id=1','left');
        $this->db->join('contract_review crv','crv.contract_id=c.id_contract','left');
        $this->db->join('relationship_classification_language rc','rc.relationship_classification_id=c.classification_id','left');
        $this->db->join('user u','c.updated_by = u.id_user','left');
        if(isset($data['customer_user'])) {
            $this->db->join('contract_user cur', 'c.id_contract=cur.contract_id and cur.status=1', '');
            $this->db->join('module m', 'm.id_module=cur.module_id', '');
            $this->db->where('cur.user_id',$data['customer_user']);
            $this->db->where('m.contract_review_id=(select max(id_contract_review) from contract_review where contract_id=c.id_contract)');
        }
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('c.contract_name', $data['search'], 'both');
            $this->db->or_like('rcl.relationship_category_name', $data['search'], 'both');
            $this->db->or_like('c.provider_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(c.contract_name like "%'.($data['search']).'%"
            or rcl.relationship_category_name like "%'.($data['search']).'%"
            or c.provider_name like "%'.($data['search']).'%")');*/
        if(isset($data['business_unit_id'])  && !is_array($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all')
            $this->db->where('c.business_unit_id',$data['business_unit_id']);
        if(isset($data['id_business_unit']) && !is_array($data['id_business_unit']) && strtolower($data['id_business_unit'])!='all')
            $this->db->where('c.business_unit_id',$data['id_business_unit']);
        if(isset($data['customer_id']))
            $this->db->where('bu.customer_id',$data['customer_id']);
        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        if(isset($data['contract_status']) && !is_array($data['contract_status']))
            $this->db->where('c.contract_status',$data['contract_status']);
        if(isset($data['contract_status']) && is_array($data['contract_status']))
            $this->db->where_in('c.contract_status',$data['contract_status']);
        if(isset($data['provider_name']) && strtolower($data['provider_name'])!='all')
            $this->db->where('c.provider_name',$data['provider_name']);
        if(isset($data['parent_contract_id'])  && isset($data['parent_contract_id'])>0)
            $this->db->where('c.parent_contract_id',$data['parent_contract_id']);
        $this->db->where('c.is_deleted','1');
        $this->db->group_by('c.id_contract');

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('c.updated_on','desc');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return array('total_records' => $all_clients_count,'data' => $query->result_array());
    }

    public function getProviders($data){
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/

        $this->db->select('distinct(c.provider_name)');
        $this->db->from('contract c');
        $this->db->join('business_unit bu','bu.id_business_unit=c.business_unit_id','left');
        $this->db->join('currency cu','c.currency_id=cu.id_currency','left');
        $this->db->join('relationship_category_language rcl','c.relationship_category_id=rcl.relationship_category_id and language_id=1','left');
        $this->db->join('contract_review crv','crv.contract_id=c.id_contract and crv.contract_review_status="initiated"','left');
        if(isset($data['customer_user'])) {
            $this->db->join('contract_user cur', 'c.id_contract=cur.contract_id', '');
            $this->db->where('cur.user_id',$data['customer_user']);
        }
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('c.contract_name', $data['search'], 'both');
            $this->db->or_like('rcl.relationship_category_name', $data['search'], 'both');
            $this->db->or_like('c.provider_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(c.contract_name like "%'.$data['search'].'%" or rcl.relationship_category_name like "%'.$data['search'].'%" or c.provider_name like "%'.$data['search'].'%")');*/
        if(isset($data['business_unit_id']) && !is_array($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all')
            $this->db->where('c.business_unit_id',$data['business_unit_id']);
        if(isset($data['id_business_unit']) && !is_array($data['id_business_unit']) && strtolower($data['id_business_unit'])!='all')
            $this->db->where('c.business_unit_id',$data['id_business_unit']);
        if(isset($data['customer_id']))
            $this->db->where('bu.customer_id',$data['customer_id']);
        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where('c.id_contract in (select contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id='.$data['session_user_id'].'  and cux.status=1)',null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        if(isset($data['contract_status']))
            $this->db->where('c.contract_status',$data['contract_status']);
        $this->db->where('c.is_deleted','0');
        $this->db->group_by('c.id_contract');
        $this->db->order_by('c.provider_name','asc');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getContracts($data){
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/

        $this->db->select('c.contract_name,c.id_contract as contract_id');
        $this->db->from('contract c');
        $this->db->join('business_unit bu','bu.id_business_unit=c.business_unit_id','left');
        $this->db->join('currency cu','c.currency_id=cu.id_currency','left');
        $this->db->join('relationship_category_language rcl','c.relationship_category_id=rcl.relationship_category_id and language_id=1','left');
        $this->db->join('contract_review crv','crv.contract_id=c.id_contract and crv.contract_review_status="initiated"','left');
        if(isset($data['customer_user'])) {
            $this->db->join('contract_user cur', 'c.id_contract=cur.contract_id', '');
            $this->db->where('cur.user_id',$data['customer_user']);
        }
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('c.contract_name', $data['search'], 'both');
            $this->db->or_like('rcl.relationship_category_name', $data['search'], 'both');
            $this->db->or_like('c.provider_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(c.contract_name like "%'.$data['search'].'%" or rcl.relationship_category_name like "%'.$data['search'].'%" or c.provider_name like "%'.$data['search'].'%")');*/
        if(isset($data['business_unit_id']) && !is_array($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all')
            $this->db->where('c.business_unit_id',$data['business_unit_id']);
        if(isset($data['customer_id']))
            $this->db->where('bu.customer_id',$data['customer_id']);
        /*if(isset($data['business_unit_id']) && is_array($data['business_unit_id']))
            $this->db->where_in('c.business_unit_id',$data['business_unit_id']);*/
        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        /*if(isset($data['delegate_id']))
            $this->db->where('c.delegate_id',$data['delegate_id']);*/
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        if(isset($data['contract_status']))
            $this->db->where('c.contract_status',$data['contract_status']);
        $this->db->where('c.is_deleted','0');
        $this->db->group_by('c.id_contract');
        $this->db->order_by('c.contract_name','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getContractListCount($data)
    {
        if(isset($data['contract_status']))
            $data['contract_status']=explode(',',$data['contract_status']);
        $this->db->select('COUNT(DISTINCT c.id_contract) as total_records');
        $this->db->from('contract c');
        $this->db->join('business_unit bu','bu.id_business_unit=c.business_unit_id','left');
        if(isset($data['customer_user'])) {
            $this->db->join('contract_user cur', 'c.id_contract=cur.contract_id and cur.status=1', '');
            $this->db->join('module m', 'm.id_module=cur.module_id', '');
            $this->db->where('cur.user_id',$data['customer_user']);
            $this->db->where('m.contract_review_id=(select max(id_contract_review) from contract_review where contract_id=c.id_contract)');
        }
        if(isset($data['business_unit_id']) && !is_array($data['business_unit_id']))
            $this->db->where('c.business_unit_id',$data['business_unit_id']);
        if(isset($data['customer_id']))
            $this->db->where('bu.customer_id',$data['customer_id']);
        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']) && count($data['business_unit_id'])>0)
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        if(isset($data['contract_status']) && !is_array($data['contract_status']))
            $this->db->where('c.contract_status',$data['contract_status']);
        if(isset($data['contract_status']) && is_array($data['contract_status']))
            $this->db->where_in('c.contract_status',$data['contract_status']);
        if(isset($data['contract_owner_id']))
            $this->db->where('c.contract_owner_id',$data['contract_owner_id']);
        if(isset($data['parent_contract_id']) && isset($data['parent_contract_id'])>0)
            $this->db->where('c.parent_contract_id',$data['parent_contract_id']);
        else
            $this->db->where('c.parent_contract_id',0);
        $this->db->where('c.is_deleted','0');
        $this->db->group_by('c.id_contract');
        $query = $this->db->get();
        /*echo "<pre>";print_r($query->num_rows());echo "</pre>";exit;
        $all_clients_count = $query->result_array();*/
        return $query->num_rows();
    }

    public function getContractDetails($data)
    {
        if(isset($data['contract_review_status']))
            $data['contract_review_status']=$this->db->escape($data['contract_review_status']);
        $this->db->select('c.*,bu.bu_name,rcl.relationship_category_name,rcll.classification_name,cr.currency_name,CONCAT_WS(\' \',u.first_name,u.last_name) as delegate_user_name,CONCAT_WS(\' \',u1.first_name,u1.last_name) as responsible_user_name,crv.id_contract_review,crv.contract_review_status,if(crv.contract_review_status="review in progress","itako","annus") as reaaer,IF(IFNULL(c.parent_contract_id,0)>0,\'sub_agreement\',IF((select count(cpa.id_contract) from contract cpa where cpa.parent_contract_id=c.id_contract)>0,\'parent_agreement\',\'agreement\')) as agreement_type,contract_progress(c.id_contract) as contract_progress_percentage');
        $this->db->from('contract c');
        $this->db->join('business_unit bu','bu.id_business_unit=c.business_unit_id','left');
        $this->db->join('relationship_category_language rcl','rcl.relationship_category_id=c.relationship_category_id and rcl.language_id=1','LEFT');
        $this->db->join('relationship_classification_language rcll','rcll.relationship_classification_id=c.classification_id and rcll.language_id=1','LEFT');
        $this->db->join('currency cr','cr.id_currency=c.currency_id','LEFT');
        $this->db->join('user u','u.id_user=c.delegate_id','left');
        $this->db->join('user u1','u1.id_user=c.contract_owner_id','left');
        if(isset($data['contract_review_status']))
            $this->db->join('contract_review crv','crv.contract_id=c.id_contract and crv.contract_review_status="'.$data["contract_review_status"].'"','left',false);
        else
            $this->db->join('contract_review crv','crv.contract_id=c.id_contract','left');
        if(isset($data['business_unit_id']))
            $this->db->where('c.business_unit_id',$data['business_unit_id']);
        if(isset($data['id_contract']))
            $this->db->where('c.id_contract',$data['id_contract']);
        if(isset($data['contract_review_id']))
            $this->db->where('crv.id_contract_review',$data['contract_review_id']);
        $this->db->where('c.is_deleted','0');
        $this->db->order_by('crv.id_contract_review','DESC');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getContractCurrentDetails($data){
        $this->db->select('c.*,rcl.relationship_category_name,bu.bu_name as business_unit,CONCAT_WS(\' \',u2.first_name,u2.last_name) as created_by,rcl.relationship_category_name,rcll.classification_name,cr.currency_name,CONCAT_WS(\' \',u.first_name,u.last_name) as delegate_user_name,CONCAT_WS(\' \',u1.first_name,u1.last_name) as contract_owner_name');
        $this->db->from('contract c');
        $this->db->join('relationship_category_language rcl','rcl.relationship_category_id=c.relationship_category_id and rcl.language_id=1','LEFT');
        $this->db->join('relationship_classification_language rcll','rcll.relationship_classification_id=c.classification_id and rcll.language_id=1','LEFT');
        $this->db->join('currency cr','cr.id_currency=c.currency_id','LEFT');
        $this->db->join('user u','u.id_user=c.delegate_id','left');
        $this->db->join('user u1','u1.id_user=c.contract_owner_id','left');
        $this->db->join('user u2','u2.id_user=c.created_by','left');
        $this->db->join('business_unit bu','bu.id_business_unit=c.business_unit_id','left');
        $this->db->where('c.id_contract',$data['contract_id']);
        $this->db->where('c.is_deleted','0');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getContractLogId($data){
        $this->db->select('c.id_contract_log,c.created_on log_created_on,CONCAT( `u`.`first_name`,\' \', u.last_name) log_user_name,CONCAT(DATE_FORMAT(c.created_on,\'%d-%m-%Y\'),\' \', TIME(c.created_on),\' by \', CONCAT( `u`.`first_name`,\' \', u.last_name)) as log_by');
        $this->db->from('contract_log c');
        $this->db->join('user u','c.created_by = u.id_user','left');
        $this->db->where('contract_id',$data['contract_id']);
        $this->db->order_by('id_contract_log','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getContractLogDetails($data){
        $this->db->select('c.*,rcl.relationship_category_name,bu.bu_name as business_unit,CONCAT_WS(\' \',u2.first_name,u2.last_name) as created_by,rcl.relationship_category_name,rcll.classification_name,cr.currency_name,CONCAT_WS(\' \',u.first_name,u.last_name) as delegate_user_name,CONCAT_WS(\' \',u1.first_name,u1.last_name) as contract_owner_name');
        $this->db->from('contract_log c');
        $this->db->join('relationship_category_language rcl','rcl.relationship_category_id=c.relationship_category_id and rcl.language_id=1','LEFT');
        $this->db->join('relationship_classification_language rcll','rcll.relationship_classification_id=c.classification_id and rcll.language_id=1','LEFT');
        $this->db->join('currency cr','cr.id_currency=c.currency_id','LEFT');
        $this->db->join('user u','u.id_user=c.delegate_id','left');
        $this->db->join('user u1','u1.id_user=c.contract_owner_id','left');
        $this->db->join('user u2','u2.id_user=c.created_by','left');
        $this->db->join('business_unit bu','bu.id_business_unit=c.business_unit_id','left');
        $this->db->where('c.id_contract_log',$data['contract_log_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addContract($data)
    {
        $this->db->insert('contract', $data);
        return $this->db->insert_id();
    }

    public function updateContract($data)
    {
        $this->db->where('id_contract', $data['id_contract']);
        $this->db->update('contract', $data);
        return 1;
    }

    public function getContractReviewActionItems($data)
    {
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/
        $this->db->select('c.*,concat(u.first_name," ",u.last_name) as user_name,ml.module_name,tl.topic_name,u1.user_role_id,concat(u1.first_name," ",u1.last_name) as created_by_name');
        $this->db->from('contract_review_action_item c');
        $this->db->join('user u','c.responsible_user_id=u.id_user','left');
        $this->db->join('module_language ml','ml.module_id=c.module_id and ml.language_id=1','left');
        $this->db->join('topic_language tl','tl.topic_id=c.topic_id and tl.language_id=1','left');
        $this->db->join('user u1','u1.id_user=c.created_by');
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('c.action_item', $data['search'], 'both');
            $this->db->or_like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.last_name', $data['search'], 'both');
            $this->db->or_like('c.description', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(c.action_item like "%'.$data['search'].'%" or u.first_name like "%'.$data['search'].'%" or u.last_name like "%'.$data['search'].'%" or c.description like "%'.$data['search'].'%")');*/
        if(isset($data['contract_id']))
            $this->db->where('c.contract_id',$data['contract_id']);

        if(isset($data['page_type']) && $data['page_type']='contract_review'){
            if(isset($data['module_id']))
                $this->db->where('c.module_id IN (select m.id_module from contract_review cr JOIN module m on m.contract_review_id=cr.id_contract_review join module m2 on m2.parent_module_id=m.parent_module_id where cr.contract_id=c.contract_id and m2.id_module='.$data['module_id'].')');
            if(isset($data['topic_id']))
                $this->db->where('c.topic_id IN (select t.id_topic from contract_review cr LEFT JOIN module m on m.contract_review_id=cr.id_contract_review JOIN topic t on t.module_id=m.id_module JOIN topic t2 on t2.parent_topic_id=t.parent_topic_id where cr.contract_id=c.contract_id and t2.id_topic='.$data['topic_id'].')');
        }
        else{
            if(isset($data['contract_review_id']))
                $this->db->where('c.contract_review_id',$data['contract_review_id']);
            if(isset($data['id_contract_review']))
                $this->db->where('c.contract_review_id',$data['id_contract_review']);
            if(isset($data['module_id']))
                $this->db->where('c.module_id',$data['module_id']);
            if(isset($data['topic_id']))
                $this->db->where('c.topic_id',$data['topic_id']);
        }
        if(isset($data['item_status']))
            $this->db->where('c.item_status',$data['item_status']);
        if(isset($data['id_contract_review_action_item']))
            $this->db->where('c.id_contract_review_action_item',$data['id_contract_review_action_item']);
        if(isset($data['id_user']) && isset($data['user_role_id'])){
            if($data['user_role_id']==5){
                $this->db->group_start();
                $this->db->where('c.created_by', $data['id_user']);
                $this->db->or_where('c.responsible_user_id', $data['id_user']);
                $this->db->group_end();
            }
            else if($data['user_role_id']==4 || $data['user_role_id']==3 || $data['user_role_id']==2 || $data['user_role_id']==1){
                $this->db->group_start();
                $this->db->where('c.created_by', $data['id_user']);
                $this->db->or_where('u1.user_role_id>=', 2);
                $this->db->or_where('c.responsible_user_id', $data['id_user']);
                $this->db->group_end();
            }
            else if($data['user_role_id']==6){
                $this->db->group_start();
                $this->db->where('c.created_by', $data['id_user']);
                $this->db->or_where('u1.user_role_id>=', 2);
                $this->db->or_where('c.responsible_user_id', $data['id_user']);
                $this->db->group_end();
            }
        }
        /* results count start */
        $all_clients_db = clone $this->db;
        $all_clients_count = $all_clients_db->count_all_results();
        /* results count end */

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('c.id_contract_review_action_item','DESC');
        $query = $this->db->get();
        $result= $query->result_array();

        foreach ($result as $k => $v) {
            $view_access = 'annus';
            $edit_access = 'annus';
            $delete_access = 'annus';
            $status_change_access='annus';
            if(isset($data['id_user']) && isset($data['user_role_id'])) {
                $view_access = "itako";
                if ($data['user_role_id'] == 6 || $data['user_role_id'] == 5) {
                    if ($v['created_by'] == $data['id_user']) {
                        $edit_access = $delete_access = 'itako';
                    }
                    if ($v['responsible_user_id'] == $data['id_user'] || $v['created_by'] == $data['id_user']) {
                        $view_access = "itako";
                    }
                } else if ($data['user_role_id'] == 4 || $data['user_role_id'] == 3 || $data['user_role_id'] == 2 || $data['user_role_id'] == 1) {
                    $view_access = "itako";
                    if ($v['created_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $edit_access = $delete_access = 'itako';
                    }
                    if ($v['responsible_user_id'] == $data['id_user'] || $v['created_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $view_access = "itako";
                    }
                }
            }
            else{
                $view_access = $edit_access = $delete_access = 'itako';
            }
            //$view_access="itako;
            if($view_access=="itako" && $v['status']!='completed')
                $status_change_access="itako";
            if($v['status']=='completed')
                $edit_access=$delete_access='annus';
            $result[$k]['vaav']=$view_access;
            $result[$k]['eaae']=$edit_access;
            $result[$k]['daad']=$delete_access;
            $result[$k]['scaacs']=$status_change_access;

            $this->db->select('c.*,concat(u.first_name," ",u.last_name) as user_name');
            $this->db->from('contract_review_action_item_log c');
            $this->db->join('user u','c.updated_by=u.id_user','left');
            $this->db->where('c.contract_review_action_item_id', $v['id_contract_review_action_item']);
            $this->db->where('c.updated_by is not null');
            $query_log = $this->db->get();
            $result[$k]['comments_log']= $query_log->result_array();
        }

        return array('total_records' => $all_clients_count,'data' => $result);
    }

    public function contractReviewActionItemLog($data)
    {
        $this->db->select('c.id_contract_review_action_item_log,c.contract_review_action_item_id,c.comments,c.updated_by,DATE_FORMAT(c.updated_on,"%Y-%m-%d") as updated_on,concat(u.first_name," ",u.last_name) as user_name');
        $this->db->from('contract_review_action_item_log c');
        $this->db->join('user u','c.updated_by=u.id_user','left');
        if(isset($data['id_contract_review_action_item']))
            $this->db->where('c.contract_review_action_item_id', $data['id_contract_review_action_item']);
        $this->db->where('c.updated_by is not null');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addContractReviewActionItem($data)
    {
        $this->db->insert('contract_review_action_item', $data);
        return $this->db->insert_id();
    }

    public function getContractReview($data)
    {
        $this->db->select('cr.*,IFNULL(cr.updated_on,cr.created_on) as updated_date,CONCAT_WS(\' \',u.first_name,u.last_name) as updated_user_name,c.business_unit_id');
        $this->db->from('contract_review cr');
        $this->db->join('user u','u.id_user=cr.created_by','LEFT');
        $this->db->join('contract c','c.id_contract=cr.contract_id and c.is_deleted=0','LEFT');
        if(isset($data['contract_id']))
            $this->db->where('cr.contract_id',$data['contract_id']);
        if(isset($data['status']))
            $this->db->where('cr.contract_review_status',$data['status']);
        if(isset($data['id_contract_review']))
            $this->db->where('cr.id_contract_review',$data['id_contract_review']);
        if(isset($data['order']))
            $this->db->order_by('cr.id_contract_review',$data['order']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateContractReviewActionItem($data)
    {
        //$this->addContractReviewActionItemLog($data);
        $this->db->where('id_contract_review_action_item', $data['id_contract_review_action_item']);
        $this->db->update('contract_review_action_item', $data);
        return 1;
    }

    /*public function addContractReviewActionItemLog($data){
        $this->db->select("id_contract_review_action_item,comments,CASE WHEN updated_on IS NOT NULL THEN updated_on ELSE created_on END AS created_on,CASE WHEN updated_by IS NOT NULL THEN updated_by ELSE created_by END AS created_by",FALSE);
        $this->db->from('contract_review_action_item crai');
        $this->db->where('crai.id_contract_review_action_item',$data['id_contract_review_action_item']);
        $query = $this->db->get();
        $result=$query->result_array();
        foreach($result as $k=>$v){
            $insert_log_data=array();
            $insert_log_data['contract_review_action_item_id']=$v['id_contract_review_action_item'];
            $insert_log_data['comments']=$v['comments'];
            $insert_log_data['created_by']=$v['created_by'];
            $insert_log_data['created_on']=$v['created_on'];
            $this->db->insert('contract_review_action_item_log', $insert_log_data);
        }
        return 1;

    }*/

    public function getDelegates($data=array()){
        $this->db->select('u.id_user,CONCAT(CONCAT_WS(" ",u.first_name,u.last_name), CONCAT(" (", CONCAT_WS(" | ", u.email, ur.user_role_name, bu.bu_name), ")")) as user_name,u.email');
        $this->db->from('business_unit_user buu');
        $this->db->join('user u','u.id_user=buu.user_id','left');
        $this->db->join('business_unit bu','bu.id_business_unit=buu.business_unit_id','left');
        $this->db->join('user_role ur','ur.id_user_role=u.user_role_id and ur.role_status=1','left');
        $this->db->where('ur.user_role_name','BU Delegate');
        $this->db->where('buu.status','1');
        $this->db->where('bu.status','1');
        $this->db->where('u.user_status','1');
        if(isset($data['id_business_unit']))
            $this->db->where('buu.business_unit_id',$data['id_business_unit']);
        $query = $this->db->get();
        // echo $this->db->last_query(); die('test');
        return $query->result_array();
    }

    public function getBusinessUnitUsers($data)
    {
        $this->db->select('u.id_user,u.user_role_id,CONCAT(u.first_name," ",u.last_name) as showname,CONCAT(u.first_name," ",u.last_name," ( ",u.email," | ",bu.bu_name," | ",ur.user_role_name,")") as name');
        $this->db->from('user u');
        $this->db->join('user_role ur','u.user_role_id=ur.id_user_role and ur.role_status=1','left');
        $this->db->join('business_unit_user bur','bur.user_id=u.id_user and bur.status=1','left');
        $this->db->join('business_unit bu','bur.business_unit_id=bu.id_business_unit and bu.status=1','left');
        if(isset($data['business_unit_id']))
            $this->db->where('bur.business_unit_id',$data['business_unit_id']);
        if(isset($data['user_role_id']))
            $this->db->where('u.user_role_id',$data['user_role_id']);
        if(isset($data['customer_id']))                             //added new by sp
            $this->db->where('u.customer_id',$data['customer_id']);
        if(isset($data['user_id_not']))
            $this->db->where('ur.id_user_role not in (2,5,6)');
        $this->db->where('ur.role_status = 1');
        $this->db->group_by('u.id_user');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function getCustomerUsers($data)
    {
        $this->db->select('u.id_user,u.user_role_id,CONCAT(u.first_name," ",u.last_name) as name');
        $this->db->from('user u');
        $this->db->join('user_role ur','u.user_role_id=ur.id_user_role and ur.role_status=1','left');
        $this->db->join('business_unit_user bur','bur.user_id=u.id_user and status=1','left');
        if(isset($data['business_unit_id']))
            $this->db->where('bur.business_unit_id',$data['business_unit_id']);
        if(isset($data['customer_id']))
            $this->db->where('u.customer_id',$data['customer_id']);
        if(isset($data['type']) && $data['type']='contributor'){
            $this->db->where('u.id_user not in (select contract_owner_id from contract where id_contract='.$data['contract_id'].')');
            $this->db->where('u.id_user not in (select delegate_id from contract where id_contract='.$data['contract_id'].')');
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCustomerUsers_add($data)
    {
        $this->db->select('u.id_user,u.user_role_id,CONCAT(u.first_name," ",u.last_name) as showname,CONCAT(u.first_name," ",u.last_name,"( ",u.email," | ",ur.user_role_name," | ",IFNULL(GROUP_CONCAT(bu.bu_name),\'\')," )") as name');
        $this->db->from('user u');
        $this->db->join('user_role ur','u.user_role_id=ur.id_user_role','left');
        $this->db->join('business_unit_user bur','bur.user_id=u.id_user and bur.status=1','left');
        $this->db->join('business_unit bu','bur.business_unit_id=bu.id_business_unit and bu.status=1','left');
        if(isset($data['business_unit_id']))
            $this->db->where('bu.id_business_unit',$data['business_unit_id']);
        if(isset($data['customer_id']))
            $this->db->where('u.customer_id',$data['customer_id']);
        if(isset($data['type']) && $data['type']='contributor'){
            $this->db->where('u.id_user not in (select contract_owner_id from contract where id_contract='.$data['contract_id'].')');
            $this->db->where('u.id_user not in (select delegate_id from contract where id_contract='.$data['contract_id'].')');
        }
        $this->db->where('ur.role_status = 1');
        $this->db->where('u.user_status = 1');
        $this->db->where('ur.id_user_role not in (2,5,6)');
        $this->db->group_by('u.id_user');
        $query = $this->db->get();//echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function getContractReviewUsers($data)
    {
        $this->db->select('u.id_user,u.user_role_id,CONCAT(u.first_name," ",u.last_name) as name');
        $this->db->from('contract_user cu');
        $this->db->join('user u','u.id_user=cu.user_id','left');
        if(isset($data['contract_id']))
            $this->db->where('cu.contract_id',$data['contract_id']);
        if(isset($data['module_id']))
            $this->db->where('cu.module_id',$data['module_id']);
        if(isset($data['user_id']))
            $this->db->where('cu.user_id',$data['user_id']);
        if(isset($data['user_role_id']))
            $this->db->where('u.user_role_id',$data['user_role_id']);
        $this->db->where('cu.status',1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addContractReview($data)
    {
        $this->db->insert('contract_review', $data);
        return $this->db->insert_id();
    }
    public function checkContributorForContractReview($data){
        $this->db->select('cu.*');
        $this->db->from('contract_user cu');
        $this->db->where('cu.contract_review_id',$data['contract_review_id']);
        $this->db->where('cu.user_id',$data['id_user']);
        $this->db->where('cu.status',1);
        $result1 = $this->db->get()->result_array();
        if(count($result1)>0){
            return true;
        }
        else{
            return false;
        }
    }
    public function getContractReviewModule($data){

        $this->db->select('cu.*');
        $this->db->from('contract_user cu');
        $this->db->where('cu.contract_review_id',$data['contract_review_id']);
        $this->db->where('cu.user_id',$data['id_user']);
        $this->db->where('cu.status',1);
        $result1 = $this->db->get()->result_array();

        $this->db->select('m.id_module,ml.module_name,m.contract_review_id,count(crai.id_contract_review_action_item) as action_item_count,m.type');
        $this->db->from('module_language ml');
        $this->db->join('module m','m.id_module = ml.module_id','');
        $this->db->join('contract_review_action_item crai','m.id_module=crai.module_id and crai.item_status=1','left');
        $this->db->join('contract_review cr','cr.id_contract_review=m.contract_review_id');
        $this->db->join('user u','u.id_user=cr.created_by');
        $this->db->where('m.contract_review_id',$data['contract_review_id']);
        $this->db->group_by('m.id_module');
        if(count($result1)>0){
            $this->db->join('contract_user cu','m.id_module=cu.module_id and cu.status=1','');
            $this->db->where('cu.user_id',$data['id_user']);
        }

        $this->db->order_by('m.module_order','asc');

        $result = $this->db->get()->result_array();
        foreach($result as $k=>$v){
            $result[$k]['changes_count']=0;
            //$result[$k]['last_review']=date('Y-m-d');
            $result[$k]['score']='N/A';
        }
        return $result;
    }

    public function cloneModuleTopicQuestionForContract($data)
    {
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('id_customer',$data['customer_id']);
        $query = $this->db->get();
        $customer = $query->result_array();

        $this->db->select('tm.*,m.*,group_concat(ml.module_name SEPARATOR "@@@") as module_name,group_concat(ml.language_id SEPARATOR "@@@") as language_id');
        $this->db->from('template_module tm');
        $this->db->join('module m','tm.module_id=m.id_module','left');
        $this->db->join('module_language ml','m.id_module=ml.module_id','left');
        $this->db->where('tm.template_id',$customer[0]['template_id']);
        $this->db->where('tm.status',1);
        $this->db->group_by('tm.module_id');
        $this->db->order_by('tm.module_order','ASC');
        $query = $this->db->get();
        $template_module = $query->result_array();
        if(isset($data['created_by']))
            $data['created_by']=$this->db->escape($data['created_by']);
        if(isset($data['contract_review_id']))
            $data['contract_review_id']=$this->db->escape($data['contract_review_id']);
        for($s=0;$s<count($template_module);$s++)
        {
            $this->db->query('insert into module(module_order,created_by,created_on,module_status,contract_review_id,parent_module_id)
                              values("' . $template_module[$s]['module_order'] . '",
                                     "' . $this->db->escape($data['created_by']) . '",
                                     "' . currentDate() . '",
                                     "' . $template_module[$s]['module_status'] . '",
                                     "' . $data['contract_review_id'] . '",
                                     "' . $template_module[$s]['module_id'] . '"
                                     )');

            $module_id = $this->db->insert_id();
            $module_name = explode('@@@',$template_module[$s]['module_name']);
            $language_id = explode('@@@',$template_module[$s]['language_id']);
            for($t=0;$t<count($module_name);$t++)
            {
                $this->db->query('insert into module_language(module_id,module_name,language_id)
                              values("' . $module_id . '",
                                     "' . $module_name[$t] . '",
                                     "' . $language_id[$t] . '"
                                     )');
            }



            $this->db->select('tmt.*,t.*,group_concat(tl.topic_name SEPARATOR "@@@") as topic_name,group_concat(tl.language_id  SEPARATOR "@@@") as language_id');
            $this->db->from('template_module_topic tmt');
            $this->db->join('topic t','tmt.topic_id=t.id_topic','left');
            $this->db->join('topic_language tl','t.id_topic=tl.topic_id','left');
            $this->db->where('tmt.template_module_id',$template_module[$s]['id_template_module']);
            $this->db->where('tmt.status',1);
            $this->db->group_by('tmt.id_template_module_topic');
            $this->db->order_by('tmt.topic_order','ASC');
            $query = $this->db->get();
            $template_module_topics = $query->result_array();

            for($sr=0;$sr<count($template_module_topics);$sr++)
            {
                $this->db->query('insert into topic(module_id,topic_order,created_by,created_on,topic_status)
                              values("' . $module_id . '",
                                     "' . $template_module_topics[$sr]['topic_order'] . '",
                                     "' . $this->db->escape($data['created_by']) . '",
                                     "' . currentDate() . '",
                                     "' . $template_module_topics[$sr]['topic_status'] . '"
                                     )');
                $topic_id = $this->db->insert_id();
                $topic_name = explode('@@@',$template_module_topics[$sr]['topic_name']);
                $topic_language_id = explode('@@@',$template_module_topics[$sr]['language_id']);

                for($t=0;$t<count($topic_name);$t++)
                {
                    $this->db->query('insert into topic_language(topic_id,topic_name,language_id)
                              values("' . $topic_id . '",
                                     "' . $topic_name[$t] . '",
                                     "' . $topic_language_id[$t] . '"
                                     )');
                }

                $this->db->select('tmtq.*,q.*,group_concat(ql.question_text SEPARATOR "@@@") as question_text,group_concat(ql.request_for_proof SEPARATOR "@@@") as request_for_proof,group_concat(ql.language_id SEPARATOR "@@@") as language_id');
                $this->db->from('template_module_topic_question tmtq');
                $this->db->join('question q','tmtq.question_id=q.id_question','left');
                $this->db->join('question_language ql','q.id_question=ql.question_id','left');
                $this->db->where('tmtq.template_module_topic_id',$template_module_topics[$sr]['id_template_module_topic']);
                $this->db->where('tmtq.status',1);
                $this->db->group_by('tmtq.id_template_module_topic_question');
                $this->db->order_by('tmtq.question_order','ASC');
                $query = $this->db->get();
                $template_module_topic_questions = $query->result_array();
                for($st=0;$st<count($template_module_topic_questions);$st++)
                {
                    $this->db->query('insert into question(topic_id,question_order,question_required,question_type,created_by,created_on,question_status,parent_question_id)
                              values("' . $topic_id . '",
                                     "' . $template_module_topic_questions[$st]['question_order'] . '",
                                     "' . $template_module_topic_questions[$st]['question_required'] . '",
                                     "' . $template_module_topic_questions[$st]['question_type'] . '",
                                     "' . $this->db->escape($data['created_by']) . '",
                                     "' . currentDate() . '",
                                     "' . $template_module_topic_questions[$st]['question_status'] . '",
                                     "' . $template_module_topic_questions[$st]['id_question'] . '"
                                     )');
                    $question_id = $this->db->insert_id();
                    $question_name = explode('@@@',$template_module_topic_questions[$st]['question_text']);
                    $question_proof = explode('@@@',$template_module_topic_questions[$st]['request_for_proof']);
                    $question_language_id = explode(',',$template_module_topic_questions[$st]['language_id']);
                    for($t=0;$t<count($question_name);$t++)
                    {
                        $this->db->query('insert into question_language(question_id,question_text,request_for_proof,language_id)
                              values("' . $question_id . '",
                                     "' . $question_name[$t] . '",
                                     "' . $question_proof[$t] . '",
                                     "' . $question_language_id[$t] . '"
                                     )');
                    }

                    $this->db->select('qo.*,group_concat(qol.option_name SEPARATOR "@@@") as option_name,group_concat(qol.language_id SEPARATOR "@@@") as language_id,group_concat(qol.status SEPARATOR "@@@") as status');
                    $this->db->from('question_option qo');
                    $this->db->join('question_option_language qol','qo.id_question_option=qol.question_option_id','left');
                    $this->db->where('qo.question_id',$template_module_topic_questions[$st]['id_question']);
                    $this->db->where('qo.status',1);
                    $this->db->group_by('qo.id_question_option');
                    $query = $this->db->get();
                    $question_options = $query->result_array();

                    for($sth=0;$sth<count($question_options);$sth++)
                    {
                        $this->db->query('insert into question_option(question_id,option_value,status,created_by,created_on)
                              values("' . $question_id . '",
                                     "' . $question_options[$sth]['option_value'] . '",
                                     "' . $question_options[$sth]['status'] . '",
                                     "' . $this->db->escape($data['created_by']) . '",
                                     "' . currentDate() . '"
                                     )');
                        $question_option_id = $this->db->insert_id();
                        $option_name = explode('@@@',$question_options[$sth]['option_name']);
                        $option_language_id = explode('@@@',$question_options[$sth]['language_id']);
                        $status = explode('@@@',$question_options[$sth]['status']);
                        for($t=0;$t<count($option_name);$t++)
                        {
                            $this->db->query('insert into question_option_language(question_option_id,option_name,language_id,status)
                              values("' . $question_option_id . '",
                                     "' . $option_name[$t] . '",
                                     "' . $option_language_id[$t] . '",
                                     "' . $status[$t] . '"
                                     )');
                        }
                    }
                }
            }
        }
    }
    public function cloneModuleTopicQuestionForContractNew($data)
    {
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('id_customer',$data['customer_id']);
        $query = $this->db->get();
        $customer = $query->result_array();
        $data['contract_review_id']=($data['contract_review_id']);
        $data['created_by']=($data['created_by']);
        $data['parent_relationship_category_id']=($data['parent_relationship_category_id']);
        $storeproc='CALL dumpModulesForContractReview("'.$customer[0]['template_id'].'","'.$data['contract_review_id'].'","'.$data['created_by'].'","'.currentDate().'","'.$data['parent_relationship_category_id'].'")';
        $res_insert=$this->db->query($storeproc);

        if($res_insert){

            $this->db->select('*');
            $this->db->from('module');
            $this->db->where('contract_review_id',$data['contract_review_id']);
            $this->db->where('type','static');
            $query = $this->db->get();
            $modules = $query->result_array();
            foreach($modules as $key=>$val){
                $static_query="insert into contract_question_review (contract_review_id,question_id,question_answer,question_feedback,updated_by,updated_on,parent_question_id) (
select m.contract_review_id,q.id_question as question_id,cqr2.question_answer,cqr2.question_feedback,? updated_by, now() updated_on,q.parent_question_id from contract_question_review cqr2
join question q2 on q2.id_question=cqr2.question_id
join topic t2 on t2.id_topic=q2.topic_id
join module m2 on m2.id_module=t2.module_id and m2.id_module=(select max(m2x.id_module) from module mx join contract_review crx on crx.id_contract_review=mx.contract_review_id,module m2x join contract_review cr2x on cr2x.id_contract_review=m2x.contract_review_id where crx.contract_id=cr2x.contract_id and mx.parent_module_id=m2x.parent_module_id and m2x.id_module<? and mx.id_module=?)
join question q on q.parent_question_id=q2.parent_question_id
join topic t on t.id_topic=q.topic_id
join module m on m.id_module=t.module_id and m.id_module=?
where m.type='static')";
                $this->db->query($static_query,array($data['created_by'],$val['id_module'],$val['id_module'],$val['id_module']));
            }

        }
    }

    public function getContractReviewActionItemsList($data)
    {
        $this->db->select('crai.*,CONCAT_WS(\' \',u.first_name,u.last_name) as responsible_user_name,u1.user_role_id');
        $this->db->from('contract_review_action_item crai');
        $this->db->join('contract_review cr','cr.id_contract_review=crai.contract_review_id','LEFT');
        $this->db->join('user u','u.id_user=crai.responsible_user_id','LEFT');
        $this->db->join('user u1','u1.id_user=crai.created_by');
        if(isset($data['id_contract']))
            $this->db->where('crai.contract_id',$data['id_contract']);
        if(isset($data['page_type']) && $data['page_type']='contract_review'){
            if(isset($data['id_module']))
                $this->db->where('crai.module_id IN (select m.id_module from contract_review cr JOIN module m on m.contract_review_id=cr.id_contract_review join module m2 on m2.parent_module_id=m.parent_module_id where cr.contract_id=crai.contract_id and m2.id_module='.$this->db->escape($data['id_module']).')');
            if(isset($data['topic_id']))
                $this->db->where('crai.topic_id IN (select t.id_topic from contract_review cr LEFT JOIN module m on m.contract_review_id=cr.id_contract_review JOIN topic t on t.module_id=m.id_module JOIN topic t2 on t2.parent_topic_id=t.parent_topic_id where cr.contract_id=crai.contract_id and t2.id_topic='.$this->db->escape($data['topic_id']).')');
        }
        else {
            if (isset($data['id_module']))
                $this->db->where('crai.module_id', $data['id_module']);
            if (isset($data['id_contract_review']))
                $this->db->where('cr.id_contract_review', $data['id_contract_review']);
        }
        if(isset($data['item_status']))
            $this->db->where('crai.item_status',$data['item_status']);
        if(isset($data['status']))
            $this->db->where('crai.status',$data['status']);
        if(isset($data['contract_id'])) {
           // $this->db->where('cr.contract_id', $data['contract_id']);
            $this->db->where('crai.contract_id', $data['contract_id']);
        }
        if(isset($data['id_user']) && isset($data['user_role_id'])){
            if($data['user_role_id']==5){
                $this->db->group_start();
                $this->db->where('crai.created_by', $data['id_user']);
                $this->db->or_where('crai.responsible_user_id', $data['id_user']);
                $this->db->group_end();
            }
            else if($data['user_role_id']==4 || $data['user_role_id']==3 || $data['user_role_id']==2 || $data['user_role_id']==1 || $data['user_role_id']==6){
                $this->db->group_start();
                $this->db->where('crai.created_by', $data['id_user']);
                $this->db->or_where('u1.user_role_id>=', 2);
                $this->db->or_where('crai.responsible_user_id', $data['id_user']);
                $this->db->group_end();
            }
        }
        if(isset($data['responsible_user_id'])) {
            $this->db->where('crai.responsible_user_id', $data['responsible_user_id']);
        }
        $query = $this->db->get();
        $result= $query->result_array();

        foreach ($result as $k => $v) {
            $view_access = 'annus';
            $edit_access = 'annus';
            $delete_access = 'annus';
            $status_change_access = 'annus';
            if(isset($data['id_user']) && isset($data['user_role_id'])) {
                $view_access = "itako";
                if ($data['user_role_id'] == 6 || $data['user_role_id'] == 5) {
                    if ($v['created_by'] == $data['id_user']) {
                        $edit_access = $delete_access = 'itako';
                    }
                    if ($v['responsible_user_id'] == $data['id_user'] || $v['created_by'] == $data['id_user']) {
                        $view_access = "itako";
                    }
                } else if ($data['user_role_id'] == 4 || $data['user_role_id'] == 3 || $data['user_role_id'] == 2 || $data['user_role_id'] == 1) {
                    $view_access = "itako";
                    if ($v['created_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $edit_access = $delete_access = 'itako';
                    }
                    if ($v['responsible_user_id'] == $data['id_user']|| $v['created_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $view_access = "itako";
                    }
                }
            }
            else{
                $view_access = $edit_access = $delete_access = 'itako';
            }
            //$view_access="itako;
            if($view_access=="itako" && $v['status']!='completed')
                $status_change_access="itako";
            if($v['status']=='completed')
                $edit_access=$delete_access='annus';
            $result[$k]['vaav']=$view_access;
            $result[$k]['eaae']=$edit_access;
            $result[$k]['daad']=$delete_access;
            $result[$k]['scaacs']=$status_change_access;

            $this->db->select('c.*,concat(u.first_name," ",u.last_name) as user_name');
            $this->db->from('contract_review_action_item_log c');
            $this->db->join('user u','c.updated_by=u.id_user','left');
            $this->db->where('c.contract_review_action_item_id', $v['id_contract_review_action_item']);
            $query_log = $this->db->get();
            $result[$k]['comments_log']= $query_log->result_array();
        }

        return $result;
    }

    public function getContractReviewModuleData($data){

        $this->db->select('m.*,ml.module_name');
        $this->db->from('module_language ml');
        $this->db->join('module m','m.id_module = ml.module_id','');
        $this->db->where('m.contract_review_id',$data['contract_review_id']);
        $this->db->where('m.id_module',$data['module_id']);
        $this->db->order_by('m.module_order','ASC');
        $result = $this->db->get()->result_array();

        foreach($result as $key=>$val){
            $send['contract_review_id']=$data['contract_review_id'];
            $send['module_id']=$val['id_module'];
            $result[$key]['review_user_name'] = '---';
            $result[$key]['last_review'] = NULL;

            $latest = $this->getContractReviewModulelatestUpdate(array('module_id' => $val['id_module'],'contract_review_id' => $data['contract_review_id']));

            $progress = $this->progress($send);
            $result[$key]['progress']=$progress;
            if(!empty($latest)) {
                $result[$key]['review_user_name'] = $latest[0]->name;
                $result[$key]['last_review'] = date('Y-m-d',strtotime($latest[0]->date));
            }
            else{
                $latest_recent = $this->getContractReviewRecentModulelatestUpdate(array('module_id' => $val['id_module'],'contract_review_id' => $data['contract_review_id']));
                if(!empty($latest_recent)) {
                    $result[$key]['review_user_name'] = $latest_recent[0]->name;
                    $result[$key]['last_review'] = date('Y-m-d',strtotime($latest_recent[0]->date));
                }
            }

            //'id_contract_review'=>$data['contract_review_id'],
            //$result[$key]['action_items']=$this->getContractReviewActionItemsList(array('id_module'=>$val['id_module'],'contract_id'=>$data['contract_id']));
            $result[$key]['contributors']=$this->getContractContributors(array('module_id'=>$val['id_module'],'contract_id'=>$data['contract_id']));
            $result[$key]['contract_details']=$this->getContractDetails(array('contract_review_id'=>$data['contract_review_id'],'id_contract'=>$data['contract_id']));
            /*$this->db->select('t.id_topic,tl.topic_name');
            $this->db->from('topic t');
            $this->db->join('topic_language tl','tl.topic_id=t.id_topic and tl.language_id=1','LEFT');
            $this->db->where('t.module_id',$val['id_module']);
            $this->db->where('t.topic_status','1');
            $this->db->order_by('t.topic_order','ASC');*/
            $this->db->select('t.id_topic,tl.topic_name,count(q.id_question) questions_cnt');
            $this->db->from('topic t');
            $this->db->join('topic_language tl','tl.topic_id=t.id_topic and tl.language_id=1','LEFT');
            $this->db->join('question q','q.topic_id=t.id_topic and q.question_status=1','LEFT');
            $this->db->where('t.module_id',$val['id_module']);
            $this->db->where('t.topic_status','1');
            $this->db->group_by('t.id_topic');
            $this->db->having('count(q.id_question)>0');
            $this->db->order_by('t.topic_order','ASC');
            $topics = $this->db->get()->result_array();
            $inc=0;
            $topic_ids=array();
            $topic_names=array();
            foreach($topics as $kt1=>$vt1){
                $topic_ids[$inc]=$vt1['id_topic'];
                $topic_names[$vt1['id_topic']]=$vt1['topic_name'];
                $inc=$inc+1;
            }
            if(isset($data['id_topic']))
                $current_topic_index = array_search($data['id_topic'], $topic_ids);
            else
                $current_topic_index=0;
            $result[$key]['topic_pagination']['previous']=isset($topic_ids[$current_topic_index-1])?$topic_ids[$current_topic_index-1]:NULL;
            $result[$key]['topic_pagination']['previous_text']=isset($topic_names[$result[$key]['topic_pagination']['previous']])?$topic_names[$result[$key]['topic_pagination']['previous']]:NULL;
            $result[$key]['topic_pagination']['current']=isset($topic_ids[$current_topic_index])?$topic_ids[$current_topic_index]:NULL;
            $result[$key]['topic_pagination']['current_text']=isset($topic_names[$result[$key]['topic_pagination']['current']])?$topic_names[$result[$key]['topic_pagination']['current']]:NULL;
            $result[$key]['topic_pagination']['next']=isset($topic_ids[$current_topic_index+1])?$topic_ids[$current_topic_index+1]:NULL;
            $result[$key]['topic_pagination']['next_text']=isset($topic_names[$result[$key]['topic_pagination']['next']])?$topic_names[$result[$key]['topic_pagination']['next']]:NULL;
            $result[$key]['topic_pagination']['count']=count($topic_ids);
            $result[$key]['topic_pagination']['current_count']=$current_topic_index+1;

            $this->db->select('t.*,tl.topic_name');
            $this->db->from('topic t');
            $this->db->join('topic_language tl','tl.topic_id = t.id_topic and tl.language_id=1','LEFT');
            $this->db->where('t.module_id',$val['id_module']);
            $this->db->where('t.topic_status','1');
            if(isset($data['id_topic'])) {
                $this->db->where('t.id_topic', $data['id_topic']);
            }
            else{
                $this->db->where('t.id_topic', isset($topic_ids[$current_topic_index])?$topic_ids[$current_topic_index]:0);
            }
            $topics = $this->db->get()->result_array();
            $result[$key]['topics']=$topics;
            foreach($result[$key]['topics'] as $kt=>$vt){
                $this->db->select('q.*,ql.question_text,ql.request_for_proof,cqr.question_feedback, count(l.id_contract_question_review_log) as question_change');
                $this->db->from('question q');
                $this->db->join('question_language ql','ql.question_id = q.id_question and ql.language_id=1','LEFT');
                $this->db->join('contract_question_review cqr','cqr.question_id = q.id_question and cqr.contract_review_id='.$this->db->escape($data['contract_review_id']),'LEFT');
                if(isset($data['last_review_id'])){
                    $this->db->select('IFNULL(cqr.question_answer,cqr1.question_answer) as question_answer,IFNULL(cqr.question_feedback,cqr1.question_feedback) as question_feedback,IFNULL((select parent_question_option_id from question_option where id_question_option=cqr.question_option_id), (select parent_question_option_id from question_option where id_question_option=cqr1.question_option_id)) parent_question_answer,cqr.question_option_id');
                    $this->db->join('contract_question_review cqr1','cqr1.parent_question_id = q.parent_question_id and cqr1.contract_review_id='.$this->db->escape($data['last_review_id']),'LEFT');
                }
                else{
                    $this->db->select('cqr.question_answer,cqr.question_feedback,(select parent_question_option_id from question_option where id_question_option=cqr.question_option_id) as parent_question_answer,cqr.question_option_id');
                }
                $this->db->join('contract_question_review_log l','cqr.id_contract_question_review=l.contract_question_review_id','left');
                $this->db->where('q.topic_id',$vt['id_topic']);
                $this->db->where('q.question_status','1');
                $this->db->group_by('q.id_question');
                $this->db->order_by('q.question_order','ASC');
                $this->db->order_by('q.id_question','ASC');

                $questions = $this->db->get()->result_array();

                $result[$key]['topics'][$kt]['questions']=$questions;
                foreach($result[$key]['topics'][$kt]['questions'] as $ktq=>$vtq){

                    $this->db->select('q.*,ql.option_name');
                    $this->db->from('question_option q');
                    $this->db->join('question_option_language ql','ql.question_option_id = q.id_question_option and ql.language_id=1','LEFT');
                    $this->db->where('q.question_id',$vtq['id_question']);
                    $this->db->where('q.status','1');
                    $question_options = $this->db->get()->result_array();
                    $result[$key]['topics'][$kt]['questions'][$ktq]['options']=$question_options;
                    foreach($result[$key]['topics'][$kt]['questions'][$ktq]['options'] as $ktqo=>$vtqo){
                        if($vtq['parent_question_answer']==$vtqo['parent_question_option_id'])
                            $result[$key]['topics'][$kt]['questions'][$ktq]['parent_question_answer']=$vtqo['id_question_option'];
                    }

                    $this->db->select('count(*) as total_records');
                    $this->db->from('document d');
                    //$this->db->where('d.reference_id',$vtq['id_question']);
                    $this->db->where('d.reference_id IN (select q_sub.id_question from question q_sub LEFT JOIN question q2_sub on q2_sub.parent_question_id=q_sub.parent_question_id LEFT JOIN topic t2_sub on t2_sub.id_topic=q2_sub.topic_id LEFT JOIN module m2_sub on m2_sub.id_module=t2_sub.module_id LEFT JOIN contract_review cr2_sub on cr2_sub.id_contract_review=m2_sub.contract_review_id
LEFT JOIN contract c2_sub on c2_sub.id_contract=cr2_sub.contract_id and c2_sub.is_deleted=0 LEFT JOIN topic t1_sub on t1_sub.id_topic=q_sub.topic_id LEFT JOIN module m1_sub on m1_sub.id_module=t1_sub.module_id LEFT JOIN contract_review cr1_sub on cr1_sub.id_contract_review=m1_sub.contract_review_id where q2_sub.id_question='.$vtq['id_question'].' and `cr1_sub`.`contract_id` = `cr2_sub`.`contract_id`)',false,false);
                    $this->db->where('d.reference_type','question');
                    $this->db->where('d.document_status',1);
                    $query = $this->db->get();
                    $attachment=$query->result_array();
                    $result[$key]['topics'][$kt]['questions'][$ktq]['attachment_count'] = $attachment[0]['total_records'];
                }
            }
        }

        return $result;
    }

    public  function progress($data){
        /*foreach($data as $k=>$v){
            $data[$k]=$this->db->escape($v);
        }*/
        $q='select *,IFNULL(ROUND((b.answer_questions*100)/a.total_questions),0) percentage from
                            (select COUNT(q.id_question) as total_questions from module m
                            LEFT JOIN topic t on m.id_module=t.module_id
                            LEFT JOIN question q on t.id_topic=q.topic_id
                            where m.id_module=? ) a,
                            (select count(cqr.id_contract_question_review) as answer_questions from module m
                            LEFT JOIN topic t on m.id_module=t.module_id
                            LEFT JOIN question q on t.id_topic=q.topic_id
                            JOIN contract_question_review cqr on q.id_question=cqr.question_id
                            where m.id_module=? and  cqr.question_answer!="" and cqr.contract_review_id=?) b';
        $query = $this->db->query($q,array($data["module_id"],$data["module_id"],$data["contract_review_id"]));
        //echo "<pre>";echo $this->db->last_query($query);echo "</pre>";exit;
        $result = $query->result();

        return $result[0]->percentage;


    }
    public function getActionItemResponsibleUsers($data=array()){
        $q='SELECT * from (
SELECT u.id_user,u.user_role_id,CONCAT(CONCAT_WS(" ",u.first_name,u.last_name), CONCAT(" (", CONCAT_WS(" | ", u.email, ur.user_role_name, bu.bu_name), ")")) as name FROM `contract` c left join business_unit_user buu on buu.business_unit_id=c.business_unit_id
LEFT JOIN user u on u.id_user=buu.user_id
LEFT JOIN user_role ur ON u.user_role_id=ur.id_user_role
LEFT JOIN business_unit_user as buusr ON u.id_user=buusr.user_id
LEFT JOIN business_unit as bu ON bu.id_business_unit=buusr.business_unit_id
 where c.id_contract=? and u.user_status=1 and u.user_role_id not in (2,5,6)
union ALL
select u.id_user,u.user_role_id, CONCAT(CONCAT_WS(" ",u.first_name,u.last_name), CONCAT(" (", CONCAT_WS(" | ", u.email, ur.user_role_name, bu.bu_name), ")")) as name from user u
LEFT JOIN customer c on c.id_customer=u.customer_id
left join business_unit_user buu on buu.user_id=u.id_user
left join business_unit bu on bu.id_business_unit=buu.business_unit_id
LEFT JOIN user_role ur ON u.user_role_id=ur.id_user_role
left join contract cn on cn.business_unit_id=bu.id_business_unit and cn.is_deleted=0
where cn.id_contract=?  and u.user_status=1 and u.user_role_id not in (2,5,6)
UNION ALL
select u.id_user,u.user_role_id,CONCAT(CONCAT_WS(" ",u.first_name,u.last_name),
 CONCAT(" (", CONCAT_WS(" | ", u.email, ur.user_role_name, bu.bu_name), ")")) as name
from user u
LEFT JOIN contract_user cu on cu.user_id=u.id_user
LEFT JOIN user_role ur ON u.user_role_id=ur.id_user_role
LEFT JOIN customer c on c.id_customer=u.customer_id
left join business_unit_user buu on buu.user_id=u.id_user
left join business_unit bu on bu.id_business_unit=buu.business_unit_id
where cu.contract_review_id=?  and u.user_status=1 and cu.`status`=1
) z group by z.id_user order by z.id_user asc';
        $query = $this->db->query($q,array($data["contract_id"],$data["contract_id"],$data["contract_review_id"]));
        // echo $this->db->last_query(); die('asd');
        $result = $query->result();
        return $result;
    }

    public function contract_progress($data){
        $q='select *,IFNULL(ROUND((b.answer_questions*100)/a.total_questions,2),0) percentage from
                            (select COUNT(q.id_question) as total_questions from module m
                            LEFT JOIN topic t on m.id_module=t.module_id
                            LEFT JOIN question q on t.id_topic=q.topic_id
                            where m.id_module IN
								(select m.id_module from module m join contract_review cr on m.contract_review_id = cr.id_contract_review JOIN
								contract c on c.id_contract = cr.contract_id and c.id_contract = ? where c.is_deleted=0 and cr.id_contract_review = ?)  and q.question_type!="input") a,
                            (select count(cqr.id_contract_question_review) as answer_questions from module m
                            LEFT JOIN topic t on m.id_module=t.module_id
                            LEFT JOIN question q on t.id_topic=q.topic_id
                            JOIN contract_question_review cqr on q.id_question=cqr.question_id
                            where m.id_module IN
								(select m.id_module from module m join contract_review cr on m.contract_review_id = cr.id_contract_review JOIN
								contract c on c.id_contract = cr.contract_id and c.id_contract = ? where c.is_deleted=0) and cqr.question_answer!="" and cqr.contract_review_id=? and q.question_type!="input") b';
        $query = $this->db->query($q,array($data['contract_id'],$data['contract_review_id'],$data['contract_id'],$data['contract_review_id']));
        $result = $query->result();

        return $result[0]->percentage;

    }

    public function getContractReviewModulelatestUpdate($data){
        $q='select concat(u.first_name," ",u.last_name) as name,cqr.updated_on as date FROM
                                module m
                                JOIN topic t on m.id_module=t.module_id
                                JOIN question q on t.id_topic=q.topic_id
                                JOIN contract_question_review cqr on q.id_question=cqr.question_id
                                LEFT JOIN user u on cqr.updated_by=u.id_user
                                where m.id_module =? order by cqr.updated_on desc limit 1';
        $query = $this->db->query($q,array($data['module_id']));

        return $query->result();
    }
    public function getContractReviewRecentModulelatestUpdate($data){
        $q='select concat(u.first_name," ",u.last_name) as name,cqr.updated_on as date FROM
                                module m
                                JOIN topic t on m.id_module=t.module_id
                                JOIN question q on t.id_topic=q.topic_id
                                JOIN contract_question_review cqr on q.id_question=cqr.question_id
                                LEFT JOIN user u on cqr.updated_by=u.id_user
                                where m.id_module =(select max(m.id_module) from module m,module m1,contract_review cr,contract_review cr1 where m.parent_module_id=m1.parent_module_id and m1.id_module=? and m1.id_module!=m.id_module and cr.contract_id=cr1.contract_id and m.contract_review_id=cr.id_contract_review and m1.contract_review_id=cr1.id_contract_review) order by cqr.updated_on desc limit 1';
        $query = $this->db->query($q,array($data['module_id']));

        return $query->result();
    }

    public function getContributors($data=array()){
        $this->db->select('u.id_user,CONCAT_WS(\' \',u.first_name,u.last_name) as user_name,u.email');
        $this->db->from('business_unit_user buu');
        $this->db->join('user u','u.id_user=buu.user_id','left');
        $this->db->join('business_unit bu','bu.id_business_unit=buu.business_unit_id','left');
        $this->db->join('user_role ur','ur.id_user_role=u.user_role_id and ur.role_status=1','left');
        $this->db->where('ur.user_role_name','Contributor');
        $this->db->where('buu.status','1');
        $this->db->where('bu.status','1');
        $this->db->where('u.user_status','1');
        if(isset($data['id_business_unit']))
            $this->db->where('buu.business_unit_id',$data['id_business_unit']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addContractContributors($data=array()){

        $contributors_add=$data['contributors_add'];
        $contributors_added=array();
        $contributors_remove=$data['contributors_remove'];
        $contributors_removed=array();
        $total=array();
        $this->db->select('cu.user_id,cu.id_contract_user,cu.status');
        $this->db->from('contract_user cu');
        $this->db->where('cu.module_id',$data['module_id']);
        $this->db->where('cu.contract_id',$data['contract_id']);
        $query = $this->db->get();
        $result=$query->result_array();
        foreach($result as $key=>$val){
            if($val['status']==0)
                $contributors_removed[]=$val['user_id'];
            if($val['status']==1)
                $contributors_added[]=$val['user_id'];
            $total[]=$val['user_id'];
        }

        $update_to_active=array_intersect($contributors_add,$contributors_removed);
        $new_inserts=array_filter(array_diff($contributors_add,$total));

        $update_to_remove=array_intersect($contributors_remove,$contributors_added);
        /*print_r($total);
        print_r($update_to_remove);
        print_r($new_inserts);exit;*/
        foreach($new_inserts as $k=>$v){
            $inner_data=array();
            if(isset($v) && !empty($v)) {
                $inner_data['user_id'] = $v;
                $inner_data['module_id'] = $data['module_id'];
                $inner_data['contract_id'] = $data['contract_id'];
                $inner_data['status'] = 1;
                $inner_data['created_by'] = $data['created_by'];
                $inner_data['created_on'] = $data['created_on'];
                if(isset($data['contract_review_id']))
                    $inner_data['contract_review_id'] = $data['contract_review_id'];
                $this->addContractContributor($inner_data);
            }
        }
        foreach($update_to_active as $k=>$v){
            $inner_data=array();
            if(isset($v) && !empty($v)) {
                $inner_data['user_id'] = $v;
                $inner_data['module_id'] = $data['module_id'];
                $inner_data['contract_id'] = $data['contract_id'];
                $inner_data['status'] = 1;
                $inner_data['created_by'] = $data['created_by'];
                $inner_data['created_on'] = $data['created_on'];
                if(isset($data['contract_review_id']))
                    $inner_data['contract_review_id'] = $data['contract_review_id'];
                $this->updateContractContributor($inner_data);
            }
        }
        foreach($update_to_remove as $k=>$v){
            $inner_data=array();
            if(isset($v) && !empty($v)) {
                $inner_data['user_id'] = $v;
                $inner_data['module_id'] = $data['module_id'];
                $inner_data['contract_id'] = $data['contract_id'];
                $inner_data['status'] = 0;
                $inner_data['updated_by'] = $data['created_by'];
                $inner_data['updated_on'] = $data['created_on'];
                if(isset($data['contract_review_id']))
                    $inner_data['contract_review_id'] = $data['contract_review_id'];
                $this->updateContractContributor($inner_data);
            }
        }
        return $new_inserts;
        return 1;
    }

    public function addContractContributor($data)
    {
        $this->db->insert('contract_user', $data);
        return $this->db->insert_id();
    }

    public function updateContractContributor($data)
    {
        if(isset($data['user_id']))
            $this->db->where('user_id', $data['user_id']);
        if(isset($data['module_id']))
            $this->db->where('module_id', $data['module_id']);
        if(isset($data['contract_id']))
            $this->db->where('contract_id', $data['contract_id']);
        if(isset($data['contract_id']))
            $this->db->where('contract_id', $data['contract_id']);
        if(isset($data['id_contract_user']))
            $this->db->where('id_contract_user', $data['id_contract_user']);

        $this->db->update('contract_user', $data);
        return 1;
    }

    public function getReviewQuestionAnswer($data)
    {
        $this->db->select('*');
        $this->db->from('contract_question_review cr');
        if(isset($data['contract_review_id']))
            $this->db->where('cr.contract_review_id',$data['contract_review_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addReviewQuestionAnswer_bulk($data)
    {
        $this->db->insert_batch('contract_question_review', $data);
        return 1;
    }

    public function updateReviewQuestionAnswer($data)
    {
        if(isset($data['contract_review_id']))
            $this->db->where('contract_review_id', $data['contract_review_id']);
        if(isset($data['question_id']))
            $this->db->where('question_id', $data['question_id']);
        $this->db->update('contract_question_review', $data);
        return 1;
    }

    public function getContractContributors($data)
    {
        $this->db->select('c.*,CONCAT_WS(\' \',u1.first_name,u1.last_name) as contributor_user_name');
        $this->db->from('contract_user c');
        $this->db->join('user u1','u1.id_user=c.user_id','left');
        $this->db->where('c.status',1);
        if(isset($data['contract_id']))
            $this->db->where('c.contract_id',$data['contract_id']);
        if(isset($data['module_id']))
            $this->db->where('c.module_id',$data['module_id']);
        if(isset($data['user_id']))
            $this->db->where('c.user_id',$data['user_id']);
        if(isset($data['contract_review_id']))
            $this->db->where('c.contract_review_id',$data['contract_review_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateContractReview($data)
    {
        if(isset($data['id_contract_review']))
            $this->db->where('id_contract_review', $data['id_contract_review']);
        $this->db->update('contract_review', $data);
        return 1;
    }

    public function getContractModuleChanges($data)
    {
        $this->db->select('*');
        $this->db->from('module m');
        $this->db->join('topic t','m.id_module=t.module_id','');
        $this->db->join('question q','t.id_topic=q.topic_id','');
        $this->db->join('contract_question_review_log l','q.id_question=l.question_id','');
        if(isset($data['contract_review_id']))
            $this->db->where('l.contract_review_id',$data['contract_review_id']);
        if(isset($data['module_id']))
            $this->db->where('m.id_module',$data['module_id']);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getLastReviewByContractId($data)
    {
        $this->db->select('cr.id_contract_review,IFNULL(max(cr.updated_on),"---") as review_on,IFNULL(concat(u.first_name," ",u.last_name),"---") as review_by');
        $this->db->from('contract_review cr');
        $this->db->join('contract c','cr.contract_id=c.id_contract','');
        $this->db->join('user u','cr.updated_by=u.id_user','left');
        $this->db->where('cr.contract_id',$data['contract_id']);
        if(isset($data['contract_review_status']))
            $this->db->where('cr.contract_review_status',$data['contract_review_status']);
        $this->db->where('c.is_deleted','0');
        $this->db->group_by('cr.id_contract_review');
        if(isset($data['order']))
            $this->db->order_by('cr.id_contract_review',$data['order']);
        $query = $this->db->get();
        return $query->result_array();
    }

    /*public function getContractDashboard($data)
    {
        $query = $this->db->query("call getContractReviewTopicScore(".$data['contract_review_id'].")");
        $result = $query->result_array();
        $this->Mcommon->clean_mysqli_connection($this->db->conn_id);
        return $result;
    }*/
    public function getContractDashboard1($data)
    {
        $this->db->flush_cache();
        $query="select
	module_id,module_name,topic_id,topic_name,
	(
    CASE
        WHEN topic_avg_weight_score = 'N/A' THEN 'N/A'
        WHEN topic_avg_weight_score >= 0.75 THEN 'Green'
        WHEN topic_avg_weight_score >= 0.50 THEN 'Amber'
        WHEN topic_avg_weight_score >= 0 THEN 'Red'
        WHEN topic_avg_weight_score < 0 THEN ''
        ELSE 'N/A'
    END) AS topic_score,topic_avg_weight_score,str,total_topic_progress

 from(
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,(sum(q.question_weight*(case when cqr.question_answer is null then -1 else cqr.question_answer END)))/(sum(q.question_weight)-sum(case when cqr.question_answer = 'NA' then q.question_weight else 0 END)) as topic_avg_weight_score,m.module_order,t.topic_order,'' str,
ROUND((sum(case when cqr.question_answer is null then 0 else (case when cqr.question_answer = 'NA' then q.question_weight else cqr.question_answer END) END)/(sum(case when cqr.question_answer is null then 0 else 1 END))) *100) as total_topic_progress from question q
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
left join contract_question_review cqr on cqr.question_id = q.id_question #and cqr.question_answer != 'NA'
where q.question_type != 'input' and m.contract_review_id = ? and t.type='general'
GROUP BY t.id_topic  ORDER BY m.module_order asc,t.topic_order asc) A1
UNION ALL
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,(CASE WHEN t.type='data' THEN (CASE
        WHEN GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') IN ('Yes-Yes-Yes','Yes-Yes-No','Yes-Yes-N/A','Yes-Yes-B','Yes-No-Yes','Yes-N/A-Yes','Yes-B-Yes','No-Yes-Yes','No-Yes-No','No-Yes-N/A','No-Yes-B','No-No-Yes') THEN '0.75'
		WHEN GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') IN ('Yes-No-No','Yes-No-N/A','Yes-No-B','Yes-N/A-No','Yes-B-No','Yes-N/A-N/A','Yes-N/A-B','Yes-B-N/A','Yes-B-B','No-N/A-Yes','No-B-Yes','No-N/A-N/A','No-B-N/A','No-N/A-B','No-B-B') THEN '0.50'
        WHEN GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') IN ('N/A-N/A-N/A') THEN 'N/A'
        WHEN GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') IN ('B-B-B') THEN -1
        WHEN GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') IN ('No-No-No','No-N/A-No','No-B-No','No-No-N/A','No-No-B') OR GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') like 'N/A-%' THEN '0'
		ELSE '-1'
    END) WHEN t.type='relationship' THEN (relationship_score_calculation(count(q.id_question),GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-'))) END) as topic_avg_weight_score,m.module_order,t.topic_order,GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') as str,0 total_topic_progress
from question q
LEFT JOIN question_language ql on ql.question_id=q.id_question and ql.language_id=1
left join contract_question_review cqr on cqr.question_id = q.id_question #and cqr.question_answer != 'NA'
left join question_option qo on q.id_question=qo.question_id and cqr.question_answer=qo.option_value
LEFT JOIN question_option_language qol on qol.question_option_id=qo.id_question_option and qol.language_id=1
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
where q.question_type != 'input' and m.contract_review_id = ?
and t.type in ('data','relationship') GROUP BY t.id_topic ORDER BY q.id_question asc) A2 order by module_order asc,topic_order asc )temp";
        //$this->Mcommon->clean_mysqli_connection($this->db->conn_id);
        $query = $this->db->query($query,array(($data['contract_review_id']),($data['contract_review_id'])));
        $result =  $query->result_array();
        //$this->Mcommon->clean_mysqli_connection($this->db->conn_id);
        return $result;
    }
    public function getContractDashboard($data)
    {
        $this->db->flush_cache();
        /*$query="select
	module_id,module_name,topic_id,topic_name,simple_score_calculation AS topic_score,topic_avg_weight_score,str,total_topic_progress
  from(
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,(sum(q.question_weight*(case when cqr.question_answer is null then -1 else cqr.question_answer END)))/(sum(q.question_weight)-sum(case when cqr.question_answer = 'NA' then q.question_weight else 0 END)) as topic_avg_weight_score,
m.module_order,t.topic_order,'' str,
ROUND((sum(case when cqr.question_answer is null then 0 else (case when cqr.question_answer = 'NA' then q.question_weight else cqr.question_answer END) END)/(sum(case when cqr.question_answer is null then 0 else 1 END))) *100) as total_topic_progress,count(q.id_question),GROUP_CONCAT((case when cqr.question_answer=0 then 'R' when cqr.question_answer=1 then 'G' when cqr.question_answer is null then 'E' when cqr.question_answer='NA' then 'N' else 'A' END) SEPARATOR ' '),
simple_score_calculation(count(q.id_question),GROUP_CONCAT((case when cqr.question_answer is null then 'E' when cqr.question_answer='NA' then 'N' when cqr.question_answer=0 then 'R' when cqr.question_answer=1 then 'G' else 'A' END) SEPARATOR ' ')) as simple_score_calculation
 from question q
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
left join contract_question_review cqr on cqr.question_id = q.id_question #and cqr.question_answer != 'NA'
where q.question_type != 'input' and m.contract_review_id = ?
GROUP BY t.id_topic  ORDER BY m.module_order asc,t.topic_order asc) A1)temp";*/
     $query="select
	module_id,module_name,topic_id,topic_name,final_score(topic_avg_weight_score) AS topic_score,topic_avg_weight_score,str,total_topic_progress

 from(
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,(sum(q.question_weight*(case when cqr.question_answer is null then -1 else cqr.question_answer END)))/(sum(q.question_weight)-sum(case when cqr.question_answer = 'NA' then q.question_weight else 0 END)) as topic_avg_weight_score,m.module_order,t.topic_order,'' str,
ROUND((sum(case when cqr.question_answer is null then 0 else (case when cqr.question_answer = 'NA' then q.question_weight else cqr.question_answer END) END)/(sum(case when cqr.question_answer is null then 0 else 1 END))) *100) as total_topic_progress from question q
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
left join contract_question_review cqr on cqr.question_id = q.id_question and cqr.question_answer != 'NA'
where q.question_type != 'input' and m.contract_review_id = ? and t.type='general'
GROUP BY t.id_topic  ORDER BY m.module_order asc,t.topic_order asc) A1
UNION ALL
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,simple_score_calculation(count(q.id_question),GROUP_CONCAT((case when cqr.question_answer is null then 'E' when cqr.question_answer='NA' then 'N' when cqr.question_answer=0 then 'R' when cqr.question_answer=1 then 'G' else 'A' END) SEPARATOR ' ')) as topic_avg_weight_score,m.module_order,t.topic_order,'' str,
ROUND((sum(case when cqr.question_answer is null then 0 else (case when cqr.question_answer = 'NA' then q.question_weight else cqr.question_answer END) END)/(sum(case when cqr.question_answer is null then 0 else 1 END))) *100) as total_topic_progress
 from question q
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
left join contract_question_review cqr on cqr.question_id = q.id_question and cqr.question_answer != 'NA'
where q.question_type != 'input' and m.contract_review_id = ? and t.type='simple'
GROUP BY t.id_topic  ORDER BY m.module_order asc,t.topic_order asc) A2
UNION ALL
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,(CASE WHEN t.type='data' THEN (data_score_calculation(GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-'))) WHEN t.type='relationship' THEN (relationship_score_calculation_new(count(q.id_question),GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-'))) END) as topic_avg_weight_score,m.module_order,t.topic_order,GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') as str,0 total_topic_progress
from question q
LEFT JOIN question_language ql on ql.question_id=q.id_question and ql.language_id=1
left join contract_question_review cqr on cqr.question_id = q.id_question and cqr.question_answer != 'NA'
left join question_option qo on q.id_question=qo.question_id and cqr.question_answer=qo.option_value
LEFT JOIN question_option_language qol on qol.question_option_id=qo.id_question_option and qol.language_id=1
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
where q.question_type != 'input' and m.contract_review_id = ?
and t.type in ('data','relationship') GROUP BY t.id_topic ORDER BY q.id_question asc) A3 order by module_order asc,topic_order asc )temp";
        $query = $this->db->query($query,array($data['contract_review_id'],$data['contract_review_id'],$data['contract_review_id']));
        //echo $this->db->last_query();exit;
        $result =  $query->result_array();
        return $result;
    }

    /*public function getContractReviewModuleScore($data)
    {
        $query = $this->db->query("call getContractModuleScore(".$data['contract_review_id'].")");
        $result =  $query->result_array();
        $this->Mcommon->clean_mysqli_connection($this->db->conn_id);
        return $result;
    }*/
    public function getContractReviewModuleScore1($data)
    {

        $query="select
	module_id,module_name,topic_id,topic_name,topic_avg_weight_score,
	COUNT(CASE WHEN topic_avg_weight_score is NULL OR topic_avg_weight_score='N/A'  THEN 1 END) AS na_total,
	COUNT(CASE WHEN topic_avg_weight_score >= 0.75 AND topic_avg_weight_score!='N/A' THEN 1 END) AS green_total,
	COUNT(CASE WHEN topic_avg_weight_score >= 0.50 and topic_avg_weight_score < 0.75 AND topic_avg_weight_score!='N/A' THEN 1 END) AS amber_total,
	COUNT(CASE WHEN topic_avg_weight_score >= 0 and topic_avg_weight_score < 0.50 AND topic_avg_weight_score!='N/A' THEN 1 END) AS red_total,
	COUNT(CASE WHEN topic_avg_weight_score < 0 AND topic_avg_weight_score!='N/A' THEN 1 END) AS no_answer_total,parent_module_id
	#(CASE WHEN topic_avg_weight_score >= 0.75 THEN count(topic_id) END) AS green_total,
	#(CASE WHEN topic_avg_weight_score >= 0.50 and topic_avg_weight_score < 0.75 THEN (topic_id) END) AS amber_total,
	#(CASE WHEN topic_avg_weight_score = 0 THEN (topic_id) END) AS red_total,
	#(CASE WHEN topic_avg_weight_score is NULL THEN (topic_id) END) AS na_total,


 from(
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,(sum(q.question_weight*(case when cqr.question_answer is null then -1 else cqr.question_answer END)))/(sum(q.question_weight)-sum(case when cqr.question_answer = 'NA' then q.question_weight else 0 END)) as topic_avg_weight_score,m.module_order,t.topic_order,'' str,m.parent_module_id from question q
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
left join contract_question_review cqr on cqr.question_id = q.id_question #and cqr.question_answer != 'NA'
where q.question_type != 'input' and m.contract_review_id = ? and t.type='general'
GROUP BY t.id_topic  ORDER BY m.module_order asc,t.topic_order asc) A1
UNION ALL
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,(CASE WHEN t.type='data' THEN (CASE
        WHEN GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') IN ('Yes-Yes-Yes','Yes-Yes-No','Yes-Yes-N/A','Yes-Yes-B','Yes-No-Yes','Yes-N/A-Yes','Yes-B-Yes','No-Yes-Yes','No-Yes-No','No-Yes-N/A','No-Yes-B','No-No-Yes') THEN '0.75'
		WHEN GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') IN ('Yes-No-No','Yes-No-N/A','Yes-No-B','Yes-N/A-No','Yes-B-No','Yes-N/A-N/A','Yes-N/A-B','Yes-B-N/A','Yes-B-B','No-N/A-Yes','No-B-Yes','No-N/A-N/A','No-B-N/A','No-N/A-B','No-B-B') THEN '0.50'
		WHEN GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') IN ('N/A-N/A-N/A') THEN 'N/A'
        WHEN GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') IN ('B-B-B') THEN -1
        WHEN GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') IN ('No-No-No','No-N/A-No','No-B-No','No-No-N/A','No-No-B') OR GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') like 'N/A-%' THEN '0'
				ELSE '-1'
    END) WHEN t.type='relationship' THEN (relationship_score_calculation(count(q.id_question),GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-'))) END) topic_avg_weight_score,m.module_order,t.topic_order,GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') as str,m.parent_module_id
from question q
LEFT JOIN question_language ql on ql.question_id=q.id_question and ql.language_id=1
left join contract_question_review cqr on cqr.question_id = q.id_question #and cqr.question_answer != 'NA'
left join question_option qo on q.id_question=qo.question_id and cqr.question_answer=qo.option_value
LEFT JOIN question_option_language qol on qol.question_option_id=qo.id_question_option and qol.language_id=1
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
where q.question_type != 'input' and m.contract_review_id = ?
and t.type in ('data','relationship') GROUP BY t.id_topic ORDER BY q.id_question asc) A2 order by module_order asc,topic_order asc )temp
GROUP BY module_id";
        $query = $this->db->query($query,array($data['contract_review_id'],$data['contract_review_id']));
        $result =  $query->result_array();
        //echo "<pre>";print_r($result);echo "</pre>";exit;
        //echo $this->db->last_query();
        //$this->Mcommon->clean_mysqli_connection($this->db->conn_id);
        return $result;
    }
    public function getContractReviewModuleScore($data)
    {

        /*$query="select temp1.module_id,temp1.module_name,COUNT(CASE WHEN topic_score='Red'  THEN 1 END) AS red_total,
COUNT(CASE WHEN topic_score='Amber'  THEN 1 END) AS amber_total,
COUNT(CASE WHEN topic_score='Green'  THEN 1 END) AS green_total,
COUNT(CASE WHEN topic_score='N/A'  THEN 1 END) AS na_total,
COUNT(CASE WHEN topic_score=''  THEN 1 END) AS no_answer_total,parent_module_id from (
select
	module_id,module_name,topic_id,topic_name,simple_score_calculation AS topic_score,topic_avg_weight_score,str,total_topic_progress,parent_module_id
  from(
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,(sum(q.question_weight*(case when cqr.question_answer is null then -1 else cqr.question_answer END)))/(sum(q.question_weight)-sum(case when cqr.question_answer = 'NA' then q.question_weight else 0 END)) as topic_avg_weight_score,
m.module_order,t.topic_order,'' str,
ROUND((sum(case when cqr.question_answer is null then 0 else (case when cqr.question_answer = 'NA' then q.question_weight else cqr.question_answer END) END)/(sum(case when cqr.question_answer is null then 0 else 1 END))) *100) as total_topic_progress,count(q.id_question),GROUP_CONCAT((case when cqr.question_answer=0 then 'R' when cqr.question_answer=1 then 'G' when cqr.question_answer is null then 'E' when cqr.question_answer='NA' then 'N' else 'A' END) SEPARATOR ' '),
simple_score_calculation(count(q.id_question),GROUP_CONCAT((case when cqr.question_answer is null then 'E' when cqr.question_answer='NA' then 'N' when cqr.question_answer=0 then 'R' when cqr.question_answer=1 then 'G' else 'A' END) SEPARATOR ' ')) as simple_score_calculation,m.parent_module_id
 from question q
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
left join contract_question_review cqr on cqr.question_id = q.id_question #and cqr.question_answer != 'NA'
where q.question_type != 'input' and m.contract_review_id = ?
GROUP BY t.id_topic  ORDER BY m.module_order asc,t.topic_order asc) A1)temp) temp1 GROUP BY temp1.module_id";*/

        $query="select
	module_id,module_name,topic_id,topic_name,topic_avg_weight_score,
	COUNT(CASE WHEN topic_avg_weight_score='' OR topic_avg_weight_score IS NULL   THEN 1 END) AS na_total,
	COUNT(CASE WHEN topic_avg_weight_score=1 THEN 1 END) AS green_total,
	COUNT(CASE WHEN topic_avg_weight_score>0 and topic_avg_weight_score < 1 THEN 1 END) AS amber_total,
	COUNT(CASE WHEN topic_avg_weight_score=0 THEN 1 END) AS red_total,
	COUNT(CASE WHEN topic_avg_weight_score<0 THEN 1 END) AS no_answer_total,parent_module_id



 from(
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,(sum(q.question_weight*(case when cqr.question_answer is null then -1 else cqr.question_answer END)))/(sum(q.question_weight)-sum(case when cqr.question_answer = 'NA' then q.question_weight else 0 END)) as topic_avg_weight_score,m.module_order,t.topic_order,'' str,m.parent_module_id from question q
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
left join contract_question_review cqr on cqr.question_id = q.id_question and cqr.question_answer != 'NA'
where q.question_type != 'input' and m.contract_review_id = ? and t.type='general'
GROUP BY t.id_topic  ORDER BY m.module_order asc,t.topic_order asc) A1
UNION ALL
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,simple_score_calculation(count(q.id_question),GROUP_CONCAT((case when cqr.question_answer is null then 'E' when cqr.question_answer='NA' then 'N' when cqr.question_answer=0 then 'R' when cqr.question_answer=1 then 'G' else 'A' END) SEPARATOR ' ')) as topic_avg_weight_score,
m.module_order,t.topic_order,'' str,m.parent_module_id
 from question q
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
left join contract_question_review cqr on cqr.question_id = q.id_question and cqr.question_answer != 'NA'
where q.question_type != 'input' and m.contract_review_id = ? and t.type='simple' GROUP BY t.id_topic  ORDER BY m.module_order asc,t.topic_order asc) A2
UNION ALL
select * from (
select
	t.module_id,ml.module_name,q.topic_id,tl.topic_name,(CASE WHEN t.type='data' THEN (data_score_calculation(GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-'))) WHEN t.type='relationship' THEN (relationship_score_calculation_new(count(q.id_question),GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-'))) END) topic_avg_weight_score,m.module_order,t.topic_order,GROUP_CONCAT(IFNULL(qol.option_name,'B') ORDER BY q.id_question asc SEPARATOR '-') as str,m.parent_module_id
from question q
LEFT JOIN question_language ql on ql.question_id=q.id_question and ql.language_id=1
left join contract_question_review cqr on cqr.question_id = q.id_question and cqr.question_answer != 'NA'
left join question_option qo on q.id_question=qo.question_id and cqr.question_answer=qo.option_value
LEFT JOIN question_option_language qol on qol.question_option_id=qo.id_question_option and qol.language_id=1
join topic t on t.id_topic = q.topic_id
join topic_language tl on tl.topic_id = t.id_topic
join module m on m.id_module = t.module_id
join module_language ml on ml.module_id = m.id_module
where q.question_type != 'input' and m.contract_review_id = ?
and t.type in ('data','relationship') GROUP BY t.id_topic ORDER BY q.id_question asc) A3 order by module_order asc,topic_order asc )temp
GROUP BY module_id";
        $query = $this->db->query($query,array($data['contract_review_id'],$data['contract_review_id'],$data['contract_review_id']));
        //echo $this->db->last_query();exit;
        $result =  $query->result_array();
        return $result;
    }

    public function getModuleDashboard($data){
        $this->db->select('m.id_module,ml.module_name');
        $this->db->from('module m');
        $this->db->join('module_language ml','m.id_module = ml.module_id','');
        $this->db->where('m.contract_review_id',$data);
        $result =  $this->db->get()->result_array();
        foreach($result as $key => $value){
            $result[$key]['topic'] = $this->getTopicDashboard($result[$key]['id_module']);
        }
        return $result;
    }

    public function getTopicDashboard($data){
        $this->db->select('t.id_topic,tl.topic_name');
        $this->db->from('topic t');
        $this->db->join('topic_language tl','t.id_topic = tl.topic_id','');
        $this->db->where('t.module_id',$data);
        return $this->db->get()->result_array();
    }

    public function addContractReviewActionItemLog($data)
    {
        $this->db->insert('contract_review_action_item_log', $data);
        return $this->db->insert_id();
    }

    public function getActionItemDetails($data)
    {
        $this->db->select("ml.module_name,tl.topic_name,concat(u.first_name,' ',u.last_name) as user_name,crai.*");
        $this->db->from('contract_review_action_item crai');
        $this->db->join('user u','u.id_user = crai.responsible_user_id','left');
        $this->db->join('module_language ml','ml.module_id = crai.module_id  and ml.language_id=1','left');
        $this->db->join('topic_language tl','tl.topic_id = crai.topic_id and tl.language_id=1','left');
        if(isset($data['id_contract_review_action_item']))
            $this->db->where('crai.id_contract_review_action_item',$data['id_contract_review_action_item']);

        $result = $this->db->get();
        return $result->result_array();
    }

    public function getActionItems($data){

        $this->db->select("ml.module_name,tl.topic_name,concat(u.first_name,' ',u.last_name) as user_name,crai.*,ql.question_text,concat(u1.first_name,' ',u1.last_name) as created_by_name");
        $this->db->from('contract_review_action_item crai');
        $this->db->join('user u','u.id_user = crai.responsible_user_id','left');
        $this->db->join('user u1','u1.id_user = crai.created_by','left');
        $this->db->join('module_language ml','ml.module_id = crai.module_id  and ml.language_id=1','left');
        $this->db->join('topic_language tl','tl.topic_id = crai.topic_id and tl.language_id=1','left');
        $this->db->join('question_language ql','ql.question_id = crai.question_id and ql.language_id=1','left');
        $this->db->join('contract c','c.id_contract = crai.contract_id and c.is_deleted=0','left');
        $this->db->join('business_unit bu','bu.id_business_unit = c.business_unit_id','left');
        $this->db->where('bu.customer_id',$data['customer_id']);
        if(isset($data['id_contract_review_action_item']))
            $this->db->where('crai.id_contract_review_action_item',$data['id_contract_review_action_item']);
        if(isset($data['contract_id']))
            $this->db->where('contract_id',$data['contract_id']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('ml.module_name', $data['search'], 'both');
            $this->db->or_like('crai.action_item', $data['search'], 'both');
            $this->db->or_like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.last_name', $data['search'], 'both');
            $this->db->or_like('crai.status', $data['search'], 'both');
            $this->db->or_like('crai.due_date', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search'])) {
            $data['search']=$this->db->escape($data['search']);
            $this->db->where('(ml.module_name like "%' . $data['search'] . '%"
            or crai.action_item like "%' . $data['search'] . '%"
            or u.first_name  like "%' . $data['search'] . '%"
            or u.last_name like "%' . $data['search'] . '%"
            or crai.status like "%' . $data['search'] . '%"
            or crai.due_date like "%' . $data['search'] . '%")');
        }*/
        /*if(isset($data['business_unit_id']))
            $this->db->where_in('bu.id_business_unit',$data['business_unit_id']);*/

        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("(c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1) and (crai.responsible_user_id=".$data['session_user_id']." or crai.created_by=".$data['session_user_id']."))",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        /*if(isset($data['delegate_id']))
            $this->db->where('c.delegate_id',$data['delegate_id']);*/
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where("(c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1) and (crai.responsible_user_id=".$data['session_user_id']." or crai.created_by=".$data['session_user_id']."))",null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        if(isset($data['responsible_user_id']) || isset($data['created_by'])){
            $this->db->group_start();
            if(isset($data['responsible_user_id']))
                $this->db->where('responsible_user_id',$data['responsible_user_id']);
            if(isset($data['created_by']))
                $this->db->or_where('crai.created_by',$data['created_by']);
            $this->db->group_end();
        }
        if(isset($data['start_date']))
            $this->db->where('due_date >=',$data['start_date']);
        if(isset($data['end_date']))
            $this->db->where('due_date <=',$data['end_date']);
        if(isset($data['contract_review_action_item_status']) && strtolower($data['contract_review_action_item_status'])!='all')
            $this->db->where('crai.status',$data['contract_review_action_item_status']);
        if(isset($data['item_status']))
            $this->db->where('crai.item_status',$data['item_status']);
        if(isset($data['provider_name']))
            $this->db->where('c.provider_name',$data['provider_name']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        $all_clients_count = count($query->result_array());

        $this->db->select("ml.module_name,tl.topic_name,concat(u.first_name,' ',u.last_name) as user_name,crai.*, IFNULL(ql.question_text,'-') question_text");
        $this->db->from('contract_review_action_item crai');
        $this->db->join('user u','u.id_user = crai.responsible_user_id','left');
        $this->db->join('module_language ml','ml.module_id = crai.module_id  and ml.language_id=1','left');
        $this->db->join('topic_language tl','tl.topic_id = crai.topic_id and tl.language_id=1','left');
        $this->db->join('question_language ql','ql.question_id = crai.question_id and ql.language_id=1','left');
        $this->db->join('contract c','c.id_contract = crai.contract_id and c.is_deleted=0','left');
        $this->db->join('business_unit bu','bu.id_business_unit = c.business_unit_id','left');
        $this->db->where('bu.customer_id',$data['customer_id']);
        if(isset($data['id_contract_review_action_item']))
            $this->db->where('crai.id_contract_review_action_item',$data['id_contract_review_action_item']);
        if(isset($data['contract_id']))
            $this->db->where('contract_id',$data['contract_id']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('ml.module_name', $data['search'], 'both');
            $this->db->or_like('crai.action_item', $data['search'], 'both');
            $this->db->or_like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.last_name', $data['search'], 'both');
            $this->db->or_like('crai.status', $data['search'], 'both');
            $this->db->or_like('crai.due_date', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(ml.module_name like "%'.$data['search'].'%"
            or crai.action_item like "%'.$data['search'].'%"
            or u.first_name  like "%'.$data['search'].'%"
            or u.last_name like "%'.$data['search'].'%"
            or crai.status like "%'.$data['search'].'%"
            or crai.due_date like "%'.$data['search'].'%")');*/
        /*if(isset($data['business_unit_id']))
            $this->db->where_in('bu.id_business_unit',$data['business_unit_id']);*/
        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("(c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1) and (crai.responsible_user_id=".$data['session_user_id']." or crai.created_by=".$data['session_user_id']."))",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        /*if(isset($data['delegate_id']))
            $this->db->where('c.delegate_id',$data['delegate_id']);*/
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where("(c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1) and (crai.responsible_user_id=".$data['session_user_id']." or crai.created_by=".$data['session_user_id']."))",null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        if(isset($data['responsible_user_id']) || isset($data['created_by'])){
            $this->db->group_start();
            if(isset($data['responsible_user_id']))
                $this->db->where('responsible_user_id',$data['responsible_user_id']);
            if(isset($data['created_by']))
                $this->db->or_where('crai.created_by',$data['created_by']);
            $this->db->group_end();
        }
        if(isset($data['start_date']))
            $this->db->where('due_date >=',$data['start_date']);
        if(isset($data['end_date']))
            $this->db->where('due_date <=',$data['end_date']);
        if(isset($data['contract_review_action_item_status']) && strtolower($data['contract_review_action_item_status'])!='all')
            $this->db->where('crai.status',$data['contract_review_action_item_status']);
        if(isset($data['item_status']))
            $this->db->where('crai.item_status',$data['item_status']);
        if(isset($data['provider_name']))
            $this->db->where('c.provider_name',$data['provider_name']);
        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('c.provider_name,c.contract_name','ASC');
        $this->db->where('c.is_deleted','0');
        $result = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        return array('total_records' => $all_clients_count,'data' => $result);
    }

    public function getActionItemsCount($data)
    {
        $this->db->select("count(*) as total_records");
        $this->db->from('contract_review_action_item crai');
        $this->db->join('contract c','c.id_contract = crai.contract_id and c.is_deleted=0','left');
        $this->db->join('business_unit bu','bu.id_business_unit = c.business_unit_id','left');
        $this->db->where('bu.customer_id',$data['customer_id']);
        if(isset($data['contract_id']))
            $this->db->where('contract_id',$data['contract_id']);
        /*if(isset($data['business_unit_id']) && count($data['business_unit_id'])>0)
            $this->db->where_in('bu.id_business_unit',$data['business_unit_id']);*/
        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']) && count($data['business_unit_id'])>0)
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("(c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1) and (crai.responsible_user_id=".$data['session_user_id']." or crai.created_by=".$data['session_user_id']."))",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']) && count($data['business_unit_id'])>0)
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        /*if(isset($data['delegate_id']))
            $this->db->where('c.delegate_id',$data['delegate_id']);*/
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where("(c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1) and (crai.responsible_user_id=".$data['session_user_id']." or crai.created_by=".$data['session_user_id']."))",null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        if(isset($data['contract_status']))
            $this->db->where('c.contract_status',$data['contract_status']);
        if(isset($data['item_status']))
            $this->db->where('crai.item_status',$data['item_status']);
        if(isset($data['contract_review_action_item_status']))
            $this->db->where('crai.status',$data['contract_review_action_item_status']);
        if(isset($data['responsible_user_id']) || isset($data['created_by']) || isset($data['id_user'])){
            $this->db->group_start();
            if(isset($data['responsible_user_id']))
                $this->db->where('responsible_user_id',$data['responsible_user_id']);
            if(isset($data['created_by']))
                $this->db->or_where('crai.created_by',$data['created_by']);
            if(isset($data['id_user']))
                $this->db->or_where('responsible_user_id',$data['id_user']);
            $this->db->group_end();
        }
        $result = $this->db->get()->result_array();
        return $result[0]['total_records'];
    }

    public function getProvidersList($data)
    {
        $this->db->select('c.id_contract as contract_id,c.provider_name,count(cri.id_contract_review_action_item) as action_items_count');
        $this->db->from('contract c');
        $this->db->join('business_unit bu','c.business_unit_id=bu.id_business_unit','left');
        $this->db->join('contract_review_action_item cri','c.id_contract=cri.contract_id','left');
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('c.provider_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search'])) {
            $data['search']=$this->db->escape($data['search']);
            $this->db->where('(c.provider_name like "%' . $data['search'] . '%")');
        }*/
        if(isset($data['customer_id']))
            $this->db->where('bu.customer_id',$data['customer_id']);
        /*if(isset($data['delegate_id']))
            $this->db->where('c.delegate_id',$data['delegate_id']);*/
        if(isset($data['delegate_id'])) {
            if(isset($data['session_user_role'])){
                $this->db->group_start();
                $this->db->where('c.delegate_id', $data['delegate_id']);
                $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
                $this->db->group_end();
            }
            else
                $this->db->where('c.delegate_id', $data['delegate_id']);
        }
        //if(isset($data['responsible_user_id']))
            //$this->db->where('cri.responsible_user_id',$data['responsible_user_id']);

        //$this->db->or_where('cri.responsible_user_id',$data['id_user']);
        if(isset($data['responsible_user_id']) || isset($data['created_by'])){
            $this->db->group_start();
            if(isset($data['responsible_user_id']))
                $this->db->where('cri.responsible_user_id',$data['responsible_user_id']);
            if(isset($data['created_by']))
                $this->db->or_where('cri.created_by',$data['created_by']);
            $this->db->group_end();
        }
        if(isset($data['session_user_role']) && $data['session_user_role']==3){
            $this->db->group_start();
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
            $this->db->or_where("c.id_contract in (select cux.contract_id from contract_user cux where cux.contract_review_id in (select max(crx.id_contract_review) from contract_review crx where crx.contract_id=c.id_contract) and cux.user_id=".$data['session_user_id']." and cux.status=1)",null,false);
            $this->db->group_end();
        }
        else {
            if (isset($data['business_unit_id']) && is_array($data['business_unit_id']))
                $this->db->where_in('c.business_unit_id', $data['business_unit_id']);
        }
        $this->db->where('c.is_deleted','0');


        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);

        $this->db->group_by('c.id_contract');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function checkContractReviewSchedule($data)
    {
        $this->db->select('*');
        $this->db->from('calender c');
        $this->db->join('contract cr','c.relationship_category_id=cr.relationship_category_id and cr.is_deleted=0','left');
        $this->db->join('relationship_category_remainder rcr','c.relationship_category_id=rcr.relationship_category_id','left');
        $this->db->where('cr.id_contract',$data['contract_id']);
        $this->db->where('CURDATE() between DATE_SUB(c.date,INTERVAL rcr.days DAY) and c.date');
        $this->db->where('c.status',1);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function checkContractReviewCompletedSchedule($data)
    {
        $this->db->select('*');
        $this->db->from('calender c');
        $this->db->join('contract cr','c.relationship_category_id=cr.relationship_category_id and cr.is_deleted=0','left');
        $this->db->join('relationship_category_remainder rcr','c.relationship_category_id=rcr.relationship_category_id','left');
        $this->db->join('contract_review crv','crv.contract_id=cr.id_contract','left');
        $this->db->where('cr.id_contract',$data['contract_id']);
        $this->db->where('CURDATE() between DATE_SUB(c.date,INTERVAL rcr.days DAY) and c.date');
        $this->db->where('DATE(crv.contract_review_due_date) between DATE_SUB(c.date,INTERVAL rcr.days DAY) and c.date');
        $this->db->where('c.status',1);
        $query = $this->db->get();
       // echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function getTopicData($data){


        $this->db->select('q.id_question,q.question_type,ql.question_text,ql.request_for_proof,cqr.question_answer,cqr.question_feedback,if(q.question_type="input",cqr.question_answer,qol.option_name) question_option_answer');
        $this->db->from('question q');
        $this->db->join('question_language ql','ql.question_id = q.id_question and ql.language_id=1','LEFT');
        $this->db->join('contract_question_review cqr','cqr.question_id = q.id_question ','LEFT');
        $this->db->join('contract_question_review_log l','cqr.id_contract_question_review=l.contract_question_review_id','left');
        $this->db->join('question_option qo', 'qo.question_id=q.id_question and qo.option_value=cqr.question_answer', 'left');
        $this->db->join('question_option_language qol', 'qol.question_option_id=qo.id_question_option and qol.language_id=1', 'left');
        $this->db->where('q.topic_id',$data['id_topic']);
        $this->db->where('q.question_status','1');
        $this->db->group_by('q.id_question');
        $this->db->order_by('q.question_order','ASC');
        $this->db->order_by('q.id_question','ASC');
        $questions = $this->db->get()->result_array();
        $result['questions'] = $questions;

        foreach($result['questions'] as $k => $v){
            //echo $result['questions'][$k]['id_question'];
            $this->db->select('d.*,concat(u.first_name," ",u.last_name) as uploaded_by');
            $this->db->from('document d');
            $this->db->join('user u','u.id_user = d.uploaded_by','');
            $this->db->where('d.reference_type','question');
            $this->db->where('d.document_status',1);
            if(isset($data['page_type']) && $data['page_type']='contract_review'){
                $this->db->where('d.reference_id IN (select q_sub.id_question from question q_sub LEFT JOIN question q2_sub on q2_sub.parent_question_id=q_sub.parent_question_id LEFT JOIN topic t2_sub on t2_sub.id_topic=q2_sub.topic_id LEFT JOIN module m2_sub on m2_sub.id_module=t2_sub.module_id LEFT JOIN contract_review cr2_sub on cr2_sub.id_contract_review=m2_sub.contract_review_id
LEFT JOIN contract c2_sub on c2_sub.id_contract=cr2_sub.contract_id and c2_sub.is_deleted=0 LEFT JOIN topic t1_sub on t1_sub.id_topic=q_sub.topic_id LEFT JOIN module m1_sub on m1_sub.id_module=t1_sub.module_id LEFT JOIN contract_review cr1_sub on cr1_sub.id_contract_review=m1_sub.contract_review_id where q2_sub.id_question='.$result['questions'][$k]['id_question'].' and cr1_sub.contract_id=cr2_sub.contract_id)',false,false);
            }
            else {
                $this->db->where('d.reference_id',$result['questions'][$k]['id_question']);
            }
            $query = $this->db->get();
            $attachment=$query->result_array();

            $result['questions'][$k]['attachments'] = $attachment;
        }
        return $result;
    }
    function getContractDeadline($data=array()){
        $this->db->select('c.*');
        $this->db->from('calender c');
        $this->db->join('business_unit bu','bu.customer_id=c.customer_id','left');
        $this->db->join('contract co','co.business_unit_id=bu.id_business_unit and co.is_deleted=0','left');
        $this->db->where('c.date>=CURRENT_DATE()');
        $this->db->where('c.status',1);
        if(isset($data['relationship_category_id']))
            $this->db->where('c.relationship_category_id',$data['relationship_category_id']);
        if(isset($data['id_contract']))
            $this->db->where('co.id_contract',$data['id_contract']);
        $this->db->order_by('c.date','asc');
        $this->db->limit('1');
        $query = $this->db->get();
        $result=$query->result_array();
        return isset($result[0]['date'])?$result[0]['date']:NULL;
    }

    public function getDownloadedFile($data){
        if(isset($data['id_document'])) {
            $this->db->select('cd.document_source, cd.document_name');
            $this->db->from('document cd');
            $this->db->where('cd.id_document',$data['id_document']);
            $query = $this->db->get();
            return $query->result_array();
        }
    }

    public function getContractReviewChangeLog($data)
    {

        $modules=$topics=$questions=array();
        if(isset($data['contract_review_id'])) {
            $this->db->select('m.id_module,ml.module_name');
            $this->db->from('module m');
            $this->db->join('module_language ml', 'ml.module_id=m.id_module and ml.language_id=1', 'left');
            $this->db->join('contract_review cr', 'cr.id_contract_review=m.contract_review_id', 'left');
            if (isset($data['id_contract_review']))
                $this->db->where('cr.id_contract_review', $data['id_contract_review']);
            if (isset($data['contract_review_id']))
                $this->db->where('cr.id_contract_review', $data['contract_review_id']);
            if(isset($data['contract_user'])){
                $this->db->join('contract_user cu','m.id_module=cu.module_id','');
                $this->db->where('cu.user_id',$data['contract_user']);
                $this->db->group_by('m.id_module');
            }
            $this->db->order_by('m.module_order','asc');
            $query = $this->db->get();
            $modules = $query->result_array();
        }

        if(isset($data['id_module'])) {
            $this->db->select('t.id_topic,tl.topic_name,m.id_module');
            $this->db->from('topic t');
            $this->db->join('topic_language tl', 'tl.topic_id=t.id_topic and tl.language_id=1', 'left');
            $this->db->join('module m','m.id_module=t.module_id');
            $this->db->join('contract_review cr', 'cr.id_contract_review=m.contract_review_id', 'left');
            if (isset($data['id_contract_review']))
                $this->db->where('cr.id_contract_review', $data['id_contract_review']);
            if (isset($data['contract_review_id']))
                $this->db->where('cr.id_contract_review', $data['contract_review_id']);
            if (isset($data['id_module']) && $data['id_module']!='all')
                $this->db->where('t.module_id', $data['id_module']);
            if(isset($data['contract_user'])){
                $this->db->join('contract_user cu','m.id_module=cu.module_id','');
                $this->db->where('cu.user_id',$data['contract_user']);
                //$this->db->group_by('m.id_module');
            }
            $this->db->order_by('m.module_order','asc');
            $this->db->order_by('t.topic_order','asc');
            $query = $this->db->get();
            $topics = $query->result_array();
        }

        if(isset($data['id_module']) || isset($data['id_topic'])) {
            $this->db->select('m.id_module,t.id_topic,q.id_question,ql.question_text,ql.request_for_proof,tl.topic_name,ml.module_name,if(q.question_type="input",cqrl.question_answer,qol.option_name) question_answer,q.question_type,CONCAT_WS(\' \',u.first_name,u.last_name) as answer_by_username,cqrl.updated_on,0 as is_current');
            $this->db->from('contract_question_review_log cqrl');
            $this->db->join('contract_review cr', 'cr.id_contract_review=cqrl.contract_review_id', 'left');
            $this->db->join('contract c', 'c.id_contract=cr.contract_id and c.is_deleted=0', 'left');
            $this->db->join('question q', 'q.id_question=cqrl.question_id', 'left');
            $this->db->join('question_language ql', 'ql.question_id=q.id_question and ql.language_id=1', 'left');
            $this->db->join('question_option qo', 'qo.question_id=q.id_question and qo.id_question_option=cqrl.question_option_id', 'left');
            $this->db->join('question_option_language qol', 'qol.question_option_id=qo.id_question_option and qol.language_id=1', 'left');
            $this->db->join('topic t', 't.id_topic=q.topic_id', 'left');
            $this->db->join('topic_language tl', 'tl.topic_id=t.id_topic and tl.language_id=1', 'left');
            $this->db->join('module m', 'm.id_module=t.module_id', 'left');
            $this->db->join('module_language ml', 'ml.module_id=m.id_module and ml.language_id=1', 'left');
            $this->db->join('user u', 'u.id_user=cqrl.updated_by', 'left');
            if (isset($data['id_contract_review']))
                $this->db->where('cr.id_contract_review', $data['id_contract_review']);
            if (isset($data['contract_review_id']))
                $this->db->where('cr.id_contract_review', $data['contract_review_id']);
            if (isset($data['id_contract']))
                $this->db->where('c.id_contract', $data['id_contract']);
            if (isset($data['id_module']) && $data['id_module']!='all')
                $this->db->where('m.id_module', $data['id_module']);
            if (isset($data['id_topic']) && $data['id_topic']!='all')
                $this->db->where('t.id_topic', $data['id_topic']);
            if (isset($data['id_question']))
                $this->db->where('q.id_question', $data['id_question']);
            if(isset($data['contract_user'])){
                $this->db->join('contract_user cu','m.id_module=cu.module_id','');
                $this->db->where('cu.user_id',$data['contract_user']);
                //$this->db->group_by('m.id_module');
            }

            //$this->db->order_by('cqrl.id_contract_question_review_log','desc');
            $this->db->order_by('m.module_order','asc');
            $this->db->order_by('t.topic_order','asc');
            $this->db->order_by('q.question_order','asc');
            $query=$this->db->get();
            $questions_log=$query->result_array();
            $question_ids=array();
            foreach($questions_log as $k=>$v){
                $question_ids[]=$v['id_question'];

            }
            $question_ids=array_unique($question_ids);
            $questions_current=array();
            if(count($question_ids)>0) {
                $this->db->select('m.id_module,t.id_topic,q.id_question,ql.question_text,ql.request_for_proof,tl.topic_name,ml.module_name,if(q.question_type="input",cqrl.question_answer,qol.option_name) question_answer,q.question_type,CONCAT_WS(\' \',u.first_name,u.last_name) as answer_by_username,cqrl.updated_on,1 as is_current');
                $this->db->from('contract_question_review cqrl');
                $this->db->join('contract_review cr', 'cr.id_contract_review=cqrl.contract_review_id', 'left');
                $this->db->join('contract c', 'c.id_contract=cr.contract_id and c.is_deleted=0', 'left');
                $this->db->join('question q', 'q.id_question=cqrl.question_id', 'left');
                $this->db->join('question_language ql', 'ql.question_id=q.id_question and ql.language_id=1', 'left');
                $this->db->join('question_option qo', 'qo.question_id=q.id_question and qo.id_question_option=cqrl.question_option_id', 'left');
                $this->db->join('question_option_language qol', 'qol.question_option_id=qo.id_question_option and qol.language_id=1', 'left');
                $this->db->join('topic t', 't.id_topic=q.topic_id', 'left');
                $this->db->join('topic_language tl', 'tl.topic_id=t.id_topic and tl.language_id=1', 'left');
                $this->db->join('module m', 'm.id_module=t.module_id', 'left');
                $this->db->join('module_language ml', 'ml.module_id=m.id_module and ml.language_id=1', 'left');
                $this->db->join('user u', 'u.id_user=cqrl.updated_by', 'left');
                if (isset($data['id_contract_review']))
                    $this->db->where('cr.id_contract_review', $data['id_contract_review']);
                if (isset($data['contract_review_id']))
                    $this->db->where('cr.id_contract_review', $data['contract_review_id']);
                if (isset($data['id_contract']))
                    $this->db->where('c.id_contract', $data['id_contract']);
                if (isset($data['id_module']) && $data['id_module'] != 'all')
                    $this->db->where('m.id_module', $data['id_module']);
                if (isset($data['id_topic']) && $data['id_topic'] != 'all')
                    $this->db->where('t.id_topic', $data['id_topic']);
                if (isset($data['id_question']))
                    $this->db->where('q.id_question', $data['id_question']);
                if (isset($data['contract_user'])) {
                    $this->db->join('contract_user cu', 'm.id_module=cu.module_id', '');
                    $this->db->where('cu.user_id', $data['contract_user']);
                    //$this->db->group_by('m.id_module');
                }
                $this->db->where_in('q.id_question', $question_ids);
                $this->db->order_by('m.module_order', 'asc');
                $this->db->order_by('t.topic_order', 'asc');
                $this->db->order_by('q.question_order', 'asc');
                $query = $this->db->get();
                $questions_current = $query->result_array();
            }
            /*echo "<pre>";print_r($questions_current);echo "</pre>";*/
            $questions=array_merge($questions_log,$questions_current);
        }
        return array('modules'=>$modules,'topics'=>$topics,'questions'=>$questions);
    }
    public function getContractReviewDisucussionData($data){

        $this->db->select('q.id_question,ql.question_text,t.id_topic,tl.topic_name,m.id_module,ml.module_name,m.contract_review_id,crd.id_contract_review_discussion,crdq.id_contract_review_discussion_question,crdq.remarks,crdq.status,if(crdq.updated_on is not null,CONCAT_WS(\' \',u1.first_name,u1.last_name),CONCAT_WS(\' \',u.first_name,u.last_name)) as created_by,if(crdq.updated_on is not null,crdq.updated_on,crdq.created_on) as created_on,CONCAT_WS(\' \',u2.first_name,u2.last_name) discussion_created_by,crd.created_on as discussion_created_on,crd.discussion_status,CONCAT_WS(\' \',u3.first_name,u3.last_name) discussion_closed_by,crd.updated_on as discussion_closed_on,crd.is_auto_close');
        $this->db->from('question q');
        $this->db->join('question_language ql', 'ql.question_id=q.id_question and ql.language_id=1', 'left');
        $this->db->join('topic t', 't.id_topic=q.topic_id', 'left');
        $this->db->join('topic_language tl', 'tl.topic_id=t.id_topic and tl.language_id=1', 'left');
        $this->db->join('module m', 'm.id_module=t.module_id', 'left');
        $this->db->join('module_language ml', 'ml.module_id=m.id_module and ml.language_id=1', 'left');
        $this->db->join('contract_review_discussion crd', 'crd.module_id=m.id_module and m.contract_review_id=crd.contract_review_id', 'left');
        if(isset($data['contract_user'])) {
            /*$this->db->join('contract_review_discussion_question crdq', 'crdq.question_id=q.id_question and crdq.status=1', 'left');
            $this->db->join('contract_review_discussion crd', 'crd.id_contract_review_discussion=crdq.contract_review_discussion_id and m.contract_review_id=crd.contract_review_id and crd.discussion_status=1', 'left');*/
            $this->db->join('contract_review_discussion_question crdq', 'crdq.question_id=q.id_question and crdq.status=1 and crd.id_contract_review_discussion=crdq.contract_review_discussion_id', 'left');
            //$this->db->join('contract_review_discussion crd', 'crd.id_contract_review_discussion=crdq.contract_review_discussion_id and m.contract_review_id=crd.contract_review_id and crd.discussion_status=1', 'left');
        }
        else{
            //$this->db->join('contract_review_discussion_question crdq', 'crdq.question_id=q.id_question and crdq.status=1', '');
            //$this->db->join('contract_review_discussion crd', 'crd.id_contract_review_discussion=crdq.contract_review_discussion_id and m.contract_review_id=crd.contract_review_id and crd.discussion_status=1', '');
            $this->db->join('contract_review_discussion_question crdq', 'crdq.question_id=q.id_question and crdq.status=1 and crd.id_contract_review_discussion=crdq.contract_review_discussion_id', 'left');
            $this->db->where('crd.id_contract_review_discussion is not null');
            $this->db->where('crdq.id_contract_review_discussion_question is not null');
        }
        if (isset($data['id_contract_review']))
            $this->db->where('m.contract_review_id', $data['id_contract_review']);
        if (isset($data['contract_review_id']))
            $this->db->where('m.contract_review_id', $data['contract_review_id']);
        if(isset($data['contract_user']) && !isset($data['id_contract_review_discussion'])){
            $this->db->join('contract_user cu','m.id_module=cu.module_id','');
            $this->db->where('cu.user_id',$data['contract_user']);
            //$this->db->group_by('q.id_question');
        }
        if(isset($data['discussion_status'])){
            $this->db->where('crd.discussion_status',$data['discussion_status']);
        }
        if(isset($data['id_contract_review_discussion'])) {
            $this->db->where('crd.id_contract_review_discussion',$data['id_contract_review_discussion']);
        }
        $this->db->join('user u', 'u.id_user=crdq.created_by', 'left');
        $this->db->join('user u1', 'u1.id_user=crdq.updated_by', 'left');
        $this->db->join('user u2', 'u2.id_user=crd.created_by', 'left');
        $this->db->join('user u3', 'u3.id_user=crd.updated_by', 'left');
        $this->db->order_by('m.module_order','ASC');
        $this->db->order_by('t.topic_order','ASC');
        $this->db->order_by('q.question_order','ASC');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $result=$query->result_array();
        foreach($result as $k=>$v){
            $result[$k]['change_log']=$this->getContractReviewDisucussionLogData(array('contract_review_discussion_question_id'=>($v['id_contract_review_discussion_question']==NULL?0:$v['id_contract_review_discussion_question'])));
            if(isset($v['discussion_status']) && $v['discussion_status']==2 && $v['id_contract_review_discussion_question']==NULL)
                unset($result[$k]);

        }
        return $result;
    }
    public function getContractReviewDisucussionLogData($data){
        $this->db->select('crdql.*,CONCAT_WS(\' \',u.first_name,u.last_name) as created_by_name');
        $this->db->from('contract_review_discussion_question_log crdql');
        if(isset($data['contract_review_discussion_question_id'])) {
            $this->db->where('crdql.contract_review_discussion_question_id',$data['contract_review_discussion_question_id']);
        }
        $this->db->join('user u', 'u.id_user=crdql.created_by');
        $this->db->order_by('crdql.id_contract_review_discussion_question_log','desc');
        $query = $this->db->get();
        $result=$query->result_array();
        return $result;
    }
    public function getContractReviewDiscussion($data=array()){
        $this->db->select('*');
        $this->db->from('contract_review_discussion crd');
        if (isset($data['id_contract_review']))
            $this->db->where('crd.contract_review_id', $data['id_contract_review']);
        if (isset($data['id_module']))
            $this->db->where('crd.module_id', $data['id_module']);
        $query = $this->db->get();
        $result=$query->result_array();
        return isset($result[0])?$result[0]:'';
    }
    public function getContractDiscussion($data=array()){
        $this->db->select('crd.*,CONCAT_WS(\' \',u2.first_name,u2.last_name) discussion_created_by,crd.created_on as discussion_created_on,CONCAT_WS(\' \',u3.first_name,u3.last_name) discussion_closed_by,crd.updated_on as discussion_closed_on,CONCAT_WS(\'/\',CONCAT_WS(\' \',u2.first_name,u2.last_name),crd.updated_on,ml.module_name) as option_name');
        $this->db->from('contract_review_discussion crd');
        $this->db->join('contract_review cr','cr.id_contract_review=crd.contract_review_id');
        $this->db->join('contract c','c.id_contract=cr.contract_id');
        $this->db->join('module m','m.id_module=crd.module_id');
        $this->db->join('module_language ml','ml.module_id=m.id_module and ml.language_id=1');
        $this->db->join('user u2', 'u2.id_user=crd.created_by', 'left');
        $this->db->join('user u3', 'u3.id_user=crd.updated_by', 'left');
        if (isset($data['id_contract_review']))
            $this->db->where('crd.contract_review_id', $data['id_contract_review']);
        if (isset($data['id_module']))
            $this->db->where('crd.module_id', $data['id_module']);
        if (isset($data['id_contract']))
            $this->db->where('cr.contract_id', $data['id_contract']);
        if (isset($data['discussion_status']))
            $this->db->where('crd.discussion_status', $data['discussion_status']);
        if(isset($data['id_contract_review_discussion'])) {
            $this->db->where('crd.id_contract_review_discussion',$data['id_contract_review_discussion']);
        }
        if (isset($data['contract_review_status']))
            $this->db->where('cr.contract_review_status', $data['contract_review_status']);
        $this->db->where('c.is_deleted','0');
        $query = $this->db->get();
        $result=$query->result_array();
        return $result;
    }
    public function getContractReviewDiscussionModuleCount($data=array()){
        $this->db->select('*');
        $this->db->from('contract_review_discussion crd');
        if (isset($data['id_contract_review']))
            $this->db->where('crd.contract_review_id', $data['id_contract_review']);
        if(isset($data['discussion_status'])){
            $this->db->where('crd.discussion_status',$data['discussion_status']);
        }
        $query = $this->db->get();
        $result=$query->result_array();
        //echo $this->db->last_query();exit;
        return $result;
    }
    public function addContractReviewDiscussion($data=array()){

        $this->db->insert('contract_review_discussion', $data);
        return $this->db->insert_id();

    }
    public function updateContractReviewDiscussion($data=array()){
        $this->db->where('id_contract_review_discussion', $data['id_contract_review_discussion']);
        if(isset($data['discussion_status']))
            $this->db->where('discussion_status!=', $data['discussion_status']);
        $this->db->update('contract_review_discussion', $data);
    }
    public function addContractReviewDiscussionQuestion($data=array()){
        foreach($data as $k=>$v) {
            $this->db->insert('contract_review_discussion_question', $v);
            $data[$k]['id_contract_review_discussion_question']=$this->db->insert_id();
            $data[$k]['status']=1;
        }
        /*foreach($data as $k=>$v) {
            $log='';
            $log['contract_review_discussion_question_id']=$v['id_contract_review_discussion_question'];
            $log['remarks']=$v['remarks'];
            $log['status']=$v['status'];
            $log['created_by']=$v['created_by'];
            $log['created_on']=$v['created_on'];
            $this->db->insert('contract_review_discussion_question_log', $log);
        }*/
    }
    public function updateContractReviewDiscussionQuestion($data=array()){

        foreach($data as $k=>$v) {
            $this->db->where('id_contract_review_discussion_question', $v['id_contract_review_discussion_question']);
            $this->db->update('contract_review_discussion_question', $v);
        }
        /*foreach($data as $k=>$v) {
            $log='';
            $log['contract_review_discussion_question_id']=$v['id_contract_review_discussion_question'];
            $log['remarks']=$v['remarks'];
            $log['status']=$v['status'];
            $log['created_by']=$v['updated_by'];
            $log['created_on']=$v['updated_on'];
            $this->db->insert('contract_review_discussion_question_log', $log);
        }*/

    }
    public function getContractsToBeScheduled($data=array()){

        if(isset($data['reminder']) && $data['reminder']=='r2')
        {
            $this->db->select('*,CURDATE() as reminder_date_1,DATE_ADD(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\'),INTERVAL rcr.r2_days DAY) as reminder_date_2,DATE_ADD(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\'),INTERVAL (rcr.r3_days+rcr.r2_days) DAY) as reminder_date_3');
            $this->db->from('calender c');
            $this->db->join('contract cr','c.relationship_category_id=cr.relationship_category_id and cr.is_deleted=0','left');
            $this->db->join('relationship_category_remainder rcr','c.relationship_category_id=rcr.relationship_category_id','left');
            $this->db->where_in('cr.contract_status',array('pending review'));
            $this->db->where('CURDATE() between DATE_SUB(c.date,INTERVAL rcr.days DAY) and c.date');
            $this->db->where('c.status',1);
            $this->db->where('cr.reminder_date2 IS NOT NULL');
            $this->db->where('CURDATE() = cr.reminder_date2');
        }else if(isset($data['reminder']) && $data['reminder']=='r3')
        {
            $this->db->select('*,CURDATE() as reminder_date_1,DATE_ADD(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\'),INTERVAL rcr.r2_days DAY) as reminder_date_2,DATE_ADD(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\'),INTERVAL (rcr.r3_days+rcr.r2_days) DAY) as reminder_date_3');
            $this->db->from('calender c');
            $this->db->join('contract cr','c.relationship_category_id=cr.relationship_category_id and cr.is_deleted=0','left');
            $this->db->join('relationship_category_remainder rcr','c.relationship_category_id=rcr.relationship_category_id','left');
            $this->db->where_in('cr.contract_status',array('pending review'));
            $this->db->where('CURDATE() between DATE_SUB(c.date,INTERVAL rcr.days DAY) and c.date');
            $this->db->where('c.status',1);
            $this->db->where('cr.reminder_date3 IS NOT NULL');
            $this->db->where('CURDATE() = cr.reminder_date3');
        }
        else if(isset($data['reminder']) && $data['reminder']=='r1')
        {
            $this->db->select('*,CURDATE() as reminder_date_1,DATE_ADD(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\'),INTERVAL rcr.r2_days DAY) as reminder_date_2,DATE_ADD(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\'),INTERVAL (rcr.r3_days+rcr.r2_days) DAY) as reminder_date_3');
            $this->db->from('calender c');
            $this->db->join('contract cr','c.relationship_category_id=cr.relationship_category_id and cr.is_deleted=0','left');
            $this->db->join('relationship_category_remainder rcr','c.relationship_category_id=rcr.relationship_category_id','left');
            $this->db->where_in('cr.contract_status',array('pending review'));
            $this->db->where('CURDATE() between DATE_SUB(c.date,INTERVAL rcr.days DAY) and c.date');
            $this->db->where('c.status',1);
            $this->db->where('cr.reminder_date1 IS NOT NULL');
            $this->db->where('CURDATE() = cr.reminder_date1');
        }else
        {
            $this->db->select('*,CURDATE() as reminder_date_1,DATE_ADD(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\'),INTERVAL rcr.r2_days DAY) as reminder_date_2,DATE_ADD(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\'),INTERVAL (rcr.r3_days+rcr.r2_days) DAY) as reminder_date_3');
            $this->db->from('calender c');
            $this->db->join('contract cr','c.relationship_category_id=cr.relationship_category_id and cr.is_deleted=0','left');
            $this->db->join('relationship_category_remainder rcr','c.relationship_category_id=rcr.relationship_category_id','left');
            $this->db->where_in('cr.contract_status',array('new','review finalized'));
            $this->db->where('CURDATE() between DATE_SUB(c.date,INTERVAL rcr.days DAY) and c.date');
            $this->db->where('c.status',1);
        }


        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function getEmailTemplate($data){

        /*if(isset($data['search'])) {
            $data['search']=$this->db->escape($data['search']);
        }*/
        $this->db->select('et.*,etl.*');
        $this->db->from('email_template et');
        $this->db->join('email_template_language etl','et.id_email_template = etl.email_template_id and etl.language_id=1','');
        if(isset($data['id_email_template']))
            $this->db->where('et.id_email_template',$data['id_email_template']);
        if(isset($data['customer_id']))
            $this->db->where('et.customer_id',$data['customer_id']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('et.module_name', $data['search'], 'both');
            $this->db->or_like('etl.template_subject', $data['search'], 'both');
            $this->db->or_like('etl.template_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(et.module_name like "%'.$data['search'].'%"
            or etl.template_subject like "%'.$data['search'].'%"
            or etl.template_name like "%'.$data['search'].'%")');*/
        $count = $this->db->get();


        $this->db->select('et.*,etl.*');
        $this->db->from('email_template et');
        $this->db->join('email_template_language etl','et.id_email_template = etl.email_template_id and etl.language_id=1','');
        if(isset($data['id_email_template']))
            $this->db->where('et.id_email_template',$data['id_email_template']);
        if(isset($data['customer_id']))
            $this->db->where('et.customer_id',$data['customer_id']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('et.module_name', $data['search'], 'both');
            $this->db->or_like('etl.template_subject', $data['search'], 'both');
            $this->db->or_like('etl.template_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(et.module_name like "%'.$data['search'].'%"
            or etl.template_subject like "%'.$data['search'].'%"
            or etl.template_name like "%'.$data['search'].'%")');*/
        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('et.id_email_template','ASC');
        $result = $this->db->get();
        $final_result=$result->result_array();
        foreach($final_result as $k=>$v){
            $final_result[$k]['header']=EMAIL_HEADER_CONTENT;
            $final_result[$k]['footer']=EMAIL_FOOTER_CONTENT;
        }
        return array('total_records'=>$count->num_rows(),'data'=>$final_result);
    }

    public function updateEmailTemplate($data){
        if(isset($data['id_email_template'])){
            $this->db->where('id_email_template',$data['id_email_template']);
            $this->db->update('email_template',$data);
        }
        if(isset($data['id_email_template_language'])) {
            $this->db->where('id_email_template_language',$data['id_email_template_language']);
            $this->db->update('email_template_language', $data);
        }
    }

    public function insertEmailTemplate($data){
        if(isset($data['customer_id'])){
            $customer_id = $this->getUniqueCustomerId($data['email_template']['parent_email_template_id']);
            if (count($customer_id)>1){
                $data['email_template']['customer_id'] = $data['customer_id'];
                $this->db->insert('email_template', $data['email_template']);
                $data['email_template_language']['email_template_id'] = $this->db->insert_id();
                $this->db->insert('email_template_language', $data['email_template_language']);
            }


        }else {
            $customer_id = $this->getUniqueCustomerId($data['email_template']['parent_email_template_id']);
            foreach ($customer_id as $k => $v) {
                if ($v->customer_id == 0)
                    continue;
                $data['email_template']['customer_id'] = $v->customer_id;
                $this->db->insert('email_template', $data['email_template']);
                $data['email_template_language']['email_template_id'] = $this->db->insert_id();
                $this->db->insert('email_template_language', $data['email_template_language']);

            }
        }
        return true;
    }
    public function getUniqueCustomerId($data){
        $q='SELECT DISTINCT(A.customer_id)
                                    FROM `email_template` A
                                    WHERE A.`customer_id` not IN(SELECT DISTINCT(A.customer_id)
                                    FROM `email_template` A
                                    WHERE A.`parent_email_template_id` =?) and A.`customer_id`>0';
        $query = $this->db->query($q,array($data));
        $result = $query->result();
        return $result;
    }
    public function migrateContractUsersFromOldReview($data=array()){
        $query = $this->db->query('INSERT into contract_user (contract_id,module_id,user_id,status,created_by,created_on,contract_review_id)
select cr.contract_id,m2.id_module,cu.user_id,\'1\' status,"'.$this->db->escape($data['created_by']).'" created_by,"'.currentDate().'" as created_on,m2.contract_review_id from contract_user cu join module m on m.id_module=cu.module_id join contract_review cr on cr.id_contract_review=m.contract_review_id
left join module m2 on m2.parent_module_id=m.parent_module_id and m2.contract_review_id='.$this->db->escape($data['new_contract_review_id']).'
where cu.`status`=1 and m.contract_review_id='.$this->db->escape($data['old_contract_review_id']));
        $q='select cr.contract_id,m2.id_module,cu.user_id,\'1\' status,? created_by,"'.currentDate().'" as created_on,m2.contract_review_id from contract_user cu join module m on m.id_module=cu.module_id join contract_review cr on cr.id_contract_review=m.contract_review_id
left join module m2 on m2.parent_module_id=m.parent_module_id and m2.contract_review_id=?
where cu.`status`=1 and m.contract_review_id=?';
        $query = $this->db->query($q,array($data['created_by'],$data['new_contract_review_id'],$data['old_contract_review_id']));
        $result = $query->result_array();
        return $result;
        /*
         * INSERT into contract_user (contract_id,module_id,user_id,status,created_by,created_on)
select cr.contract_id,m2.id_module,cu.user_id,'1' status,'46' created_by,NOW() as created_on from contract_user cu join module m on m.id_module=cu.module_id join contract_review cr on cr.id_contract_review=m.contract_review_id
left join module m2 on m2.parent_module_id=m.parent_module_id and m2.contract_review_id=17
where cu.`status`=1 and m.contract_review_id=15;
         */
    }

    public function getDailyMailData($data){
        $date1=$data['date'];
        //echo $date1;exit;
        $date2=date('Y-m-d',strtotime('+1 day'. $data['date']));
        if(isset($data['run']) && $data['run']==1){
            $this->db->query('INSERT into daily_update_customer (customer_id,date,created_on) (
                            select DISTINCT(c.id_customer),"'.$date1.'",now() from customer c LEFT JOIN daily_update_customer dup on
                            dup.customer_id=c.id_customer and date="'.$date1.'" JOIN user u on c.id_customer=u.customer_id and u.user_role_id=2
                            and dup.date is null);');
            return 1;
        }

        if(isset($data['review_started']) && $data['review_started']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,cr.created_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_review cr','cl.id_contract=cr.contract_id','');
            $this->db->join('user u','u.id_user = cr.created_by','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role  and ur.role_status=1','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->where('cr.created_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');
        }
        if(isset($data['review_updated']) && $data['review_updated']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,cr.updated_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_review cr','cl.id_contract=cr.contract_id','');
            $this->db->join('user u','u.id_user = cr.updated_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role  and ur.role_status=1','');
            $this->db->where('cr.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['review_finalized']) && $data['review_finalized']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,cr.updated_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_review cr','cl.id_contract=cr.contract_id','');
            $this->db->join('user u','u.id_user = cr.updated_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('cr.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cr.contract_review_status','finished');
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['contributor_add']) && $data['contributor_add']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,cr.created_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_user cr','cl.id_contract=cr.contract_id','');
            $this->db->join('user u','u.id_user = cr.created_by','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->where('cr.created_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['contributor_remove']) && $data['contributor_remove']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,cr.updated_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_user cr','cl.id_contract=cr.contract_id','');
            $this->db->join('user u','u.id_user = cr.updated_by','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->where('cr.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cr.status',0);
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['discussion_started']) && $data['discussion_started']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,crd.created_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_review cr','cl.id_contract=cr.contract_id');
            $this->db->join('contract_review_discussion crd','cr.id_contract_review=crd.contract_review_id','');
            $this->db->join('user u','u.id_user = crd.created_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('crd.created_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['discussion_updated']) && $data['discussion_updated']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,crd.updated_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_review cr','cl.id_contract=cr.contract_id');
            $this->db->join('contract_review_discussion crd','cr.id_contract_review=crd.contract_review_id','');
            $this->db->join('user u','u.id_user = crd.updated_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('crd.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['discussion_closed']) && $data['discussion_closed']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,crd.updated_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_review cr','cl.id_contract=cr.contract_id');
            $this->db->join('contract_review_discussion crd','cr.id_contract_review=crd.contract_review_id','');
            $this->db->join('user u','u.id_user = crd.updated_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('crd.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('crd.discussion_status',2);
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['action_item_created']) && $data['action_item_created']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,crd.created_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_review cr','cl.id_contract=cr.contract_id');
            $this->db->join('contract_review_action_item crd','cr.id_contract_review=crd.contract_review_id','');
            $this->db->join('user u','u.id_user = crd.created_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('crd.created_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['action_item_updated']) && $data['action_item_updated']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,crd.updated_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_review cr','cl.id_contract=cr.contract_id');
            $this->db->join('contract_review_action_item crd','cr.id_contract_review=crd.contract_review_id','');
            $this->db->join('user u','u.id_user = crd.updated_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('crd.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['action_item_closed']) && $data['action_item_closed']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,crd.updated_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('contract_review cr','cl.id_contract=cr.contract_id');
            $this->db->join('contract_review_action_item crd','cr.id_contract_review=crd.contract_review_id','');
            $this->db->join('user u','u.id_user = crd.created_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('crd.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('crd.status','completed');
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['report_created']) && $data['report_created']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,r.created_on as date,u.customer_id,ur.user_role_name');
            $this->db->from('report r');
            $this->db->join('user u','u.id_user = r.created_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('r.created_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
        }
        if(isset($data['report_edited']) && $data['report_edited']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,r.updated_on as date,u.customer_id,ur.user_role_name');
            $this->db->from('report r');
            $this->db->join('user u','u.id_user = r.updated_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('r.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
        }
        if(isset($data['report_deleted']) && $data['report_deleted']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,r.updated_on as date,u.customer_id,ur.user_role_name');
            $this->db->from('report r');
            $this->db->join('user u','u.id_user = r.updated_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('r.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('r.report_status',2);
            $this->db->where('cu.id_customer',$data['customer_id']);
        }
        if(isset($data['changes_in_contract']) && $data['changes_in_contract']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,cl.created_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract_log cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('user u','u.id_user = cl.created_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('cl.created_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
        }
        if(isset($data['changes_in_contract_status']) && $data['changes_in_contract_status']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,cl.created_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name,cl.contract_status');
            $this->db->from('contract_log cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('user u','u.id_user = cl.created_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('cl.created_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cl.is_status_change',1);
            $this->db->where('cu.id_customer',$data['customer_id']);
        }
        if(isset($data['new_contract']) && $data['new_contract']==1){
            $this->db->select('cu.company_name,CONCAT(u.first_name," ",u.last_name) as name,cl.created_on as date,cl.provider_name,cl.contract_name,bu.customer_id,bu.bu_name,ur.user_role_name');
            $this->db->from('contract cl');
            $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
            $this->db->join('user u','u.id_user = cl.created_by','');
            $this->db->join('customer cu','u.customer_id = cu.id_customer','');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('cl.created_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('cu.id_customer',$data['customer_id']);
            $this->db->where('c1.is_deleted','0');

        }
        if(isset($data['user_create']) && $data['user_create']==1){
            $this->db->select('CONCAT(u.first_name," ",u.last_name) as name,c.company_name,GROUP_CONCAT(bu.bu_name) business_unit,ur.user_role_name,u.created_on,\'User Deleted\' action');
            $this->db->from('user u');
            $this->db->join('customer c','u.customer_id = c.id_customer','left');
            $this->db->join('business_unit_user buu','buu.user_id=u.id_user and buu.status=1','left');
            $this->db->join('business_unit bu','buu.business_unit_id = bu.id_business_unit','left');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('u.created_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('u.customer_id',$data['customer_id']);
            $this->db->group_by('u.id_user');
        }
        if(isset($data['user_update']) && $data['user_update']==1){
            $this->db->select('CONCAT(u.first_name," ",u.last_name) as name,c.company_name,GROUP_CONCAT(bu.bu_name) business_unit,ur.user_role_name,u.updated_on as created_on,\'User Updated\' action');
            $this->db->from('user u');
            $this->db->join('customer c','u.customer_id = c.id_customer','left');
            $this->db->join('business_unit_user buu','buu.user_id=u.id_user and buu.status=1','left');
            $this->db->join('business_unit bu','buu.business_unit_id = bu.id_business_unit','left');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('u.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('c.id_customer',$data['customer_id']);
            $this->db->group_by('u.id_user');
        }
        if(isset($data['user_delete']) && $data['user_delete']==1){
            $this->db->select('CONCAT(u.first_name," ",u.last_name) as name,c.company_name,GROUP_CONCAT(bu.bu_name) business_unit,ur.user_role_name,u.updated_on as created_on,\'User Created\' action');
            $this->db->from('user u');
            $this->db->join('customer c','u.customer_id = c.id_customer','left');
            $this->db->join('business_unit_user buu','buu.user_id=u.id_user and buu.status=1','left');
            $this->db->join('business_unit bu','buu.business_unit_id = bu.id_business_unit','left');
            $this->db->join('user_role ur','u.user_role_id = ur.id_user_role and ur.role_status=1','');
            $this->db->where('u.updated_on between "'.$date1.'" and "'.$date2.'"');
            $this->db->where('u.user_status',0);
            $this->db->where('c.id_customer',$data['customer_id']);
            $this->db->group_by('u.id_user');
        }

        $data_info=$this->db->get();
        //echo '<br>';echo $this->db->last_query();
        return $data_info->result_array();

        /*$this->db->select('bu.customer_id,bu.bu_name,ur.user_role_name');
        $this->db->join('business_unit bu','cl.business_unit_id = bu.id_business_unit','');
        $this->db->join('user u','u.id_user = cl.created_by','');
        $this->db->join('user_role ur','u.user_role_id = ur.id_user_role','');
        if(isset($data['status_change']) && $data['status_change']==1){
            $this->db->from('contract_log cl');
            $this->db->where('cl.is_status_change=1');
            $this->db->where('DATE_FORMAT(cl.created_on,\'%Y-%m-%d\')',$data['date']);
        }
        if(isset($data['changes_in_contract']) && $data['changes_in_contract']==1){
            $this->db->from('contract_log cl');
            $this->db->where('DATE_FORMAT(cl.created_on,\'%Y-%m-%d\')',$data['date']);
        }
        if(isset($data['new_contract']) && $data['new_contract']==1){
            $this->db->from('contract cl');
            $this->db->where('DATE_FORMAT(cl.created_on,\'%Y-%m-%d\')',$data['date']);
        }
        $this->db->group_by('bu.customer_id');

        $adminlist=$this->db->get();


        return array('data'=>$data_info->result_array(),'admin_list'=>$adminlist->result_array());*/

    }
    public function checkReviewUserAccess($data=array()){
        $this->db->select('cu.*');
        $this->db->from('contract_user cu');
        $this->db->where('cu.contract_review_id',$data['contract_review_id']);
        $this->db->where('cu.user_id',$data['id_user']);
        $this->db->where('cu.status',1);
        $result1 = $this->db->get()->result_array();
        return count($result1);
    }

    public function getPreviousReviewQuestions($prev_review_id){
        return $this->db->select('*')->from('contract_question_review cqr')->join('question q ',' cqr.question_id = q.id_question')->where('cqr.contract_review_id',$prev_review_id)->get()->result_array();
    }
    public function getCurrentReviewQuestions($review_id){
        return $this->db->select('question_id')->from('contract_question_review cqr')->where('cqr.contract_review_id',$review_id)->get()->result_array();
    }

    public function getCurrentReviewQuestion($cur_review_id,$parent_q_id,$q_o_value){
        $this->db->select('q.id_question,qo.id_question_option')->from('module m');
        $this->db->join('topic t ',' m.id_module = t.module_id');
        $this->db->join('question q ',' q.topic_id = t.id_topic');
        $this->db->join('question_option qo ',' qo.question_id = q.id_question');
        $this->db->where('m.contract_review_id',$cur_review_id)->where('q.parent_question_id',$parent_q_id)->where('qo.option_value',$q_o_value);
        $result1 = $this->db->get()->row_array();
        return $result1;
    }

    public function getCurrentReviewQuestion1($cur_review_id,$parent_q_id){
        $this->db->select('q.id_question')->from('module m');
        $this->db->join('topic t ',' m.id_module = t.module_id');
        $this->db->join('question q ',' q.topic_id = t.id_topic');
        $this->db->where('m.contract_review_id',$cur_review_id)->where('q.parent_question_id',$parent_q_id);
        $result1 = $this->db->get()->row_array();
        return $result1;
    }

    public function moduleQuestionCount($module_id,$contract_review_id){
        $q='select concat(b.answer_questions,"/",a.total_questions) as count from
                            (select COUNT(q.id_question) as total_questions from module m
                            LEFT JOIN topic t on m.id_module=t.module_id
                            LEFT JOIN question q on t.id_topic=q.topic_id
                            where m.id_module=? ) a,
                            (select count(cqr.id_contract_question_review) as answer_questions from module m
                            LEFT JOIN topic t on m.id_module=t.module_id
                            LEFT JOIN question q on t.id_topic=q.topic_id
                            JOIN contract_question_review cqr on q.id_question=cqr.question_id
                            where m.id_module=? and cqr.question_answer!="" and cqr.contract_review_id=?) b';
        $query = $this->db->query($q,array($module_id,$module_id,$contract_review_id));
        //echo "<pre>";echo $this->db->last_query($query);echo "</pre>";exit;
        $result = $query->result();
        if(count($result>0)){
            return $result[0]->count;
        }else{
            return '';
        }
    }

    public function getContractName($id){
        $this->db->select('c.contract_name')->from('contract c')->where('c.id_contract',$id)->where('c.is_deleted=0');
        $result = $this->db->get()->result_array();
        if(count($result>0)){
            return $result[0];
        }else{
            return '';
        }

    }

    public function getContractReviewOpenActionItemsList($data)
    {
        $this->db->select('crai.*,CONCAT_WS(\' \',u.first_name,u.last_name) as responsible_user_name,u1.user_role_id');
        $this->db->from('contract_review_action_item crai');
        //$this->db->join('contract_review cr','cr.id_contract_review=crai.contract_review_id','LEFT');
        $this->db->join('contract_question_review cqr','cqr.question_id=crai.question_id','LEFT');
        $this->db->join('user u','u.id_user=crai.responsible_user_id','LEFT');
        $this->db->join('user u1','u1.id_user=crai.created_by');
        if(isset($data['id_contract']))
            $this->db->where('crai.contract_id',$data['id_contract']);
        if(isset($data['page_type']) && $data['page_type']='contract_review'){
            if(isset($data['id_module']))
                $this->db->where('crai.module_id IN (select m.id_module from contract_review cr JOIN module m on m.contract_review_id=cr.id_contract_review join module m2 on m2.parent_module_id=m.parent_module_id where cr.contract_id=crai.contract_id and m2.id_module='.$this->db->escape($data['id_module']).')');
            if(isset($data['topic_id']))
                $this->db->where('crai.topic_id IN (select t.id_topic from contract_review cr LEFT JOIN module m on m.contract_review_id=cr.id_contract_review JOIN topic t on t.module_id=m.id_module JOIN topic t2 on t2.parent_topic_id=t.parent_topic_id where cr.contract_id=crai.contract_id and t2.id_topic='.$this->db->escape($data['topic_id']).')');
            if (isset($data['id_contract_review']))
                $this->db->where('cqr.contract_review_id', $data['id_contract_review']);
            $this->db->where('(cqr.question_answer IS NULL OR cqr.question_answer = "")');
        }
        else {
            if (isset($data['id_module']))
                $this->db->where('crai.module_id', $data['id_module']);
            if (isset($data['id_contract_review']))
                $this->db->where('cr.id_contract_review', $data['id_contract_review']);
        }
        if(isset($data['item_status']))
            $this->db->where('crai.item_status',$data['item_status']);
        if(isset($data['action_status']))
            $this->db->where('crai.status','open');
        if(isset($data['contract_id'])) {
            // $this->db->where('cr.contract_id', $data['contract_id']);
            $this->db->where('crai.contract_id', $data['contract_id']);
        }
        if(isset($data['id_user']) && isset($data['user_role_id'])){
            if($data['user_role_id']==5){
                $this->db->group_start();
                $this->db->where('crai.created_by', $data['id_user']);
                $this->db->or_where('crai.responsible_user_id', $data['id_user']);
                $this->db->group_end();
            }
            else if($data['user_role_id']==4 || $data['user_role_id']==3 || $data['user_role_id']==2 || $data['user_role_id']==1 || $data['user_role_id']==6){
                $this->db->group_start();
                $this->db->where('crai.created_by', $data['id_user']);
                $this->db->or_where('u1.user_role_id>=', 2);
                $this->db->or_where('crai.responsible_user_id', $data['id_user']);
                $this->db->group_end();
            }
        }
        if(isset($data['responsible_user_id'])) {
            $this->db->where('crai.responsible_user_id', $data['responsible_user_id']);
        }
        $this->db->where('crai.question_id >0');
        $this->db->group_by('crai.question_id');
        $query = $this->db->get();//echo $this->db->last_query().'<br>';
        $result= $query->result_array();

        foreach ($result as $k => $v) {
            $view_access = 'annus';
            $edit_access = 'annus';
            $delete_access = 'annus';
            $status_change_access = 'annus';
            if(isset($data['id_user']) && isset($data['user_role_id'])) {
                $view_access = "itako";
                if ($data['user_role_id'] == 6 || $data['user_role_id'] == 5) {
                    if ($v['created_by'] == $data['id_user']) {
                        $edit_access = $delete_access = 'itako';
                    }
                    if ($v['responsible_user_id'] == $data['id_user'] || $v['created_by'] == $data['id_user']) {
                        $view_access = "itako";
                    }
                } else if ($data['user_role_id'] == 4 || $data['user_role_id'] == 3 || $data['user_role_id'] == 2 || $data['user_role_id'] == 1) {
                    $view_access = "itako";
                    if ($v['created_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $edit_access = $delete_access = 'itako';
                    }
                    if ($v['responsible_user_id'] == $data['id_user']|| $v['created_by'] == $data['id_user'] || $v['user_role_id'] > $data['user_role_id']) {
                        $view_access = "itako";
                    }
                }
            }
            else{
                $view_access = $edit_access = $delete_access = 'itako';
            }
            //$view_access="itako;
            if($view_access=="itako" && $v['status']!='completed')
                $status_change_access="itako";
            if($v['status']=='completed')
                $edit_access=$delete_access='annus';
            $result[$k]['vaav']=$view_access;
            $result[$k]['eaae']=$edit_access;
            $result[$k]['daad']=$delete_access;
            $result[$k]['scaacs']=$status_change_access;

            $this->db->select('c.*,concat(u.first_name," ",u.last_name) as user_name');
            $this->db->from('contract_review_action_item_log c');
            $this->db->join('user u','c.updated_by=u.id_user','left');
            $this->db->where('c.contract_review_action_item_id', $v['id_contract_review_action_item']);
            $query_log = $this->db->get();
            $result[$k]['comments_log']= $query_log->result_array();
        }

        return $result;
    }
}