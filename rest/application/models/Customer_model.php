<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
        $this->load->model('Mcommon');
    }

    public function customerList($data)
    {
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/
        $this->db->select('c.*,co.country_name,count(con.id_contract) as contracts_count');
        $this->db->from('customer c');
        $this->db->join('country co','c.country_id=co.id_country','left');
        $this->db->join('business_unit b','c.id_customer=b.customer_id','left');
        $this->db->join('contract con','b.id_business_unit=con.business_unit_id and con.is_deleted=0','left');
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('c.company_name', $data['search'], 'both');
            $this->db->or_like('c.company_address', $data['search'], 'both');
            $this->db->group_end();
        }
            //$this->db->where('(c.company_name like "%'.$data['search'].'%" or c.company_address like "%'.$data['search'].'%" or c.city like "%'.$data['search'].'%" or co.country_name like "%'.$data['search'].'%")');
        $this->db->group_by('c.id_customer');
        /* results count start */
        $all_clients_db = clone $this->db;
        $all_clients_count = $all_clients_db->get()->num_rows();
        /* results count end */

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('c.id_customer','DESC');

        $query = $this->db->get();
        return array('total_records' => $all_clients_count,'data' => $query->result_array());
    }

    public function getCustomer($data)
    {
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/
        $this->db->select('c.*,co.country_name');
        $this->db->from('customer c');
        $this->db->join('country co','c.country_id=co.id_country','left');
        if(isset($data['id_customer']))
            $this->db->where('c.id_customer',$data['id_customer']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('c.company_name', $data['search'], 'both');
            $this->db->or_like('c.company_address', $data['search'], 'both');
            $this->db->or_like('c.city', $data['search'], 'both');
            $this->db->or_like('co.country_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(c.company_name like "%'.$data['search'].'%"
            or c.company_address like "%'.$data['search'].'%"
            or c.city like "%'.$data['search'].'%"
            or co.country_name like "%'.$data['search'].'%")');*/
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCustomer($data)
    {
        $this->db->insert('customer', $data);
        return $this->db->insert_id();
    }

    public function updateCustomer($data)
    {
        $this->db->where('id_customer', $data['id_customer']);
        $this->db->update('customer', $data);
        return 1;
    }

    public function getCustomerAdminList($data)
    {
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/
        $this->db->select('u.id_user,ur.user_role_name,u.first_name,u.last_name,CONCAT(u.first_name," ",u.last_name) as name,u.email,u.gender,u.user_status,u.last_logged_on,u.first_name,u.last_name,u.is_blocked');
        $this->db->from('user u');
        $this->db->join('user_role ur','u.user_role_id=ur.id_user_role and ur.role_status=1','left');
        if(isset($data['customer_id']))
            $this->db->where('u.customer_id',$data['customer_id']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.email', $data['search'], 'both');
            $this->db->or_like('u.gender', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(u.first_name like "%'.$data['search'].'%"
            or u.first_name like "%'.$data['search'].'%"
            or u.email like "%'.$data['search'].'%"
            or u.gender like "%'.$data['search'].'%")');*/
        $this->db->where('u.user_role_id',2);
        /* results count start */
        $all_clients_db = clone $this->db;
        $all_clients_count = $all_clients_db->count_all_results();
        /* results count end */

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('u.id_user','DESC');
        $query = $this->db->get();
        return array('total_records' => $all_clients_count,'data' => $query->result_array());
    }

    public function getCustomerUserList($data)
    {
        /*user role not in (1-with admin),(2-customer admin)*/
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/
        $this->db->where_not_in('u.user_role_id',array(1,2));

        $this->db->select('u.id_user,ur.user_role_name,CONCAT(u.first_name," ",u.last_name) as name,u.email,u.gender,u.user_status,u.last_logged_on,u.is_blocked');
        $this->db->from('user u');
        $this->db->join('user_role ur','u.user_role_id=ur.id_user_role and ur.role_status=1','left');
        $this->db->join('business_unit_user bur','bur.user_id=u.id_user and status=1','left');
        $this->db->join('business_unit bu','bu.id_business_unit=bur.business_unit_id','left');
        if(isset($data['customer_id']))
            $this->db->where('u.customer_id',$data['customer_id']);
        if(isset($data['business_unit_id']))
            $this->db->where('bur.business_unit_id',$data['business_unit_id']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('bu.bu_name', $data['search'], 'both');
            $this->db->or_like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.last_name', $data['search'], 'both');
            $this->db->or_like('u.email', $data['search'], 'both');
            $this->db->or_like('u.gender', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(bu.bu_name like "%'.$data['search'].'%"
            or u.first_name like "%'.$data['search'].'%"
            or u.last_name like "%'.$data['search'].'%"
            or u.email like "%'.$data['search'].'%"
            or u.gender like "%'.$data['search'].'%")');*/
        $this->db->group_by('u.id_user');

        if(isset($data['current_user_not']))
            $this->db->where('u.id_user !=',$data['current_user_not']);
        if(isset($data['business_unit_array']) && count($data['business_unit_array'])>0) {
            //$this->db->where_in('bur.business_unit_id', $data['business_unit_array']);
            $this->db->where('(CASE WHEN  u.user_role_id in (5,6) THEN 1 WHEN u.user_role_id not in (5,6) AND bur.business_unit_id in ('.implode(',',$data['business_unit_array']).') THEN 1 END)=1');
        }
        if(isset($data['user_role_not']))
            $this->db->where_not_in('u.user_role_id',$data['user_role_not']);

        /* results count start */
        $all_clients_db = $this->db->get();
        $all_clients_count = count($all_clients_db->result_array());
        /* results count end */

        $this->db->where_not_in('u.user_role_id',array(1,2));

        $this->db->select('u.id_user,ur.user_role_name,CONCAT(u.first_name," ",u.last_name) as name,u.email,u.gender,u.user_status,u.last_logged_on,group_concat(bu.id_business_unit) as business_unit_id,group_concat(bu.bu_name) as bu_name,u.is_blocked');
        $this->db->from('user u');
        $this->db->join('user_role ur','u.user_role_id=ur.id_user_role and ur.role_status=1','left');
        $this->db->join('business_unit_user bur','bur.user_id=u.id_user and status=1','left');
        $this->db->join('business_unit bu','bu.id_business_unit=bur.business_unit_id','left');
        if(isset($data['customer_id']))
            $this->db->where('u.customer_id',$data['customer_id']);
        if(isset($data['business_unit_id']))
            $this->db->where('bur.business_unit_id',$data['business_unit_id']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('bu.bu_name', $data['search'], 'both');
            $this->db->or_like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.last_name', $data['search'], 'both');
            $this->db->or_like('u.email', $data['search'], 'both');
            $this->db->or_like('u.gender', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(bu.bu_name like "%'.$data['search'].'%"
            or u.first_name like "%'.$data['search'].'%"
            or u.last_name like "%'.$data['search'].'%"
            or u.email like "%'.$data['search'].'%"
            or u.gender like "%'.$data['search'].'%")');*/
        $this->db->group_by('u.id_user');

        if(isset($data['business_unit_array']) && count($data['business_unit_array'])>0) {
            //$this->db->where_in('bur.business_unit_id', $data['business_unit_array']);
            $this->db->where('(CASE WHEN  u.user_role_id in (5,6) THEN 1 WHEN u.user_role_id not in (5,6) AND bur.business_unit_id in ('.implode(',',$data['business_unit_array']).') THEN 1 END)=1');
        }
        if(isset($data['user_role_not']))
                    $this->db->where_not_in('u.user_role_id',$data['user_role_not']);

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('u.id_user','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return array('total_records' => $all_clients_count,'data' => $query->result_array());
    }

    public function addCalender($data)
    {
        $this->db->insert_batch('calender', $data);
        return $this->db->insert_id();
    }

    public function updateCalenderByCategory($data)
    {
        $this->db->where('relationship_category_id',$data['relationship_category_id']);
        $this->db->where('date',$data['date']);
        $this->db->update('calender',$data);
    }

    public function getCalender($data){
        $this->db->select('c.*,rc.relationship_category_name');
        $this->db->from('calender c');
        $this->db->join('relationship_category_language rc','c.relationship_category_id = rc.relationship_category_id','');
        if(isset($data['customer_id']))
            $this->db->where('c.customer_id',$data['customer_id']);
        if(isset($data['month']))
            $this->db->where('month(c.date)',$data['month']);
        if(isset($data['date']))
            $this->db->where('date',$data['date']);
        if(isset($data['status']))
                    $this->db->where('status',$data['status']);

        return $this->db->get()->result_array();

    }

    public function getYearCalender($data){
        //previous: $this->db->select('distinct(rc.relationship_category_name),DATE_FORMAT(c.date,"%M") as month,DATE_FORMAT(c.date,"%m") as month_id');
        $this->db->select('rc.relationship_category_id,rc.relationship_category_name,DATE_FORMAT(c.date,"%M") as month,IF(DATE_FORMAT(c.date, "%m")<10,REPLACE(DATE_FORMAT(c.date, "%m"),0,\'\'),DATE_FORMAT(c.date, "%m")) as month_id,count(c.id_calender) as relationship_count, r.relationship_category_quadrant');
        $this->db->from('calender c');
        $this->db->join('relationship_category r','r.id_relationship_category=c.relationship_category_id','');
        $this->db->join('relationship_category_language rc','c.relationship_category_id = rc.relationship_category_id','');
        if(isset($data['customer_id']))
            $this->db->where('c.customer_id',$data['customer_id']);
        if(isset($data['year']))
            $this->db->where('year(c.date)',$data['year']);
        if(isset($data['status']))
            $this->db->where('status',$data['status']);
        $this->db->group_by('month_id,c.relationship_category_id');
        $result = $this->db->get();
        //echo $this->db->last_query();exit;
        return $result->result_array();
    }

    public function checkAlreadyExist($data){
        /*
         * SELECT c.*,rcr.days,DATE_SUB(c.date,INTERVAL rcr.days DAY),rcl.relationship_category_name FROM calender c join relationship_category_remainder rcr on rcr.relationship_category_id=c.relationship_category_id join relationship_category_language rcl on rcl.relationship_category_id=c.relationship_category_id and rcl.language_id=1 where c.relationship_category_id=110 and '2017-04-04' BETWEEN DATE_SUB(c.date,INTERVAL rcr.days DAY) and c.date and c.status=1;
         */
        $this->db->select('c.*,rcr.days,DATE_SUB(c.date,INTERVAL rcr.days DAY),rcl.relationship_category_name');
        $this->db->from('calender c');
        $this->db->join('relationship_category_remainder rcr','rcr.relationship_category_id=c.relationship_category_id','');
        $this->db->join('relationship_category_language rcl','rcl.relationship_category_id=c.relationship_category_id and rcl.language_id=1','');
        $this->db->where('c.relationship_category_id',$data['relationship_category_id']);
        $this->db->group_start();
        $this->db->where('"'.$data['date'].'" between DATE_SUB(c.date,INTERVAL rcr.days DAY) and c.date and c.status=1');
        $this->db->or_where('"'.$data['date'].'" between c.date and DATE_ADD(c.date,INTERVAL rcr.days DAY) and c.status=1');
        $this->db->group_end();
        $query=$this->db->get();
        /*echo $this->db->last_query(); exit;*/
        return $query->result_array();
    }

    public function updateCalender($data)
    {
        if(isset($data['id_calender']))
            $this->db->where('id_calender', $data['id_calender']);
        if(isset($data['date']))
            $this->db->where('date', $data['date']);
        $this->db->update('calender', $data);
        return 1;
    }

    public function addRelationshipRemainder($data)
    {
        $this->db->insert_batch('relationship_category_remainder', $data);
        return 1;
    }

    public function updateRelationshipRemainder($data)
    {
        $this->db->update_batch('relationship_category_remainder', $data, 'id_relationship_category_remainder');
        return 1;

    }

    public function getRelationshipCategoryRemainder($data)
    {
        $this->db->select('r.id_relationship_category,rl.relationship_category_name,rr.*');
        $this->db->from('relationship_category r');
        $this->db->join('relationship_category_language rl','r.id_relationship_category=rl.relationship_category_id and language_id=1','left');
        $this->db->join('relationship_category_remainder rr','rr.relationship_category_id = r.id_relationship_category and rr.customer_id='.$this->db->escape($data['customer_id']),'left');
        if(isset($data['customer_id']))
            $this->db->where('r.customer_id',$data['customer_id']);
        return $this->db->get()->result_array();
    }

    public function getUserCount($data)
    {
        $this->db->select('count(distinct u.id_user) as total_records');
        $this->db->from('user u');
        $this->db->join('business_unit_user bur','bur.user_id=u.id_user and status=1','left');
        $this->db->join('business_unit bu','bu.id_business_unit=bur.business_unit_id','left');
        if(isset($data['customer_id']))
            $this->db->where('u.customer_id',$data['customer_id']);
        if(isset($data['user_role_id_not']) & count($data['user_role_id_not'])>0)
            $this->db->where_not_in('u.user_role_id', $data['user_role_id_not']);
        if(isset($data['business_unit_id']))
            $this->db->where('bur.business_unit_id',$data['business_unit_id']);
        if(isset($data['business_unit_array']) && count($data['business_unit_array'])>0)
            $this->db->where_in('bur.business_unit_id',$data['business_unit_array']);
        $this->db->where_not_in('u.user_role_id', array(5,6));
        $result1 = $this->db->get()->result_array();

        $this->db->select('count(distinct u.id_user) as total_records');
        $this->db->from('user u');
        if(isset($data['customer_id']))
            $this->db->where('u.customer_id',$data['customer_id']);
        if(isset($data['user_role_id_not']) & count($data['user_role_id_not'])>0)
            $this->db->where_not_in('u.user_role_id', $data['user_role_id_not']);
        $this->db->where_in('u.user_role_id', array(5,6));
        $result2 = $this->db->get()->result_array();

        return $result1[0]['total_records']+$result2[0]['total_records'];
    }
    public function EmailTemplateList($data)
    {
        $this->db->select('*');
        $this->db->from('email_template e');
        $this->db->join('email_template_language el','e.id_email_template=el.email_template_id','left');
        if(isset($data['language_id']))
            $this->db->where('el.language_id',$data['language_id']);
        if(isset($data['customer_id']))
            $this->db->where('e.customer_id',$data['customer_id']);
        if(isset($data['module_key']))
            $this->db->where('e.module_key',$data['module_key']);
        if(isset($data['parent_email_template_id']))
            $this->db->where('e.parent_email_template_id',$data['parent_email_template_id']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('l.relationship_category_name', $data['search'], 'both');
            $this->db->or_like('r.relationship_category_quadrant', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(l.relationship_category_name like "%'.$data['search'].'%"
        or r.relationship_category_quadrant like "%'.$data['search'].'%")');*/
        if(isset($data['status']))
            $this->db->where_in('e.status',explode(',',$data['status']));
        else
            $this->db->where('e.status',1);
        /* results count start */
        $all_clients_db = clone $this->db;
        $all_clients_count = $all_clients_db->count_all_results();
        /* results count end */

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('e.id_email_template','ASC');
        $query = $this->db->get();
        if(isset($data['customer_id']) && $data['customer_id']>0 && isset($data['module_key']) && $data['module_key']!='' && $all_clients_count<=0){
            $data['customer_id']=0;
            return $this->EmailTemplateList($data);
        }
        $final_result=$query->result_array();
        /*foreach($final_result as $k=>$v){
            $final_result[$k]['template_content']=EMAIL_HEADER_CONTENT.$v['template_content'].EMAIL_FOOTER_CONTENT;
        }*/
        return array('total_records' => $all_clients_count,'data' => $final_result);
    }
    public function addEmailTemplate($data)
    {
        $this->db->insert('email_template', $data);
        return $this->db->insert_id();
    }

    public function addEmailTemplateLanguage($data)
    {
        $this->db->insert('email_template_language', $data);
        return $this->db->insert_id();
    }

    public function addMailer($data)
    {
        $this->db->insert('mailer', $data);
        return $this->db->insert_id();
    }

    public function getMailer($data=array())
    {
        $this->db->select('*');
        $this->db->from('mailer m');
        $this->db->where('m.is_cron',1);
        if(isset($data['limit']))
            $this->db->limit($data['limit']);
        $this->db->where('m.cron_status',0);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateMailer($data)
    {
        if(isset($data['mailer_id'])) {
            $this->db->where('mailer_id', $data['mailer_id']);
            $this->db->update('mailer', $data);
            return 1;
        }
    }

    public function getDailyUpdatesData($data){
        $this->db->select('id_daily_update_customer,customer_id');
        $this->db->from('daily_update_customer');
        $this->db->where('date',$data['date']);
        $this->db->where('status',$data['status']);

        $result = $this->db->get();
        return $result->result_array();
        //print_r($result->result_array());exit;
        //echo $this->db->last_query();
    }

    public function getDailyUpdates($data){
        $this->db->select('*');
        $this->db->from('daily_update_customer');
        $this->db->where('customer_id',$data['customer_id']);
        $this->db->where('DATE(created_on)',$data['date']);
        //$this->db->where('date <=',$data['to_date']);


        $result = $this->db->get();
        return $result->result_array();
        //print_r($result->result_array());
        //echo $this->db->last_query();exit;
    }

    public function addDailyUpdatesData($data){
        $this->db->insert('daily_update_customer',$data);
        return 1;
    }
    public function updateDailyMail($data,$condition){
        $this->db->where('id_daily_update_customer',$condition['id_daily_update_customer']);
        $this->db->update('daily_update_customer',$data);
        //echo $this->db->last_query();
        return 1;
    }
    public function getCustomerUserListHistory($data)
    {
        /*user role not in (1-with admin),(2-customer admin)*/
        //$this->db->where_not_in('u.user_role_id',array(1,2));
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/
        $this->db->select('u.id_user,ur.user_role_name,CONCAT(u.first_name," ",u.last_name) as name,u.email,u.gender,u.user_status,u.last_logged_on');
        $this->db->from('user u');
        $this->db->join('user_role ur','u.user_role_id=ur.id_user_role and ur.role_status=1','left');
        $this->db->join('business_unit_user bur','bur.user_id=u.id_user and status=1','left');
        $this->db->join('business_unit bu','bu.id_business_unit=bur.business_unit_id','left');
        if(isset($data['customer_id']))
            $this->db->where('u.customer_id',$data['customer_id']);
        if(isset($data['business_unit_id']))
            $this->db->where('bur.business_unit_id',$data['business_unit_id']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('bu.bu_name', $data['search'], 'both');
            $this->db->or_like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.last_name', $data['search'], 'both');
            $this->db->or_like('u.email', $data['search'], 'both');
            $this->db->or_like('u.gender', $data['search'], 'both');
            $this->db->or_like('ur.user_role_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(bu.bu_name like "%'.$data['search'].'%"
            or u.first_name like "%'.$data['search'].'%"
            or u.last_name like "%'.$data['search'].'%"
            or u.email like "%'.$data['search'].'%"
            or u.gender like "%'.$data['search'].'%"
            or ur.user_role_name like "%'.$data['search'].'%")');*/
        $this->db->group_by('u.id_user');

        if(isset($data['current_user_not']))
            $this->db->where('u.id_user !=',$data['current_user_not']);
        if(isset($data['business_unit_array']) && count($data['business_unit_array'])>0) {
            //$this->db->where_in('bur.business_unit_id', $data['business_unit_array']);
            $this->db->where('(CASE WHEN  u.user_role_id in (5,6) THEN 1 WHEN u.user_role_id not in (5,6) AND bur.business_unit_id in ('.implode(',',$data['business_unit_array']).') THEN 1 END)=1');
        }
        if(isset($data['user_role_not']))
            $this->db->where_not_in('u.user_role_id',$data['user_role_not']);

        /* results count start */
        $all_clients_db = $this->db->get();
        $all_clients_count = count($all_clients_db->result_array());
        /* results count end */

        //$this->db->where_not_in('u.user_role_id',array(1,2));

        $this->db->select('u.id_user,ur.user_role_name,CONCAT(u.first_name," ",u.last_name) as name,u.email,u.gender,u.user_status,u.last_logged_on,group_concat(bu.id_business_unit) as business_unit_id,group_concat(bu.bu_name) as bu_name');
        $this->db->from('user u');
        $this->db->join('user_role ur','u.user_role_id=ur.id_user_role and ur.role_status=1','left');
        $this->db->join('business_unit_user bur','bur.user_id=u.id_user and status=1','left');
        $this->db->join('business_unit bu','bu.id_business_unit=bur.business_unit_id','left');
        if(isset($data['customer_id']))
            $this->db->where('u.customer_id',$data['customer_id']);
        if(isset($data['business_unit_id']))
            $this->db->where('bur.business_unit_id',$data['business_unit_id']);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('bu.bu_name', $data['search'], 'both');
            $this->db->or_like('u.first_name', $data['search'], 'both');
            $this->db->or_like('u.last_name', $data['search'], 'both');
            $this->db->or_like('u.email', $data['search'], 'both');
            $this->db->or_like('u.gender', $data['search'], 'both');
            $this->db->or_like('ur.user_role_name', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(bu.bu_name like "%'.$data['search'].'%"
            or u.first_name like "%'.$data['search'].'%"
            or u.last_name like "%'.$data['search'].'%" or
             u.email like "%'.$data['search'].'%" or
             u.gender like "%'.$data['search'].'%" or
             ur.user_role_name like "%'.$data['search'].'%")');*/
        $this->db->group_by('u.id_user');

        if(isset($data['business_unit_array']) && count($data['business_unit_array'])>0) {
            //$this->db->where_in('bur.business_unit_id', $data['business_unit_array']);
            $this->db->where('(CASE WHEN  u.user_role_id in (5,6) THEN 1 WHEN u.user_role_id not in (5,6) AND bur.business_unit_id in ('.implode(',',$data['business_unit_array']).') THEN 1 END)=1');
        }
        if(isset($data['user_role_not']))
            $this->db->where_not_in('u.user_role_id',$data['user_role_not']);

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('u.id_user','ASC');
        $query = $this->db->get();
        $final_result=$query->result_array();
        foreach($final_result as $k=>$v){
            $last_login=$this->getUserLastLogin(array('id_user'=>$v['id_user']));
            $final_result[$k]['last_logged_on']=isset($last_login[0]['created_at'])?$last_login[0]['created_at']:NULL;
        }
        //echo $this->db->last_query(); exit;
        return array('total_records' => $all_clients_count,'data' => $final_result);
    }
    public function getUserLoginHistory($data){
        /*select u.id_user,CONCAT(u.first_name,' ',u.last_name) user_name,(oat.access_token),SEC_TO_TIME((TIME_TO_SEC(TIMEDIFF(oat.updated_at,oat.created_at)))) as time_spent,os.client_browser,os.client_remote_address,oat.created_at as login_date,oat.updated_at as logout_date,(select count(id_access_log) from access_log where access_token=oat.access_token) as actions
FROM oauth_access_tokens oat
LEFT JOIN oauth_sessions os on oat.session_id = os.id
LEFT JOIN  oauth_clients oc on os.client_id = oc.id
LEFT JOIN  user u on oc.user_id = u.id_user
where  oat.created_at BETWEEN '2017-05-12' AND '2017-05-16' and u.id_user=69 ORDER BY oat.id asc;*/

        if($data['type']=='detail') {
            $data['to_date']=date('Y-m-d',strtotime($data['to_date'] .' +1 day'));
            $this->db->select('u.id_user,CONCAT(u.first_name,\' \',u.last_name) user_name,(oat.access_token),SEC_TO_TIME((TIME_TO_SEC(TIMEDIFF(oat.updated_at,oat.created_at)))) as time_spent,os.client_browser,os.client_remote_address,oat.created_at as login_date,oat.updated_at as logout_date,(select count(id_access_log) from access_log where access_token=oat.access_token) as actions_count');
            $this->db->from('oauth_access_tokens oat');
            $this->db->join('oauth_sessions os', 'oat.session_id = os.id', 'LEFT');
            $this->db->join('oauth_clients oc', 'os.client_id = oc.id', 'LEFT');
            $this->db->join('user u', 'oc.user_id = u.id_user', 'LEFT');
            $this->db->where('oat.created_at BETWEEN \'' . $data['from_date'] . '\' AND \'' . $data['to_date'] . '\'');
            $this->db->where('u.id_user', $data['id_user']);
            //$this->db->order_by('oat.id', 'asc');
            $all_clients_db = clone $this->db;
            $all_clients_count = $all_clients_db->count_all_results();
            /* results count end */

            if (isset($data['pagination']['number']) && $data['pagination']['number'] != '')
                $this->db->limit($data['pagination']['number'], $data['pagination']['start']);
            if (isset($data['sort']['predicate']) && $data['sort']['predicate'] != '' && isset($data['sort']['reverse']))
                $this->db->order_by($data['sort']['predicate'], $data['sort']['reverse']);
            else
                $this->db->order_by('oat.id', 'DESC');
        }
        else{
            //SUM((select count(id_access_log) from access_log where access_token=oat.access_token)) as actions_count,
            $this->db->select('u.id_user,CONCAT(u.first_name,\' \',u.last_name) user_name,count(oat.access_token) logins_count,SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(oat.updated_at,oat.created_at)))) as time_spent,ur.user_role_name,u.email');
            $this->db->from('oauth_access_tokens oat');
            $this->db->join('oauth_sessions os', 'oat.session_id = os.id', 'LEFT');
            $this->db->join('oauth_clients oc', 'os.client_id = oc.id', 'LEFT');
            $this->db->join('user u', 'oc.user_id = u.id_user', 'LEFT');
            $this->db->join('user_role ur', 'ur.id_user_role=u.user_role_id', 'LEFT');
            $this->db->where('u.id_user', $data['id_user']);
            //$this->db->order_by('oat.id', 'asc');
            $all_clients_db = clone $this->db;
            $all_clients_count = 1;
            /* results count end */

            if (isset($data['pagination']['number']) && $data['pagination']['number'] != '')
                $this->db->limit($data['pagination']['number'], $data['pagination']['start']);
            if (isset($data['sort']['predicate']) && $data['sort']['predicate'] != '' && isset($data['sort']['reverse']))
                $this->db->order_by($data['sort']['predicate'], $data['sort']['reverse']);
            else
                $this->db->order_by('oat.id', 'ASC');
        }
        $query = $this->db->get();
        $final_result=$query->result_array();
        foreach($final_result as $k=>$v){
            $last_login=$this->getUserLastLogin($data);
            $final_result[$k]['last_logged_on']=isset($last_login[0]['created_at'])?$last_login[0]['created_at']:NULL;
            //$final_result[$k]['last_logged_on']=currentDate();
        }
        /*foreach($final_result as $k=>$v){
            $final_result[$k]['template_content']=EMAIL_HEADER_CONTENT.$v['template_content'].EMAIL_FOOTER_CONTENT;
        }*/
        return array('total_records' => $all_clients_count,'data' => $final_result);

    }
    public function getUserLastLogin($data){
        /*select oat.created_at
FROM oauth_access_tokens oat
LEFT JOIN oauth_sessions os on oat.session_id = os.id
LEFT JOIN  oauth_clients oc on os.client_id = oc.id
LEFT JOIN  user u on oc.user_id = u.id_user
where  u.id_user=69 ORDER BY oat.id desc limit 1;*/

        $this->db->select('oat.created_at');
        $this->db->from('oauth_access_tokens oat');
        $this->db->join('oauth_sessions os', 'oat.session_id = os.id', 'LEFT');
        $this->db->join('oauth_clients oc', 'os.client_id = oc.id', 'LEFT');
        $this->db->join('user u', 'oc.user_id = u.id_user', 'LEFT');
        $this->db->where('u.id_user', $data['id_user']);
        $this->db->order_by('oat.id', 'desc');
        $this->db->limit('1');
        $query = $this->db->get();
        $final_result=$query->result_array();
        return $final_result;


    }

    public function getproviderlist($data){
        /*if(isset($data['search']))
            $data['search']=$this->db->escape($data['search']);*/
        $this->db->select('*');
        $this->db->from('provider');
        $this->db->where('customer_id',$data['customer_id']);
        if(isset($data['status']))
            $this->db->where('status',$data['status']);
        else
            $this->db->where('status !=',2);
        if(isset($data['search'])){
            $this->db->group_start();
            $this->db->like('provider_name', $data['search'], 'both');
            $this->db->or_like('email', $data['search'], 'both');
            $this->db->or_like('address', $data['search'], 'both');
            $this->db->or_like('contact_no', $data['search'], 'both');
            $this->db->group_end();
        }
        /*if(isset($data['search']))
            $this->db->where('(provider_name like "%'.$data['search'].'%"
            or email like "%'.$data['search'].'%"
            or address like"%'.$data['search'].'%"
            or contact_no like "%'.$data['search'].'%")');*/
        $count = clone $this->db;
        $rec_count = $count->count_all_results();

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('id_provider','DESC');

        $result = $this->db->get()->result_array();
        //echo $this->db->last_query(); print_r($data);
        return array('total_count'=>$rec_count,'data'=>$result);


    }

    public function addprovider($data){
        $this->db->insert('provider',$data);
        return $this->db->insert_id();
    }
    public function updateprovider($data,$id){
        $this->db->where('id_provider',$id);
        $this->db->update('provider',$data);
        return 1;
    }
    public function dailynotificationcount($data){
        $this->db->select('m.*');
        $this->db->from('mailer m');
        $this->db->join('email_template t', 'm.email_template_id=t.id_email_template', 'LEFT');
        $this->db->where('t.module_key','CONTRACT_DAILY_UPDATE');
        $this->db->where('m.mail_to_user_id',$data['id_user']);
        if(isset($data['is_opened'])){
            if($data['is_opened']==1)
                $this->db->where('m.is_notification_opened',1); //new
            else if($data['is_opened']==0)
                $this->db->where('m.is_notification_opened',0); //old
        }
        /*$this->db->where('m.is_notification_opened',0);*/
        $result = $this->db->get()->num_rows();
        return $result;
    }
    /*Sam*/
    public function dailyNotificationList($data){
        $this->db->select('m.*,d.content, d.`date`');
        $this->db->from('mailer m');
        $this->db->join('email_template t', 'm.email_template_id=t.id_email_template', 'LEFT');
        $this->db->join('user u', 'm.mail_to_user_id=u.id_user', 'LEFT');
        $this->db->join('daily_update_customer d', "u.customer_id=d.customer_id", 'LEFT');
        $this->db->where('DATE(`d`.`created_on`)=DATE(`m`.`send_date`)');
        $this->db->where('t.module_key','CONTRACT_DAILY_UPDATE');
        $this->db->where('m.mail_to_user_id',$data['id_user']);
        if(isset($data['is_opened'])){
            if($data['is_opened']==1)
                $this->db->where('m.is_notification_opened',1);
            else if($data['is_opened']==0)
                $this->db->where('m.is_notification_opened',0);
        }
        /*$this->db->where('m.is_notification_opened',0);*/

        /* results count start */
        $all_notification_db = clone $this->db;
        $all_notification_count = $all_notification_db->get()->num_rows();
        /* results count end */

        if(isset($data['pagination']['number']) && $data['pagination']['number']!='')
            $this->db->limit($data['pagination']['number'],$data['pagination']['start']);
        if(isset($data['sort']['predicate']) && $data['sort']['predicate']!='' && isset($data['sort']['reverse']))
            $this->db->order_by($data['sort']['predicate'],$data['sort']['reverse']);
        else
            $this->db->order_by('m.send_date','DESC');

        $query = $this->db->get();
        return array('total_records' => $all_notification_count,'data' => $query->result_array());

        /*$result = $this->db->get()->result_array();
        return $result;*/
    }
    public function updatedailynotificationcount($data){

        $query="UPDATE mailer m, email_template t SET m.is_notification_opened = 1
WHERE t.module_key = 'CONTRACT_DAILY_UPDATE'
AND m.mail_to_user_id = ?
AND m.is_notification_opened =0
AND DATE(m.send_date) =?
AND m.email_template_id = t.id_email_template";
        $result = $this->db->query($query,array($data['id_user'],$data['date']));
        return 1;
    }

}