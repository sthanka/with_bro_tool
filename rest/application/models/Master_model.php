<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
        $this->load->model('Mcommon');
    }

    public function getCountryList($data)
    {
        $this->db->select('*');
        $this->db->from('country');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCurrencyList($data)
    {
        $this->db->select('*');
        $this->db->from('currency');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUserRole($data)
    {
        $this->db->select('*');
        $this->db->from('user_role');
        if(isset($data['user_role_id']) && $data['user_role_id']>2)
            $this->db->where_not_in('id_user_role',array(1,2,6));
        else
            $this->db->where_not_in('id_user_role',array(1,2));
        if(isset($data['user_role_id']))
            $this->db->where('id_user_role >',$data['user_role_id']);
        $this->db->where('role_status',1);
        $query = $this->db->get();
        return $query->result_array();
    }
}