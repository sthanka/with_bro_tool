<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/mailer/mailer.php';

class Cron extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $language = 'en';
        if(isset($_SERVER['HTTP_LANG']) && $_SERVER['HTTP_LANG']!=''){
            $language = $_SERVER['HTTP_LANG'];
            if(is_dir('application/language/'.$language)==0){
                $language = $this->config->item('rest_language');
            }
        }
        $this->lang->load('rest_controller', $language);
    }
    // send email through cron
    public function sendemails()
    {
        $limit=30;
        $mailer_data = $this->Customer_model->getMailer(array('limit'=>$limit));
        $this->load->library('sendgridlibrary');
        foreach($mailer_data as $k=>$v){
            if($v['cron_status']==0 && $v['is_cron']==1){
                $this->Customer_model->updateMailer(array('cron_status'=>1,'mailer_id'=>$v['mailer_id']));
                $from_name=$v['mail_from_name'];
                $from=$v['mail_from'];
                $subject=$v['mail_subject'];
                $body=$v['mail_message'];
                $to_name=$v['mail_to_name'];
                $to=$v['mail_to'];
                $mailer_id=$v['mailer_id'];
                $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                if($mail_sent_status==1) {
                    $this->Customer_model->updateMailer(array('status' => 1,'cron_status'=>2,'mailer_id' => $mailer_id));
                }
                else{
                    $this->Customer_model->updateMailer(array('cron_status'=>3,'mailer_id'=>$mailer_id));
                }
            }
        }

        /*$result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        echo json_encode($result);exit;*/
    }
    // change contract status to pending review when review date is approaching (calender)
    public function contractstatuschange(){
        $contracts=$this->Contract_model->getContractsToBeScheduled();
        foreach($contracts as $k=>$v){
            $check_review_schedule1 = $this->Contract_model->checkContractReviewCompletedSchedule(array('contract_id' => $v['id_contract']));
            if(empty($check_review_schedule1)){
                $update_data=array('contract_status'=>'pending review','id_contract' => $v['id_contract'],'reminder_date1'=>$v['reminder_date_1'],'reminder_date2'=>$v['reminder_date_2'],'reminder_date3'=>$v['reminder_date_3']);
                $update_status=$this->Contract_model->updateContract($update_data);
               // echo "<pre>";print_r($update_data);echo "</pre>";
            }
        }
    }
    public function contractreviewreminder($type=''){

        if(!empty($this->input->get('type')) && in_array($this->input->get('type'),array('r1','r2','r3'))){
            $reminder_type=trim($this->input->get('type'));

            $contracts=$this->Contract_model->getContractsToBeScheduled(array('reminder'=>$reminder_type));
            if($reminder_type == 'r1') {
                $module_key = 'CONTRACT_INITIATE_REMINDER1';
                $update_data['reminder_date1']=NULL;
            }
            if($reminder_type == 'r2') {
                $module_key = 'CONTRACT_INITIATE_REMINDER2';
                $update_data['reminder_date2']=NULL;
            }
            if($reminder_type == 'r3') {
                $module_key = 'CONTRACT_INITIATE_REMINDER3';
                $update_data['reminder_date3']=NULL;
            }
            //echo '<pre>';print_r($contracts);exit;
            ///////////////////////////$relationship_info[0]['relationship_category_name']
            $contracts_array = array();
            foreach($contracts as $k => $v){
                $customer_id[]=$v['customer_id'];
                $cust_admin_list = $this->Customer_model->getCustomerAdminList(array('customer_id' => $v['customer_id']));
                $relationship_info = $this->Relationship_category_model->getRelationshipCategory(array('id_relationship_category'=>$v['relationship_category_id']));
                $delegate_info = $this->User_model->getUserInfo(array('user_id' => $v['delegate_id']));

                foreach($cust_admin_list['data'] as $ka => $va){
                    $contracts_array['customer_admin'][$va['id_user']]['contracts'][] = array('contract_name'=>$v['contract_name'],'relationship_category_name'=>$relationship_info[0]['relationship_category_name'],'last_date_of_initiate'=>$v['date'],'customer_id'=>$v['customer_id']);
                }
                if(isset($delegate_info)){
                    $contracts_array['delegate'][$v['delegate_id']]['contracts'][] = array('delegate_id'=>$v['delegate_id'],'contract_id'=>$v['id_contract'],'contract_name'=>$v['contract_name'],'relationship_category_name'=>$relationship_info[0]['relationship_category_name'],'last_date_of_initiate'=>$v['date'],'customer_id'=>$v['customer_id']);
                }
                $contracts_array['bu_owner'][$v['contract_owner_id']]['contracts'][] = array('contract_owner_id'=>$v['contract_owner_id'],'contract_id'=>$v['id_contract'],'contract_name'=>$v['contract_name'],'relationship_category_name'=>$relationship_info[0]['relationship_category_name'],'last_date_of_initiate'=>$v['date'],'customer_id'=>$v['customer_id']);
            }
            //echo '<pre>';print_r($contracts_array);
            $cust_admin_content ='';            $bu_owner_content ='';            $delegate_content ='';
            if(isset($contracts_array['customer_admin'])) {

                foreach ($contracts_array['customer_admin'] as $k => $v) {
                    $cust_admin_content = "<table border='1' style='border-collapse:collapse;font-size:12px;' cellpadding='3' width='100%' >";
                    $cust_admin_content .= "<thead align='left'><th>Contract</th><th>Category</th><th>Review deadline</th></thead>";
                    foreach ($v['contracts'] as $kc => $vc) {
                        $cust_admin_content .= "<tr><td>" . $vc['contract_name'] . "</td><td>" . $vc['relationship_category_name'] . "</td><td>" . $vc['last_date_of_initiate'] . "</td></tr>";
                    }
                    $cust_admin_content .= "</table>";
                    $contracts_array['customer_admin'][$k]['content']=$cust_admin_content;
                }
            }
            //echo '<pre>';print_r($contracts_array['bu_owner']);exit;
            if(isset($contracts_array['bu_owner'])) {
                foreach ($contracts_array['bu_owner'] as $k => $v) {
                    $bu_owner_content = "<table border='1' style='border-collapse:collapse;font-size:12px;' cellpadding='3' width='100%' >";
                    $bu_owner_content .= "<thead align='left'><th>Contract</th><th>Category</th><th>Review deadline</th></thead>";
                    foreach ($v['contracts'] as $kc => $vc) {
                        $bu_owner_content .= "<tr><td>" . $vc['contract_name'] . "</td><td>" . $vc['relationship_category_name'] . "</td><td>" . $vc['last_date_of_initiate'] . "</td></tr>";
                    }
                    $bu_owner_content .= "</table>";
                    $contracts_array['bu_owner'][$k]['content']=$bu_owner_content;
                }
            }
            if(isset($contracts_array['delegate'])) {
                foreach($contracts_array['delegate'] as $k => $v){
                    $delegate_content = "<table border='1' style='border-collapse:collapse;font-size:12px;' cellpadding='3' width='100%' >";
                    $delegate_content .= "<thead align='left'><th>Contract</th><th>Category</th><th>Review deadline</th></thead>";
                    foreach($v['contracts'] as $kc => $vc){
                        $delegate_content .= "<tr><td>".$vc['contract_name']."</td><td>".$vc['relationship_category_name']."</td><td>".$vc['last_date_of_initiate']."</td></tr>";
                    }
                    $delegate_content .="</table>";
                    $contracts_array['delegate'][$k]['content']=$delegate_content;
                }
            }
           // echo "<pre>";print_r($contracts_array);echo "</pre>";exit;
            //echo 'ca'.$cust_admin_content.'<br>'.'bu'.$bu_owner_content.'<br>'.'de'.$delegate_content;exit;



                foreach($contracts as $k => $v){
                    $update_data['id_contract']=$v['id_contract'];
                    $update_status=$this->Contract_model->updateContract($update_data);
                }
                if(isset($customer_id)){

                            foreach($contracts_array['customer_admin'] as $kb =>$vb) {

                                $customer_admin_info = $this->User_model->getUserInfo(array('user_id' => $kb));
                                $template_configurations_parent = $this->Customer_model->EmailTemplateList(array('customer_id' => $customer_admin_info->customer_id, 'language_id' => 1, 'module_key' => $module_key));
                                $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $customer_admin_info->customer_id));
                                if ($template_configurations_parent['total_records'] > 0) {
                                    if ($customer_details[0]['company_logo'] == '') {
                                        $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
                                    } else {
                                        $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

                                    }

                                    $template_configurations = $template_configurations_parent['data'][0];
                                    $wildcards = $template_configurations['wildcards'];
                                    $wildcards_replaces = array();
                                    $wildcards_replaces['first_name'] = $customer_admin_info->first_name;
                                    $wildcards_replaces['last_name'] = $customer_admin_info->last_name;
                                    $wildcards_replaces['contracts'] = $vb['content'];
                                    $wildcards_replaces['category'] = $relationship_info[0]['relationship_category_name'];
                                    $wildcards_replaces['logo'] = $customer_logo;
                                    $wildcards_replaces['url'] = WEB_BASE_URL . 'html';
                                    $body = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_content']);
                                    $subject = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_subject']);
                                    /*$from_name=SEND_GRID_FROM_NAME;
                                    $from=SEND_GRID_FROM_EMAIL;
                                    $from_name=$cust_admin['name'];
                                    $from=$cust_admin['email'];*/
                                    $from_name = $template_configurations['email_from_name'];
                                    $from = $template_configurations['email_from'];
                                    $to = $customer_admin_info->email;
                                    $to_name = $customer_admin_info->first_name . ' ' . $customer_admin_info->last_name;
                                    $mailer_data['mail_from_name'] = $from_name;
                                    $mailer_data['mail_to_name'] = $to_name;
                                    $mailer_data['mail_to_user_id'] = $customer_admin_info->id_user;
                                    $mailer_data['mail_from'] = $from;
                                    $mailer_data['mail_to'] = $to;
                                    $mailer_data['mail_subject'] = $subject;
                                    $mailer_data['mail_message'] = $body;
                                    $mailer_data['status'] = 0;
                                    $mailer_data['send_date'] = currentDate();
                                    $mailer_data['is_cron'] = 0;
                                    $mailer_data['email_template_id'] = $template_configurations['id_email_template'];
                                    //print_r($mailer_data);
                                    $mailer_id = $this->Customer_model->addMailer($mailer_data);
                                    //sending mail to bu owner
                                    if ($mailer_data['is_cron'] == 0) {
                                        //$mail_sent_status=sendmail($to, $subject, $body, $from);
                                        $this->load->library('sendgridlibrary');
                                        $mail_sent_status = $this->sendgridlibrary->sendemail($from_name, $from, $subject, $body, $to_name, $to, array(), $mailer_id);
                                        if ($mail_sent_status == 1)
                                            $this->Customer_model->updateMailer(array('status' => 1, 'mailer_id' => $mailer_id));
                                    }


                                }
                            }
                            foreach($contracts_array['bu_owner'] as $kb =>$vb) {

                                $bu_info = $this->User_model->getUserInfo(array('user_id' => $kb));
                                $template_configurations_parent = $this->Customer_model->EmailTemplateList(array('customer_id' => $bu_info->customer_id, 'language_id' => 1, 'module_key' => $module_key));
                                $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $bu_info->customer_id));
                                if ($template_configurations_parent['total_records'] > 0) {
                                    if ($customer_details[0]['company_logo'] == '') {
                                        $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
                                    } else {
                                        $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

                                    }
                                    $template_configurations = $template_configurations_parent['data'][0];
                                    $wildcards = $template_configurations['wildcards'];
                                    $wildcards_replaces = array();
                                    $wildcards_replaces['first_name'] = $bu_info->first_name;
                                    $wildcards_replaces['last_name'] = $bu_info->last_name;
                                    $wildcards_replaces['contracts'] = $vb['content'];
                                    $wildcards_replaces['logo'] = $customer_logo;
                                    $wildcards_replaces['url'] = WEB_BASE_URL . 'html';
                                    $body = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_content']);
                                    $subject = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_subject']);
                                    /*$from_name=SEND_GRID_FROM_NAME;
                                    $from=SEND_GRID_FROM_EMAIL;
                                    $from_name=$cust_admin['name'];
                                    $from=$cust_admin['email'];*/
                                    $from_name = $template_configurations['email_from_name'];
                                    $from = $template_configurations['email_from'];
                                    $to = $bu_info->email;
                                    $to_name = $bu_info->first_name . ' ' . $bu_info->last_name;
                                    $mailer_data['mail_from_name'] = $from_name;
                                    $mailer_data['mail_to_name'] = $to_name;
                                    $mailer_data['mail_to_user_id'] = $bu_info->id_user;
                                    $mailer_data['mail_from'] = $from;
                                    $mailer_data['mail_to'] = $to;
                                    $mailer_data['mail_subject'] = $subject;
                                    $mailer_data['mail_message'] = $body;
                                    $mailer_data['status'] = 0;
                                    $mailer_data['send_date'] = currentDate();
                                    $mailer_data['is_cron'] = 0;
                                    $mailer_data['email_template_id'] = $template_configurations['id_email_template'];
                                    //print_r($mailer_data);
                                    $mailer_id = $this->Customer_model->addMailer($mailer_data);
                                    //sending mail to bu owner
                                    if ($mailer_data['is_cron'] == 0) {
                                        //$mail_sent_status=sendmail($to, $subject, $body, $from);
                                        $this->load->library('sendgridlibrary');
                                        $mail_sent_status = $this->sendgridlibrary->sendemail($from_name, $from, $subject, $body, $to_name, $to, array(), $mailer_id);
                                        if ($mail_sent_status == 1)
                                            $this->Customer_model->updateMailer(array('status' => 1, 'mailer_id' => $mailer_id));
                                    }
                                }
                            }
                            if(isset($contracts_array['delegate'])){
                                foreach($contracts_array['delegate'] as $kd =>$vd) {
                                    $delegate_info = $this->User_model->getUserInfo(array('user_id' => $kd));
                                    $template_configurations_parent = $this->Customer_model->EmailTemplateList(array('customer_id' => $delegate_info->customer_id, 'language_id' => 1, 'module_key' => $module_key));
                                    $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $delegate_info->customer_id));
                                    if ($template_configurations_parent['total_records'] > 0) {
                                        if ($customer_details[0]['company_logo'] == '') {
                                            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
                                        } else {
                                            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

                                        }
                                        $template_configurations = $template_configurations_parent['data'][0];
                                        $wildcards = $template_configurations['wildcards'];
                                        $wildcards_replaces = array();
                                        $wildcards_replaces['first_name'] = $delegate_info->first_name;
                                        $wildcards_replaces['last_name'] = $delegate_info->last_name;
                                        $wildcards_replaces['contracts'] = $vd['content'];
                                        $wildcards_replaces['logo'] = $customer_logo;
                                        $wildcards_replaces['url'] = WEB_BASE_URL . 'html';
                                        $body = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_content']);
                                        $subject = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_subject']);
                                        /*$from_name=SEND_GRID_FROM_NAME;
                                        $from=SEND_GRID_FROM_EMAIL;
                                        $from_name=$cust_admin['name'];
                                        $from=$cust_admin['email'];*/
                                        $from_name = $template_configurations['email_from_name'];
                                        $from = $template_configurations['email_from'];
                                        $to = $delegate_info->email;
                                        $to_name = $delegate_info->first_name . ' ' . $delegate_info->last_name;
                                        $mailer_data['mail_from_name'] = $from_name;
                                        $mailer_data['mail_to_name'] = $to_name;
                                        $mailer_data['mail_to_user_id'] = $delegate_info->id_user;
                                        $mailer_data['mail_from'] = $from;
                                        $mailer_data['mail_to'] = $to;
                                        $mailer_data['mail_subject'] = $subject;
                                        $mailer_data['mail_message'] = $body;
                                        $mailer_data['status'] = 0;
                                        $mailer_data['send_date'] = currentDate();
                                        $mailer_data['is_cron'] = 0;
                                        $mailer_data['email_template_id'] = $template_configurations['id_email_template'];
                                        //print_r($mailer_data);
                                        $mailer_id = $this->Customer_model->addMailer($mailer_data);
                                        //sending mail to bu owner
                                        if ($mailer_data['is_cron'] == 0) {
                                            //$mail_sent_status=sendmail($to, $subject, $body, $from);
                                            $this->load->library('sendgridlibrary');
                                            $mail_sent_status = $this->sendgridlibrary->sendemail($from_name, $from, $subject, $body, $to_name, $to, array(), $mailer_id);
                                            if ($mail_sent_status == 1)
                                                $this->Customer_model->updateMailer(array('status' => 1, 'mailer_id' => $mailer_id));
                                        }
                                    }
                                }
                            }

                        }









            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
            echo json_encode($result);exit;
        }
        else{
            echo 'Bad request';
        }

    }
    // send daily updates
    public function senddailymail(){
        $current_date_main=currentDate();
        $current_date=date('Y-m-d',strtotime($current_date_main .' -1 day'));
        //echo $current_date_main.' '.$current_date;exit;
        $this->Contract_model->getDailyMailData(array('run'=>1,'date'=>$current_date));
        $daily_customers=$this->Customer_model->getDailyUpdatesData(array('status'=>0,'date'=>$current_date));

        foreach($daily_customers as $dk => $dv){
            $this->Customer_model->updateDailyMail(array('status'=>1),array('id_daily_update_customer'=>$dv['id_daily_update_customer']));
            //echo '<pre>';print_r($dv);exit;
            $review_started = $this->Contract_model->getDailyMailData(array('review_started'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $review_updated = $this->Contract_model->getDailyMailData(array('review_updated'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $review_finalized = $this->Contract_model->getDailyMailData(array('review_finalized'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $contributor_add = $this->Contract_model->getDailyMailData(array('contributor_add'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $contributor_remove = $this->Contract_model->getDailyMailData(array('contributor_remove'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $discussion_started = $this->Contract_model->getDailyMailData(array('discussion_started'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $discussion_updated = $this->Contract_model->getDailyMailData(array('discussion_updated'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $discussion_closed = $this->Contract_model->getDailyMailData(array('discussion_closed'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $action_item_created = $this->Contract_model->getDailyMailData(array('action_item_created'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $action_item_updated = $this->Contract_model->getDailyMailData(array('action_item_updated'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $action_item_closed = $this->Contract_model->getDailyMailData(array('action_item_closed'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $report_created = $this->Contract_model->getDailyMailData(array('report_created'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $report_edited = $this->Contract_model->getDailyMailData(array('report_edited'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $report_deleted = $this->Contract_model->getDailyMailData(array('report_deleted'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $changes_in_contract_data = $this->Contract_model->getDailyMailData(array('changes_in_contract'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $changes_in_contract_status_data = $this->Contract_model->getDailyMailData(array('changes_in_contract_status'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $new_contract_data = $this->Contract_model->getDailyMailData(array('new_contract'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $user_create = $this->Contract_model->getDailyMailData(array('user_create'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $user_update = $this->Contract_model->getDailyMailData(array('user_update'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            $user_delete = $this->Contract_model->getDailyMailData(array('user_delete'=>1,'date'=>$current_date,'customer_id'=>$dv['customer_id']));
            //echo '<pre>';print_r($user_create);exit;
            $total_data = array('review_started'=>$review_started,
                'review_updated'=>$review_updated,
                'review_finalized'=>$review_finalized ,
                'contributor_add'=>$contributor_add ,
                'contributor_remove'=>$contributor_remove ,
                'discussion_started'=>$discussion_started ,
                'discussion_updated'=>$discussion_updated ,
                'discussion_closed'=>$discussion_closed ,
                'action_item_created'=>$action_item_created ,
                'action_item_updated'=>$action_item_updated ,
                'action_item_closed'=>$action_item_closed ,
                'report_created'=>$report_created ,
                'report_edited'=>$report_edited ,
                'report_deleted'=>$report_deleted ,
                'changes_contract'=>$changes_in_contract_data,
                'changes_contract_status'=>$changes_in_contract_status_data,
                'new_contract'=>$new_contract_data,
                'user_create'=>$user_create,
                'user_update'=>$user_update,
                'user_delete'=>$user_delete);
            //echo $dv['customer_id'].'<pre>';print_r($total_data);

            $save_content = json_encode($total_data);
            $this->Customer_model->updateDailyMail(array('content'=>$save_content),array('id_daily_update_customer'=>$dv['id_daily_update_customer']));

           if(!empty($review_updated) || !empty($review_finalized) || !empty($contributor_add) || !empty($contributor_remove) || !empty($discussion_started)
               || !empty($discussion_updated) || !empty($discussion_closed) || !empty($action_item_created) || !empty($action_item_updated)
               || !empty($action_item_closed) || !empty($report_created) || !empty($report_edited) || !empty($report_deleted) || !empty($changes_in_contract_data)
               || !empty($new_contract_data) || !empty($user_create) || !empty($user_update) || !empty($user_delete))
           {
               $cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id'=>$dv['customer_id']));
           }


            if(isset($cust_admin['data']))
            {
                $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $dv['customer_id']));
                if($customer_details[0]['company_logo']=='') {
                    $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
                }
                else{
                    $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);
                }
                //////////making table of status change
                $status_change='';
                $user_change='';
                if(isset($review_started)){
                    $status_change= "<table border='1' style='border-collapse:collapse;font-size:9px;' cellpadding='3'width='100%' ><thead ><th colspan='7' style='text-align:center'>CONTRACTS</th></thead>";
                    $status_change.="<th>User (first name + last name)</th><th>Date + time</th><th>Business Unit</th><th>Provider</th><th>Contract</th><th>Action</th>";
                    foreach ($review_started as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Review started'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($review_updated)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($review_updated as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Review updated'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($review_finalized)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($review_finalized as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Review finalized'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($contributor_add)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($contributor_add as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Contributor added'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($contributor_remove)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($contributor_remove as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Conntributor removee'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($discussion_started)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($discussion_started as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Discussion started'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($discussion_updated)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($discussion_updated as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Discussion updated'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($discussion_closed)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($discussion_closed as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Discussion closed'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($action_item_created)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($action_item_created as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Action item created'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($action_item_updated)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($action_item_updated as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Action item updated'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($action_item_closed)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($action_item_closed as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Action item closed'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($report_created)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($report_created as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".'---'."</td>"."<td>".'---'."</td>"."<td>".'---'."</td>"."<td>".'Report created'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($report_edited)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($report_edited as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".'---'."</td>"."<td>".'---'."</td>"."<td>".'---'."</td>"."<td>".'Report edited'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($report_deleted)){
                    //$status_change= "<table border='1' >";
                    //$status_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($report_deleted as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".'---'."</td>"."<td>".'---'."</td>"."<td>".'---'."</td>"."<td>".'report_deleted'."</td>";;
                        $status_change.="</tr>";
                    }
                    //$status_change.="</table>";
                }
                if(isset($changes_in_contract_data)){
                    //$contract_change= "<table border='1' >";
                    //$contract_change.="<thead style='text-align:center'><th>by Whom</th><th>When</th><th>Provider</th><th>Contract</th><th>Business Unit</th><th>Action</th></thead>";
                    foreach ($changes_in_contract_data as $row) {
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Contract updated'."</td>";
                        $status_change.="</tr>";
                    }
                    //$contract_change.= "</table>";
                }
                if(isset($new_contract_data)){
                    foreach ($new_contract_data as $row) {
                        unset($row['customer_id']);
                        $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['date'])), new DateTimeZone('UTC'));
                        $date->setTimeZone(new DateTimeZone('CET'));
                        $date_cet= $date->format('Y-m-d H:i:s');
                        $status_change.="<tr>";
                        $status_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['bu_name']."</td>"."<td>".$row['provider_name']."</td>"."<td>".$row['contract_name']."</td>"."<td>".'Contract created'."</td>";;
                        $status_change.="</tr>";
                    }
                    $status_change.= "</table><br>";
                }

                if(!empty($user_create) || !empty($user_update) || !empty($user_delete)){
                    if(isset($user_create)){
                        $user_change.= "<table border='1' style='border-collapse:collapse;font-size:9px;' cellpadding='3'width='100%' ><thead ><th colspan='6' style='text-align:center'>USERS</th></thead>";
                        $user_change.="<th>User (first name + last name)</th><th>Date + time</th><th>Business Unit</th><th>User role</th><th>Action</th>";
                        foreach ($user_create as $row) {
                            unset($row['customer_id']);
                            $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['created_on'])), new DateTimeZone('UTC'));
                            $date->setTimeZone(new DateTimeZone('CET'));
                            $date_cet= $date->format('Y-m-d H:i:s');
                            $user_change.="<tr>";
                            $user_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['business_unit']."</td>"."<td>".$row['user_role_name']."</td>"."<td>".$row['action']."</td>";;
                            $user_change.="</tr>";
                        }

                    }
                    if(isset($user_update)){
                        foreach ($user_update as $row) {
                            unset($row['customer_id']);
                            $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['created_on'])), new DateTimeZone('UTC'));
                            $date->setTimeZone(new DateTimeZone('CET'));
                            $date_cet= $date->format('Y-m-d H:i:s');
                            $user_change.="<tr>";
                            $user_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['business_unit']."</td>"."<td>".$row['user_role_name']."</td>"."<td>".$row['action']."</td>";;
                            $user_change.="</tr>";
                        }

                    }
                    if(isset($user_delete)){
                        foreach ($user_delete as $row) {
                            unset($row['customer_id']);
                            $date = new DateTime(date('Y-m-d H:i:s',strtotime($row['created_on'])), new DateTimeZone('UTC'));
                            $date->setTimeZone(new DateTimeZone('CET'));
                            $date_cet= $date->format('Y-m-d H:i:s');
                            $user_change.="<tr>";
                            $user_change.="<td>".$row['name']."</td>"."<td>".$date_cet." CET"."</td>"."<td>".$row['business_unit']."</td>"."<td>".$row['user_role_name']."</td>"."<td>".$row['action']."</td>";;
                            $user_change.="</tr>";
                        }
                        $user_change.= "</table>";
                    }
                }

                //echo $dv['customer_id'].'Contract<br>'.$status_change.'<br/>User<br>'.print_r($user_change);

                $template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $dv['customer_id'],'language_id' =>1,'module_key'=>'CONTRACT_DAILY_UPDATE'));


                foreach($cust_admin['data']as $k2=>$v2){
                    $template_configurations=$template_configurations_parent;
                    if($template_configurations['total_records']>0){
                        $template_configurations=$template_configurations['data'][0];
                        $wildcards=$template_configurations['wildcards'];
                        $wildcards_replaces=array();
                        $wildcards_replaces['first_name']=$v2['first_name'];
                        $wildcards_replaces['last_name']=$v2['last_name'];
                        $wildcards_replaces['daily_update_date']=$current_date;
                        $wildcards_replaces['logo']=$customer_logo;
                        $wildcards_replaces['contract_change_log']=isset($status_change)?$status_change:'';
                        $wildcards_replaces['user_log']=isset($user_change)?$user_change:'';
                        $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                        $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                        /*$from_name=SEND_GRID_FROM_NAME;
                        $from=SEND_GRID_FROM_EMAIL;
                        $from_name=$cust_admin['name'];
                        $from=$cust_admin['email'];*/
                        $from_name=$template_configurations['email_from_name'];
                        $from=$template_configurations['email_from'];
                        $to=$v2['email'];
                        $to_name=$v2['first_name'].' '.$v2['last_name'];
                        $mailer_data['mail_from_name']=$from_name;
                        $mailer_data['mail_to_name']=$to_name;
                        $mailer_data['mail_to_user_id']=$v2['id_user'];
                        $mailer_data['mail_from']=$from;
                        $mailer_data['mail_to']=$to;
                        $mailer_data['mail_subject']=$subject;
                        $mailer_data['mail_message']=$body;
                        $mailer_data['status']=0;
                        $mailer_data['send_date']=currentDate();
                        $mailer_data['is_cron']=0;
                        $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                        $mailer_id=$this->Customer_model->addMailer($mailer_data);
                        if($mailer_data['is_cron']==0) {
                            //$mail_sent_status=sendmail($to, $subject, $body, $from);
                            $this->load->library('sendgridlibrary');
                            $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                            if($mail_sent_status==1){
                                $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                                $this->Customer_model->updateDailyMail(array('status'=>2),array('id_daily_update_customer'=>$dv['id_daily_update_customer']));
                            }
                            else{
                                $this->Customer_model->updateDailyMail(array('status'=>3),array('id_daily_update_customer'=>$dv['id_daily_update_customer']));

                            }

                        }
                    }
                }
            }
            else{
                $this->Customer_model->updateDailyMail(array('status'=>2),array('id_daily_update_customer'=>$dv['id_daily_update_customer']));
            }
            unset($cust_admin);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        echo json_encode($result);exit;

    }
    // to delete the hard copy of files from the file system for soft deleted attachmnets
    public function removedownloads(){
        $delete_file_after_hours=2;//delete files after 2 hours
        $dir = FCPATH."downloads/*";
        $path = FCPATH."downloads/";
        $date1 = new DateTime(null);
        $files_to_be_deleted=array();
        foreach(glob($dir) as $file)
        {
            if(!is_dir($file)) {
                $loop_file='';
                $loop_file=$path.basename($file);
                $date2=new DateTime(date("Y-m-d H:i:s",filemtime($path.basename($file))));
                $diff = $date2->diff($date1);
                $hours = $diff->h;
                $hours = $hours + ($diff->days*24);
                if($hours>$delete_file_after_hours){
                    $files_to_be_deleted[]=$loop_file;
                }
            }
        }
        $dir = FILE_SYSTEM_PATH."downloads/*";
        $path = FILE_SYSTEM_PATH."downloads/";
        $date1 = new DateTime(null);
        $files_to_be_deleted=array();
        foreach(glob($dir) as $file)
        {
            if(!is_dir($file)) {
                $loop_file='';
                $loop_file=$path.basename($file);
                $date2=new DateTime(date("Y-m-d H:i:s",filemtime($path.basename($file))));
                $diff = $date2->diff($date1);
                $hours = $diff->h;
                $hours = $hours + ($diff->days*24);
                if($hours>$delete_file_after_hours){
                    $files_to_be_deleted[]=$loop_file;
                }
            }
        }
        if(count($files_to_be_deleted)>0){
            foreach($files_to_be_deleted as $k=>$v){
                if(file_exists($v)){
                    unlink($v);
                }
            }
        }
    }

}