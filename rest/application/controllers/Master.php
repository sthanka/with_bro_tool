<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Master extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function countryList_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getCountryList($data);
        foreach($result as $k=>$v)
            $result[$k]['id_country'] = pk_encrypt($v['id_country']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function role_get()
    {
        $data = $this->input->get();
        if(isset($data['user_role_id']))
            $data['user_role_id']=pk_decrypt($data['user_role_id']);
        $result = $this->Master_model->getUserRole($data);
        foreach($result as $k=>$v){
            $result[$k]['id_user_role']=pk_encrypt($result[$k]['id_user_role']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currencyList_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getCurrencyList($data);
        foreach($result as $k=>$v)
            $result[$k]['id_currency'] = pk_encrypt($v['id_currency']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
}