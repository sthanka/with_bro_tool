<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Contract extends REST_Controller
{
    public $session_user_id=NULL;
    public $session_user_info=NULL;
    public $session_user_business_units=NULL;
    public $session_user_business_units_user=NULL;
    public $session_user_contracts=NULL;
    public $session_user_contract_reviews=NULL;
    public $session_user_contract_documents=NULL;
    public $session_user_contract_action_items=NULL;
    public $session_user_delegates=NULL;
    public $session_user_contributors=NULL;
    public $session_user_reporting_owners=NULL;
    public $session_user_bu_owners=NULL;
    public $session_user_customer_admins=NULL;
    public $session_user_customer_all_users=NULL;
    public $session_user_customer_relationship_categories=NULL;
    public $session_user_customer_relationship_classifications=NULL;
    public $session_user_customer_calenders=NULL;
    public $session_user_master_currency=NULL;
    public $session_user_master_language=NULL;
    public $session_user_master_countries=NULL;
    public $session_user_master_templates=NULL;
    public $session_user_master_customers=NULL;
    public $session_user_master_users=NULL;
    public $session_user_master_user_roles=NULL;
    public $session_user_contract_review_modules=NULL;
    public $session_user_master_contract_review_modules=NULL;
    public $session_user_contract_review_topics=NULL;
    public $session_user_master_contract_review_topics=NULL;
    public $session_user_contract_review_questions=NULL;
    public $session_user_contract_review_question_options=NULL;
    public $session_user_wadmin_relationship_categories=NULL;
    public $session_user_wadmin_relationship_classifications=NULL;
    public $session_user_master_contract_review_questions=NULL;
    public $session_user_master_contract_review_question_options=NULL;
    public $session_user_master_template_modules=NULL;
    public $session_user_master_template_module_topics=NULL;
    public $session_user_master_template_module_topic_questions=NULL;
    public $session_user_contract_review_discussions=NULL;
    public $session_user_contract_review_discussion_questions=NULL;
    public $session_user_wadmin_email_templates=NULL;
    public $session_user_customer_email_templates=NULL;
    public $session_user_own_business_units=NULL;
    public $session_user_review_business_units=NULL;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Validation_model');
        //$this->session_user_id=!empty($this->session->userdata('session_user_id_acting'))?($this->session->userdata('session_user_id_acting')):($this->session->userdata('session_user_id'));
        $getLoggedUserId=$this->User_model->getLoggedUserId();
        $this->session_user_id=$getLoggedUserId[0]['id'];
        $this->session_user_info=$this->User_model->getUserInfo(array('user_id'=>$this->session_user_id));
        if($this->session_user_info->user_role_id<3 || $this->session_user_info->user_role_id==5)
            $this->session_user_business_units=$this->Validation_model->getBusinessUnitList(array('customer_id'=>$this->session_user_info->customer_id));
        else if($this->session_user_info->user_role_id==3 || $this->session_user_info->user_role_id==4)
            $this->session_user_business_units=$this->Validation_model->getBusinessUnitListByUser(array('user_id'=>$this->session_user_info->id_user));
        else if($this->session_user_info->user_role_id==6){
            if($this->session_user_info->is_allow_all_bu==1)
                $this->session_user_business_units=$this->Validation_model->getBusinessUnitList(array('customer_id'=>$this->session_user_info->customer_id));
            else
                $this->session_user_business_units=$this->Validation_model->getBusinessUnitListByUser(array('user_id'=>$this->session_user_info->id_user));
        }
        $this->session_user_own_business_units=$this->session_user_business_units;
        $this->session_user_review_business_units=$this->Validation_model->getReviewBusinessUnits(array('id_user'=>$this->session_user_id));
        $this->session_user_business_units=array_merge($this->session_user_business_units,$this->session_user_review_business_units);
        if($this->session_user_info->user_role_id==5)
            $this->session_user_contracts=$this->Validation_model->getContributorContract(array('business_unit_id'=>$this->session_user_business_units,'customer_user'=>$this->session_user_info->id_user));
        else
            $this->session_user_contracts=$this->Validation_model->getContracts(array('business_unit_id'=>$this->session_user_business_units));
        //$this->session_user_contracts=$this->Validation_model->getContracts(array('business_unit_id'=>$this->session_user_business_units_user));
        $assigned_contracts=$this->Validation_model->getContributorContract(array('customer_user'=>$this->session_user_info->id_user));
        $this->session_user_contracts=array_merge($this->session_user_contracts,$assigned_contracts);
        $this->session_user_contract_reviews=$this->Validation_model->getContractReviews(array('contract_id'=>$this->session_user_contracts));
        $review_documents=$this->Validation_model->getContractReviewDocuments(array('contract_review_id'=>$this->session_user_contract_reviews));
        $documents=$this->Validation_model->getContractDocuments(array('contract_id'=>$this->session_user_contracts));
        $this->session_user_contract_documents=array_merge($review_documents,$documents);
        $getContractActionItems=$this->Validation_model->getContractActionItems(array('contract_id'=>$this->session_user_contracts));
        $getContractActionItemsByUser=$this->Validation_model->getContractActionItemsByUser(array('user_id'=>$this->session_user_id));
        $this->session_user_contract_action_items=array_merge($getContractActionItems,$getContractActionItemsByUser);
        $this->session_user_delegates=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id),'user_role_id'=>4));
        $this->session_user_contributors=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id),'user_role_id'=>5));
        $this->session_user_reporting_owners=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id),'user_role_id'=>6));
        $this->session_user_bu_owners=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id),'user_role_id'=>3));
        $this->session_user_customer_admins=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id),'user_role_id'=>2));
        $this->session_user_customer_all_users=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id)));

        $this->session_user_customer_relationship_categories=$this->Validation_model->getCustomerRelationshipCategories(array('customer_id'=>array($this->session_user_info->customer_id)));
        $this->session_user_customer_relationship_classifications=$this->Validation_model->getCustomerRelationshipClassifications(array('customer_id'=>array($this->session_user_info->customer_id)));
        $this->session_user_customer_calenders=$this->Validation_model->getCustomerCalenders(array('customer_id'=>array($this->session_user_info->customer_id)));
        $this->session_user_master_currency=$this->Validation_model->getCurrency();
        $this->session_user_master_language=$this->Validation_model->getLanguage();
        $this->session_user_master_countries=$this->Validation_model->getCountries();
        $this->session_user_master_templates=$this->Validation_model->getTemplates();
        $this->session_user_master_customers=$this->Validation_model->getCustomers();
        $this->session_user_master_users=$this->Validation_model->getUsers();
        $this->session_user_master_user_roles=$this->Validation_model->getUserRoles();
        $this->session_user_contract_review_modules=$this->Validation_model->getContractReviewModules(array('contract_review_id'=>$this->session_user_contract_reviews));
        $this->session_user_master_contract_review_modules=$this->Validation_model->getMasterContractReviewModules();
        $this->session_user_contract_review_topics=$this->Validation_model->getContractReviewTopics(array('module_id'=>$this->session_user_contract_review_modules));
        $this->session_user_master_contract_review_topics=$this->Validation_model->getMasterContractReviewTopics();
        $this->session_user_contract_review_questions=$this->Validation_model->getContractReviewQuestions(array('topic_id'=>$this->session_user_contract_review_topics));
        $this->session_user_master_contract_review_questions=$this->Validation_model->getContractReviewMasterQuestions();
        $this->session_user_contract_review_question_options=$this->Validation_model->getContractReviewQuestionOptions(array('question_id'=>$this->session_user_contract_review_questions));
        $this->session_user_master_contract_review_question_options=$this->Validation_model->getContractReviewMasterQuestionOptions();

        $this->session_user_wadmin_relationship_categories=$this->Validation_model->getCustomerRelationshipCategories(array('customer_id'=>array(0)));
        $this->session_user_wadmin_relationship_classifications=$this->Validation_model->getCustomerRelationshipClassifications(array('customer_id'=>array(0)));

        $this->session_user_master_template_modules=$this->Validation_model->getTemplateModules();
        $this->session_user_master_template_module_topics=$this->Validation_model->getTemplateModuleTopics();
        $this->session_user_master_template_module_topic_questions=$this->Validation_model->getTemplateModuleTopicQuestions();

        $this->session_user_contract_review_discussions=$this->Validation_model->getContractReviewDiscussions(array('contract_review_id'=>$this->session_user_contract_reviews));
        $this->session_user_contract_review_discussion_questions=$this->Validation_model->getContractReviewDiscussionQuestions(array('contract_review_discussion_id'=>$this->session_user_contract_review_discussions));

        $this->session_user_wadmin_email_templates=$this->Validation_model->getCustomerEmailTemplates(array('customer_id'=>array(0)));
        $this->session_user_customer_email_templates=$this->Validation_model->getCustomerEmailTemplates(array('customer_id'=>array($this->session_user_info->customer_id)));

        $this->load->model('Download_model');
        $this->router->fetch_method();
        //echo "<pre>";print_r($this->input->server('REQUEST_METHOD'));echo "</pre>";exit;
    }

    public function list_get()
    {
        $data = $this->input->get();

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($data['customer_id']!=$this->session_user_info->customer_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all') {
            $data['id_business_unit'] = pk_decrypt($data['business_unit_id']);
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['id_business_unit'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['delegate_id'])) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_owner_id'])) {
            $data['contract_owner_id'] = pk_decrypt($data['contract_owner_id']);
            if(!in_array($data['contract_owner_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_user'])) {
            $data['customer_user'] = pk_decrypt($data['customer_user']);
            /*if(!in_array($data['customer_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }*/
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
        }
        if(isset($data['user_role_id']) && isset($data['id_user'])){
            if(in_array($data['user_role_id'],array(3))){
                $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $data['id_user'],'status' => '1'));
                $data['business_unit_id'] = array_map(function($i){ return $i['id_business_unit']; },$business_unit);
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            /*if($data['user_role_id']==3){
                $data['contract_owner_id'] = $data['id_user'];
            }*/
            if($data['user_role_id']==4){
                $data['delegate_id'] = $data['id_user'];
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
                if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['user_role_id']==5){
                $data['customer_user'] = $data['id_user'];
                if(!in_array($data['customer_user'],$this->session_user_contributors)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['user_role_id']==6){
                $data['business_unit_id'] = $this->session_user_business_units;
            }
        }
        if(isset($data['parent_contract_id'])) {
            $data['parent_contract_id'] = pk_decrypt($data['parent_contract_id']);
        }
        /*helper function for ordering smart table grid options*/
        $data = tableOptions($data);
        $result = $this->Contract_model->getContractList($data);//echo $this->db->last_query();exit;

        for($s=0;$s<count($result['data']);$s++) //parent contracts
        {
            preg_match_all('/[A-Z]/', ucwords(strtolower($result['data'][$s]['relationship_category_name'])), $matches);
            $result['data'][$s]['relationship_category_short_name'] = implode('',$matches[0]);
            $result['data'][$s]['review_by'] = '---';$result['data'][$s]['last_review']=NULL;
            $last_finalized_review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result['data'][$s]['id_contract'],'order' => 'DESC','contract_review_status'=>'finished'));
            if(!empty($last_finalized_review) && isset($last_finalized_review[0]['id_contract_review']) && $last_finalized_review[0]['id_contract_review']!='' && $last_finalized_review[0]['id_contract_review']!=0) {
                $result['data'][$s]['review_by'] = $last_finalized_review[0]['review_by'];
                if($last_finalized_review[0]['review_on']!='---')
                    $result['data'][$s]['last_review'] = date('Y-m-d',strtotime($last_finalized_review[0]['review_on']));
            }
            $review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result['data'][$s]['id_contract'],'order' => 'DESC'));
            if(!empty($review) && isset($review[0]['id_contract_review']) && $review[0]['id_contract_review']!='' && $review[0]['id_contract_review']!=0) {
                /*$result['data'][$s]['review_by'] = $review[0]['review_by'];
                if($review[0]['review_on']!='---')
                    $result['data'][$s]['last_review'] = date('Y-m-d',strtotime($review[0]['review_on']));*/

                //getting score of recent review
                $result['data'][$s]['id_contract_review'] = $review[0]['id_contract_review'];
                $module_score = $this->Contract_model->getContractReviewModuleScore(array('contract_review_id' => $review[0]['id_contract_review']));
                $result['data'][$s]['contract_progress'] =0;
                for($sr=0;$sr<count($module_score);$sr++)
                {
                    $module_score[$sr]['score'] = getScoreByCount($module_score[$sr]);
                    $result['data'][$s]['contract_progress'] += $this->Contract_model->progress(array('module_id'=>$module_score[$sr]['module_id'],'contract_review_id'=>$review[0]['id_contract_review']));
                }
                if(count($module_score)>0)
                    $result['data'][$s]['contract_progress'] = round($result['data'][$s]['contract_progress']/count($module_score)).'%';
                else
                    $result['data'][$s]['contract_progress'] = '0%';
                $result['data'][$s]['score'] = getScore($scope = array_map(function($i){ return strtolower($i['score']); },$module_score));
            }

            $result['data'][$s]['contract_start_date'] = date('Y-m-d',strtotime($result['data'][$s]['contract_start_date']));
            $result['data'][$s]['contract_end_date'] = date('Y-m-d',strtotime($result['data'][$s]['contract_end_date']));
            //getting action items of a recent review based on user role
            $action_data = array('id_contract' => $result['data'][$s]['id_contract']);
            if (isset($data['id_user']))
                $action_data['id_user'] = $data['id_user'];
            if (isset($data['user_role_id']))
                $action_data['user_role_id'] = $data['user_role_id'];
            $action_data['item_status'] = 1;
            //$action_data['id_contract_review'] = $review[0]['id_contract_review'];
            $action_data['responsible_user_id'] = $data['id_user'];
            $action_data['status'] = 'open';
            //getting action items count of a recent review
            $result['data'][$s]['action_item_count'] = count($this->Contract_model->getContractReviewActionItemsList($action_data));
            /*if(!empty($review) && isset($review[0]['id_contract_review']) && $review[0]['id_contract_review']!='' && $review[0]['id_contract_review']!=0) {
                $action_data = array('id_contract' => $result['data'][$s]['id_contract']);
                if (isset($data['id_user']))
                    $action_data['id_user'] = $data['id_user'];
                if (isset($data['user_role_id']))
                    $action_data['user_role_id'] = $data['user_role_id'];
                $action_data['item_status'] = 1;
                $action_data['id_contract_review'] = $review[0]['id_contract_review'];
                $action_data['responsible_user_id'] = $data['id_user'];
                //getting action items count of a recent review
                $result['data'][$s]['action_item_count'] = count($this->Contract_model->getContractReviewActionItemsList($action_data));
            }
            else{
                $result['data'][$s]['action_item_count']=0;
            }*/
            $result['data'][$s]['deadline'] = ($this->Contract_model->getContractDeadline(array('relationship_category_id'=>$result['data'][$s]['relationship_category_id'],'id_contract'=>$result['data'][$s]['id_contract'])));
            ///Assigning edit access for contracts
            $result['data'][$s]['ieaaei'] = 'annus';
            //checking is customer admin
            if(isset($data['user_role_id']) && $data['user_role_id'] == 2) {
                $result['data'][$s]['ieaaei'] = 'itako';
            }//chechking is contract created_by or contract_owner
            else if($data['id_user'] == $result['data'][$s]['created_by'] || $data['id_user'] == $result['data'][$s]['contract_owner_id']) {
                $result['data'][$s]['ieaaei'] = 'itako';
            }
            $result['data'][$s]['ideedi']=count($this->Contract_model->getContractDiscussion(array('id_contract'=>$result['data'][$s]['id_contract'],'discussion_status'=>1)))>0?"itako":'annus';

            $result['data'][$s]['business_unit_id']=pk_encrypt($result['data'][$s]['business_unit_id']);
            $result['data'][$s]['classification_id']=pk_encrypt($result['data'][$s]['classification_id']);
            $result['data'][$s]['contract_owner_id']=pk_encrypt($result['data'][$s]['contract_owner_id']);
            $result['data'][$s]['created_by']=pk_encrypt($result['data'][$s]['created_by']);
            $result['data'][$s]['currency_id']=pk_encrypt($result['data'][$s]['currency_id']);
            $result['data'][$s]['id_contract']=pk_encrypt($result['data'][$s]['id_contract']);
            $result['data'][$s]['relationship_category_id']=pk_encrypt($result['data'][$s]['relationship_category_id']);
            $result['data'][$s]['updated_by']=pk_encrypt($result['data'][$s]['updated_by']);
            $result['data'][$s]['id_contract_review']=pk_encrypt($result['data'][$s]['id_contract_review']);
            $result['data'][$s]['parent_contract_id']=pk_encrypt($result['data'][$s]['parent_contract_id']);
        }
        unset($data['pagination']);
        for($s = 0;$s<count($result['data']); $s++) //sub contracts
        {
            $data['parent_contract_id'] = pk_decrypt($result['data'][$s]['id_contract']);
            $result['data'][$s]['sub_contracts'] = $this->Contract_model->getContractList($data);//echo $this->db->last_query();
            $sub_contracts = $result['data'][$s]['sub_contracts']['data'];
            for($t = 0; $t < count($sub_contracts); $t++){
                preg_match_all('/[A-Z]/', ucwords(strtolower($sub_contracts[$t]['relationship_category_name'])), $matches);
                $sub_contracts[$t]['relationship_category_short_name'] = implode('',$matches[0]);
                $sub_contracts[$t]['review_by'] = '---';$sub_contracts[$t]['last_review']=NULL;
                $last_finalized_review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $sub_contracts[$t]['id_contract'],'order' => 'DESC','contract_review_status'=>'finished'));
                if(!empty($last_finalized_review) && isset($last_finalized_review[0]['id_contract_review']) && $last_finalized_review[0]['id_contract_review']!='' && $last_finalized_review[0]['id_contract_review']!=0) {
                    $sub_contracts[$t]['review_by'] = $last_finalized_review[0]['review_by'];
                    if($last_finalized_review[0]['review_on']!='---')
                        $sub_contracts[$t]['last_review'] = date('Y-m-d',strtotime($last_finalized_review[0]['review_on']));
                }
                $review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $sub_contracts[$t]['id_contract'],'order' => 'DESC'));
                if(!empty($review) && isset($review[0]['id_contract_review']) && $review[0]['id_contract_review']!='' && $review[0]['id_contract_review']!=0) {
                    /*$sub_contracts[$t]['review_by'] = $review[0]['review_by'];
                    if($review[0]['review_on']!='---')
                        $sub_contracts[$t]['last_review'] = date('Y-m-d',strtotime($review[0]['review_on']));*/

                    //getting score of recent review
                    $sub_contracts[$t]['id_contract_review'] = $review[0]['id_contract_review'];
                    $module_score = $this->Contract_model->getContractReviewModuleScore(array('contract_review_id' => $review[0]['id_contract_review']));
                    $sub_contracts[$t]['contract_progress'] =0;
                    for($sr=0;$sr<count($module_score);$sr++)
                    {
                        $module_score[$sr]['score'] = getScoreByCount($module_score[$sr]);
                        $sub_contracts[$t]['contract_progress'] += $this->Contract_model->progress(array('module_id'=>$module_score[$sr]['module_id'],'contract_review_id'=>$review[0]['id_contract_review']));
                    }
                    if(count($module_score)>0)
                        $sub_contracts[$t]['contract_progress'] = round($sub_contracts[$t]['contract_progress']/count($module_score),2).'%';
                    else
                        $sub_contracts[$t]['contract_progress'] = '0%';
                        $sub_contracts[$t]['score'] = getScore($scope = array_map(function($i){ return strtolower($i['score']); },$module_score));
                }

                $sub_contracts[$t]['contract_start_date'] = date('Y-m-d',strtotime($sub_contracts[$t]['contract_start_date']));
                $sub_contracts[$t]['contract_end_date'] = date('Y-m-d',strtotime($sub_contracts[$t]['contract_end_date']));
                //getting action items of a recent review based on user role
                $action_data = array('id_contract' => $sub_contracts[$t]['id_contract']);
                if (isset($data['id_user']))
                    $action_data['id_user'] = $data['id_user'];
                if (isset($data['user_role_id']))
                    $action_data['user_role_id'] = $data['user_role_id'];
                $action_data['item_status'] = 1;
                //$action_data['id_contract_review'] = $review[0]['id_contract_review'];
                $action_data['responsible_user_id'] = $data['id_user'];
                $action_data['status'] = 'open';
                //getting action items count of a recent review
                $sub_contracts[$t]['action_item_count'] = count($this->Contract_model->getContractReviewActionItemsList($action_data));
                /*if(!empty($review) && isset($review[0]['id_contract_review']) && $review[0]['id_contract_review']!='' && $review[0]['id_contract_review']!=0) {
                    $action_data = array('id_contract' => $sub_contracts[$t]['id_contract']);
                    if (isset($data['id_user']))
                        $action_data['id_user'] = $data['id_user'];
                    if (isset($data['user_role_id']))
                        $action_data['user_role_id'] = $data['user_role_id'];
                    $action_data['item_status'] = 1;
                    $action_data['id_contract_review'] = $review[0]['id_contract_review'];
                    $action_data['responsible_user_id'] = $data['id_user'];
                    //getting action items count of a recent review
                    $sub_contracts[$t]['action_item_count'] = count($this->Contract_model->getContractReviewActionItemsList($action_data));
                }
                else{
                    $sub_contracts[$t]['action_item_count']=0;
                }*/
                $sub_contracts[$t]['deadline'] = ($this->Contract_model->getContractDeadline(array('relationship_category_id'=>$sub_contracts[$t]['relationship_category_id'],'id_contract'=>$sub_contracts[$t]['id_contract'])));
                ///Assigning edit access for contracts
                $sub_contracts[$t]['ieaaei'] = 'annus';
                //checking is customer admin
                if(isset($data['user_role_id']) && $data['user_role_id'] == 2) {
                    $sub_contracts[$t]['ieaaei'] = 'itako';
                }//chechking is contract created_by or contract_owner
                else if($data['id_user'] == $sub_contracts[$t]['created_by'] || $data['id_user'] == $sub_contracts[$t]['contract_owner_id']) {
                    $sub_contracts[$t]['ieaaei'] = 'itako';
                }
                $sub_contracts[$t]['ideedi']=count($this->Contract_model->getContractDiscussion(array('id_contract'=>$sub_contracts[$t]['id_contract'],'discussion_status'=>1)))>0?"itako":'annus';

                $sub_contracts[$t]['business_unit_id']=pk_encrypt($sub_contracts[$t]['business_unit_id']);
                $sub_contracts[$t]['classification_id']=pk_encrypt($sub_contracts[$t]['classification_id']);
                $sub_contracts[$t]['contract_owner_id']=pk_encrypt($sub_contracts[$t]['contract_owner_id']);
                $sub_contracts[$t]['created_by']=pk_encrypt($sub_contracts[$t]['created_by']);
                $sub_contracts[$t]['currency_id']=pk_encrypt($sub_contracts[$t]['currency_id']);
                $sub_contracts[$t]['id_contract']=pk_encrypt($sub_contracts[$t]['id_contract']);
                $sub_contracts[$t]['relationship_category_id']=pk_encrypt($sub_contracts[$t]['relationship_category_id']);
                $sub_contracts[$t]['updated_by']=pk_encrypt($sub_contracts[$t]['updated_by']);
                $sub_contracts[$t]['id_contract_review']=pk_encrypt($sub_contracts[$t]['id_contract_review']);
                $sub_contracts[$t]['parent_contract_id']=pk_encrypt($sub_contracts[$t]['parent_contract_id']);
            }
            $result['data'][$s]['sub_contracts'] = $sub_contracts;
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function deletedList_get()
    {
        $data = $this->input->get();

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($data['customer_id']!=$this->session_user_info->customer_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all') {
            $data['id_business_unit'] = pk_decrypt($data['business_unit_id']);
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['id_business_unit'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['delegate_id'])) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_owner_id'])) {
            $data['contract_owner_id'] = pk_decrypt($data['contract_owner_id']);
            if(!in_array($data['contract_owner_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_user'])) {
            $data['customer_user'] = pk_decrypt($data['customer_user']);
            if(!in_array($data['customer_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id']) && isset($data['id_user'])){
            if(in_array($data['user_role_id'],array(3))){
                $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $data['id_user'],'status' => '1'));
                $data['business_unit_id'] = array_map(function($i){ return $i['id_business_unit']; },$business_unit);
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            /*if($data['user_role_id']==3){
                $data['contract_owner_id'] = $data['id_user'];
            }*/
            if($data['user_role_id']==4){
                $data['delegate_id'] = $data['id_user'];
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
                if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['user_role_id']==5){
                $data['customer_user'] = $data['id_user'];
                if(!in_array($data['customer_user'],$this->session_user_contributors)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['user_role_id']==6){
                $data['business_unit_id'] = $this->session_user_business_units;
            }
        }
        if(isset($data['parent_contract_id'])) {
            $data['parent_contract_id'] = pk_decrypt($data['parent_contract_id']);
        }
        /*helper function for ordering smart table grid options*/
        $data = tableOptions($data);
        $result = $this->Contract_model->getDeletedContractList($data);
        for($s=0;$s<count($result['data']);$s++)
        {
            preg_match_all('/[A-Z]/', ucwords(strtolower($result['data'][$s]['relationship_category_name'])), $matches);
            $result['data'][$s]['relationship_category_short_name'] = implode('',$matches[0]);
            $result['data'][$s]['review_by'] = '---';$result['data'][$s]['last_review']=NULL;
            $last_finalized_review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result['data'][$s]['id_contract'],'order' => 'DESC','contract_review_status'=>'finished'));
            if(!empty($last_finalized_review) && isset($last_finalized_review[0]['id_contract_review']) && $last_finalized_review[0]['id_contract_review']!='' && $last_finalized_review[0]['id_contract_review']!=0) {
                $result['data'][$s]['review_by'] = $last_finalized_review[0]['review_by'];
                if($last_finalized_review[0]['review_on']!='---')
                    $result['data'][$s]['last_review'] = date('Y-m-d',strtotime($last_finalized_review[0]['review_on']));
            }
            $review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result['data'][$s]['id_contract'],'order' => 'DESC'));
            if(!empty($review) && isset($review[0]['id_contract_review']) && $review[0]['id_contract_review']!='' && $review[0]['id_contract_review']!=0) {
                /*$result['data'][$s]['review_by'] = $review[0]['review_by'];
                if($review[0]['review_on']!='---')
                    $result['data'][$s]['last_review'] = date('Y-m-d',strtotime($review[0]['review_on']));*/

                //getting score of recent review
                $result['data'][$s]['id_contract_review'] = $review[0]['id_contract_review'];
                $module_score = $this->Contract_model->getContractReviewModuleScore(array('contract_review_id' => $review[0]['id_contract_review']));
                for($sr=0;$sr<count($module_score);$sr++)
                {
                    $module_score[$sr]['score'] = getScoreByCount($module_score[$sr]);
                }
                $result['data'][$s]['score'] = getScore($scope = array_map(function($i){ return strtolower($i['score']); },$module_score));
            }

            $result['data'][$s]['contract_start_date'] = date('Y-m-d',strtotime($result['data'][$s]['contract_start_date']));
            $result['data'][$s]['contract_end_date'] = date('Y-m-d',strtotime($result['data'][$s]['contract_end_date']));
            //getting action items of a recent review based on user role
            $action_data = array('id_contract' => $result['data'][$s]['id_contract']);
            if (isset($data['id_user']))
                $action_data['id_user'] = $data['id_user'];
            if (isset($data['user_role_id']))
                $action_data['user_role_id'] = $data['user_role_id'];
            $action_data['item_status'] = 1;
            //$action_data['id_contract_review'] = $review[0]['id_contract_review'];
            $action_data['responsible_user_id'] = $data['id_user'];
            $action_data['status'] = 'open';
            //getting action items count of a recent review
            $result['data'][$s]['action_item_count'] = count($this->Contract_model->getContractReviewActionItemsList($action_data));
            /*if(!empty($review) && isset($review[0]['id_contract_review']) && $review[0]['id_contract_review']!='' && $review[0]['id_contract_review']!=0) {
                $action_data = array('id_contract' => $result['data'][$s]['id_contract']);
                if (isset($data['id_user']))
                    $action_data['id_user'] = $data['id_user'];
                if (isset($data['user_role_id']))
                    $action_data['user_role_id'] = $data['user_role_id'];
                $action_data['item_status'] = 1;
                $action_data['id_contract_review'] = $review[0]['id_contract_review'];
                $action_data['responsible_user_id'] = $data['id_user'];
                //getting action items count of a recent review
                $result['data'][$s]['action_item_count'] = count($this->Contract_model->getContractReviewActionItemsList($action_data));
            }
            else{
                $result['data'][$s]['action_item_count']=0;
            }*/
            $result['data'][$s]['deadline'] = ($this->Contract_model->getContractDeadline(array('relationship_category_id'=>$result['data'][$s]['relationship_category_id'],'id_contract'=>$result['data'][$s]['id_contract'])));
            ///Assigning edit access for contracts
            $result['data'][$s]['ieaaei'] = 'annus';
            //checking is customer admin
            if($data['user_role_id'] == 2) {
                $result['data'][$s]['ieaaei'] = 'itako';
            }//chechking is contract created_by or contract_owner
            else if($data['id_user'] == $result['data'][$s]['created_by'] || $data['id_user'] == $result['data'][$s]['contract_owner_id']) {
                $result['data'][$s]['ieaaei'] = 'itako';
            }
            $result['data'][$s]['ideedi']=count($this->Contract_model->getContractDiscussion(array('id_contract'=>$result['data'][$s]['id_contract'],'discussion_status'=>1)))>0?"itako":'annus';

            $result['data'][$s]['business_unit_id']=pk_encrypt($result['data'][$s]['business_unit_id']);
            $result['data'][$s]['classification_id']=pk_encrypt($result['data'][$s]['classification_id']);
            $result['data'][$s]['contract_owner_id']=pk_encrypt($result['data'][$s]['contract_owner_id']);
            $result['data'][$s]['created_by']=pk_encrypt($result['data'][$s]['created_by']);
            $result['data'][$s]['currency_id']=pk_encrypt($result['data'][$s]['currency_id']);
            $result['data'][$s]['id_contract']=pk_encrypt($result['data'][$s]['id_contract']);
            $result['data'][$s]['relationship_category_id']=pk_encrypt($result['data'][$s]['relationship_category_id']);
            $result['data'][$s]['updated_by']=pk_encrypt($result['data'][$s]['updated_by']);
            $result['data'][$s]['id_contract_review']=pk_encrypt($result['data'][$s]['id_contract_review']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function listProviders_get(){
        $data = $this->input->get();

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($data['customer_id']!=$this->session_user_info->customer_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all') {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            $data['id_business_unit'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['id_business_unit'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['delegate_id'])) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_owner_id'])) {
            $data['contract_owner_id'] = pk_decrypt($data['contract_owner_id']);
            if(!in_array($data['contract_owner_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_user'])) {
            $data['customer_user'] = pk_decrypt($data['customer_user']);
            if(!in_array($data['customer_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(isset($data['user_role_id']) && isset($data['id_user'])){
            if(in_array($data['user_role_id'],array(3))){
                $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $data['id_user'],'status' => '1'));
                $data['business_unit_id'] = array_map(function($i){ return $i['id_business_unit']; },$business_unit);
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            if($data['user_role_id']==3){
                /*$data['contract_owner_id'] = $data['id_user'];
                if(!in_array($data['contract_owner_id'],$this->session_user_bu_owners)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }*/
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            if($data['user_role_id']==4){
                $data['delegate_id'] = $data['id_user'];
                if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            if($data['user_role_id']==5){
                $data['customer_user'] = $data['id_user'];
                if(!in_array($data['customer_user'],$this->session_user_contributors)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['user_role_id']==6){
                $data['business_unit_id'] = $this->session_user_business_units;
            }
        }

        /*helper function for ordering smart table grid options*/
        $data = tableOptions($data);
        $result = $this->Contract_model->getProviders($data);
        //echo '<pre>';print_r($result);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function actionItemFilters_get(){
        $data = $this->input->get();

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($data['customer_id']!=$this->session_user_info->customer_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all') {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['business_unit_id'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['delegate_id'])) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_owner_id'])) {
            $data['contract_owner_id'] = pk_decrypt($data['contract_owner_id']);
            if(!in_array($data['contract_owner_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_user'])) {
            $data['customer_user'] = pk_decrypt($data['customer_user']);
            if(!in_array($data['customer_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id']) && isset($data['id_user'])){
            if(in_array($data['user_role_id'],array(3))){
                $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $data['id_user'],'status' => '1'));
                $data['business_unit_id'] = array_map(function($i){ return $i['id_business_unit']; },$business_unit);
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            if($data['user_role_id']==3){
                $data['contract_owner_id'] = $data['id_user'];
                /*if(!in_array($data['contract_owner_id'],$this->session_user_bu_owners)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }*/
            }
            if($data['user_role_id']==4){
                $data['delegate_id'] = $data['id_user'];
                if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            if($data['user_role_id']==5){
                $data['customer_user'] = $data['id_user'];
                if(!in_array($data['customer_user'],$this->session_user_contributors)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['user_role_id']==6){
                $data['business_unit_id'] = $this->session_user_business_units;
            }
        }

        /*helper function for ordering smart table grid options*/
        $result['providers'] = $this->Contract_model->getProviders($data);
        $result['contracts'] = $this->Contract_model->getContracts($data);
        foreach($result['contracts'] as $k=>$v){
            $result['contracts'][$k]['contract_id']=pk_encrypt($v['contract_id']);
        }
        //echo '<pre>';print_r($result);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contractStatus_get(){

        $data[]=['key'=>'new','value'=>'New'];
        $data[]=['key'=>'pending review','value'=>'Pending Review'];
        $data[]=['key'=>'review in progress','value'=>'Review in Progress'];
        $data[]=['key'=>'review finalized','value'=>'Review Finalized'];

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$data);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function add_post()
    {
        $data = $this->input->post();
        //parent_contract_id
        //echo '<pre>';print_r($data);exit;
        if(isset($data['contract'])){
            $data = $data['contract'];
        }
        if(isset($_FILES['file']))
            $totalFilesCount = count($_FILES['file']['name']);
        else
            $totalFilesCount=0;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('business_unit_id', array('required'=>$this->lang->line('business_unit_id_req')));
        $this->form_validator->add_rules('contract_name', array('required'=>$this->lang->line('contract_name_req')));
        $this->form_validator->add_rules('contract_owner_id', array('required'=>$this->lang->line('contract_owner_id_req')));
        //$this->form_validator->add_rules('delegate_id', array('required'=>$this->lang->line('contract_delegate_id_req')));
        $this->form_validator->add_rules('description', array('required'=>$this->lang->line('contract_description_req')));
        $this->form_validator->add_rules('contract_start_date', array(
            'required'=>$this->lang->line('contract_start_date_req'),
            'date' => $this->lang->line('contract_start_date_invalid')
        ));
        /*$this->form_validator->add_rules('contract_end_date', array(
            'required'=>$this->lang->line('contract_end_date_req'),
            'date' => $this->lang->line('contract_end_date_invalid')
        ));*/
        $this->form_validator->add_rules('relationship_category_id', array('required'=>$this->lang->line('relationship_category_id_req')));
        $this->form_validator->add_rules('classification_id', array('required'=>$this->lang->line('relationship_classification_id_req')));
        $this->form_validator->add_rules('contract_value', array('required'=>$this->lang->line('contract_value_req')));
        $this->form_validator->add_rules('currency_id', array('required'=>$this->lang->line('currency_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($data['customer_id']!=$this->session_user_info->customer_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all') {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['business_unit_id'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_owner_id'])) {
            $data['contract_owner_id'] = pk_decrypt($data['contract_owner_id']);
            if(!in_array($data['contract_owner_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['relationship_category_id'])) {
            $data['relationship_category_id'] = pk_decrypt($data['relationship_category_id']);
            if(!in_array($data['relationship_category_id'],$this->session_user_customer_relationship_categories)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['classification_id'])) {
            $data['classification_id'] = pk_decrypt($data['classification_id']);
            if(!in_array($data['classification_id'],$this->session_user_customer_relationship_classifications)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['currency_id'])) {
            $data['currency_id'] = pk_decrypt($data['currency_id']);
            if(!in_array($data['currency_id'],$this->session_user_master_currency)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['delegate_id']) && $data['delegate_id'] != 0) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['parent_contract_id'])) {
            $data['parent_contract_id'] = pk_decrypt($data['parent_contract_id']);
        }
        $add = array(
            'business_unit_id' => $data['business_unit_id'],
            'provider_name' => isset($data['provider_name'])?$data['provider_name']:'',
            'contract_name' => $data['contract_name'],
            'contract_owner_id' => $data['contract_owner_id'],
            'contract_start_date' => $data['contract_start_date'],
            'contract_end_date' => isset($data['contract_end_date'])?$data['contract_end_date']:'',
            'auto_renewal' => isset($data['auto_renewal'])?$data['auto_renewal']:'0',
            'relationship_category_id' => $data['relationship_category_id'],
            'classification_id' => $data['classification_id'],
            'contract_value' => $data['contract_value'],
            'currency_id' => $data['currency_id'],
            'provider_contract_sponsor' => isset($data['provider_contract_sponsor'])?$data['provider_contract_sponsor']:'',
            'internal_contract_sponsor' => isset($data['internal_contract_sponsor'])?$data['internal_contract_sponsor']:'',
            'internal_partner_relationship_manager' => isset($data['internal_partner_relationship_manager'])?$data['internal_partner_relationship_manager']:'',
            'provider_partner_relationship_manager' => isset($data['provider_partner_relationship_manager'])?$data['provider_partner_relationship_manager']:'',
            'provider_contract_responsible' => isset($data['provider_contract_responsible'])?$data['provider_contract_responsible']:'',
            'internal_contract_responsible' => isset($data['internal_contract_responsible'])?$data['internal_contract_responsible']:'',
            'delegate_id' => isset($data['delegate_id'])?$data['delegate_id']:'',
            'description' => isset($data['description'])?$data['description']:'',
            'created_by' => $data['created_by'],
            'created_on' => currentDate(),
            'parent_contract_id' => isset($data['parent_contract_id'])?$data['parent_contract_id']:0
        );
        $id_contract=$this->Contract_model->addContract($add);
        $customer_id=$data['customer_id'];
        $path=FILE_SYSTEM_PATH.'uploads/';
        $contract_documents=array();
        if(!is_dir($path.$customer_id)){ mkdir($path.$customer_id); }
        if(isset($_FILES) && $totalFilesCount>0)
        {
            $i_attachment=0;
            for($i_attachment=0; $i_attachment<$totalFilesCount; $i_attachment++) {
                $imageName = doUpload(array(
                    'temp_name' => $_FILES['file']['tmp_name'][$i_attachment],
                    'image' => $_FILES['file']['name'][$i_attachment],
                    'upload_path' => $path,
                    'folder' => $customer_id));
                $contract_documents[$i_attachment]['module_id']=$customer_id;
                $contract_documents[$i_attachment]['module_type']='customer';
                $contract_documents[$i_attachment]['reference_id']=$id_contract;
                $contract_documents[$i_attachment]['reference_type']='contract';
                $contract_documents[$i_attachment]['document_name']=$_FILES['file']['name'][$i_attachment];
                $contract_documents[$i_attachment]['document_source']=$imageName;
                $contract_documents[$i_attachment]['document_mime_type']=$_FILES['file']['type'][$i_attachment];
                $contract_documents[$i_attachment]['document_status']=1;
                $contract_documents[$i_attachment]['uploaded_by']=$data['created_by'];
                $contract_documents[$i_attachment]['uploaded_on']=currentDate();
            }
        }
        if(count($contract_documents)>0){
            $this->Document_model->addBulkDocuments($contract_documents);
        }
        $result = $this->User_model->getUserInfo(array('user_id' => $data['created_by']));
        $user_info = $this->User_model->getUserInfo(array('user_id' => $data['contract_owner_id']));
        $contract_assigned_to_user_names=$user_info->first_name.' '.$user_info->last_name.' ('.$user_info->user_role_name.')';
        if(isset($data['delegate_id']) && $data['delegate_id']!=NULL && $data['delegate_id']>0) {
            $assigned = $this->User_model->getUserInfo(array('user_id' => $add['delegate_id']));
            $contract_assigned_to_user_names.=', '.$assigned->first_name.' '.$assigned->last_name.' ('.$assigned->user_role_name.')';
        }
        $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $result->customer_id));

        if($customer_details[0]['company_logo']=='') {
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
        }
        else{
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

        }
        if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }

        $customer_admin_list=$this->Customer_model->getCustomerAdminList(array('customer_id'=>$user_info->customer_id));

        //echo '<pre>';print_r($assigned);exit;
        $template_configurations=$this->Customer_model->EmailTemplateList(array('customer_id' => $user_info->customer_id,'language_id' =>1,'module_key'=>'CONTRACT_CREATION'));
        /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
        $cust_admin = $cust_admin['data'][0];*/
        //echo '<pre>';print_r($email_from);exit;
        $template_configurations_parent=$template_configurations;
        if($template_configurations_parent['total_records']>0){
            foreach($customer_admin_list['data'] as $kd=>$vd){
                $mailer_data='';
                $template_configurations=$template_configurations_parent['data'][0];
                $wildcards=$template_configurations['wildcards'];
                $wildcards_replaces=array();
                $wildcards_replaces['first_name']=$vd['first_name'];
                $wildcards_replaces['last_name']=$vd['last_name'];
                $wildcards_replaces['contract_name']=$data['contract_name'];
                $wildcards_replaces['contract_owner_name']=$result->first_name.' '.$result->last_name;
                $wildcards_replaces['contract_created_date']=dateFormat($add['created_on']);
                $wildcards_replaces['contract_assigned_to_user_names']=$contract_assigned_to_user_names;
                $wildcards_replaces['logo']=$customer_logo;
                $wildcards_replaces['url']=WEB_BASE_URL.'html';
                $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                /*$from_name=SEND_GRID_FROM_NAME;
                $from=SEND_GRID_FROM_EMAIL;
                $from_name=$cust_admin['name'];
                $from=$cust_admin['email'];*/
                $from_name=$template_configurations['email_from_name'];
                $from=$template_configurations['email_from'];
                $to=$vd['email'];
                $to_name=$vd['first_name'].' '.$vd['last_name'];
                $mailer_data['mail_from_name']=$from_name;
                $mailer_data['mail_to_name']=$to_name;
                $mailer_data['mail_to_user_id']=$vd['id_user'];
                $mailer_data['mail_from']=$from;
                $mailer_data['mail_to']=$to;
                $mailer_data['mail_subject']=$subject;
                $mailer_data['mail_message']=$body;
                $mailer_data['status']=0;
                $mailer_data['send_date']=currentDate();
                $mailer_data['is_cron']=0;
                $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                $mailer_id=$this->Customer_model->addMailer($mailer_data);
                if($mailer_data['is_cron']==0) {
                    //$mail_sent_status=sendmail($to, $subject, $body, $from);
                    $this->load->library('sendgridlibrary');
                    $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                    if($mail_sent_status==1)
                        $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                }
            }
            $mailer_data='';
            $template_configurations=$template_configurations_parent['data'][0];
            $wildcards=$template_configurations['wildcards'];
            $wildcards_replaces=array();
            $wildcards_replaces['first_name']=$user_info->first_name;
            $wildcards_replaces['last_name']=$user_info->last_name;
            $wildcards_replaces['contract_name']=$data['contract_name'];
            $wildcards_replaces['contract_owner_name']=$result->first_name.' '.$result->last_name;
            $wildcards_replaces['contract_created_date']=dateFormat($add['created_on']);
            $wildcards_replaces['contract_assigned_to_user_names']=$contract_assigned_to_user_names;
            $wildcards_replaces['logo']=$customer_logo;
            $wildcards_replaces['url']=WEB_BASE_URL.'html';
            $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
            $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
            /*$from_name=SEND_GRID_FROM_NAME;
            $from=SEND_GRID_FROM_EMAIL;
            $from_name=$cust_admin['name'];
            $from=$cust_admin['email'];*/
            $from_name=$template_configurations['email_from_name'];
            $from=$template_configurations['email_from'];
            $to=$user_info->email;
            $to_name=$user_info->first_name.' '.$user_info->last_name;
            $mailer_data['mail_from_name']=$from_name;
            $mailer_data['mail_to_name']=$to_name;
            $mailer_data['mail_to_user_id']=$user_info->id_user;
            $mailer_data['mail_from']=$from;
            $mailer_data['mail_to']=$to;
            $mailer_data['mail_subject']=$subject;
            $mailer_data['mail_message']=$body;
            $mailer_data['status']=0;
            $mailer_data['send_date']=currentDate();
            $mailer_data['is_cron']=0;
            $mailer_data['email_template_id']=$template_configurations['id_email_template'];
            $mailer_id=$this->Customer_model->addMailer($mailer_data);
            if($mailer_data['is_cron']==0) {
                //$mail_sent_status=sendmail($to, $subject, $body, $from);
                $this->load->library('sendgridlibrary');
                $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                if($mail_sent_status==1)
                    $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
            }

            if(isset($data['delegate_id']) && $data['delegate_id']!=NULL && $data['delegate_id']>0){
                //$delegate = $this->User_model->getUserInfo(array('user_id'=>$data['delegate_id']));
                //echo '<pre>';print_r($delegate);exit;
                $assigned = $this->User_model->getUserInfo(array('user_id' => $add['delegate_id']));
                //echo '<pre>';print_r($assigned);exit;
                $mailer_data='';
                $template_configurations=$template_configurations_parent['data'][0];
                $wildcards=$template_configurations['wildcards'];
                $wildcards_replaces=array();
                $wildcards_replaces['first_name']=$assigned->first_name;
                $wildcards_replaces['last_name']=$assigned->last_name;
                $wildcards_replaces['contract_name']=$data['contract_name'];
                $wildcards_replaces['contract_owner_name']=$result->first_name.' '.$result->last_name;
                $wildcards_replaces['contract_created_date']=dateFormat($add['created_on']);
                $wildcards_replaces['contract_assigned_to_user_names']=$contract_assigned_to_user_names;
                $wildcards_replaces['logo']=$customer_logo;
                $wildcards_replaces['url']=WEB_BASE_URL.'html';
                $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                /*$from_name=SEND_GRID_FROM_NAME;
                $from=SEND_GRID_FROM_EMAIL;
                $from_name=$cust_admin['name'];
                $from=$cust_admin['email'];*/
                $from_name=$template_configurations['email_from_name'];
                $from=$template_configurations['email_from'];
                $to = $assigned->email;
                $to_name=$assigned->first_name.' '.$assigned->last_name;
                $mailer_data['mail_from_name']=$from_name;
                $mailer_data['mail_to_name']=$to_name;
                $mailer_data['mail_to_user_id']=$assigned->id_user;
                $mailer_data['mail_from']=$from;
                $mailer_data['mail_to']=$to;
                $mailer_data['mail_subject']=$subject;
                $mailer_data['mail_message']=$body;
                $mailer_data['status']=0;
                $mailer_data['send_date']=currentDate();
                $mailer_data['is_cron']=0;
                $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                $mailer_id=$this->Customer_model->addMailer($mailer_data);
                if($mailer_data['is_cron']==0){
                    //$mail_sent_status=sendmail($to_delegate, $subject, $body, $from);
                    $this->load->library('sendgridlibrary');
                    $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                    if($mail_sent_status==1)
                        $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                }

            }
        }

        if(isset($data['parent_contract_id'])){
            $result = array('status'=>TRUE, 'message' => $this->lang->line('contract_add'), 'data'=>'', 'contract_data'=>array('contract_name'=>$data['contract_name'],'contract_id'=>pk_encrypt($id_contract)));
        }else{
            $result = array('status'=>TRUE, 'message' => $this->lang->line('contract_add'), 'data'=>'');
        }
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function update_post()
    {
        $data = $this->input->post();

        if(isset($data['contract'])){
            $data = $data['contract'];
        }

        if(isset($_FILES['file']))
            $totalFilesCount = count($_FILES['file']['name']);
        else
            $totalFilesCount=0;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('id_contract', array('required'=>$this->lang->line('id_contract_req')));
        $this->form_validator->add_rules('business_unit_id', array('required'=>$this->lang->line('business_unit_id_req')));
        $this->form_validator->add_rules('contract_name', array('required'=>$this->lang->line('contract_name_req')));
        $this->form_validator->add_rules('contract_owner_id', array('required'=>$this->lang->line('contract_owner_id_req')));
        //$this->form_validator->add_rules('delegate_id', array('required'=>$this->lang->line('contract_delegate_id_req')));
        $this->form_validator->add_rules('description', array('required'=>$this->lang->line('contract_description_req')));
        $this->form_validator->add_rules('contract_start_date', array(
                'required'=>$this->lang->line('contract_start_date_req'),
                'date' => $this->lang->line('contract_start_date_invalid')
                ));
        $this->form_validator->add_rules('contract_end_date', array(
                'required'=>$this->lang->line('contract_end_date_req'),
                'date' => $this->lang->line('contract_end_date_invalid')
                ));
        $this->form_validator->add_rules('relationship_category_id', array('required'=>$this->lang->line('relationship_category_id_req')));
        $this->form_validator->add_rules('classification_id', array('required'=>$this->lang->line('relationship_classification_id_req')));
        $this->form_validator->add_rules('contract_value', array('required'=>$this->lang->line('contract_value_req')));
        $this->form_validator->add_rules('currency_id', array('required'=>$this->lang->line('currency_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);

        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($data['customer_id']!=$this->session_user_info->customer_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract'])) {
            $data['id_contract'] = pk_decrypt($data['id_contract']);
            if(!in_array($data['id_contract'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all') {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['business_unit_id'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_owner_id'])) {
            $data['contract_owner_id'] = pk_decrypt($data['contract_owner_id']);
            if(!in_array($data['contract_owner_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['relationship_category_id'])) {
            $data['relationship_category_id'] = pk_decrypt($data['relationship_category_id']);
            if(!in_array($data['relationship_category_id'],$this->session_user_customer_relationship_categories)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['classification_id'])) {
            $data['classification_id'] = pk_decrypt($data['classification_id']);
            if(!in_array($data['classification_id'],$this->session_user_customer_relationship_classifications)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['currency_id'])) {
            $data['currency_id'] = pk_decrypt($data['currency_id']);
            if(!in_array($data['currency_id'],$this->session_user_master_currency)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['delegate_id'])) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if($data['delegate_id'] > 0 && !in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message9'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['parent_contract_id'])) {
            $data['parent_contract_id'] = pk_decrypt($data['parent_contract_id']);
        }
        $add = array(
            'id_contract' => $data['id_contract'],
            'business_unit_id' => $data['business_unit_id'],
            'provider_name' => isset($data['provider_name'])?$data['provider_name']:'',
            'contract_name' => $data['contract_name'],
            'contract_owner_id' => $data['contract_owner_id'],
            'contract_start_date' => $data['contract_start_date'],
            'contract_end_date' => isset($data['contract_end_date'])?$data['contract_end_date']:'',
            'auto_renewal' => isset($data['auto_renewal'])?$data['auto_renewal']:'0',
            'relationship_category_id' => $data['relationship_category_id'],
            'classification_id' => $data['classification_id'],
            'contract_value' => $data['contract_value'],
            'currency_id' => $data['currency_id'],
            'provider_contract_sponsor' => isset($data['provider_contract_sponsor'])?$data['provider_contract_sponsor']:'',
            'internal_contract_sponsor' => isset($data['internal_contract_sponsor'])?$data['internal_contract_sponsor']:'',
            'internal_partner_relationship_manager' => isset($data['internal_partner_relationship_manager'])?$data['internal_partner_relationship_manager']:'',
            'provider_partner_relationship_manager' => isset($data['provider_partner_relationship_manager'])?$data['provider_partner_relationship_manager']:'',
            'provider_contract_responsible' => isset($data['provider_contract_responsible'])?$data['provider_contract_responsible']:'',
            'internal_contract_responsible' => isset($data['internal_contract_responsible'])?$data['internal_contract_responsible']:'',
            'delegate_id' => isset($data['delegate_id'])?$data['delegate_id']:'',
            'description' => isset($data['description'])?$data['description']:'',
            'updated_by' => $data['created_by'],
            'updated_on' => currentDate(),
            'parent_contract_id' => isset($data['parent_contract_id'])?$data['parent_contract_id']:0
        );

        $this->Contract_model->updateContract($add);

        $customer_id=$data['customer_id'];
        $path=FILE_SYSTEM_PATH.'uploads/';
        $contract_documents=array();
        if(!is_dir($path.$customer_id)){ mkdir($path.$customer_id); }
        if(isset($_FILES) && $totalFilesCount>0)
        {
            for($i_attachment=0; $i_attachment<$totalFilesCount; $i_attachment++) {
                $imageName = doUpload(array(
                    'temp_name' => $_FILES['file']['tmp_name'][$i_attachment],
                    'image' => $_FILES['file']['name'][$i_attachment],
                    'upload_path' => $path,
                    'folder' => $customer_id));
                $contract_documents[$i_attachment]['module_id']=$customer_id;
                $contract_documents[$i_attachment]['module_type']='customer';
                $contract_documents[$i_attachment]['reference_id']=$data['id_contract'];
                $contract_documents[$i_attachment]['reference_type']='contract';
                $contract_documents[$i_attachment]['document_name']=$_FILES['file']['name'][$i_attachment];
                $contract_documents[$i_attachment]['document_source']=$imageName;
                $contract_documents[$i_attachment]['document_mime_type']=$_FILES['file']['type'][$i_attachment];
                $contract_documents[$i_attachment]['document_status']=1;
                $contract_documents[$i_attachment]['uploaded_by']=$data['created_by'];
                $contract_documents[$i_attachment]['uploaded_on']=currentDate();

            }

        }
        if(count($contract_documents)>0){
            $this->Document_model->addBulkDocuments($contract_documents);
        }
        if(isset($data['attachment_delete'])) { //for deleted options
            for ($s = 0; $s < count($data['attachment_delete']); $s++) {
                $data['attachment_delete'][$s]['id_document']=pk_decrypt($data['attachment_delete'][$s]['id_document']);
                $this->Document_model->updateDocument(array(
                    'id_document' => $data['attachment_delete'][$s]['id_document'],
                    'document_status' => 0
                ));
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('contract_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function info_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_contract', array('required'=>$this->lang->line('contract_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(count($this->session_user_info)==0)
        {
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_contract'])) {
            $data['id_contract'] = pk_decrypt($data['id_contract']);
            if(!in_array($data['id_contract'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id'])) {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['business_unit_id'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        //$data['contract_review_status']='review in progress';
        $result = $this->Contract_model->getContractDetails($data);
        //echo $this->db->last_query(); exit;

        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['review_scheduled'] = 0;
            if($result[$s]['parent_contract_id']>0){
                $result[$s]['parent_contract_name'] = $this->Contract_model->getContractName($result[$s]['parent_contract_id'])['contract_name'];
            }

            $check_review_schedule = $this->Contract_model->checkContractReviewSchedule(array('contract_id' => $result[$s]['id_contract']));
            $check_review_schedule1 = $this->Contract_model->checkContractReviewCompletedSchedule(array('contract_id' => $result[$s]['id_contract']));
            if(!empty($check_review_schedule) && empty($check_review_schedule1)){
                $result[$s]['review_scheduled'] = 1;
            }
            $result[$s]['contract_start_date'] = date('Y-m-d',strtotime($result[$s]['contract_start_date']));
            $result[$s]['contract_end_date'] = date('Y-m-d',strtotime($result[$s]['contract_end_date']));
            $inner_data=array();
            if(isset($data['id_user']))
                $inner_data['id_user']=$action_data['id_user']=$data['id_user'];
            if(isset($data['user_role_id']))
                $inner_data['user_role_id']=$action_data['user_role_id']=$data['user_role_id'];
            $inner_data['reference_id']=$result[$s]['id_contract'];
            $inner_data['reference_type']='contract';
            $inner_data['contract_owner_id']=$result[$s]['contract_owner_id'];
            $inner_data['delegate_id']=$result[$s]['delegate_id'];
            if(isset($data['deleted'])){

            }else{
                $inner_data['document_status']=1;
            }
            $result[$s]['attachment'] = $result[$s]['unique_attachment'] = $this->Document_model->getDocumentsList($inner_data);
            foreach($result[$s]['attachment'] as $ka=>$va){
                $result[$s]['attachment'][$ka]['updated_by']=0;
            }
            $inner_data['updated_by']=isset($data['updated_by'])?$data['updated_by']:1;
            $result[$s]['attachment'] = array_merge($this->Document_model->getDocumentsList($inner_data),$result[$s]['attachment']);
            foreach($result[$s]['attachment'] as $ka=>$va){
                $result[$s]['attachment'][$ka]['document_source_exactpath']=($va['document_source']);
                $result[$s]['attachment'][$ka]['id_document']=pk_encrypt($result[$s]['attachment'][$ka]['id_document']);
                $result[$s]['attachment'][$ka]['module_id']=pk_encrypt($result[$s]['attachment'][$ka]['module_id']);
                $result[$s]['attachment'][$ka]['reference_id']=pk_encrypt($result[$s]['attachment'][$ka]['reference_id']);
                $result[$s]['attachment'][$ka]['uploaded_by']=pk_encrypt($result[$s]['attachment'][$ka]['uploaded_by']);
                $result[$s]['attachment'][$ka]['user_role_id']=pk_encrypt($result[$s]['attachment'][$ka]['user_role_id']);
            }
            foreach($result[$s]['unique_attachment'] as $ka=>$va){
                $result[$s]['unique_attachment'][$ka]['document_source_exactpath']=($va['document_source']);
                $result[$s]['unique_attachment'][$ka]['id_document']=pk_encrypt($result[$s]['unique_attachment'][$ka]['id_document']);
                $result[$s]['unique_attachment'][$ka]['module_id']=pk_encrypt($result[$s]['unique_attachment'][$ka]['module_id']);
                $result[$s]['unique_attachment'][$ka]['reference_id']=pk_encrypt($result[$s]['unique_attachment'][$ka]['reference_id']);
                $result[$s]['unique_attachment'][$ka]['uploaded_by']=pk_encrypt($result[$s]['unique_attachment'][$ka]['uploaded_by']);
                $result[$s]['unique_attachment'][$ka]['user_role_id']=pk_encrypt($result[$s]['unique_attachment'][$ka]['user_role_id']);
            }
            $action_data=array('id_contract'=>$result[$s]['id_contract']);
            if(isset($data['id_user']))
                $action_data['id_user']=$data['id_user'];
            if(isset($data['user_role_id']))
                $action_data['user_role_id']=$data['user_role_id'];
            if(isset($data['id_contract_review']))
                $action_data['id_contract_review']=$data['id_contract_review'];
            $action_data['item_status']=1;
            $result[$s]['action_items'] = $this->Contract_model->getContractReviewActionItemsList($action_data);
            foreach($result[$s]['action_items'] as $ka=>$va){
                $result[$s]['action_items'][$ka]['contract_id']=pk_encrypt($result[$s]['action_items'][$ka]['contract_id']);
                $result[$s]['action_items'][$ka]['contract_review_id']=pk_encrypt($result[$s]['action_items'][$ka]['contract_review_id']);
                $result[$s]['action_items'][$ka]['created_by']=pk_encrypt($result[$s]['action_items'][$ka]['created_by']);
                $result[$s]['action_items'][$ka]['id_contract_review_action_item']=pk_encrypt($result[$s]['action_items'][$ka]['id_contract_review_action_item']);
                $result[$s]['action_items'][$ka]['module_id']=pk_encrypt($result[$s]['action_items'][$ka]['module_id']);
                $result[$s]['action_items'][$ka]['responsible_user_id']=pk_encrypt($result[$s]['action_items'][$ka]['responsible_user_id']);
                $result[$s]['action_items'][$ka]['topic_id']=pk_encrypt($result[$s]['action_items'][$ka]['topic_id']);
                $result[$s]['action_items'][$ka]['updated_by']=pk_encrypt($result[$s]['action_items'][$ka]['updated_by']);
                $result[$s]['action_items'][$ka]['user_role_id']=pk_encrypt($result[$s]['action_items'][$ka]['user_role_id']);
                foreach($result[$s]['action_items'][$ka]['comments_log'] as $kac=>$vac){
                    $result[$s]['action_items'][$ka]['comments_log'][$kac]['contract_review_action_item_id']=pk_encrypt($result[$s]['action_items'][$ka]['comments_log'][$kac]['contract_review_action_item_id']);
                    $result[$s]['action_items'][$ka]['comments_log'][$kac]['id_contract_review_action_item_log']=pk_encrypt($result[$s]['action_items'][$ka]['comments_log'][$kac]['id_contract_review_action_item_log']);
                    $result[$s]['action_items'][$ka]['comments_log'][$kac]['updated_by']=pk_encrypt($result[$s]['action_items'][$ka]['comments_log'][$kac]['updated_by']);
                }
            }
            $result[$s]['contract_sponsor'] = array('internal'=>$result[$s]['internal_contract_sponsor'],'provider'=>$result[$s]['provider_contract_sponsor']);
            $result[$s]['partner_relationship_manager'] = array('internal'=>$result[$s]['internal_partner_relationship_manager'],'provider'=>$result[$s]['provider_partner_relationship_manager']);
            $result[$s]['contract_responsible'] = array('internal'=>$result[$s]['internal_contract_responsible'],'provider'=>$result[$s]['provider_contract_responsible']);
            $result[$s]['score'] = 0;
            $result[$s]['review_by']  = '---';
            $result[$s]['last_review'] = NULL;
            $review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result[$s]['id_contract']));
            $last_finalized_review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result[$s]['id_contract'],'order' => 'DESC','contract_review_status'=>'finished'));
            if(!empty($last_finalized_review)) {
                $result[$s]['review_by'] = $last_finalized_review[0]['review_by'];
                if($last_finalized_review[0]['review_on']!='---')
                    $result[$s]['last_review'] = date('Y-m-d',strtotime($last_finalized_review[0]['review_on']));
            }
            /*if(!empty($review)) {
                $result[$s]['review_by'] = $review[0]['review_by'];
                if($review[0]['review_on']!='---')
                    $result[$s]['last_review'] = date('Y-m-d',strtotime($review[0]['review_on']));
            }*/
            if(empty($result[$s]['id_contract_review']) || $result[$s]['id_contract_review']==NULL){
                $result[$s]['id_contract_review']=0;
            }
            $contract_review_id = 0;
            if($result[$s]['id_contract_review']!=0) {
                $contract_review_id = $result[$s]['id_contract_review'];
                $module_score = $this->Contract_model->getContractReviewModuleScore(array('contract_review_id' => $contract_review_id));
                $result[$s]['contract_progress'] =0;
                for($sr=0;$sr<count($module_score);$sr++)
                {
                    $result[$s]['contract_progress'] += $this->Contract_model->progress(array('module_id'=>$module_score[$sr]['module_id'],'contract_review_id'=>$contract_review_id));
                }
                if(count($module_score)>0)
                    $result[$s]['contract_progress'] = round($result[$s]['contract_progress']/count($module_score)).'%';
                else
                    $result[$s]['contract_progress'] = '0%';
            }
            else{
                $contract_review_id = isset($review[0]['id_contract_review'])?$review[0]['id_contract_review']:0;
            }
            if($contract_review_id!=0) {
                $module_score = $this->Contract_model->getContractReviewModuleScore(array('contract_review_id' => $result[$s]['id_contract_review']));
                for ($sr = 0; $sr < count($module_score); $sr++) {
                    $module_score[$sr]['score'] = getScoreByCount($module_score[$sr]);
                }

                $result[$s]['score'] = getScore($scope = array_map(function ($i) {
                    return strtolower($i['score']);
                }, $module_score));
            }
            $result[$s]['ideedi']=(count($this->Contract_model->getContractReviewDiscussionModuleCount(array('id_contract_review'=>$result[$s]['id_contract_review'],'discussion_status'=>1)))>0)?"itako":'annus';
            $result[$s]['idaadi']='annus';
            $idaadi=$this->Contract_model->checkContributorForContractReview(array('contract_review_id'=>$result[$s]['id_contract_review'],'id_user'=>$data['id_user']));
            if($data['user_role_id'] == 5 || $idaadi===true) {
                $result[$s]['idaadi']="itako";
            }
            else{
                $result[$s]['idaadi']=(count($this->Contract_model->getContractDiscussion(array('id_contract'=>$result[$s]['id_contract'])))>0)?"itako":'annus';
            }
            $result[$s]['contract_user_access']=$this->session_user_info->access;
            if($this->Contract_model->checkReviewUserAccess(array('contract_review_id'=>$result[$s]['id_contract_review'],'id_user'=>$this->session_user_info->id_user))>0)
                $result[$s]['contract_user_access']='co';
            ///Assigning edit access for contracts
            $result[$s]['ieaaei'] = 'annus';
            //checking is customer admin
            if($data['user_role_id'] == 2) {
                $result[$s]['ieaaei'] = 'itako';
            }//chechking is contract created_by or contract_owner or delegate
            else if($data['id_user'] == $result[$s]['created_by'] || $data['id_user'] == $result[$s]['contract_owner_id']  || $data['id_user'] == $result[$s]['delegate_id']) {
                $result[$s]['ieaaei'] = 'itako';
            }
            $result[$s]['business_unit_id']=pk_encrypt($result[$s]['business_unit_id']);
            $result[$s]['classification_id']=pk_encrypt($result[$s]['classification_id']);
            $result[$s]['contract_owner_id']=pk_encrypt($result[$s]['contract_owner_id']);
            $result[$s]['created_by']=pk_encrypt($result[$s]['created_by']);
            $result[$s]['currency_id']=pk_encrypt($result[$s]['currency_id']);
            $result[$s]['delegate_id']=pk_encrypt($result[$s]['delegate_id']);
            $result[$s]['id_contract']=pk_encrypt($result[$s]['id_contract']);
            $result[$s]['id_contract_review']=pk_encrypt($result[$s]['id_contract_review']);
            $result[$s]['parent_contract_id']=pk_encrypt($result[$s]['parent_contract_id']);
            $result[$s]['relationship_category_id']=pk_encrypt($result[$s]['relationship_category_id']);
            $result[$s]['updated_by']=pk_encrypt($result[$s]['updated_by']);
        }
        if(isset($data['contract_review_id'])){
            $result['progress'] = $this->Contract_model->contract_progress($data);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function relationshipCategory_get()
    {
        $data = $this->input->get();

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($data['customer_id']!=$this->session_user_info->customer_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['language_id'])){
            $data['language_id']=pk_decrypt($data['language_id']);
            if(!in_array($data['language_id'],$this->session_user_master_language)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(isset($data['id_relationship_category'])) {
            $data['id_relationship_category'] = pk_decrypt($data['id_relationship_category']);
            if(!in_array($data['id_relationship_category'],$this->session_user_customer_relationship_categories)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_relationship_category_not'])) {
            $data['id_relationship_category_not'] = pk_decrypt($data['id_relationship_category_not']);
            if(!in_array($data['id_relationship_category_not'],$this->session_user_customer_relationship_categories)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $result = $this->Relationship_category_model->getRelationshipCategory($data);
        foreach($result as $k=>$v){
            $result[$k]['created_by']=pk_encrypt($result[$k]['created_by']);
            $result[$k]['customer_id']=pk_encrypt($result[$k]['customer_id']);
            $result[$k]['id_relationship_category']=pk_encrypt($result[$k]['id_relationship_category']);
            $result[$k]['id_relationship_category_language']=pk_encrypt($result[$k]['id_relationship_category_language']);
            $result[$k]['language_id']=pk_encrypt($result[$k]['language_id']);
            $result[$k]['parent_relationship_category_id']=pk_encrypt($result[$k]['parent_relationship_category_id']);
            $result[$k]['relationship_category_id']=pk_encrypt($result[$k]['relationship_category_id']);
            $result[$k]['updated_by']=pk_encrypt($result[$k]['updated_by']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function relationshipClassification_get()
    {
        $data = $this->input->get();

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($data['customer_id']!=$this->session_user_info->customer_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['language_id'])){
            $data['language_id']=pk_decrypt($data['language_id']);
            if(!in_array($data['language_id'],$this->session_user_master_language)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['parent_classification_id'])) {
            $data['parent_classification_id'] = pk_decrypt($data['parent_classification_id']);
            if(!in_array($data['parent_classification_id'],$this->session_user_customer_relationship_classifications)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_relationship_classification_not'])) {
            $data['id_relationship_classification_not'] = pk_decrypt($data['id_relationship_classification_not']);
            if(!in_array($data['id_relationship_classification_not'],$this->session_user_customer_relationship_classifications)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $data['classification_position'] = 'y';
        $result = $this->Relationship_category_model->getRelationshipClassificationForContract($data);
        foreach($result as $k=>$v){
            $result[$k]['created_by']=pk_encrypt($result[$k]['created_by']);
            $result[$k]['customer_id']=pk_encrypt($result[$k]['customer_id']);
            $result[$k]['id_relationship_classification']=pk_encrypt($result[$k]['id_relationship_classification']);
            $result[$k]['id_relationship_classification_language']=pk_encrypt($result[$k]['id_relationship_classification_language']);
            $result[$k]['language_id']=pk_encrypt($result[$k]['language_id']);
            $result[$k]['parent_classification_id']=pk_encrypt($result[$k]['parent_classification_id']);
            $result[$k]['parent_relationship_classification_id']=pk_encrypt($result[$k]['parent_relationship_classification_id']);
            $result[$k]['relationship_classification_id']=pk_encrypt($result[$k]['relationship_classification_id']);
            $result[$k]['updated_by']=pk_encrypt($result[$k]['updated_by']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function reviewActionItems_get()
    {
        $data = $this->input->get();

        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'1');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['module_id'])) {
            $data['module_id'] = pk_decrypt($data['module_id']);
            if(!in_array($data['module_id'],$this->session_user_contract_review_modules)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'2');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['topic_id'])) {
            $data['topic_id'] = pk_decrypt($data['topic_id']);
            if(!in_array($data['topic_id'],$this->session_user_contract_review_topics)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'3');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])){
            $data['contract_review_id']=pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'4');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review'])){
            $data['id_contract_review']=pk_decrypt($data['id_contract_review']);
            if(!in_array($data['id_contract_review'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'5');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review_action_item'])) $data['id_contract_review_action_item']=pk_decrypt($data['id_contract_review_action_item']);
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        /*helper function for ordering smart table grid options*/
        $data = tableOptions($data);
        $data['item_status']=1;
        $result = $this->Contract_model->getContractReviewActionItems($data);
        for($s=0;$s<count($result['data']);$s++)
        {
            $result['data'][$s]['due_date'] = date('Y-m-d',strtotime($result['data'][$s]['due_date']));
            $contract_review = $this->Contract_model->getContractReview(array('id_contract_review'=>$result['data'][$s]['contract_review_id']));
            $result['data'][$s]['last_review'] = $contract_review[0]['updated_date'];
            $result['data'][$s]['contract_id']=pk_encrypt($result['data'][$s]['contract_id']);
            $result['data'][$s]['contract_review_id']=pk_encrypt($result['data'][$s]['contract_review_id']);
            $result['data'][$s]['created_by']=pk_encrypt($result['data'][$s]['created_by']);
            $result['data'][$s]['id_contract_review_action_item']=pk_encrypt($result['data'][$s]['id_contract_review_action_item']);
            $result['data'][$s]['module_id']=pk_encrypt($result['data'][$s]['module_id']);
            $result['data'][$s]['responsible_user_id']=pk_encrypt($result['data'][$s]['responsible_user_id']);
            $result['data'][$s]['topic_id']=pk_encrypt($result['data'][$s]['topic_id']);
            $result['data'][$s]['updated_by']=pk_encrypt($result['data'][$s]['updated_by']);
            $result['data'][$s]['user_role_id']=pk_encrypt($result['data'][$s]['user_role_id']);
            foreach($result['data'][$s]['comments_log'] as $kac=>$vac){
                $result['data'][$s]['comments_log'][$kac]['contract_review_action_item_id']=pk_encrypt($result['data'][$s]['comments_log'][$kac]['contract_review_action_item_id']);
                $result['data'][$s]['comments_log'][$kac]['id_contract_review_action_item_log']=pk_encrypt($result['data'][$s]['comments_log'][$kac]['id_contract_review_action_item_log']);
                $result['data'][$s]['comments_log'][$kac]['updated_by']=pk_encrypt($result['data'][$s]['comments_log'][$kac]['updated_by']);
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function ReviewActionItem_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $this->form_validator->add_rules('contract_review_id', array('required'=>$this->lang->line('contract_review_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=>$this->lang->line('module_id_req')));
        $this->form_validator->add_rules('topic_id', array('required'=>$this->lang->line('topic_id_req')));
        $this->form_validator->add_rules('action_item', array('required'=>$this->lang->line('action_item_req')));
        $this->form_validator->add_rules('responsible_user_id', array('required'=>$this->lang->line('responsible_user_id_req')));
        $this->form_validator->add_rules('due_date', array('required'=>$this->lang->line('due_date_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])){
            $data['contract_review_id']=pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['module_id'])) {
            $data['module_id'] = pk_decrypt($data['module_id']);
            if(!in_array($data['module_id'],$this->session_user_contract_review_modules)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['topic_id'])) {
            $data['topic_id'] = pk_decrypt($data['topic_id']);
            if(!in_array($data['topic_id'],$this->session_user_contract_review_topics)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['question_id'])) {
            $data['question_id'] = pk_decrypt($data['question_id']);
        }
        if(isset($data['responsible_user_id'])) {
            $data['responsible_user_id'] = pk_decrypt($data['responsible_user_id']);
            if(!in_array($data['responsible_user_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review_action_item'])) {
            $data['id_contract_review_action_item'] = pk_decrypt($data['id_contract_review_action_item']);
            if(!in_array($data['id_contract_review_action_item'],$this->session_user_contract_action_items)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $update = array(
            'contract_id' => $data['contract_id'],
            'action_item' => $data['action_item'],
            'responsible_user_id' => $data['responsible_user_id'],
            'due_date' => $data['due_date'],
            'contract_review_id' => $data['contract_review_id'],
            'module_id' => $data['module_id'],
            'topic_id' => $data['topic_id'],
            'question_id' => isset($data['question_id'])?$data['question_id']:''
        );
        if(isset($data['comments']))
            $update['comments']=$data['comments'];
        if(isset($data['description']))
            $update['description']=$data['description'];

        if(!isset($data['id_contract_review_action_item'])){
            $update['created_by'] = $data['created_by'];
            $update['created_on'] = currentDate();
            $this->Contract_model->addContractReviewActionItem($update);
            $msg = $this->lang->line('contract_review_action_item_add');
        }
        else{
            $update['id_contract_review_action_item'] = $data['id_contract_review_action_item'];
            $update['updated_by'] = $data['updated_by'];
            $update['updated_on'] = currentDate();
            $this->Contract_model->updateContractReviewActionItem($update);
            $msg = $this->lang->line('contract_review_action_item_update');
        }
        if(!isset($data['id_contract_review_action_item'])) {
            $module_info = $this->Module_model->getModuleName(array('language_id' => 1, 'module_id' => $data['module_id']));
            $contract_info = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
            $topic_info = $this->Topic_model->getTopicName(array('topic_id' => $data['topic_id']));
            $cust_admin_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['created_by']));
            $created_user_info = $this->User_model->getUserInfo(array('user_id' => $data['created_by']));
            $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $cust_admin_info->customer_id));
            /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
            $cust_admin = $cust_admin['data'][0];*/
            /* echo 'module'.'<pre>';print_r($module_info);
             echo 'contract_info'.'<pre>';print_r($contract_info);
             echo 'cust_admin'.'<pre>';print_r($cust_admin_info);
             echo 'customer_detail'.'<pre>';print_r($customer_details);
             echo 'to_id'.'<pre>';print_r($topic_info);exit;*/
            if ($customer_details[0]['company_logo'] == '') {
                $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
            } else {
                $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

            }
            if (!empty($customer_details)) {
                $customer_name = $customer_details[0]['company_name'];
            }

            $To = $this->User_model->getUserInfo(array('user_id' => $data['responsible_user_id']));
            $template_configurations_parent = $this->Customer_model->EmailTemplateList(array('customer_id' => $cust_admin_info->customer_id, 'language_id' => 1, 'module_key' => 'CONTRACT_REVIEW_ACTION_ITEM_CREATION'));
            if ($template_configurations_parent['total_records'] > 0) {
                $template_configurations = $template_configurations_parent['data'][0];
                $wildcards = $template_configurations['wildcards'];
                $wildcards_replaces = array();
                $wildcards_replaces['first_name'] = $To->first_name;
                $wildcards_replaces['last_name'] = $To->last_name;
                $wildcards_replaces['contract_name'] = $contract_info[0]['contract_name'];
                $wildcards_replaces['action_item_responsible_user'] = $To->first_name . ' ' . $To->last_name . ' (' . $To->user_role_name . ')';
                $wildcards_replaces['contract_review_module_name'] = $module_info[0]['module_name'];
                $wildcards_replaces['action_item_name'] = $data['action_item'];
                if (isset($data['description']))
                    $wildcards_replaces['action_item_description'] = $data['description'];
                $wildcards_replaces['action_item_due_date'] = dateFormat($data['due_date']);
                $wildcards_replaces['contract_review_topic_name'] = $topic_info[0]['topic_name'];
                $wildcards_replaces['action_item_created_user_name'] = $created_user_info->first_name . ' ' . $created_user_info->last_name . ' (' . $created_user_info->user_role_name . ')';
                $wildcards_replaces['action_item_created_date'] = dateFormat($update['created_on']);
                $wildcards_replaces['logo'] = $customer_logo;
                $wildcards_replaces['url'] = WEB_BASE_URL . 'html';
                $body = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_content']);
                $subject = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_subject']);
                /*$from_name=SEND_GRID_FROM_NAME;
                $from=SEND_GRID_FROM_EMAIL;
                $from_name=$cust_admin['name'];
                $from=$cust_admin['email'];*/
                $from_name = $template_configurations['email_from_name'];
                $from = $template_configurations['email_from'];
                $to = $To->email;
                $to_name = $To->first_name . ' ' . $To->last_name;
                $mailer_data['mail_from_name'] = $from_name;
                $mailer_data['mail_to_name'] = $to_name;
                $mailer_data['mail_to_user_id'] = $To->id_user;
                $mailer_data['mail_from'] = $from;
                $mailer_data['mail_to'] = $to;
                $mailer_data['mail_subject'] = $subject;
                $mailer_data['mail_message'] = $body;
                $mailer_data['status'] = 0;
                $mailer_data['send_date'] = currentDate();
                $mailer_data['is_cron'] = 0;
                $mailer_data['email_template_id'] = $template_configurations['id_email_template'];
                //print_r($mailer_data);
                $mailer_id = $this->Customer_model->addMailer($mailer_data);
                //sending mail to bu owner
                if ($mailer_data['is_cron'] == 0) {
                    //$mail_sent_status=sendmail($to, $subject, $body, $from);
                    $this->load->library('sendgridlibrary');
                    $mail_sent_status = $this->sendgridlibrary->sendemail($from_name, $from, $subject, $body, $to_name, $to, array(), $mailer_id);
                    if ($mail_sent_status == 1)
                        $this->Customer_model->updateMailer(array('status' => 1, 'mailer_id' => $mailer_id));
                }

            }
        }
        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function ReviewActionItemDelete_delete()
    {
        $data = $this->input->get();
        $msg='';
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_contract_review_action_item', array('required'=>$this->lang->line('id_contract_review_action_item_req')));
        $this->form_validator->add_rules('updated_by', array('required'=>$this->lang->line('updated_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_contract_review_action_item'])) {
            $data['id_contract_review_action_item'] = pk_decrypt($data['id_contract_review_action_item']);
            if(!in_array($data['id_contract_review_action_item'],$this->session_user_contract_action_items)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['updated_by'])) {
            $data['updated_by'] = pk_decrypt($data['updated_by']);
            if($data['updated_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $delete_access=0;
        $res_del=$this->Contract_model->getContractReviewActionItems(array('id_contract_review_action_item'=>$data['id_contract_review_action_item']));
        if(isset($res_del['data'][0]['id_contract_review_action_item'])) {
            if (isset($this->session_user_info->id_user) && isset($this->session_user_info->user_role_id)) {
                if ($this->session_user_info->user_role_id == 6 || $this->session_user_info->user_role_id == 5) {
                    if ($res_del['data'][0]['created_by'] == $this->session_user_info->id_user) {
                        $delete_access = 1;
                    }

                } else if ($this->session_user_info->user_role_id == 4 || $this->session_user_info->user_role_id == 3 || $this->session_user_info->user_role_id == 2 || $this->session_user_info->user_role_id == 1) {

                    if ($res_del['data'][0]['created_by'] == $this->session_user_info->id_user || $res_del['data'][0]['user_role_id'] > $this->session_user_info->user_role_id) {
                        $delete_access = 1;
                    }

                }
            } else {
                $delete_access = 1;
            }
            if ($res_del['data'][0]['status'] == 'completed')
                $delete_access = 0;
        }
        else{
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($delete_access==0){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_contract_review_action_item'])){
            $update['id_contract_review_action_item'] = $data['id_contract_review_action_item'];
            $update['updated_by'] = $data['updated_by'];
            $update['updated_on'] = currentDate();
            $update['item_status'] = 0;
            $this->Contract_model->updateContractReviewActionItem($update);
            $msg = $this->lang->line('contract_review_action_item_delete');
        }


        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function ReviewActionItemUpdate_post()
    {
        $data = $this->input->post();
        //echo '<pre>';print_r($data);exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_contract_review_action_item', array('required'=>$this->lang->line('id_contract_review_action_item_req')));
        $this->form_validator->add_rules('updated_by', array('required'=>$this->lang->line('updated_by_req')));
        $this->form_validator->add_rules('is_finish', array('required'=>$this->lang->line('is_finish_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_contract_review_action_item'])) {
            $data['id_contract_review_action_item'] = pk_decrypt($data['id_contract_review_action_item']);
            if(!in_array($data['id_contract_review_action_item'],$this->session_user_contract_action_items)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['updated_by'])) {
            $data['updated_by'] = pk_decrypt($data['updated_by']);
            if($data['updated_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                //$this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $update = array();

        if(isset($data['id_contract_review_action_item'])){
            $update['id_contract_review_action_item'] = $data['id_contract_review_action_item'];
            $update['updated_by'] = $data['updated_by'];
            $update['updated_on'] = currentDate();
            if($data['is_finish']==1)
                $update['status'] = 'completed';
            if(isset($data['comments']))
                $update['comments'] = $data['comments'];

            //$current_records = $this->Contract_model->getActionItemDetails(array('id_contract_review_action_item' => $data['id_contract_review_action_item']));


            $this->Contract_model->updateContractReviewActionItem($update);
            $msg = $this->lang->line('contract_review_action_item_update');
        }
        $action_item_info = $this->Contract_model->getContractReviewActionItems(array('id_contract_review_action_item'=>$data['id_contract_review_action_item']));
        //$module_info = $this->Module_model->getModuleName(array('language_id'=>1,'module_id'=>$data['module_id']));
        $contract_info = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
        //$topic_info = $this->Topic_model->getTopicName(array('topic_id'=>$data['topic_id']));
        $cust_admin_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['created_by']));
        $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $cust_admin_info->customer_id));
        /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
        $cust_admin = $cust_admin['data'][0];*/
        $action_item = $action_item_info['data'][0];
         /*echo 'action_info'.'<pre>';print_r($action_item);
         echo 'contract_info'.'<pre>';print_r($contract_info);
         echo 'cust_admin'.'<pre>';print_r($cust_admin_info);
         echo 'customer_detail'.'<pre>';print_r($customer_details);
         echo 'to_id'.'<pre>';print_r($topic_info);exit;*/
        if($customer_details[0]['company_logo']=='') {
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
        }
        else{
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

        }
        if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }

        $To = $this->Contract_model->getActionItemDetails(array('id_contract_review_action_item' => $data['id_contract_review_action_item']));
        $user_info = $this->User_model->getUserInfo(array('user_id' => $To[0]['created_by']));
        $commented_by = $this->User_model->getUserInfo(array('user_id' => $To[0]['updated_by']));
        $resoponsible_user_info = $this->User_model->getUserInfo(array('user_id' => $To[0]['responsible_user_id']));
      /* echo '<pre>';print_r($To);
        echo '<pre>';print_r($user_info);
        echo '<pre>';print_r($resoponsible_user_info);exit;*/
        $template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $cust_admin_info->customer_id,'language_id' =>1,'module_key'=>'CONTRACT_REVIEW_ACTION_ITEM_COMMENT'));
        if($template_configurations_parent['total_records']>0){
            $template_configurations=$template_configurations_parent['data'][0];
            $wildcards=$template_configurations['wildcards'];
            $wildcards_replaces=array();
            $wildcards_replaces['first_name']=$user_info->first_name;
            $wildcards_replaces['last_name']=$user_info->last_name;
            $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
            $wildcards_replaces['action_item_responsible_user']=$resoponsible_user_info->first_name.' '.$resoponsible_user_info->last_name.' ('.$resoponsible_user_info->user_role_name.')';
            $wildcards_replaces['contract_review_module_name']=$action_item['module_name'];
            $wildcards_replaces['action_item_name']=$action_item['action_item'];
            if(isset($To[0]['description']))
                $wildcards_replaces['action_item_description']=$To[0]['description'];
            $wildcards_replaces['action_item_due_date']=dateFormat($To[0]['due_date']);
            $wildcards_replaces['action_item_comment']=$To[0]['comments'];
            $wildcards_replaces['action_item_comment_user_name']=$commented_by->first_name.' '.$commented_by->last_name.' ('.$commented_by->user_role_name.')';
            $wildcards_replaces['action_item_comment_date']=dateFormat($To[0]['updated_on']);
            $wildcards_replaces['contract_review_topic_name']=$action_item['topic_name'];
            $wildcards_replaces['logo']=$customer_logo;
            $wildcards_replaces['url']=WEB_BASE_URL.'html';
            $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
            $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
            /*$from_name=SEND_GRID_FROM_NAME;
            $from=SEND_GRID_FROM_EMAIL;
            $from_name=$cust_admin['name'];
            $from=$cust_admin['email'];*/
            $from_name=$template_configurations['email_from_name'];
            $from=$template_configurations['email_from'];
            $to=$user_info->email;
            $to_name=$user_info->first_name.' '.$user_info->last_name;
            $mailer_data['mail_from_name']=$from_name;
            $mailer_data['mail_to_name']=$to_name;
            $mailer_data['mail_to_user_id']=$user_info->id_user;
            $mailer_data['mail_from']=$from;
            $mailer_data['mail_to']=$to;
            $mailer_data['mail_subject']=$subject;
            $mailer_data['mail_message']=$body;
            $mailer_data['status']=0;
            $mailer_data['send_date']=currentDate();
            $mailer_data['is_cron']=0;
            $mailer_data['email_template_id']=$template_configurations['id_email_template'];
            //print_r($mailer_data);
            $mailer_id=$this->Customer_model->addMailer($mailer_data);
            if($mailer_data['is_cron']==0) {
                //$mail_sent_status=sendmail($to, $subject, $body, $from);
                $this->load->library('sendgridlibrary');
                $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                if($mail_sent_status==1)
                    $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
            }

        }
        if($data['is_finish']==1){
            $finish_user = $this->User_model->getUserInfo(array('user_id' => $data['updated_by']));
            $template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $cust_admin_info->customer_id,'language_id' =>1,'module_key'=>'CONTRACT_REVIEW_ACTION_ITEM_FINISH'));
            if($template_configurations_parent['total_records']>0){
                $template_configurations=$template_configurations_parent['data'][0];
                $wildcards=$template_configurations['wildcards'];
                $wildcards_replaces=array();
                $wildcards_replaces['first_name']=$user_info->first_name;
                $wildcards_replaces['last_name']=$user_info->last_name;
                $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                $wildcards_replaces['action_item_responsible_user']=$resoponsible_user_info->first_name.' '.$resoponsible_user_info->last_name.' ('.$resoponsible_user_info->user_role_name.')';
                $wildcards_replaces['contract_review_module_name']=$action_item['module_name'];
                $wildcards_replaces['action_item_name']=$action_item['action_item'];
                if(isset($To[0]['description']))
                    $wildcards_replaces['action_item_description']=$To[0]['description'];
                $wildcards_replaces['action_item_due_date']=dateFormat($To[0]['due_date']);
                $wildcards_replaces['action_item_finish_user_name']=$finish_user->first_name.' '.$finish_user->last_name.' ('.$finish_user->user_role_name.')';
                $wildcards_replaces['action_item_finish_date']=dateFormat($update['updated_on']);
                $wildcards_replaces['contract_review_topic_name']=$action_item['topic_name'];
                $wildcards_replaces['logo']=$customer_logo;
                $wildcards_replaces['url']=WEB_BASE_URL.'html';
                $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                /*$from_name=SEND_GRID_FROM_NAME;
                $from=SEND_GRID_FROM_EMAIL;
                $from_name=$cust_admin['name'];
                $from=$cust_admin['email'];*/
                $from_name=$template_configurations['email_from_name'];
                $from=$template_configurations['email_from'];
                $to=$user_info->email;
                $to_name=$user_info->first_name.' '.$user_info->last_name;
                $mailer_data['mail_from_name']=$from_name;
                $mailer_data['mail_to_name']=$to_name;
                $mailer_data['mail_to_user_id']=$user_info->id_user;
                $mailer_data['mail_from']=$from;
                $mailer_data['mail_to']=$to;
                $mailer_data['mail_subject']=$subject;
                $mailer_data['mail_message']=$body;
                $mailer_data['status']=0;
                $mailer_data['send_date']=currentDate();
                $mailer_data['is_cron']=0;
                $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                //print_r($mailer_data);
                $mailer_id=$this->Customer_model->addMailer($mailer_data);
                if($mailer_data['is_cron']==0) {
                    //$mail_sent_status=sendmail($to, $subject, $body, $from);
                    $this->load->library('sendgridlibrary');
                    $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                    if($mail_sent_status==1)
                        $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                }

            }

        }

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contractContributor_post()
    {
        $data = $this->input->post();
       // print_r($data);exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=>$this->lang->line('module_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        //$this->form_validator->add_rules('contributors_add', array('required'=>$this->lang->line('contributors_add')));
        //$this->form_validator->add_rules('contributors_remove', array('required'=>$this->lang->line('contributors_remove')));
        $data['contributors_add']=isset($data['contributors_add'])?$data['contributors_add']:'';
        $data['contributors_remove']=isset($data['contributors_remove'])?$data['contributors_remove']:'';
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['module_id'])) {
            $data['module_id'] = pk_decrypt($data['module_id']);
            if(!in_array($data['module_id'],$this->session_user_contract_review_modules)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract'])) {
            $data['id_contract'] = pk_decrypt($data['id_contract']);
            if(!in_array($data['id_contract'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['topic_id'])) {
            $data['topic_id'] = pk_decrypt($data['topic_id']);
            if(!in_array($data['topic_id'],$this->session_user_contract_review_topics)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if($data['contract_review_id']>0 && !in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>"You don't have permissions to this module 8", 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $contributors_add_exp=explode(',',$data['contributors_add']);
        $contributors_add_exp_new=array();
        foreach($contributors_add_exp as $k=>$v){
            $contributors_add_exp_new[]=$cntr=pk_decrypt($v);
            /*if(!in_array($cntr,$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>"You don't have permissions to this module 9", 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }*/
        }
        $data['contributors_add']=implode(',',$contributors_add_exp_new);
        /*echo "<pre>";print_r($data);echo "</pre>";
        exit;*/

        $contributors_remove_exp=explode(',',$data['contributors_remove']);
        $contributors_remove_exp_new=array();
        foreach($contributors_remove_exp as $k=>$v){
            $contributors_remove_exp_new[]=$cntr=pk_decrypt($v);
            /*if($cntr>0 && !in_array($cntr,$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }*/
        }
        $data['contributors_remove']=implode(',',$contributors_remove_exp_new);

        $update = array(
            'contract_id' => $data['contract_id'],
            'created_by' => $data['created_by'],
            'contributors_add' => explode(',',$data['contributors_add']),
            'contributors_remove' => explode(',',$data['contributors_remove']),
            'module_id' => $data['module_id'],
            'created_on' => currentDate()
        );
        if(isset($data['contract_review_id']))
            $update['contract_review_id']=$data['contract_review_id'];

        $to_id = $this->Contract_model->addContractContributors($update);
        $bu_info = $this->Contract_model->getContractCurrentDetails(array('contract_id'=>$update['contract_id']));
        /*if(count($to_id)>0)
            $this->Business_unit_model->addBusinessUnitUser(array('status'=>1,'created_by'=>$update['created_by'],'business_unit_id'=>$bu_info[0]['business_unit_id'],'users'=>$to_id));*/

        $msg = 'Contributor Updated Successfully.';

        //Mailing...
        $module_info = $this->Module_model->getModuleName(array('language_id'=>1,'module_id'=>$data['module_id']));
        $contract_info = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
        $cust_admin_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['created_by']));
        $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $cust_admin_info->customer_id));
        /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
       $cust_admin = $cust_admin['data'][0];*/

        if($customer_details[0]['company_logo']=='') {
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
        }
        else{
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

        }
        if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }
        foreach($to_id as $k => $v)
        {
            $To = $this->User_model->getUserInfo(array('user_id' => $to_id[$k]));

            //sending mail to bu owner
            $template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $cust_admin_info->customer_id,'language_id' =>1,'module_key'=>'CONTRACT_REVIEW_ASSIGN_MODULE'));
            if($template_configurations_parent['total_records']>0){
                $template_configurations=$template_configurations_parent['data'][0];
                $wildcards=$template_configurations['wildcards'];
                $wildcards_replaces=array();
                $wildcards_replaces['first_name']=$To->first_name;
                $wildcards_replaces['last_name']=$To->last_name;
                $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                $wildcards_replaces['contract_review_assigned_module_user_name']=$To->first_name.' '.$To->last_name.' ('.$To->user_role_name.')';
                $wildcards_replaces['module_name']=$module_info[0]['module_name'];
                $wildcards_replaces['logo']=$customer_logo;
                $wildcards_replaces['url']=WEB_BASE_URL.'html';
                $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                /*$from_name=SEND_GRID_FROM_NAME;
                $from=SEND_GRID_FROM_EMAIL;
                $from_name=$cust_admin['name'];
                $from=$cust_admin['email'];*/
                $from_name=$template_configurations['email_from_name'];
                $from=$template_configurations['email_from'];
                $to=$To->email;
                $to_name=$To->first_name.' '.$To->last_name;
                $mailer_data['mail_from_name']=$from_name;
                $mailer_data['mail_to_name']=$to_name;
                $mailer_data['mail_to_user_id']=$To->id_user;
                $mailer_data['mail_from']=$from;
                $mailer_data['mail_to']=$to;
                $mailer_data['mail_subject']=$subject;
                $mailer_data['mail_message']=$body;
                $mailer_data['status']=0;
                $mailer_data['send_date']=currentDate();
                $mailer_data['is_cron']=0;
                $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                //print_r($mailer_data);
                $mailer_id=$this->Customer_model->addMailer($mailer_data);
                //sending mail to bu owner
                if($mailer_data['is_cron']==0) {
                    //$mail_sent_status=sendmail($to, $subject, $body, $from);
                    $this->load->library('sendgridlibrary');
                    $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                    if($mail_sent_status==1)
                        $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                }

            }
        }
        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function users_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['type']) && $data['type']=='buowner'){

        }
        else
            $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id'])) {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['business_unit_id'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $user_role_id = 0;
        if(isset($data['user_role_id'])){ $user_role_id = $data['user_role_id']; unset($data['user_role_id']); }

        if(isset($data['type']) && $data['type']=='buowner'){
            $data['user_role_id'] = 3;
        }
        else {
            $contract_details = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
            $data['business_unit_id'] = $contract_details[0]['business_unit_id'];
            //echo '<pre>';print_r($contract_details);
            //$data['user_role_id'] = 5; //to get only contributors
        }
        if(isset($data['type']) && $data['type']=='contributor'){
            $data['user_role_id']=5;
            unset($data['business_unit_id']);
            $user_role_id = 0;
        }
        $result = $this->Contract_model->getBusinessUnitUsers($data);
        if(isset($data['customer_id']) && $user_role_id==2){
            $customer_users = $this->Contract_model->getCustomerUsers(array('customer_id' => $data['customer_id'],'user_role_id' => $user_role_id));
            $result = array_merge($customer_users,$result);
        }
        foreach($result as $k=>$v){
            $result[$k]['id_user']=pk_encrypt($result[$k]['id_user']);
            $result[$k]['user_role_id']=pk_encrypt($result[$k]['user_role_id']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function reviewlevelusers_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['type']) && $data['type']=='buowner'){

        }
        else
            $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id'])) {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            /*if(!in_array($data['business_unit_id'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }*/
        } //commented to skip this condition for contributor add
        $user_role_id = 0;
        if(isset($data['user_role_id'])){
            $user_role_id = $data['user_role_id'];
            //unset($data['user_role_id']);
        }

        if(isset($data['type']) && $data['type']=='buowner'){
            $data['user_role_id'] = 3;
        }
        else if($data['type']=='!contributor') {
            $contract_details = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
            $data['business_unit_id'] = $contract_details[0]['business_unit_id'];
            //echo '<pre>';print_r($contract_details);
            //$data['user_role_id'] = 5; //to get only contributors
        }

        $result = $this->Contract_model->getCustomerUsers_add($data);

        foreach($result as $k=>$v){
            $result[$k]['id_user']=pk_encrypt($result[$k]['id_user']);
            $result[$k]['user_role_id']=pk_encrypt($result[$k]['user_role_id']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contractreviewusers_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        //$this->form_validator->add_rules('contract_review_id', array('required'=>$this->lang->line('contract_review_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=>$this->lang->line('module_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['module_id'])) {
            $data['module_id'] = pk_decrypt($data['module_id']);
            if(!in_array($data['module_id'],$this->session_user_contract_review_modules)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_id'])) {
            $data['user_id'] = pk_decrypt($data['user_id']);
            if($data['user_id']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['type']) && $data['type']=='buowner'){
            $data['user_role_id'] = 3;
        }
        else {
            $data['user_role_id'] = 5;
        }
        $result = $this->Contract_model->getContractReviewUsers($data);
        foreach($result as $k=>$v){
            $result[$k]['id_user']=pk_encrypt($result[$k]['id_user']);
            $result[$k]['user_role_id']=pk_encrypt($result[$k]['user_role_id']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function initializeReview_get()
    {
        $data = $this->input->get();
        //echo 'data'.'<pre>';print_r($data);
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'1');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'2');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'3');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'4');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $check_contract_review = $this->Contract_model->getContractReview(array(
            'contract_id' => $data['contract_id'],
            'status' => 'review in progress'
        ));

        if(!empty($check_contract_review)){
            $this->Contract_model->updateContract(array(
                'id_contract' => $data['contract_id'],
                'contract_status' => 'review in progress', //pending review from 2 time
                'updated_by' => $data['created_by'],
                'updated_on' => currentDate(),
                'reminder_type' => NULL,
                'reminder_sent_on' => NULL,
                'reminder_date1' => NULL,
                'reminder_date2' => NULL,
                'reminder_date3' => NULL
            ));

            $result = array('status'=>TRUE, 'message' => $this->lang->line('review_initiate'), 'data'=>$check_contract_review[0]['id_contract_review']);
           // $this->response($result, REST_Controller::HTTP_OK); exit;
        }

        $this->Contract_model->updateContract(array(
            'id_contract' => $data['contract_id'],
            'contract_status' => 'review in progress',
            'updated_by' => $data['created_by'],
            'updated_on' => currentDate(),
            'reminder_type' => NULL,
            'reminder_sent_on' => NULL,
            'reminder_date1' => NULL,
            'reminder_date2' => NULL,
            'reminder_date3' => NULL
        ));
        $review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $data['contract_id'],'order' => 'DESC'));
        if(!empty($review) && isset($review[0]['id_contract_review']) && $review[0]['id_contract_review']!='' && $review[0]['id_contract_review']!=0) {
            $previous_review_id=$review[0]['id_contract_review'];
        }
        $contract_info = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
        $data['contract_review_id'] = $this->Contract_model->addContractReview(array(
                                        'contract_id' => $data['contract_id'],
                                        'contract_review_due_date' => currentDate(),
                                        'contract_review_type' => isset($data['contract_review_type'])?$data['contract_review_type']:'',
                                        'created_by' => $data['created_by'],
                                        'created_on' => currentDate(),
                                        'relationship_category_id' =>$contract_info[0]['relationship_category_id']
                                     ));

        //$this->Contract_model->cloneModuleTopicQuestionForContract($data);
        //after template assigning we are considering customer relationship_category_id
        //$parent_relationship_category_info=$this->Relationship_category_model->getRelationshipCategory(array('id_relationship_category'=>$contract_info[0]['relationship_category_id']));
        //$data['parent_relationship_category_id']=$parent_relationship_category_info[0]['parent_relationship_category_id'];
        $data['parent_relationship_category_id']=$contract_info[0]['relationship_category_id'];
        $this->Contract_model->cloneModuleTopicQuestionForContractNew($data);
        $bu_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['contract_owner_id']));
        $contract_review_info = $this->Contract_model->getContractReview(array('id_contract_review' => $data['contract_review_id']));
        $cust_admin_info = $this->User_model->getUserInfo(array('customer_id' => $data['customer_id'],'user_role_id' =>2));
        $contract_review_user = $this->User_model->getUserInfo(array('user_id' => $contract_review_info[0]['created_by']));
        $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $cust_admin_info->customer_id));
        if($customer_details[0]['company_logo']=='') {
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
        }
        else{
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);
        }
        if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }
        if(isset($previous_review_id)){
            ///saving previous review questions here $this->session_user_id
            /*$previous_review_questions = $this->Contract_model->getPreviousReviewQuestions($previous_review_id);
            //echo '<pre>'.print_r($previous_review_questions);exit;
            foreach($previous_review_questions as $k => $v){
                if($v['question_type'] == 'input'){
                    $current_review_question_id = $this->Contract_model->getCurrentReviewQuestion1($data['contract_review_id'],$v['parent_question_id']);
                    $add[] = array(
                        'contract_review_id' => $data['contract_review_id'],
                        'question_id' => $current_review_question_id['id_question'],
                        'parent_question_id' => $v['parent_question_id'],
                        'question_answer' => $v['question_answer'],
                        'question_feedback' => $v['question_feedback'],
                        'updated_by' => $data['created_by'],
                        'updated_on' => currentDate(),
                        'question_option_id' => isset($v['question_option_id'])?$v['question_option_id']:NULL
                    );
                }else{
                    $current_review_question_id = $this->Contract_model->getCurrentReviewQuestion($data['contract_review_id'],$v['parent_question_id'],$v['question_answer']);
                    $add[] = array(
                        'contract_review_id' => $data['contract_review_id'],
                        'question_id' => $current_review_question_id['id_question'],
                        'parent_question_id' => $v['parent_question_id'],
                        'question_answer' => $v['question_answer'],
                        'question_feedback' => $v['question_feedback'],
                        'updated_by' => $data['created_by'],
                        'updated_on' => currentDate(),
                        'question_option_id' => isset($v['question_option_id'])?$current_review_question_id['id_question_option']:NULL
                    );
                }

            }
            if(!empty($add)){
                $this->Contract_model->addReviewQuestionAnswer_bulk($add);
            }
            ///*/
            $migrate['old_contract_review_id']=$previous_review_id;
            $migrate['new_contract_review_id']=$data['contract_review_id'];
            $migrate['created_by']=$data['created_by'];
            $migrate_modules=$this->Contract_model->migrateContractUsersFromOldReview($migrate);
            $migrate_modules_array=array();
            foreach($migrate_modules as $km=>$vm){
                $migrate_modules_array[]=$vm['user_id'];
            }
            $migrate_modules_array=array_values(array_unique($migrate_modules_array));
            $contract_info = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
            /*$cust_admin_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['created_by']));
            $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $cust_admin_info->customer_id));*/
            /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
            $cust_admin = $cust_admin['data'][0];*/

            /*$template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $cust_admin_info->customer_id,'language_id' =>1,'module_key'=>'CONTRACT_REVIEW_ASSIGN_MODULE'));
            if($template_configurations_parent['total_records']>0) {
                foreach ($migrate_modules as $k => $v) {
                    $module_info = $this->Module_model->getModuleName(array('language_id' => 1, 'module_id' => $v['id_module']));
                    $To = $this->User_model->getUserInfo(array('user_id' => $v['user_id']));
                    //sending mail to bu owner

                    if ($template_configurations_parent['total_records'] > 0) {
                        $template_configurations = $template_configurations_parent['data'][0];
                        $wildcards = $template_configurations['wildcards'];
                        $wildcards_replaces = array();
                        $wildcards_replaces['first_name'] = $To->first_name;
                        $wildcards_replaces['last_name'] = $To->last_name;
                        $wildcards_replaces['contract_name'] = $contract_info[0]['contract_name'];
                        $wildcards_replaces['contract_review_assigned_module_user_name'] = $To->first_name . ' ' . $To->last_name . ' (' . $To->user_role_name . ')';
                        $wildcards_replaces['module_name'] = $module_info[0]['module_name'];
                        $wildcards_replaces['logo'] = $customer_logo;
                        $wildcards_replaces['url'] = WEB_BASE_URL . 'html';
                        $body = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_content']);
                        $subject = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_subject']);
                        //$from_name = SEND_GRID_FROM_NAME;
                        //$from = SEND_GRID_FROM_EMAIL;
                        //$from_name=$cust_admin['name'];
                        //$from=$cust_admin['email'];
                        $from_name=$template_configurations['email_from_name'];
                        $from=$template_configurations['email_from'];
                        $to = $To->email;
                        $to_name = $To->first_name . ' ' . $To->last_name;
                        $mailer_data['mail_from_name'] = $from_name;
                        $mailer_data['mail_to_name'] = $to_name;
                        $mailer_data['mail_to_user_id']=$To->id_user;
                        $mailer_data['mail_from'] = $from;
                        $mailer_data['mail_to'] = $to;
                        $mailer_data['mail_subject'] = $subject;
                        $mailer_data['mail_message'] = $body;
                        $mailer_data['status'] = 0;
                        $mailer_data['send_date'] = currentDate();
                        $mailer_data['is_cron'] = 0;
                        $mailer_data['email_template_id'] = $template_configurations['id_email_template'];
                        //print_r($mailer_data);
                        $mailer_id = $this->Customer_model->addMailer($mailer_data);
                        //sending mail to bu owner
                        if ($mailer_data['is_cron'] == 0) {
                            //$mail_sent_status=sendmail($to, $subject, $body, $from);
                            $this->load->library('sendgridlibrary');
                            $mail_sent_status = $this->sendgridlibrary->sendemail($from_name, $from, $subject, $body, $to_name, $to, array(), $mailer_id);
                            if ($mail_sent_status == 1)
                                $this->Customer_model->updateMailer(array('status' => 1, 'mailer_id' => $mailer_id));
                        }

                    }
                }
            }*/
            $template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $cust_admin_info->customer_id,'language_id' =>1,'module_key'=>'CONTRACT_REVIEW_INITIATE'));
            if($template_configurations_parent['total_records']>0) {
                foreach ($migrate_modules_array as $k => $v) {
                    //$module_info = $this->Module_model->getModuleName(array('language_id' => 1, 'module_id' => $v['id_module']));
                    $To = $this->User_model->getUserInfo(array('user_id' => $v));
                    //sending mail to bu owner

                    if ($template_configurations_parent['total_records'] > 0) {
                        $template_configurations = $template_configurations_parent['data'][0];
                        $wildcards = $template_configurations['wildcards'];
                        $wildcards_replaces = array();
                        $wildcards_replaces['first_name'] = $To->first_name;
                        $wildcards_replaces['last_name'] = $To->last_name;
                        $wildcards_replaces['contract_name'] = $contract_info[0]['contract_name'];
                        $wildcards_replaces['contract_review_initiated_user_name']=$contract_review_user->first_name.' '.$contract_review_user->last_name.' ('.$contract_review_user->user_role_name.')';
                        $wildcards_replaces['contract_review_created_date']=dateFormat($contract_review_info[0]['created_on']);
                        $wildcards_replaces['logo'] = $customer_logo;
                        $wildcards_replaces['url'] = WEB_BASE_URL . 'html';
                        $body = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_content']);
                        $subject = wildcardreplace($wildcards, $wildcards_replaces, $template_configurations['template_subject']);
                        //$from_name = SEND_GRID_FROM_NAME;
                        //$from = SEND_GRID_FROM_EMAIL;
                        //$from_name=$cust_admin['name'];
                        //$from=$cust_admin['email'];
                        $from_name=$template_configurations['email_from_name'];
                        $from=$template_configurations['email_from'];
                        $to = $To->email;
                        $to_name = $To->first_name . ' ' . $To->last_name;
                        $mailer_data['mail_from_name'] = $from_name;
                        $mailer_data['mail_to_name'] = $to_name;
                        $mailer_data['mail_to_user_id'] = $To->id_user;
                        $mailer_data['mail_from'] = $from;
                        $mailer_data['mail_to'] = $to;
                        $mailer_data['mail_subject'] = $subject;
                        $mailer_data['mail_message'] = $body;
                        $mailer_data['status'] = 0;
                        $mailer_data['send_date'] = currentDate();
                        $mailer_data['is_cron'] = 0;
                        $mailer_data['email_template_id'] = $template_configurations['id_email_template'];
                        //print_r($mailer_data);
                        $mailer_id = $this->Customer_model->addMailer($mailer_data);
                        //sending mail to bu owner
                        if ($mailer_data['is_cron'] == 0) {
                            //$mail_sent_status=sendmail($to, $subject, $body, $from);
                            $this->load->library('sendgridlibrary');
                            $mail_sent_status = $this->sendgridlibrary->sendemail($from_name, $from, $subject, $body, $to_name, $to, array(), $mailer_id);
                            if ($mail_sent_status == 1)
                                $this->Customer_model->updateMailer(array('status' => 1, 'mailer_id' => $mailer_id));
                        }

                    }
                }
            }


        }

        /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
        $cust_admin = $cust_admin['data'][0];*/
        /*echo 'cust_admin'.'<pre>';print_r($contract_info);
        echo 'contract_review_info'.'<pre>';print_r($contract_review_info);
        echo 'buinfo'.'<pre>';print_r($bu_info);
        echo 'contract_Review_user'.'<pre>';print_r($contract_review_user);exit;*/
        if($customer_details[0]['company_logo']=='') {
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
        }
        else{
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

        }
        if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }
        //sending mail to bu owner
        $template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $cust_admin_info->customer_id,'language_id' =>1,'module_key'=>'CONTRACT_REVIEW_INITIATE'));
        if($template_configurations_parent['total_records']>0){
            $template_configurations=$template_configurations_parent['data'][0];
            $wildcards=$template_configurations['wildcards'];
            $wildcards_replaces=array();
            $wildcards_replaces['first_name']=$cust_admin_info->first_name;
            $wildcards_replaces['last_name']=$cust_admin_info->last_name;
            $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
            $wildcards_replaces['contract_review_initiated_user_name']=$contract_review_user->first_name.' '.$contract_review_user->last_name.' ('.$contract_review_user->user_role_name.')';
            $wildcards_replaces['contract_review_created_date']=dateFormat($contract_review_info[0]['created_on']);
            $wildcards_replaces['logo']=$customer_logo;
            $wildcards_replaces['url']=WEB_BASE_URL.'html';
            $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
            $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
            /*$from_name=SEND_GRID_FROM_NAME;
            $from=SEND_GRID_FROM_EMAIL;
            $from_name=$cust_admin['name'];
            $from=$cust_admin['email'];*/
            $from_name=$template_configurations['email_from_name'];
            $from=$template_configurations['email_from'];
            $to=$cust_admin_info->email;
            $to_name=$cust_admin_info->first_name.' '.$cust_admin_info->last_name;
            $mailer_data['mail_from_name']=$from_name;
            $mailer_data['mail_to_name']=$to_name;
            $mailer_data['mail_to_user_id']=$cust_admin_info->id_user;
            $mailer_data['mail_from']=$from;
            $mailer_data['mail_to']=$to;
            $mailer_data['mail_subject']=$subject;
            $mailer_data['mail_message']=$body;
            $mailer_data['status']=0;
            $mailer_data['send_date']=currentDate();
            $mailer_data['is_cron']=0;
            $mailer_data['email_template_id']=$template_configurations['id_email_template'];
            //print_r($mailer_data);
            $mailer_id=$this->Customer_model->addMailer($mailer_data);
            //sending mail to bu owner
            if($mailer_data['is_cron']==0) {
                //$mail_sent_status=sendmail($to, $subject, $body, $from);
                $this->load->library('sendgridlibrary');
                $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                if($mail_sent_status==1)
                    $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
            }

        }
        if(isset($contract_info[0]['delegate_id'])){
            $delegate_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['delegate_id']));
            if(isset($delegate_info))
            if($template_configurations_parent['total_records']>0){
                $template_configurations=$template_configurations_parent['data'][0];
                $wildcards=$template_configurations['wildcards'];
                $wildcards_replaces=array();
                $wildcards_replaces['first_name']=$delegate_info->first_name;
                $wildcards_replaces['last_name']=$delegate_info->last_name;
                $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                $wildcards_replaces['contract_review_initiated_user_name']=$contract_review_user->first_name.' '.$contract_review_user->last_name.' ('.$contract_review_user->user_role_name.')';
                $wildcards_replaces['contract_review_created_date']=dateFormat($contract_review_info[0]['created_on']);
                $wildcards_replaces['logo']=$customer_logo;
                $wildcards_replaces['url']=WEB_BASE_URL.'html';
                $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                $subject=$template_configurations['template_subject'];
                /*$from_name = SEND_GRID_FROM_NAME;
                $from = SEND_GRID_FROM_EMAIL;
                $from_name=$cust_admin['name'];
                $from=$cust_admin['email'];*/
                $from_name=$template_configurations['email_from_name'];
                $from=$template_configurations['email_from'];
                $to=$delegate_info->email;
                $to_name=$delegate_info->first_name.' '.$delegate_info->last_name;
                $mailer_data['mail_from_name']=$from_name;
                $mailer_data['mail_to_name']=$to_name;
                $mailer_data['mail_to_user_id']=$delegate_info->id_user;
                $mailer_data['mail_from']=$from;
                $mailer_data['mail_to']=$to;
                $mailer_data['mail_subject']=$subject;
                $mailer_data['mail_message']=$body;
                $mailer_data['status']=0;
                $mailer_data['send_date']=currentDate();
                $mailer_data['is_cron']=0;
                $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                //print_r($mailer_data);
                $mailer_id=$this->Customer_model->addMailer($mailer_data);

                //sending mail to delegate
                if($mailer_data['is_cron']==0){
                    //$mail_sent_status=sendmail($to_delegate, $subject, $body, $from);
                    $this->load->library('sendgridlibrary');
                    $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                    if($mail_sent_status==1)
                        $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                }

            }
        }
        if($template_configurations_parent['total_records']>0){
            $template_configurations=$template_configurations_parent['data'][0];
            $wildcards=$template_configurations['wildcards'];
            $wildcards_replaces=array();
            $wildcards_replaces['first_name']=$bu_info->first_name;
            $wildcards_replaces['last_name']=$bu_info->last_name;
            $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
            $wildcards_replaces['contract_review_initiated_user_name']=$contract_review_user->first_name.' '.$contract_review_user->last_name.' ('.$contract_review_user->user_role_name.')';
            $wildcards_replaces['contract_review_created_date']=dateFormat($contract_review_info[0]['created_on']);
            $wildcards_replaces['logo']=$customer_logo;
            $wildcards_replaces['url']=WEB_BASE_URL.'html';
            $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
            $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
            /*$from_name = SEND_GRID_FROM_NAME;
            $from = SEND_GRID_FROM_EMAIL;
            $from_name=$cust_admin['name'];
            $from=$cust_admin['email'];*/
            $from_name=$template_configurations['email_from_name'];
            $from=$template_configurations['email_from'];
            $to=$bu_info->email;
            $to_name=$bu_info->first_name.' '.$bu_info->last_name;
            $mailer_data['mail_from_name']=$from_name;
            $mailer_data['mail_to_name']=$to_name;
            $mailer_data['mail_to_user_id']=$bu_info->id_user;
            $mailer_data['mail_from']=$from;
            $mailer_data['mail_to']=$to;
            $mailer_data['mail_subject']=$subject;
            $mailer_data['mail_message']=$body;
            $mailer_data['status']=0;
            $mailer_data['send_date']=currentDate();
            $mailer_data['is_cron']=0;
            $mailer_data['email_template_id']=$template_configurations['id_email_template'];
            //print_r($mailer_data);
            $mailer_id=$this->Customer_model->addMailer($mailer_data);
            if($mailer_data['is_cron']==0){
                //$mail_sent_status=sendmail($to_bu, $subject, $body, $from);
                $this->load->library('sendgridlibrary');
                $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                if($mail_sent_status==1)
                    $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
            }
        }
        //exit;
        $data['contract_review_id']=pk_encrypt($data['contract_review_id']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('review_initiate'), 'data'=>$data['contract_review_id']);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getDelegates_get(){
        $data = $this->input->get();
        if(isset($data['id_business_unit'])) {
            $data['id_business_unit'] = pk_decrypt($data['id_business_unit']);
            if(!in_array($data['id_business_unit'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $result = $this->Contract_model->getDelegates($data);
        foreach ($result as $k=>$v) {
            $result[$k]['id_user']=pk_encrypt($result[$k]['id_user']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function module_get()
    {   //Input: Contract_review_id :Required , Output: id_module,module_name,contract_review_id ;

        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('contract_review_id', array('required'=>$this->lang->line('contract_review_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_user'])) {
            $data['contract_user'] = pk_decrypt($data['contract_user']);
            if(!in_array($data['contract_user'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user']) && isset($data['user_role_id']) && $data['user_role_id']==5){
            $data['contract_user'] = $data['id_user'];
            if(!in_array($data['contract_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $module_score = $this->Contract_model->getContractReviewModuleScore($data);

        $result = $this->Contract_model->getContractReviewModule($data);
        $total_no_of_reviews=$this->Contract_model->getContractReview(array('contract_id'=>$data['contract_id']));
        $total_no_of_reviews=count($total_no_of_reviews);
        foreach($result as $key=>$val){

            for($s=0;$s<count($module_score);$s++)
            {
                if($module_score[$s]['module_id']==$val['id_module']){
                    $result[$key]['score'] = getScoreByCount($module_score[$s]);
                }
            }
            $result[$key]['question_count'] = $this->Contract_model->moduleQuestionCount($val['id_module'],$val['contract_review_id']);

            $result[$key]['changes_count'] = $this->Contract_model->getContractModuleChanges(array('contract_review_id'=>$data['contract_review_id'],'module_id'=>$val['id_module']));
            $default_topic=$this->Contract_model->getContractReviewModuleData(array('contract_review_id'=>$data['contract_review_id'],'module_id'=>$val['id_module'],'contract_id'=>isset($data['contract_id'])?$data['contract_id']:0));

            $result[$key]['progress']=round($default_topic[0]['progress']);

            $result[$key]['imreermi']='itako';
            if($result[$key]['type']=='static' && $total_no_of_reviews>1 && $result[$key]['progress']>=100)
                $result[$key]['imreermi']='annus';

            $result[$key]['review_user_name']=$default_topic[0]['review_user_name'];
            $result[$key]['last_review']=$default_topic[0]['last_review'];
            $result[$key]['default_topic']=isset($default_topic[0]['topics'][0]['id_topic'])?array('id_topic'=>$default_topic[0]['topics'][0]['id_topic'],'topic_name'=>$default_topic[0]['topics'][0]['topic_name']):array('id_topic'=>null,'topic_name'=>null);
            $action_data=array('id_contract'=>$data['contract_id']);
            if(isset($data['id_user']))
                $action_data['id_user']=$data['id_user'];
            if(isset($data['user_role_id']))
                $action_data['user_role_id']=$data['user_role_id'];
            $action_data['item_status']=1;
            $action_data['page_type']='contract_review';
            $action_data['id_contract_review']=$data['contract_review_id'];
            $action_data['status']='open';
            //$action_data['id_contract_review']=$data['contract_review_id'];
            $action_data['id_module']=$val['id_module'];
            $result[$key]['action_item_count']=count($this->Contract_model->getContractReviewActionItemsList($action_data));
            $result[$key]['open_question_action_item_count']=count($this->Contract_model->getContractReviewOpenActionItemsList($action_data));
            $action_data['action_status']='-';
            $result[$key]['open_question_open_action_item_count']=$result[$key]['open_question_action_item_count'].'/'.count($this->Contract_model->getContractReviewOpenActionItemsList($action_data));

            $result[$key]['contract_review_id']=pk_encrypt($result[$key]['contract_review_id']);
            $result[$key]['id_module']=pk_encrypt($result[$key]['id_module']);
            if(isset($result[$key]['default_topic']['id_topic']))
                $result[$key]['default_topic']['id_topic']=pk_encrypt($result[$key]['default_topic']['id_topic']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contractReview_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $this->form_validator->add_rules('contract_review_id', array('required'=>$this->lang->line('contract_review_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=>$this->lang->line('module_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'1');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['module_id'])) {
            $data['module_id'] = pk_decrypt($data['module_id']);
            if(!in_array($data['module_id'],$this->session_user_contract_review_modules)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'2');
                $this->response($result, REST_Controller::HTTP_OK);
            }

        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'3');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review'])) {
            $data['id_contract_review'] = pk_decrypt($data['id_contract_review']);
            if(!in_array($data['id_contract_review'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'4');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_topic'])) {
            $data['id_topic'] = pk_decrypt($data['id_topic']);
            /*if(!in_array($data['id_topic'],$this->session_user_contract_review_topics)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'5');
                $this->response($result, REST_Controller::HTTP_OK);
            }*/
        }
        if(isset($data['last_review_id'])) {
            $data['last_review_id'] = pk_decrypt($data['last_review_id']);
            /*if(!in_array($data['last_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'6');
                $this->response($result, REST_Controller::HTTP_OK);
            }*/
        }
        $module_score = $this->Contract_model->getContractReviewModuleScore($data);
        $reviews = $this->Contract_model->getContractReview(array('contract_id' => $data['contract_id'],'status' => 'finished','order' => 'DESC'));
        if(!empty($reviews)){ $data['last_review_id'] = $reviews[0]['id_contract_review']; }

        $result = $this->Contract_model->getContractReviewModuleData($data);
        $current_review_question = $this->Contract_model->getCurrentReviewQuestions($data['contract_review_id']);
        $questions = $this->Contract_model->getTopicData(array('id_topic'=>$data['id_topic']))['questions'];
        $answered_questions = array();
        $topic_questions = array();
        //echo '<pre>'.print_r($questions[0]['id_question']).'</pre>';exit;

        for($i = 0; $i < count($current_review_question); $i++){
            $answered_questions[$i] = $current_review_question[$i];
            $answered_questions[$i] = $answered_questions[$i]['question_id'];
        }
        $result[0]['action_item_question_link'] = true;
        for($i = 0; $i < count($questions); $i++){
            $topic_questions[$i] = $questions[$i]['id_question'];
            if(!in_array($topic_questions[$i],$answered_questions)){
                $result[0]['action_item_question_link'] = false;break;
            }
        }
        $result[0]['question_count'] = $this->Contract_model->moduleQuestionCount($data['module_id'],$data['contract_review_id']);
        $result[0]['ideedi']=(count($this->Contract_model->getContractReviewDiscussionModuleCount(array('id_contract_review'=>$data['contract_review_id'],'discussion_status'=>1)))>0)?"itako":'annus';
        $result[0]['contract_user_access']=$this->session_user_info->access;
        if($this->Contract_model->checkReviewUserAccess(array('contract_review_id'=>$data['contract_review_id'],'id_user'=>$this->session_user_info->id_user))>0)
            $result[0]['contract_user_access']='co';
        if(!empty($result))
        for($s=0;$s<count($module_score);$s++)
        {
            if($result[0]['id_module']==$module_score[$s]['module_id']){
                $result[0]['score'] = getScoreByCount($module_score[$s]);
            }
        }
        foreach($result as $k=>$v){
            foreach($result[$k]['contract_details'] as $kc=>$vc){
                $result[$k]['contract_details'][$kc]['business_unit_id']=pk_encrypt($result[$k]['contract_details'][$kc]['business_unit_id']);
                $result[$k]['contract_details'][$kc]['classification_id']=pk_encrypt($result[$k]['contract_details'][$kc]['classification_id']);
                $result[$k]['contract_details'][$kc]['contract_owner_id']=pk_encrypt($result[$k]['contract_details'][$kc]['contract_owner_id']);
                $result[$k]['contract_details'][$kc]['created_by']=pk_encrypt($result[$k]['contract_details'][$kc]['created_by']);
                $result[$k]['contract_details'][$kc]['currency_id']=pk_encrypt($result[$k]['contract_details'][$kc]['currency_id']);
                $result[$k]['contract_details'][$kc]['delegate_id']=pk_encrypt($result[$k]['contract_details'][$kc]['delegate_id']);
                $result[$k]['contract_details'][$kc]['id_contract']=pk_encrypt($result[$k]['contract_details'][$kc]['id_contract']);
                $result[$k]['contract_details'][$kc]['id_contract_review']=pk_encrypt($result[$k]['contract_details'][$kc]['id_contract_review']);
                $result[$k]['contract_details'][$kc]['relationship_category_id']=pk_encrypt($result[$k]['contract_details'][$kc]['relationship_category_id']);
                $result[$k]['contract_details'][$kc]['updated_by']=pk_encrypt($result[$k]['contract_details'][$kc]['updated_by']);

            }
            foreach($result[$k]['contributors'] as $kct=>$vct){
                $result[$k]['contributors'][$kct]['id_contract_user']=pk_encrypt($result[$k]['contributors'][$kct]['id_contract_user']);
                $result[$k]['contributors'][$kct]['contract_id']=pk_encrypt($result[$k]['contributors'][$kct]['contract_id']);
                $result[$k]['contributors'][$kct]['module_id']=pk_encrypt($result[$k]['contributors'][$kct]['module_id']);
                $result[$k]['contributors'][$kct]['user_id']=pk_encrypt($result[$k]['contributors'][$kct]['user_id']);
                $result[$k]['contributors'][$kct]['created_by']=pk_encrypt($result[$k]['contributors'][$kct]['created_by']);
                $result[$k]['contributors'][$kct]['updated_by']=pk_encrypt($result[$k]['contributors'][$kct]['updated_by']);
                $result[$k]['contributors'][$kct]['contract_review_id']=pk_encrypt($result[$k]['contributors'][$kct]['contract_review_id']);
            }
            $result[$k]['contract_review_id']=pk_encrypt($result[$k]['contract_review_id']);
            $result[$k]['created_by']=pk_encrypt($result[$k]['created_by']);
            $result[$k]['id_module']=pk_encrypt($result[$k]['id_module']);
            $result[$k]['parent_module_id']=pk_encrypt($result[$k]['parent_module_id']);
            $result[$k]['updated_by']=pk_encrypt($result[$k]['updated_by']);
            foreach($result[$k]['topics'] as $kt=>$vt){
                $result[$k]['topics'][$kt]['created_by']=pk_encrypt($result[$k]['topics'][$kt]['created_by']);
                $result[$k]['topics'][$kt]['id_topic']=pk_encrypt($result[$k]['topics'][$kt]['id_topic']);
                $result[$k]['topics'][$kt]['module_id']=pk_encrypt($result[$k]['topics'][$kt]['module_id']);
                $result[$k]['topics'][$kt]['parent_topic_id']=pk_encrypt($result[$k]['topics'][$kt]['parent_topic_id']);
                $result[$k]['topics'][$kt]['updated_by']=pk_encrypt($result[$k]['topics'][$kt]['updated_by']);
                foreach($result[$k]['topics'][$kt]['questions'] as $kq=>$vq){
                    $result[$k]['topics'][$kt]['questions'][$kq]['created_by']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['created_by']);
                    $result[$k]['topics'][$kt]['questions'][$kq]['id_question']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['id_question']);
                    $result[$k]['topics'][$kt]['questions'][$kq]['parent_question_id']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['parent_question_id']);
                    $result[$k]['topics'][$kt]['questions'][$kq]['topic_id']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['topic_id']);
                    $result[$k]['topics'][$kt]['questions'][$kq]['updated_by']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['updated_by']);
                    $result[$k]['topics'][$kt]['questions'][$kq]['parent_question_answer']=(pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['parent_question_answer'])==NULL?$result[$k]['topics'][$kt]['questions'][$kq]['question_answer']:pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['parent_question_answer']));
                    foreach($result[$k]['topics'][$kt]['questions'][$kq]['options'] as $ko=>$vo){
                        $result[$k]['topics'][$kt]['questions'][$kq]['options'][$ko]['created_by']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['options'][$ko]['created_by']);
                        $result[$k]['topics'][$kt]['questions'][$kq]['options'][$ko]['id_question_option']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['options'][$ko]['id_question_option']);
                        $result[$k]['topics'][$kt]['questions'][$kq]['options'][$ko]['parent_question_option_id']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['options'][$ko]['parent_question_option_id']);
                        $result[$k]['topics'][$kt]['questions'][$kq]['options'][$ko]['question_id']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['options'][$ko]['question_id']);
                        $result[$k]['topics'][$kt]['questions'][$kq]['options'][$ko]['updated_by']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['options'][$ko]['updated_by']);

                    }
                }
            }
            if(isset($result[$k]['topic_pagination']['current']))
                $result[$k]['topic_pagination']['current']=pk_encrypt($result[$k]['topic_pagination']['current']);
            if(isset($result[$k]['topic_pagination']['next']))
                $result[$k]['topic_pagination']['next']=pk_encrypt($result[$k]['topic_pagination']['next']);
            if(isset($result[$k]['topic_pagination']['previous']))
                $result[$k]['topic_pagination']['previous']=pk_encrypt($result[$k]['topic_pagination']['previous']);

        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function questionAnswer_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('contract_review_id', array('required'=>$this->lang->line('contract_review_id_req')));
        //$this->form_validator->add_rules('question_id', array('required'=>$this->lang->line('question_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        //$this->form_validator->add_rules('question_answer', array('required'=>$this->lang->line('question_answer_req')));
        //$this->form_validator->add_rules('question_feedback', array('required'=>$this->lang->line('question_feedback_req')));
        $this->form_validator->add_rules('data', array('required'=>$this->lang->line('question_data_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'1');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'2');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $question_data = $data['data'];
        $add = array();
        $previous_answers = $this->Contract_model->getReviewQuestionAnswer($data);

        $previous_answer_question_id = array_map(function($i){ return $i['question_id']; },$previous_answers);
        for($s=0;$s<count($question_data);$s++) {
            if (isset($question_data[$s]['question_id'])) {
                $question_id_loop = pk_decrypt($question_data[$s]['question_id']);
                if(!in_array($question_id_loop,$this->session_user_contract_review_questions)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'3');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if (isset($question_data[$s]['parent_question_id'])) {
                $question_id_loop = pk_decrypt($question_data[$s]['parent_question_id']);
                /*if(!in_array($question_id_loop,$this->session_user_master_contract_review_questions)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'4');
                    $this->response($result, REST_Controller::HTTP_OK);
                }*/
            }
            if (isset($question_data[$s]['question_option_id'])) {
                $question_option_id_loop = pk_decrypt($question_data[$s]['question_option_id']);

            }

        }
            for($s=0;$s<count($question_data);$s++)
        {

            if(isset($question_data[$s]['question_id'])) $question_data[$s]['question_id']=pk_decrypt($question_data[$s]['question_id']);
            $question_info=$this->Question_model->getQuestion(array('id_question'=>$question_data[$s]['question_id']));

            if($question_info[0]['question_type']=='input'){
                $question_data[$s]['question_option_id']=NULL;
            }
            else{
                $question_data[$s]['question_option_id']=pk_decrypt($question_data[$s]['question_answer']);
                if($question_data[$s]['question_option_id']!=NULL && $question_data[$s]['question_option_id']>0) {
                    $option_info = $this->Question_model->getQuestionOption(array('id_question_option' => $question_data[$s]['question_option_id']));
                    $question_data[$s]['question_answer'] = $option_info[0]['option_value'];
                }
                else{
                    $question_data[$s]['question_option_id']=NULL;
                    $question_data[$s]['question_answer']=NULL;
                }
            }
            if(isset($question_data[$s]['parent_question_id'])) $question_data[$s]['parent_question_id']=pk_decrypt($question_data[$s]['parent_question_id']);

            if(in_array($question_data[$s]['question_id'],$previous_answer_question_id)){
                $this->Contract_model->updateReviewQuestionAnswer(array(
                    'contract_review_id' => $data['contract_review_id'],
                    'question_id' => $question_data[$s]['question_id'],
                    'parent_question_id' => $question_data[$s]['parent_question_id'],
                    'question_answer' => $question_data[$s]['question_answer'],
                    'question_feedback' => $question_data[$s]['question_feedback'],
                    'updated_by' => $data['created_by'],
                    'updated_on' => currentDate(),
                    'question_option_id' => isset($question_data[$s]['question_option_id'])?$question_data[$s]['question_option_id']:NULL
                ));
            }
            else{
                $add[] = array(
                    'contract_review_id' => $data['contract_review_id'],
                    'question_id' => $question_data[$s]['question_id'],
                    'parent_question_id' => $question_data[$s]['parent_question_id'],
                    'question_answer' => $question_data[$s]['question_answer'],
                    'question_feedback' => $question_data[$s]['question_feedback'],
                    'updated_by' => $data['created_by'],
                    'updated_on' => currentDate(),
                    'question_option_id' => isset($question_data[$s]['question_option_id'])?$question_data[$s]['question_option_id']:NULL
                );
            }
        }

        $this->Contract_model->updateContractReview(array('id_contract_review' => $data['contract_review_id'],'updated_by' => $data['created_by'],'updated_on' => currentDate()));

        if(!empty($add)){
            $this->Contract_model->addReviewQuestionAnswer_bulk($add);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_save'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function finalize_post()
    {
        $data = $this->input->post();
        //echo 'data'.'<pre>';print_r($data);
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $this->form_validator->add_rules('contract_review_id', array('required'=>$this->lang->line('contract_review_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $is_without_discussion=isset($data['finalize_without_discussion'])?$data['finalize_without_discussion']:NULL;
        $finalize_comments=isset($data['finalize_comments'])?$data['finalize_comments']:NULL;
        $this->Contract_model->updateContractReview(array('id_contract_review' => $data['contract_review_id'],'contract_review_status' => 'finished','updated_by' => $data['created_by'],'updated_on' => currentDate(),'finalize_comments'=>$finalize_comments,'finalize_without_discussion'=>$is_without_discussion));

        $this->Contract_model->updateContract(array('id_contract' => $data['contract_id'],'contract_status' => 'review finalized'));
        $pending_discussions=$this->Contract_model->getContractDiscussion(array('id_contract_review'=>$data['contract_review_id'],'discussion_status'=>1));
        foreach($pending_discussions as $k=>$v){
            $this->closereviewdiscussion(array('id_contract_review_discussion'=>$v['id_contract_review_discussion'],'contract_id'=>$data['contract_id'],'module_id'=>$v['module_id'],'contract_review_id'=>$data['contract_review_id'],'created_by'=>$data['created_by']));
        }
        $contract_info = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
        $business_unit = $this->Business_unit_model->getBusinessUnitDetails(array('id_business_unit'=>$contract_info[0]['business_unit_id']));
        $bu_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['contract_owner_id']));
        $contract_review_info = $this->Contract_model->getContractReview(array('contract_id' => $data['contract_id']));
        $cust_admin_info = $this->User_model->getUserInfo(array('customer_id' => $business_unit[0]['customer_id'],'user_role_id' =>2));
        //$contract_review_user = $this->User_model->getUserInfo(array('user_id' => $contract_review_info[0]['created_by']));
        $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $cust_admin_info->customer_id));
        /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
        $cust_admin = $cust_admin['data'][0];*/
        /*echo 'contract_info'.'<pre>';print_r($cust_admin_info);
        echo 'business'.'<pre>';print_r($business_unit);exit;
        echo 'buinfo'.'<pre>';print_r($bu_info);*/
        //echo 'contract_Review_user'.'<pre>';print_r($contract_review_user);exit;

        if($customer_details[0]['company_logo']=='') {
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
        }
        else{
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

        }
        if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }
        $finalised_user = $this->User_model->getUserInfo(array('user_id' => $data['id_user']));
        $template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $cust_admin_info->customer_id,'language_id' =>1,'module_key'=>'CONTRACT_REVIEW_FINALIZE'));
        if($template_configurations_parent['total_records']>0){
            $template_configurations=$template_configurations_parent['data'][0];
            $wildcards=$template_configurations['wildcards'];
            $wildcards_replaces=array();
            $wildcards_replaces['first_name']=$cust_admin_info->first_name;
            $wildcards_replaces['last_name']=$cust_admin_info->last_name;
            $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
            $wildcards_replaces['contract_review_finalized_user_name']=$finalised_user->first_name.' '.$finalised_user->last_name.' ('.$finalised_user->user_role_name.')';
            $wildcards_replaces['contract_review_finalized_date']=dateFormat(currentDate());
            $wildcards_replaces['logo']=$customer_logo;
            $wildcards_replaces['url']=WEB_BASE_URL.'html';
            $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
            $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
            /*$from_name=SEND_GRID_FROM_NAME;
            $from=SEND_GRID_FROM_EMAIL;
            $from_name=$cust_admin['name'];
            $from=$cust_admin['email'];*/
            $from_name=$template_configurations['email_from_name'];
            $from=$template_configurations['email_from'];
            $to=$cust_admin_info->email;
            $to_name=$cust_admin_info->first_name.' '.$cust_admin_info->last_name;
            $mailer_data['mail_from_name']=$from_name;
            $mailer_data['mail_to_name']=$to_name;
            $mailer_data['mail_to_user_id']=$cust_admin_info->id_user;
            $mailer_data['mail_from']=$from;
            $mailer_data['mail_to']=$to;
            $mailer_data['mail_subject']=$subject;
            $mailer_data['mail_message']=$body;
            $mailer_data['status']=0;
            $mailer_data['send_date']=currentDate();
            $mailer_data['is_cron']=0;
            $mailer_data['email_template_id']=$template_configurations['id_email_template'];
            //print_r($mailer_data);
            $mailer_id=$this->Customer_model->addMailer($mailer_data);
            if($mailer_data['is_cron']==0) {
                //$mail_sent_status=sendmail($to, $subject, $body, $from);
                $this->load->library('sendgridlibrary');
                $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                if($mail_sent_status==1)
                    $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
            }

        }
        if(isset($contract_info[0]['delegate_id'])){
            $delegate_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['delegate_id']));
            if(isset($delegate_info))
                if($template_configurations_parent['total_records']>0){
                $template_configurations=$template_configurations_parent['data'][0];
                $wildcards=$template_configurations['wildcards'];
                $wildcards_replaces=array();
                $wildcards_replaces['first_name']=$delegate_info->first_name;
                $wildcards_replaces['last_name']=$delegate_info->last_name;
                $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                $wildcards_replaces['contract_review_finalized_user_name']=$finalised_user->first_name.' '.$finalised_user->last_name.' ('.$finalised_user->user_role_name.')';
                $wildcards_replaces['contract_review_finalized_date']=dateFormat(currentDate());
                $wildcards_replaces['logo']=$customer_logo;
                $wildcards_replaces['url']=WEB_BASE_URL.'html';
                $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                /*$from_name = SEND_GRID_FROM_NAME;
                $from = SEND_GRID_FROM_EMAIL;
                $from_name=$cust_admin['name'];
                $from=$cust_admin['email'];*/
                $from_name=$template_configurations['email_from_name'];
                $from=$template_configurations['email_from'];
                $to=$delegate_info->email;
                $to_name=$delegate_info->first_name.' '.$delegate_info->last_name;
                $mailer_data['mail_from_name']=$from_name;
                $mailer_data['mail_to_name']=$to_name;
                $mailer_data['mail_to_user_id']=$delegate_info->id_user;
                $mailer_data['mail_from']=$from;
                $mailer_data['mail_to']=$to;
                $mailer_data['mail_subject']=$subject;
                $mailer_data['mail_message']=$body;
                $mailer_data['status']=0;
                $mailer_data['send_date']=currentDate();
                $mailer_data['is_cron']=0;
                $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                //print_r($mailer_data);
                $mailer_id=$this->Customer_model->addMailer($mailer_data);
                if($mailer_data['is_cron']==0) {
                    //$mail_sent_status=sendmail($to_delegate, $subject, $body, $from);
                    $this->load->library('sendgridlibrary');
                    $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                    if($mail_sent_status==1)
                        $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                }

            }
        }
        if($template_configurations_parent['total_records']>0){
            $template_configurations=$template_configurations_parent['data'][0];
            $wildcards=$template_configurations['wildcards'];
            $wildcards_replaces=array();
            $wildcards_replaces['first_name']=$bu_info->first_name;
            $wildcards_replaces['last_name']=$bu_info->last_name;
            $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
            $wildcards_replaces['contract_review_finalized_user_name']=$finalised_user->first_name.' '.$finalised_user->last_name.' ('.$finalised_user->user_role_name.')';
            $wildcards_replaces['contract_review_finalized_date']=dateFormat(currentDate());
            $wildcards_replaces['logo']=$customer_logo;
            $wildcards_replaces['url']=WEB_BASE_URL.'html';
            $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
            $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
            /*$from_name = SEND_GRID_FROM_NAME;
            $from = SEND_GRID_FROM_EMAIL;
            $from_name=$cust_admin['name'];
            $from=$cust_admin['email'];*/
            $from_name=$template_configurations['email_from_name'];
            $from=$template_configurations['email_from'];
            $to=$bu_info->email;
            $to_name=$bu_info->first_name.' '.$bu_info->last_name;
            $mailer_data['mail_from_name']=$from_name;
            $mailer_data['mail_to_name']=$to_name;
            $mailer_data['mail_to_user_id']=$bu_info->id_user;
            $mailer_data['mail_from']=$from;
            $mailer_data['mail_to']=$to;
            $mailer_data['mail_subject']=$subject;
            $mailer_data['mail_message']=$body;
            $mailer_data['status']=0;
            $mailer_data['send_date']=currentDate();
            $mailer_data['is_cron']=0;
            $mailer_data['email_template_id']=$template_configurations['id_email_template'];
            //print_r($mailer_data);
            $mailer_id=$this->Customer_model->addMailer($mailer_data);
            if($mailer_data['is_cron']==0) {
                //$mail_sent_status=sendmail($to, $subject, $body, $from);
                $this->load->library('sendgridlibrary');
                $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                if($mail_sent_status==1)
                    $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
            }

        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('review_finalize'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function dashboard_get(){
            // 
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review'])) {
            $data['id_contract_review'] = pk_decrypt($data['id_contract_review']);
            if(!in_array($data['id_contract_review'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $result_array = array();
        $data['order'] = 'DESC';
        $contributor_modules=array();
        if(isset($data['id_user']) && isset($data['user_role_id']) && $data['user_role_id']==5){
            $data['contract_user'] = $data['id_user'];
            if(!in_array($data['contract_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['contract_user']));
            //echo "<pre>";print_r($contributor_modules);echo "</pre>";
            $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
            //echo "<pre>";print_r($contributor_modules);echo "</pre>";exit;
        }
        else{
            $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['id_user']));
            //echo "<pre>";print_r($contributor_modules);echo "</pre>";
            $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
        }
        $reviews = $this->Contract_model->getContractReview($data);
        for($s=0;$s<count($reviews);$s++)
        {
            if(!in_array($reviews[$s]['business_unit_id'],$this->session_user_own_business_units)){
                if($this->Contract_model->checkReviewUserAccess(array('contract_review_id'=>$reviews[$s]['id_contract_review'],'id_user'=>$this->session_user_id))>0){

                }
                else{
                    unset($reviews[$s]);
                }
            }
        }
        $reviews=array_values($reviews);
        $contract_review_id = array_map(function($i){ return $i['id_contract_review']; },$reviews);
        $data['contract_review_id']=(isset($data['contract_review_id']) && $data['contract_review_id']>0)?$data['contract_review_id']:(isset($reviews[0]['id_contract_review'])?$reviews[0]['id_contract_review']:0);
        $module_data=array();
        if(isset($data['contract_review_id'])){
            $index = array_search($data['contract_review_id'],$contract_review_id);
            for($s=0;$s<count($reviews);$s++)
            {
                if($reviews[$s]['id_contract_review']==$data['contract_review_id']){
                    $result_array['data'] = array(
                        'review_date' => ($reviews[$s]['updated_date']!='')?date('Y-m-d',strtotime($reviews[$s]['updated_date'])):'',
                        'review_status' => ($reviews[$s]['contract_review_status']=='finished')?'review finalized':$reviews[$s]['contract_review_status']
                    );
                }
            }
            $result_array['next'] = isset($contract_review_id[$index-1])?$contract_review_id[$index-1]:0;
            $result_array['prev'] = isset($contract_review_id[$index+1])?$contract_review_id[$index+1]:0;
            if($data['contract_review_id']>0){
                $module_data =  $this->Contract_model->getContractDashboard(array('contract_review_id' => $data['contract_review_id']));
                $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['id_user'],'contract_review_id' => $data['contract_review_id']));
                $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
            }
        }
        else{
            $result_array['data'] = array(
                'review_date' => ($reviews[0]['updated_on']!='')?date('Y-m-d',strtotime($reviews[0]['updated_on'])):'',
                'review_status' => $reviews[0]['contract_review_status']
            );
            $result_array['next'] = 0;
            $result_array['prev'] = isset($reviews[1])?$reviews[1]['id_contract_review']:0;
            if($data['contract_review_id']>0) {
                $module_data = $this->Contract_model->getContractDashboard(array('contract_review_id' => $reviews[0]['id_contract_review']));
                $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['id_user'],'contract_review_id' => $data['contract_review_id']));
                $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
            }
        }
        $result_array['modules'] = array();
        for($s=0;$s<count($module_data);$s++)
        {
            if(count($contributor_modules)==0 || (count($contributor_modules)>0 && in_array($module_data[$s]['module_id'],$contributor_modules))) {
                    $questions = $this->Contract_model->getTopicData(array('id_topic'=>$module_data[$s]['topic_id']));
                    foreach($questions['questions'] as $k => $v){
                        foreach($questions['questions'][$k]['attachments'] as $key =>$val){

                            $questions['questions'][$k]['attachments'][$key]['document_source']=($questions['questions'][$k]['attachments'][$key]['document_source']);
                            $questions['questions'][$k]['attachments'][$key]['id_document']=pk_encrypt($questions['questions'][$k]['attachments'][$key]['id_document']);
                            $questions['questions'][$k]['attachments'][$key]['module_id']=pk_encrypt($questions['questions'][$k]['attachments'][$key]['module_id']);
                            $questions['questions'][$k]['attachments'][$key]['reference_id']=pk_encrypt($questions['questions'][$k]['attachments'][$key]['reference_id']);
                        }
                        $questions['questions'][$k]['id_question']=pk_encrypt($questions['questions'][$k]['id_question']);
                    }
                $result_array['modules'][$module_data[$s]['module_id']]['module_id'] = pk_encrypt($module_data[$s]['module_id']);
                $result_array['modules'][$module_data[$s]['module_id']]['module_name'] = $module_data[$s]['module_name'];
                $result_array['modules'][$module_data[$s]['module_id']]['topics'][] = array(
                    'topic_id' => pk_encrypt($module_data[$s]['topic_id']),
                    'topic_name' => $module_data[$s]['topic_name'],
                    'topic_score' => $module_data[$s]['topic_score'],
                    'questions' => $questions['questions']
                );
            }
        }

        $result_array['modules'] = array_values($result_array['modules']);
        //echo "<pre>";print_r($result_array['modules']);echo "</pre>";

        for($s=0;$s<count($result_array['modules']);$s++) //getting score for module by topics score // getScore is a helper function
        {
            $result_array['modules'][$s]['module_score'] = getScore($score = array_map(function($i){ return strtolower($i['topic_score']); },$result_array['modules'][$s]['topics']));
        }
        $result_array['next'] = pk_encrypt(isset($result_array['next'])?$result_array['next']:NULL);
        $result_array['prev'] = pk_encrypt(isset($result_array['prev'])?$result_array['prev']:NULL);
        $result_array['review_score'] = getScore($score = array_map(function($i){ return strtolower($i['module_score']); },$result_array['modules']));
        $result_array['contract_review_id']=pk_encrypt($data['contract_review_id']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result_array);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function dashboardexport_get(){
            //
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review'])) {
            $data['id_contract_review'] = pk_decrypt($data['id_contract_review']);
            if(!in_array($data['id_contract_review'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $result_array = array();
        $data['order'] = 'DESC';
        $contributor_modules=array();
        if(isset($data['id_user']) && isset($data['user_role_id']) && $data['user_role_id']==5){
            $data['contract_user'] = $data['id_user'];
            if(!in_array($data['contract_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['contract_user']));
            //echo "<pre>";print_r($contributor_modules);echo "</pre>";
            $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
            //echo "<pre>";print_r($contributor_modules);echo "</pre>";exit;
        }
        else{
            $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['id_user']));
            //echo "<pre>";print_r($contributor_modules);echo "</pre>";
            $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
        }
        $reviews = $this->Contract_model->getContractReview($data);
        for($s=0;$s<count($reviews);$s++)
        {
            if(!in_array($reviews[$s]['business_unit_id'],$this->session_user_own_business_units)){
                if($this->Contract_model->checkReviewUserAccess(array('contract_review_id'=>$reviews[$s]['id_contract_review'],'id_user'=>$this->session_user_id))>0){

                }
                else{
                    unset($reviews[$s]);
                }
            }
        }
        $reviews=array_values($reviews);
        $contract_review_id = array_map(function($i){ return $i['id_contract_review']; },$reviews);
        $data['contract_review_id']=(isset($data['contract_review_id']) && $data['contract_review_id']>0)?$data['contract_review_id']:(isset($reviews[0]['id_contract_review'])?$reviews[0]['id_contract_review']:0);
        $module_data=array();
        if(isset($data['contract_review_id'])){
            $index = array_search($data['contract_review_id'],$contract_review_id);
            for($s=0;$s<count($reviews);$s++)
            {
                if($reviews[$s]['id_contract_review']==$data['contract_review_id']){
                        $result_array['review_date'] = ($reviews[$s]['updated_date']!='')?date('Y-m-d',strtotime($reviews[$s]['updated_date'])):'';
                        $result_array['review_status'] = ($reviews[$s]['contract_review_status']=='finished')?'review finalized':$reviews[$s]['contract_review_status'];
                }
            }
            //$result_array['next'] = isset($contract_review_id[$index-1])?$contract_review_id[$index-1]:0;
            //$result_array['prev'] = isset($contract_review_id[$index+1])?$contract_review_id[$index+1]:0;
            if($data['contract_review_id']>0){
                $module_data =  $this->Contract_model->getContractDashboard(array('contract_review_id' => $data['contract_review_id']));
                $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['id_user'],'contract_review_id' => $data['contract_review_id']));
                $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
            }
        }
        else{
            $result_array['review_date'] = ($reviews[0]['updated_on']!='')?date('Y-m-d',strtotime($reviews[0]['updated_on'])):'';
            $result_array['review_status'] = $reviews[0]['contract_review_status'];
            //$result_array['next'] = 0;
            //$result_array['prev'] = isset($reviews[1])?$reviews[1]['id_contract_review']:0;
            if($data['contract_review_id']>0) {
                $module_data = $this->Contract_model->getContractDashboard(array('contract_review_id' => $reviews[0]['id_contract_review']));
                $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['id_user'],'contract_review_id' => $data['contract_review_id']));
                $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
            }
        }
        $result_array['modules'] = array();
        for($s=0;$s<count($module_data);$s++)
        {
            if(count($contributor_modules)==0 || (count($contributor_modules)>0 && in_array($module_data[$s]['module_id'],$contributor_modules))) {
                    $questions = $this->Contract_model->getTopicData(array('id_topic'=>$module_data[$s]['topic_id']));
                    foreach($questions['questions'] as $k => $v){
                        foreach($questions['questions'][$k]['attachments'] as $key =>$val){

                            $questions['questions'][$k]['attachments'][$key]['document_source']=($questions['questions'][$k]['attachments'][$key]['document_source']);
                            $questions['questions'][$k]['attachments'][$key]['id_document']=pk_encrypt($questions['questions'][$k]['attachments'][$key]['id_document']);
                            $questions['questions'][$k]['attachments'][$key]['module_id']=pk_encrypt($questions['questions'][$k]['attachments'][$key]['module_id']);
                            $questions['questions'][$k]['attachments'][$key]['reference_id']=pk_encrypt($questions['questions'][$k]['attachments'][$key]['reference_id']);
                        }
                        $questions['questions'][$k]['id_question']=pk_encrypt($questions['questions'][$k]['id_question']);
                    }
                $result_array['modules'][$module_data[$s]['module_id']]['module_id'] = pk_encrypt($module_data[$s]['module_id']);
                $result_array['modules'][$module_data[$s]['module_id']]['module_name'] = $module_data[$s]['module_name'];
                $result_array['modules'][$module_data[$s]['module_id']]['topics'][] = array(
                    'topic_id' => pk_encrypt($module_data[$s]['topic_id']),
                    'topic_name' => $module_data[$s]['topic_name'],
                    'topic_score' => $module_data[$s]['topic_score'],
                    'questions' => $questions['questions']
                );
            }
        }

        $result_array['modules'] = array_values($result_array['modules']);
        $result_array['contract'] = $this->Contract_model->getContractDetails(array('id_contract'=>$data['contract_id']))[0];
        //echo "<pre>";print_r($result_array['modules']);echo "</pre>";

        for($s=0;$s<count($result_array['modules']);$s++) //getting score for module by topics score // getScore is a helper function
        {
            $result_array['modules'][$s]['module_score'] = getScore($score = array_map(function($i){ return strtolower($i['topic_score']); },$result_array['modules'][$s]['topics']));
        }
        //$result_array['next'] = pk_encrypt(isset($result_array['next'])?$result_array['next']:NULL);
        //$result_array['prev'] = pk_encrypt(isset($result_array['prev'])?$result_array['prev']:NULL);
        $result_array['review_score'] = getScore($score = array_map(function($i){ return strtolower($i['module_score']); },$result_array['modules']));
        $result_array['contract_review_id']=pk_encrypt($data['contract_review_id']);
        $this->exportdashboard($result_array);

    }

    function exportdashboard($data){
        $this->load->library('excel');
        //activate worksheet number 1
        $excelRowstartsfrom=1;
        $excelColumnstartsFrom=0;
        $columnBegin =$excelColumnstartsFrom;
        $excelstartsfrom=$excelRowstartsfrom;
        $question_count = 0;
        foreach($data['modules'] as $k => $v){
            foreach($data['modules'][$k]['topics'] as $m => $n){
                $question_count = $question_count + count($n['questions']);
            }
        }
        //echo 'question count'.$question_count;exit;
        //Heading Starts
        $head = $this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($columnBegin+8) . $excelstartsfrom;
        $body = $this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($columnBegin+8) . ($excelstartsfrom+$question_count);
        //echo 'body'.$body;exit;
        $this->excel->getActiveSheet()->getStyle($body)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'font'  => array(
                    'bold'  => false
                )
            )
        );
        $this->excel->getActiveSheet()->getStyle($head)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin))->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin+1))->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin+2))->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin+3))->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin+4))->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin+5))->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin+6))->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin+7))->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin+8))->setWidth(15);
        $this->excel->getActiveSheet()->getStyle($head)->getFill()->getStartColor()->setARGB('D1D1D1d1');
        $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin) . $excelstartsfrom,'Provider');
        $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+1) . $excelstartsfrom,'Contract Name');
        $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+2) . $excelstartsfrom,'Category');
        $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+3) . $excelstartsfrom,'Review Date');
        $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+4) . $excelstartsfrom,'Status');
        $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+5) . $excelstartsfrom,'Module Name');
        $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+6) . $excelstartsfrom,'Topic Name');
        $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+7) . $excelstartsfrom,'Question');
        $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+8) . $excelstartsfrom,'Answer');
        //Heading Ends

        foreach($data['modules'] as $k => $v){

            //echo '<pre>'.print_r($data).'</pre>';exit;
            foreach($data['modules'][$k]['topics'] as $m => $n){
                //echo '<pre>'.print_r($n).'</pre>';exit;
                foreach($data['modules'][$k]['topics'][$m]['questions'] as $o => $p){
                    $excelstartsfrom++;
                    $ans = $p['question_option_answer'];
                    if($p['question_option_answer']=='R') $ans = 'Red';
                    else if($p['question_option_answer']=='A') $ans = 'Amber';
                    else if($p['question_option_answer']=='G') $ans = 'Green';
                    else $ans = $p['question_option_answer'];
                    $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin) . $excelstartsfrom,$data['contract']['provider_name']);
                    $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+1) . $excelstartsfrom,$data['contract']['contract_name']);
                    $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+2) . $excelstartsfrom,$data['contract']['relationship_category_name']);
                    $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+3) . $excelstartsfrom,$data['review_date']);
                    $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+4) . $excelstartsfrom,$data['review_status']);
                    $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+5) . $excelstartsfrom,$v['module_name']);
                    $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+6) . $excelstartsfrom,$n['topic_name']);
                    $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+7) . $excelstartsfrom,$p['question_text']);
                    $this->excel->setActiveSheetIndex(0)->setCellValue($this->getkey($columnBegin+8) . $excelstartsfrom,$ans);
                }
            }
        }


        $this->excel->getActiveSheet()->setTitle('CONTRACT DASHBOARD');
        $filename = $data['contract']['contract_name'].'_'.date("d-m-Y",strtotime($data['review_date'])).'.xls'; //save our workbook as this file name
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $file_path = FILE_SYSTEM_PATH.'downloads/' . $filename;
        $objWriter->save($file_path);
        $view_path='downloads/' . $filename;
        $file_path = REST_API_URL.$view_path;
        $file_path = str_replace('::1','localhost',$file_path);
        $insert_id = $this->Download_model->addDownload(array('path'=>$view_path,'filename'=>$filename,'user_id'=>pk_decrypt($_SERVER['HTTP_USER']),'access_token'=>substr($_SERVER['HTTP_AUTHORIZATION'],7),'status'=>0,'created_date_time'=>currentDate()));

        $response = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>pk_encrypt($insert_id));
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function actionItems_get(){
            // 
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['responsible_user_id'])) {
            $data['responsible_user_id'] = pk_decrypt($data['responsible_user_id']);
            if($data['responsible_user_id']!=$this->session_user_customer_all_users){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['delegate_id'])) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review_action_item'])) {
            $data['id_contract_review_action_item'] = pk_decrypt($data['id_contract_review_action_item']);
            if(!in_array($data['id_contract_review_action_item'],$this->session_user_contract_action_items)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id']) && isset($data['id_user'])){
            if($data['user_role_id']==2){

            }
            else if($data['user_role_id']==3){
                $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $data['id_user'],'status' => '1'));
                $data['business_unit_id'] = array_map(function($i){ return $i['id_business_unit']; },$business_unit);
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            else if($data['user_role_id']==4) {
               // $data['delegate_id'] = $data['id_user'];
                $data['responsible_user_id'] = $data['id_user'];
                $data['created_by'] = $data['id_user'];
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            else if($data['user_role_id']==5) {
                $data['responsible_user_id'] = $data['id_user'];
                $data['created_by'] = $data['id_user'];
            }
            else if($data['user_role_id']==6){
                $data['business_unit_id'] = $this->session_user_business_units;
            }
        }
        if(isset($data['page_type']) && isset($data['id_user']) && $data['page_type']=='dashboard'){
            if(isset($data['created_by'])){
                unset($data['created_by']);
            }
            $data['responsible_user_id'] = $data['id_user'];
        }
        if(isset($data['show_my_action_items']) && isset($data['id_user']) && $data['show_my_action_items']==1){
            if(isset($data['created_by'])){
                unset($data['created_by']);
            }
            $data['responsible_user_id'] = $data['id_user'];
        }

        $data = tableOptions($data);
        $data['item_status']=1;
        $result = $this->Contract_model->getActionItems($data);

        //echo $this->db->last_query(); exit;
        foreach($result['data'] as $key => $value){
            $result['data'][$key]['comments_log'] = $this->Contract_model->contractReviewActionItemLog(array('id_contract_review_action_item' => $result['data'][$key]['id_contract_review_action_item']));
            $result['data'][$key]['due_date'] = date('Y-m-d',strtotime($result['data'][$key]['due_date']));
            $contract_review = $this->Contract_model->getContractReview(array('id_contract_review'=>$result['data'][$key]['contract_review_id']));
            $contract_info = $this->Contract_model->getContractDetails(array('id_contract'=>$result['data'][$key]['contract_id']));
            //echo '<pre>';print_r($contract_review);print_r($contract_info);
            $result['data'][$key]['last_review'] = date('Y-m-d',strtotime($contract_review[0]['updated_date']));
            $result['data'][$key]['contract_name'] = $contract_info[0]['contract_name'];
            $result['data'][$key]['provider_name'] = $contract_info[0]['provider_name'];
                $user_info = $this->User_model->getUserInfo(array('user_id'=>$value['created_by']));
            $result['data'][$key]['created_by_name'] = $user_info->first_name.' '.$user_info->last_name;
            $result['data'][$key]['contract_id'] = pk_encrypt($result['data'][$key]['contract_id']);
            $result['data'][$key]['contract_review_id'] = pk_encrypt($result['data'][$key]['contract_review_id']);
            $result['data'][$key]['created_by'] = pk_encrypt($result['data'][$key]['created_by']);
            $result['data'][$key]['id_contract_review_action_item'] = pk_encrypt($result['data'][$key]['id_contract_review_action_item']);
            $result['data'][$key]['module_id'] = pk_encrypt($result['data'][$key]['module_id']);
            $result['data'][$key]['responsible_user_id'] = pk_encrypt($result['data'][$key]['responsible_user_id']);
            $result['data'][$key]['topic_id'] = pk_encrypt($result['data'][$key]['topic_id']);
            $result['data'][$key]['updated_by'] = pk_encrypt($result['data'][$key]['updated_by']);
            $result['data'][$key]['question_id'] = pk_encrypt($result['data'][$key]['question_id']);
            foreach($result['data'][$key]['comments_log'] as $keyC => $valueC){
                $result['data'][$key]['comments_log'][$keyC]['contract_review_action_item_id']=pk_encrypt($result['data'][$key]['comments_log'][$keyC]['contract_review_action_item_id']);
                $result['data'][$key]['comments_log'][$keyC]['id_contract_review_action_item_log']=pk_encrypt($result['data'][$key]['comments_log'][$keyC]['id_contract_review_action_item_log']);
                $result['data'][$key]['comments_log'][$keyC]['updated_by']=pk_encrypt($result['data'][$key]['comments_log'][$keyC]['updated_by']);
            }

        }
        //echo '<pre>';print_r($result);exit;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function actionItemDetails_get(){
        //
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_contract_review_action_item', array('required'=>$this->lang->line('id_contract_review_action_item_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_contract_review_action_item'])) {
            $data['id_contract_review_action_item'] = pk_decrypt($data['id_contract_review_action_item']);
            if(!in_array($data['id_contract_review_action_item'],$this->session_user_contract_action_items)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $result = $this->Contract_model->getActionItemDetails($data);
        foreach($result as $key => $value){
            $result[$key]['comments_log'] = $this->Contract_model->contractReviewActionItemLog(array('id_contract_review_action_item' => $result[$key]['id_contract_review_action_item']));
            $result[$key]['due_date'] = date('Y-m-d',strtotime($result[$key]['due_date']));
            foreach($result[$key]['comments_log'] as $keyC => $valueC){
                $result[$key]['comments_log'][$keyC]['contract_review_action_item_id']=pk_encrypt($result[$key]['comments_log'][$keyC]['contract_review_action_item_id']);
                $result[$key]['comments_log'][$keyC]['id_contract_review_action_item_log']=pk_encrypt($result[$key]['comments_log'][$keyC]['id_contract_review_action_item_log']);
                $result[$key]['comments_log'][$keyC]['updated_by']=pk_encrypt($result[$key]['comments_log'][$keyC]['updated_by']);
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function providers_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('user_role_id', array('required'=>$this->lang->line('user_role_id_req')));
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['responsible_user_id'])) {
            $data['responsible_user_id'] = pk_decrypt($data['responsible_user_id']);
            if($data['responsible_user_id']!=$this->session_user_customer_all_users){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['delegate_id'])) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id']) && isset($data['id_user'])){
            if($data['user_role_id']==2){

            }
            else if($data['user_role_id']==3){
                $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $data['id_user'],'status' => '1'));
                $data['business_unit_id'] = array_map(function($i){ return $i['id_business_unit']; },$business_unit);
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            else if($data['user_role_id']==4) {
                $data['delegate_id'] = $data['id_user'];
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            else
                $data['responsible_user_id'] = $data['id_user'];
        }
        $data = tableOptions($data);
        $result = $this->Contract_model->getProvidersList($data);
        foreach($result as $k=>$v){
            $result[$k]['contract_id']=pk_encrypt($result[$k]['contract_id']);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getTopic_get()
    {
        $data = $this->input->get();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_topic', array('required'=>$this->lang->line('topic_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_topic'])) {
            $data['id_topic'] = pk_decrypt($data['id_topic']);
            if(!in_array($data['id_topic'],$this->session_user_contract_review_topics)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $result = $this->Contract_model->getTopicData($data);

        foreach($result['questions'] as $k => $v){
           foreach($result['questions'][$k]['attachments'] as $key =>$val){

               $result['questions'][$k]['attachments'][$key]['document_source']=($result['questions'][$k]['attachments'][$key]['document_source']);
               $result['questions'][$k]['attachments'][$key]['id_document']=pk_encrypt($result['questions'][$k]['attachments'][$key]['id_document']);
               $result['questions'][$k]['attachments'][$key]['module_id']=pk_encrypt($result['questions'][$k]['attachments'][$key]['module_id']);
               $result['questions'][$k]['attachments'][$key]['reference_id']=pk_encrypt($result['questions'][$k]['attachments'][$key]['reference_id']);
           }
            $result['questions'][$k]['id_question']=pk_encrypt($result['questions'][$k]['id_question']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contractDetails_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('user_role_id', array('required'=>$this->lang->line('user_role_id_req')));
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id']) && !is_array($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all') {
            $data['id_business_unit'] = pk_decrypt($data['business_unit_id']);
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['id_business_unit'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id']) && isset($data['id_user'])){
            $coordinateVal = 50;
            $coordinateMaxVal = 100;
            $coordinateMinVal = 0;
            $coordinateAddVal = 25;

            $currency = $this->Master_model->getCurrencyList(array());
            /*if($data['user_role_id']==2){*/
            $quadrantLabelTR = $quadrantLabelTL = $quadrantLabelBL = $quadrantLabelBR = '';
            $quadrants = $this->Relationship_category_model->RelationshipCategoryList(array('customer_id' => $data['customer_id']));
            if(isset($quadrants['data']) && count($quadrants['data']>0)){
                $quadrants = $quadrants['data'];
                foreach($quadrants as $k=>$v){
                    if($v['relationship_category_quadrant'] == 'Q1'){
                        $quadrantLabelTR = $v['relationship_category_name'];
                    }
                    if($v['relationship_category_quadrant'] == 'Q2'){
                        $quadrantLabelTL = $v['relationship_category_name'];
                    }
                    if($v['relationship_category_quadrant'] == 'Q3'){
                        $quadrantLabelBL = $v['relationship_category_name'];
                    }
                    if($v['relationship_category_quadrant'] == 'Q4'){
                        $quadrantLabelBR = $v['relationship_category_name'];
                    }
                }
            }

            $xaxis = $this->Relationship_category_model->getRelationshipClassification(array('customer_id' => $data['customer_id']));
            /*print_r($xaxis); die('end');*/
            if($xaxis && is_array($xaxis) && isset($xaxis[0]['classification_position'])){
                foreach($xaxis as $k=>$v){
                    if($v['classification_position'] == 'x'){
                        $xaxis = $v['is_visible'];
                    }
                    if($v['classification_position'] == 'y'){
                        $yaxis = $v['is_visible'];
                    }
                }
            }

            if($xaxis == '0'){
                $left = '';
                $right = '';
            } else {
                $left = $this->Relationship_category_model->getRelationshipClassification(array('customer_id' => $data['customer_id'], 'classification_position' => 'left','classification_status'=>1));
                if($left && is_array($left) && isset($left[0]['classification_name'])){
                    $left = $left[0]['classification_name'];
                }
                $right = $this->Relationship_category_model->getRelationshipClassification(array('customer_id' => $data['customer_id'], 'classification_position' => 'right','classification_status'=>1));
                if($right && is_array($right) && isset($right[0]['classification_name'])){
                    $right = $right[0]['classification_name'];
                }
            }

            if($yaxis == '0'){
                $high = '';
                $low = '';
            } else {
                $low = $this->Relationship_category_model->getRelationshipClassification(array('customer_id' => $data['customer_id'], 'classification_position' => 'low','classification_status'=>1,''));
                if($low && is_array($low) && isset($low[0]['classification_name'])){
                    $low = $low[0]['classification_name'];
                }
                $high = $this->Relationship_category_model->getRelationshipClassification(array('customer_id' => $data['customer_id'], 'classification_position' => 'high','classification_status'=>1));
                if($high && is_array($high) && isset($high[0]['classification_name'])){
                    $high = $high[0]['classification_name'];
                }
            }

            //Contract
            if(isset($data['user_role_id']) && isset($data['id_user'])){
                if(in_array($data['user_role_id'],array(3))){
                    $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $data['id_user'],'status' => '1'));
                    $data['business_unit_id'] = array_map(function($i){ return $i['id_business_unit']; },$business_unit);
                    $data['session_user_role']=$this->session_user_info->user_role_id;
                    $data['session_user_id']=$this->session_user_id;
                }
                if($data['user_role_id']==4){
                    $data['delegate_id'] = $data['id_user'];
                    $data['session_user_role']=$this->session_user_info->user_role_id;
                    $data['session_user_id']=$this->session_user_id;
                }
                if($data['user_role_id']==5){
                    $data['customer_user'] = $data['id_user'];
                }
                if($data['user_role_id']==6){
                    $data['business_unit_id'] = $this->session_user_business_units;
                }
            }

            /*helper function for ordering smart table grid options*/
            $data = tableOptions($data);
            $result = $this->Contract_model->getContractList($data);
            for($s=0;$s<count($result['data']);$s++)
            {
                $result['data'][$s]['review_by'] = $result['data'][$s]['last_review'] = '---';
                $last_finalized_review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result['data'][$s]['id_contract'],'order' => 'DESC','contract_review_status'=>'finished'));
                if(!empty($last_finalized_review) && isset($last_finalized_review[0]['id_contract_review']) && $last_finalized_review[0]['id_contract_review']!='' && $last_finalized_review[0]['id_contract_review']!=0) {
                    $result['data'][$s]['review_by'] = $last_finalized_review[0]['review_by'];
                    if($last_finalized_review[0]['review_on']!='---')
                        $result['data'][$s]['last_review'] = date('Y-m-d',strtotime($last_finalized_review[0]['review_on']));
                }
                $review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result['data'][$s]['id_contract'],'order' => 'DESC'));
                if(!empty($review) && isset($review[0]['id_contract_review']) && $review[0]['id_contract_review']!='' && $review[0]['id_contract_review']!=0) {
                    /*$result['data'][$s]['review_by'] = $review[0]['review_by'];
                    if($review[0]['review_on']!='---')
                        $result['data'][$s]['last_review'] = date('Y-m-d',strtotime($review[0]['review_on']));*/

                    //getting score of recent review
                    $module_score = $this->Contract_model->getContractReviewModuleScore(array('contract_review_id' => $review[0]['id_contract_review']));
                    for($sr=0;$sr<count($module_score);$sr++)
                    {
                        $module_score[$sr]['score'] = getScoreByCount($module_score[$sr]);
                    }
                    $result['data'][$s]['score'] = getScore($scope = array_map(function($i){ return strtolower($i['score']); },$module_score));
                }
                else{
                    $result['data'][$s]['score'] = 0;
                }

                $result['data'][$s]['contract_start_date'] = date('Y-m-d',strtotime($result['data'][$s]['contract_start_date']));
                $result['data'][$s]['contract_end_date'] = date('Y-m-d',strtotime($result['data'][$s]['contract_end_date']));
                //getting action items of a recent review based on user role
                if(!empty($review) && isset($review[0]['id_contract_review']) && $review[0]['id_contract_review']!='' && $review[0]['id_contract_review']!=0) {
                    $action_data = array('id_contract' => $result['data'][$s]['id_contract']);
                    if (isset($data['id_user']))
                        $action_data['id_user'] = $data['id_user'];
                    if (isset($data['user_role_id']))
                        $action_data['user_role_id'] = $data['user_role_id'];
                    $action_data['item_status'] = 1;
                    $action_data['id_contract_review'] = $review[0]['id_contract_review'];
                    //getting action items count of a recent review
                    $result['data'][$s]['action_item_count'] = count($this->Contract_model->getContractReviewActionItemsList($action_data));
                }
                else{
                    $result['data'][$s]['action_item_count']=0;
                }
            }

            $contract = $result['data'];

            $quadrantMaxVal = 0;
            $firstQuadrant = $secondQuadrant = $thirdQuadrant = $fourthQuadrant = '';
            $data = array();
            $color = array('red' => '#ff0000','amber' =>  '#ff9900','green' =>  '#5bb166','blue' =>  '#0c7cd5','n/a'=>'#ccc');
            $currentcyLabel = '&euro;';
            foreach($contract as $k=>$v){
                if($v['relationship_category_name'] == $quadrantLabelTR){
                    $firstQuadrant[$k] = $v;
                }
                if($v['relationship_category_name'] == $quadrantLabelTL){
                    $secondQuadrant[$k] = $v;
                }
                if($v['relationship_category_name'] == $quadrantLabelBL){
                    $thirdQuadrant[$k] = $v;
                }
                if($v['relationship_category_name'] == $quadrantLabelBR){
                    $fourthQuadrant[$k] = $v;
                }
                $quadrantMaxVal += $v['contract_value'];
            }

            $firstIndx = $secondIndx = $thirdIndx = $fourthIndx = 1;
            $firstQuadrantCount = $secondQuadrantCount = $thirdQuadrantCount = $fourthQuadrantCount = 1;
            $firstQuadrantCount=$firstQuadrantCount+count($firstQuadrant);
            $secondQuadrantCount=$secondQuadrantCount+count($secondQuadrant);
            $thirdQuadrantCount=$thirdQuadrantCount+count($thirdQuadrant);
            $fourthQuadrantCount=$fourthQuadrantCount+count($fourthQuadrant);

            foreach($contract as $k=>$v){
                if($v['relationship_category_name'] == $quadrantLabelTR){
                    $x = $coordinateVal+(($coordinateVal/$firstQuadrantCount)*$firstIndx);
                    $y = $coordinateVal+(($coordinateVal/$firstQuadrantCount)*$firstIndx);
                    if($firstIndx%3==0) {
                        //$y = $y + 10;//random
                    }
                    else if($firstIndx%3==1) {
                        $y = $y + 5;//random
                    }
                    else {
                        $x = $x + 5;//random
                    }
                    /*$x=rand($coordinateVal+5,$coordinateMaxVal-5);
                    $y=rand($coordinateVal+5,$coordinateMaxVal-5);*/
                    $z = ($v['contract_value']/$quadrantMaxVal)*100;

                    $coordinate = 1;
                    $firstIndx++;
                }
                if($v['relationship_category_name'] == $quadrantLabelTL){
                    $x = $coordinateVal-(($coordinateVal/$secondQuadrantCount)*$secondIndx);
                    $y = $coordinateVal+(($coordinateVal/$secondQuadrantCount)*$secondIndx);
                    if($secondIndx%3==0) {
                        //$y = $y + 10;//random
                    }
                    else if($secondIndx%3==1) {
                        $y = $y + 5;//random
                    }
                    else {
                        $x = $x - 5;//random
                    }
                    $z = ($v['contract_value']/$quadrantMaxVal)*100;
                    /*$x=rand($coordinateMinVal+5,$coordinateVal-5);
                    $y=rand($coordinateVal+5,$coordinateMaxVal-5);*/
                    $coordinate = 2;
                    $secondIndx++;
                }
                if($v['relationship_category_name'] == $quadrantLabelBL){
                    $x = $coordinateVal-(($coordinateVal/$thirdQuadrantCount)*$thirdIndx);
                    $y = $coordinateVal-(($coordinateVal/$thirdQuadrantCount)*$thirdIndx);
                    if($thirdIndx%3==0) {
                        //$y = $y - 10;//random
                    }
                    else if($thirdIndx%3==1) {
                        $y = $y - 5;//random
                    }
                    else {
                        $x = $x - 5;//random
                    }
                    /*$x=rand($coordinateMinVal+5,$coordinateVal-5);
                    $y=rand($coordinateMinVal+5,$coordinateVal-5);*/
                    $z = ($v['contract_value']/$quadrantMaxVal)*100;
                    $coordinate = 3;
                    $thirdIndx++;
                }
                if($v['relationship_category_name'] == $quadrantLabelBR){
                    $x = $coordinateVal+(($coordinateVal/$fourthQuadrantCount)*$fourthIndx);
                    $y = $coordinateVal-(($coordinateVal/$fourthQuadrantCount)*$fourthIndx);
                    if($fourthIndx%3==0) {
                        //$y = $y - 10;//random
                    }
                    else if($fourthIndx%3==1) {
                        $y = $y - 5;//random
                    }
                    else {
                       $x = $x + 5;//random
                    }
                    /*$x=rand($coordinateVal+5,$coordinateMaxVal-5);
                    $y=rand($coordinateMinVal+5,$coordinateVal-5);*/
                    $z = ($v['contract_value']/$quadrantMaxVal)*100;
                    $coordinate = 4;
                    $fourthIndx++;
                }

                if($z<1){
                    $z = 1;
                }
                foreach($currency as $k1=>$v1){
                    if($v['currency_id'] == $v1['id_currency']){
                        $z = $z*$v1['euro_equivalent_value'];
                        $currencyName = $v1['currency_name'];
                    }
                }
                $v['contract_value'] = $this->a_number_format($v['contract_value'], 2, '.',",",3);

                $status = '';
                $score = "<tr><td class='labelDiv' align='right'>Score</td><td class=''> </td><td class='allpadding'><span class='status-widget font-weight-bold' >";
                if($v['contract_status']=='new' || $v['score']=='0'){
                    $score .= "<span class=''></span><span class=''></span><span class=''></span>";
                    $scoreText = '';
                } else {
                    $scoreText = strtolower($v['score']);
                    if($scoreText == 'red')
                        $score .= "<span class='".strtolower($scoreText)."-active'></span><span class=''></span><span class=''></span>";
                    else if($scoreText == 'amber')
                        $score .= "<span class=''></span><span class='".strtolower($scoreText)."-active'></span><span class=''></span>";
                    else if($scoreText == 'green')
                        $score .= "<span class=''></span><span class=''></span><span class='".strtolower($scoreText)."-active'></span>";
                    else
                        $score .= "<span class=''></span><span class=''></span><span class=''></span>";
                }
                $score .= "</span></td></tr>";

                if($v['contract_status']=='new' || $scoreText == ''){
                    $bubble_color=$color['blue'];
                }
                else{
                    $bubble_color=$color[$scoreText];
                }

                if(isset($v['contract_status'])){
                    $status = "<tr><td class='labelDiv' align='right'>Status</td><td class=''> </td><td class='allpadding'>".ucfirst($v['contract_status'])."</td></tr>";
                }

                $last_review = "<tr><td class='labelDiv' align='right'>Last Review</td><td class=''> </td><td class='allpadding'>";
                if($v['last_review'] != '---'){
                    $last_review .= date('M d,Y',strtotime($v['last_review']));
                }else{
                    $last_review .= " -- ";
                }
                $last_review .= "</td></tr>";

                $created_on = date('M d,Y',strtotime($v['created_on']));
                $contract_value=currencyFormat($v['contract_value'],'EUR');
                $data[] = array(
                    'x' => $x,
                    'y' => $y,
                    'z' => $z,
                    'name' => $v['contract_name'],
                    'color' => $bubble_color,
                    'tooltext' => "<div class='color_change'><table width='220'><tr><td class='labelDiv' align='right'>Provider</td><td class=''> </td><td class='allpadding'>{$v['provider_name']}</td></tr><tr><td class='labelDiv' align='right'>Contract Name</td><td class=''> </td><td class='allpadding'>{$v['contract_name']}</td></tr><tr><td class='labelDiv' align='right'>Contract Value</td><td class=''> </td><td class='allpadding'>{$contract_value} {$currentcyLabel}</td></tr><tr><td class='labelDiv' align='right'>Created Date</td><td class=''> </td><td class='allpadding'>{$created_on}</td></tr>{$status}{$last_review}{$score}</table></div>",
                    'coordinate' => $coordinate,
                    'review' => $v['last_review'],
                    'score' => $scoreText
                );
            }

            $result = array(
                'chart' =>  array(
                    "xAxisMinValue" =>  "0",
                    "xAxisMaxValue" =>  ($coordinateVal*2),
                    "yAxisMinValue" =>  0,
                    "yAxisMaxValue" =>  ($coordinateVal*2),
                    "plotFillAlpha" => 80,
                    "showYAxisvalue" => "0",
                    "numDivlines" =>  "0",
                    "showValues" => "0",
                    "showTrendlineLabels" =>  "0",
                    "quadrantLabelTL" =>  $quadrantLabelTL,
                    "quadrantLabelTR" =>  $quadrantLabelTR,
                    "quadrantLabelBL" =>  $quadrantLabelBL,
                    "quadrantLabelBR" =>  $quadrantLabelBR,
                    "quadrantLabelFontBold" => "1",
                    "quadrantLabelFontSize" => "11",
                    "quadrantLabelFont" => "verdana",
                    "toolTipBgColor" =>  "#ECECEC",
                    "toolTipBorderColor" =>  "#000",
                    "drawQuadrant"  =>  "1",
                    "quadrantXVal" =>  "50",
                    "quadrantYVal" =>  "50",
                    "quadrantLineAlpha"  =>  "50",
                    "quadrantLineThickness"  =>  "1",
                    "theme" =>  "fint",
                    "showHoverEffect" => '0',
                    'maxLabelWidthPercent ' => '70'
                    /*"yAxisName" => "test&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;test2",
                    "rotateYAxisName" => "1"*/
                ),
                "categories" => array(
                    "category" => array(
                        array(
                            "label" =>  ' ',
                            "x" =>  "0"
                        ),
                        array(
                            "label" =>  ' ',
                            "x" =>  "100"
                        )
                    )
                ),
                "dataset" => array(
                    array(
                        "color" => "#00aee4",
                        "data" =>  $data
                    )
                ),
                "classficationRelation" => array(
                        'left' => $left,
                        'right' => $right,
                        'low' => $low,
                        'high' => $high,
                    )
            );

            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    private function a_number_format($number_in_iso_format, $no_of_decimals=3, $decimals_separator='.', $thousands_separator='', $digits_grouping=3){
        // Check input variables
        if (!is_numeric($number_in_iso_format)){
            error_log("Warning! Wrong parameter type supplied in my_number_format() function. Parameter \$number_in_iso_format is not a number.");
            return false;
        }
        if (!is_numeric($no_of_decimals)){
            error_log("Warning! Wrong parameter type supplied in my_number_format() function. Parameter \$no_of_decimals is not a number.");
            return false;
        }
        if (!is_numeric($digits_grouping)){
            error_log("Warning! Wrong parameter type supplied in my_number_format() function. Parameter \$digits_grouping is not a number.");
            return false;
        }


        // Prepare variables
        $no_of_decimals = $no_of_decimals * 1;


        // Explode the string received after DOT sign (this is the ISO separator of decimals)
        $aux = explode(".", $number_in_iso_format);
        // Extract decimal and integer parts
        $integer_part = $aux[0];
        $decimal_part = isset($aux[1]) ? $aux[1] : '';

        // Adjust decimal part (increase it, or minimize it)
        if ($no_of_decimals > 0){
            // Check actual size of decimal_part
            // If its length is smaller than number of decimals, add trailing zeros, otherwise round it
            if (strlen($decimal_part) < $no_of_decimals){
                $decimal_part = str_pad($decimal_part, $no_of_decimals, "0");
            } else {
                $decimal_part = substr($decimal_part, 0, $no_of_decimals);
            }
        } else {
            // Completely eliminate the decimals, if there $no_of_decimals is a negative number
            $decimals_separator = '';
            $decimal_part       = '';
        }

        // Format the integer part (digits grouping)
        if ($digits_grouping > 0){
            $aux = strrev($integer_part);
            $integer_part = '';
            for ($i=strlen($aux)-1; $i >= 0 ; $i--){
                if ( $i % $digits_grouping == 0 && $i != 0){
                    $integer_part .= "{$aux[$i]}{$thousands_separator}";
                } else {
                    $integer_part .= $aux[$i];
                }
            }
        }

        $processed_number = "{$integer_part}{$decimals_separator}{$decimal_part}";
        return $processed_number;
    }

    public function contract_log_get(){
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => $this->lang->line('invalid_data'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_log_id'])) $data['contract_log_id']=pk_decrypt($data['contract_log_id']);
        $current_cotract_detailis=array();
        $contract_log_options=array();
        if(isset($data['contract_id'])){
            $current_cotract_detailis = $this->Contract_model->getContractCurrentDetails($data);
            $contract_log_options = $this->Contract_model->getContractLogId($data);

            /*foreach($contract_log_options as $k => $v){
                $contract_log_options[$k]['log_option'] = $v['created_on'].' by '.$v['created_by'];
            }*/
        }
        $contract_log_details=array();
        if(isset($data['contract_log_id']))
            $contract_log_details = $this->Contract_model->getContractLogDetails($data);
        foreach($contract_log_options as $k=>$v){
            $contract_log_options[$k]['id_contract_log']=pk_encrypt($contract_log_options[$k]['id_contract_log']);
        }
        foreach($current_cotract_detailis as $k=>$v){
            $current_cotract_detailis[$k]['business_unit_id']=pk_encrypt($current_cotract_detailis[$k]['business_unit_id']);
            $current_cotract_detailis[$k]['classification_id']=pk_encrypt($current_cotract_detailis[$k]['classification_id']);
            $current_cotract_detailis[$k]['contract_owner_id']=pk_encrypt($current_cotract_detailis[$k]['contract_owner_id']);
            $current_cotract_detailis[$k]['currency_id']=pk_encrypt($current_cotract_detailis[$k]['currency_id']);
            $current_cotract_detailis[$k]['delegate_id']=pk_encrypt($current_cotract_detailis[$k]['delegate_id']);
            $current_cotract_detailis[$k]['id_contract']=pk_encrypt($current_cotract_detailis[$k]['id_contract']);
            $current_cotract_detailis[$k]['relationship_category_id']=pk_encrypt($current_cotract_detailis[$k]['relationship_category_id']);
            $current_cotract_detailis[$k]['updated_by']=pk_encrypt($current_cotract_detailis[$k]['updated_by']);
        }
        foreach($contract_log_details as $k=>$v){
            $contract_log_details[$k]['business_unit_id']=pk_encrypt($contract_log_details[$k]['business_unit_id']);
            $contract_log_details[$k]['classification_id']=pk_encrypt($contract_log_details[$k]['classification_id']);
            $contract_log_details[$k]['contract_id']=pk_encrypt($contract_log_details[$k]['contract_id']);
            $contract_log_details[$k]['contract_owner_id']=pk_encrypt($contract_log_details[$k]['contract_owner_id']);
            $contract_log_details[$k]['currency_id']=pk_encrypt($contract_log_details[$k]['currency_id']);
            $contract_log_details[$k]['delegate_id']=pk_encrypt($contract_log_details[$k]['delegate_id']);
            $contract_log_details[$k]['id_contract_log']=pk_encrypt($contract_log_details[$k]['id_contract_log']);
            $contract_log_details[$k]['relationship_category_id']=pk_encrypt($contract_log_details[$k]['relationship_category_id']);
        }
        $result =array('current_cotract_detailis'=>$current_cotract_detailis,'contract_log_options'=>$contract_log_options,'contract_log_details'=>$contract_log_details);
        //echo '<pre>';print_r($contract_log_options);exit;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);

    }


    public function getDownloadedFile_get(){
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => $this->lang->line('invalid_data'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_document'])) {
            $data['id_document'] = pk_decrypt($data['id_document']);
            if(!in_array($data['id_document'],$this->session_user_contract_documents)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if (isset($data['id_document'])) {
            $result = $this->Contract_model->getDownloadedFile($data);
            if (count($result) > 0) {
                $res = array('url' => REST_API_URL . 'uploads/' . $result[0]['document_source'],
                    'file' => $result[0]['document_name']);
                $insert_id = $this->Download_model->addDownload(array('path'=>'uploads/' . $result[0]['document_source'],'filename'=>$result[0]['document_name'],'user_id'=>pk_decrypt($_SERVER['HTTP_USER']),'access_token'=>substr($_SERVER['HTTP_AUTHORIZATION'],7),'status'=>0,'created_date_time'=>currentDate()));
                $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => pk_encrypt($insert_id));
                $this->response($result, REST_Controller::HTTP_OK);
            } else {
                $result = array('status' => false, 'message' => $this->lang->line('error'), 'data' => '');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        } else {
            $result = array('status' => false, 'message' => $this->lang->line('error'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }
    public function contractReviewChangelog_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $this->form_validator->add_rules('contract_review_id', array('required'=>$this->lang->line('contract_review_id_req')));
        //$this->form_validator->add_rules('module_id', array('required'=>$this->lang->line('module_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review'])) {
            $data['id_contract_review'] = pk_decrypt($data['id_contract_review']);
            if(!in_array($data['id_contract_review'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_module']) && $data['id_module']!='all') {
            $data['id_module'] = pk_decrypt($data['id_module']);
            if(!in_array($data['id_module'],$this->session_user_contract_review_modules)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_topic']) && $data['id_topic']!='all') {
            $data['id_topic'] = pk_decrypt($data['id_topic']);
            if(!in_array($data['id_topic'],$this->session_user_contract_review_topics)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_question']) && $data['id_question']!='all') {
            $data['id_question'] = pk_decrypt($data['id_question']);
            if(!in_array($data['id_question'],$this->session_user_contract_review_questions)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract'])) {
            $data['id_contract'] = pk_decrypt($data['id_contract']);
            if(!in_array($data['id_contract'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user']) && isset($data['user_role_id']) && $data['user_role_id']==5){
            $data['contract_user'] = $data['id_user'];
            if(!in_array($data['contract_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(isset($data['id_user']) && isset($data['id_contract_review'])){
            $idaadi=$this->Contract_model->checkContributorForContractReview(array('contract_review_id'=>$data['id_contract_review'],'id_user'=>$data['id_user']));
            if($idaadi===true)
            $data['contract_user'] = $data['id_user'];
        }
        if(isset($data['id_user']) && isset($data['contract_review_id'])){
            $idaadi=$this->Contract_model->checkContributorForContractReview(array('contract_review_id'=>$data['contract_review_id'],'id_user'=>$data['id_user']));
            if($idaadi===true)
                $data['contract_user'] = $data['id_user'];
        }


        $result = $this->Contract_model->getContractReviewChangeLog($data);
        //echo "<pre>";print_r($result);echo "</pre>";
        $result_parsed=array();
        foreach($result['questions'] as $k=>$v){
            $result_parsed[$v['id_question']]['id_module']=$v['id_module'];
            $result_parsed[$v['id_question']]['id_topic']=$v['id_topic'];
            $result_parsed[$v['id_question']]['id_question']=$v['id_question'];
            $result_parsed[$v['id_question']]['question_text']=$v['question_text'];
            $result_parsed[$v['id_question']]['question_type']=$v['question_type'];
            $result_parsed[$v['id_question']]['topic_name']=$v['topic_name'];
            $result_parsed[$v['id_question']]['module_name']=$v['module_name'];
            $result_parsed[$v['id_question']]['change_log'][]=$v;
        }
        $result['questions']=array_values($result_parsed);
        foreach($result['questions'] as $k=>$v){
            $result['questions'][$k]['change_log']=array_reverse($v['change_log']);
        }
        $reviews = $this->Contract_model->getContractReview(array('id_contract_review'=>$data['contract_review_id']));
        for($s=0;$s<count($reviews);$s++)
        {

            $result['review_information'] = array(
                'review_date' => ($reviews[$s]['updated_date']!='')?date('Y-m-d',strtotime($reviews[$s]['updated_date'])):'',
                'review_status' => ($reviews[$s]['contract_review_status']=='finished')?'review finalized':$reviews[$s]['contract_review_status'],
                'ideedi' => (count($this->Contract_model->getContractReviewDiscussionModuleCount(array('id_contract_review'=>$data['contract_review_id'],'discussion_status'=>1)))>0)?"itako":'annus'
            );

        }
        $module_data =  $this->Contract_model->getContractDashboard(array('contract_review_id' => $data['contract_review_id']));
        $result_array['modules'] = array();
        for($s=0;$s<count($module_data);$s++)
        {
            $result_array['modules'][$module_data[$s]['module_id']]['module_id'] = $module_data[$s]['module_id'];
            $result_array['modules'][$module_data[$s]['module_id']]['module_name'] = $module_data[$s]['module_name'];
            $result_array['modules'][$module_data[$s]['module_id']]['topics'][] = array(
                'topic_id' => $module_data[$s]['topic_id'],
                'topic_name' => $module_data[$s]['topic_name'],
                'topic_score' => $module_data[$s]['topic_score'],
            );
        }

        $result_array['modules'] = array_values($result_array['modules']);

        for($s=0;$s<count($result_array['modules']);$s++) //getting score for module by topics score // getScore is a helper function
        {
            $result_array['modules'][$s]['module_score'] = getScore($score = array_map(function($i){ return strtolower($i['topic_score']); },$result_array['modules'][$s]['topics']));
        }

        $result['review_information']['review_score'] = getScore($score = array_map(function($i){ return strtolower($i['module_score']); },$result_array['modules']));
        for($s=0;$s<count($result['modules']);$s++) //getting score for module by topics score // getScore is a helper function
        {
            $result['modules'][$s]['id_module'] = pk_encrypt($result['modules'][$s]['id_module']);
        }
        for($s=0;$s<count($result['topics']);$s++) //getting score for module by topics score // getScore is a helper function
        {
            $result['topics'][$s]['id_module'] = pk_encrypt($result['topics'][$s]['id_module']);
            $result['topics'][$s]['id_topic'] = pk_encrypt($result['topics'][$s]['id_topic']);
        }
        for($s=0;$s<count($result['questions']);$s++)
        {
            $result['questions'][$s]['id_module'] = pk_encrypt($result['questions'][$s]['id_module']);
            $result['questions'][$s]['id_topic'] = pk_encrypt($result['questions'][$s]['id_topic']);
            $result['questions'][$s]['id_question'] = pk_encrypt($result['questions'][$s]['id_question']);
            foreach($result['questions'][$s]['change_log'] as $kc=>$vc){
                $result['questions'][$s]['change_log'][$kc]['id_module']=pk_encrypt($result['questions'][$s]['change_log'][$kc]['id_module']);
                $result['questions'][$s]['change_log'][$kc]['id_question']=pk_encrypt($result['questions'][$s]['change_log'][$kc]['id_question']);
                $result['questions'][$s]['change_log'][$kc]['id_topic']=pk_encrypt($result['questions'][$s]['change_log'][$kc]['id_topic']);
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function info_export($data){
        //this is exporting contract information result to export service4
        $result = $this->Contract_model->getContractDetails($data);
        //echo $this->db->last_query(); exit;
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['contract_start_date'] = date('M d, Y',strtotime($result[$s]['contract_start_date']));
            $result[$s]['contract_end_date'] = date('M d, Y',strtotime($result[$s]['contract_end_date']));
            $result[$s]['contract_sponsor'] = array('internal'=>$result[$s]['internal_contract_sponsor'],'provider'=>$result[$s]['provider_contract_sponsor']);
            $result[$s]['partner_relationship_manager'] = array('internal'=>$result[$s]['internal_partner_relationship_manager'],'provider'=>$result[$s]['provider_partner_relationship_manager']);
            $result[$s]['contract_responsible'] = array('internal'=>$result[$s]['internal_contract_responsible'],'provider'=>$result[$s]['provider_contract_responsible']);
            $result[$s]['score'] = 0;
            $result[$s]['review_by'] = $result[$s]['last_review'] = '---';
            $review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result[$s]['id_contract']));
            if(!empty($review)) {
                $result[$s]['review_by'] = $review[0]['review_by'];
                if($review[0]['review_on']!='---')
                    $result[$s]['last_review'] = date('M d, Y',strtotime($review[0]['review_on']));
            }
        }
        return $result;
    }

    public function dashboard_export($data){
        //this is exporting dashboard result to export service4

        $result_array = array();
        $contributor_modules=array();
        if(isset($data['id_user']) && isset($data['user_role_id']) && $data['user_role_id']==5){
            $data['contract_user'] = $data['id_user'];
            $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['contract_user']));
            //echo "<pre>";print_r($contributor_modules);echo "</pre>";
            $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
            //echo "<pre>";print_r($contributor_modules);echo "</pre>";exit;
        }
        else{
            $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['id_user']));
            //echo "<pre>";print_r($contributor_modules);echo "</pre>";
            $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
        }
        $reviews = $this->Contract_model->getContractReview(array('contract_id'=>$data['id_contract'],'id_contract_review'=>$data['contract_review_id']));
        if(isset($data['contract_review_id'])){
            $contributor_modules=$this->Contract_model->getContractContributors(array('user_id'=>$data['id_user'],'contract_review_id' => $data['contract_review_id']));
            $contributor_modules = array_map(function($i){ return ($i['module_id']); },$contributor_modules);
            for($s=0;$s<count($reviews);$s++)
            {
                if($reviews[$s]['id_contract_review']==$data['contract_review_id']){
                    $result_array['data'] = array(
                        'review_date' => ($reviews[$s]['updated_date']!='')?date('M d, Y',strtotime($reviews[$s]['updated_date'])):'',
                        'review_created_date' => ($reviews[$s]['created_on']!='')?date('M d, Y',strtotime($reviews[$s]['created_on'])):'',
                        'review_status' => $reviews[$s]['contract_review_status']
                    );
                }
            }
            $module_data =  $this->Contract_model->getContractDashboard(array('contract_review_id' => $data['contract_review_id']));
        }
        $result_array['modules'] = array();
        for($s=0;$s<count($module_data);$s++)
        {
            if(count($contributor_modules)==0 || (count($contributor_modules)>0 && in_array($module_data[$s]['module_id'],$contributor_modules))) {
                $result_array['modules'][$module_data[$s]['module_id']]['module_id'] = $module_data[$s]['module_id'];
                $result_array['modules'][$module_data[$s]['module_id']]['module_name'] = $module_data[$s]['module_name'];
                $result_array['modules'][$module_data[$s]['module_id']]['topics'][] = array(
                    'topic_id' => $module_data[$s]['topic_id'],
                    'topic_name' => $module_data[$s]['topic_name'],
                    'topic_score' => $module_data[$s]['topic_score'],
                    'topic_progress' => $module_data[$s]['total_topic_progress']
                );
            }
        }

        $result_array['modules'] = array_values($result_array['modules']);

        for($s=0;$s<count($result_array['modules']);$s++) //getting score for module by topics score // getScore is a helper function
        {
            $result_array['modules'][$s]['module_score'] = getScore($score = array_map(function($i){ return strtolower($i['topic_score']); },$result_array['modules'][$s]['topics']));
        }

        $result_array['review_score'] = getScore($score = array_map(function($i){ return strtolower($i['module_score']); },$result_array['modules']));
        return $result_array;

    }

    public function reviewActionItems_export($data)
    {//this is exporting reviewActionItems to export service4

        /*helper function for ordering smart table grid options*/
        $data = tableOptions($data);
        $data['item_status']=1;
        unset($data['contract_review_id']);
        $result = $this->Contract_model->getContractReviewActionItems($data);
        for($s=0;$s<count($result['data']);$s++)
        {
            $result['data'][$s]['due_date'] = date('M d, Y',strtotime($result['data'][$s]['due_date']));
            $contract_review = $this->Contract_model->getContractReview(array('id_contract_review'=>$result['data'][$s]['contract_review_id']));
            $result['data'][$s]['last_review'] = $contract_review[0]['updated_date'];
        }
        return $result;
    }

    public function export_get(){
        //this function exports the dashboard data in excel format :
        ////////////////Geting data
        $data = $this->input->get();
        if(isset($data['id_contract'])) {
            $data['id_contract'] = pk_decrypt($data['id_contract']);
            if(!in_array($data['id_contract'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['module_id'])) {
            $data['module_id'] = pk_decrypt($data['module_id']);
            if(!in_array($data['module_id'],$this->session_user_contract_review_modules)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['topic_id'])) {
            $data['topic_id'] = pk_decrypt($data['topic_id']);
            if(!in_array($data['topic_id'],$this->session_user_contract_review_topics)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review'])) {
            $data['id_contract_review'] = pk_decrypt($data['id_contract_review']);
            if(!in_array($data['id_contract_review'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review_action_item'])) {
            $data['id_contract_review_action_item'] = pk_decrypt($data['id_contract_review_action_item']);
            if(!in_array($data['id_contract_review_action_item'],$this->session_user_contract_action_items)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_user'])) {
            $data['contract_user'] = pk_decrypt($data['contract_user']);
            if($data['contract_user']!=$this->session_user_customer_all_users){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id'])) {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['business_unit_id'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $data['contract_id']=$data['id_contract'];
        $reviews = $this->Contract_model->getContractReview(array('contract_id'=>$data['contract_id'],'order'=>'DESC'));
        $data['contract_review_id']=(isset($data['contract_review_id']) && $data['contract_review_id']>0)?$data['contract_review_id']:(isset($reviews[0]['id_contract_review'])?$reviews[0]['id_contract_review']:0);
        //$result_array = array();
        if(empty($data['contract_review_id']) || $data['contract_review_id']=='' || $data['contract_review_id']==0){
            $result = array('status'=>FALSE,'error'=>'No reviews found.','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result_array = $this->dashboard_export($data);
        $res = $result_array['modules'];
        $review_created_date = $result_array['data']['review_created_date'];
        $action_items =$this->reviewActionItems_export($data);

        $action_items = $action_items['data'];
        unset($data['contract_review_id']);
        $info = $this->info_export($data);
        $business_unit = $this->Business_unit_model->getBusinessUnitDetails(array('id_business_unit'=>$info[0]['business_unit_id']));
        $customer_info = $this->Customer_model->getCustomer(array('id_customer'=>$business_unit[0]['customer_id']));
        $logo = $customer_info[0]['company_logo'];
        $info = $info[0];
        //echo 'cust_data'.'<pre>';print_r($customer_info);
        //echo 'data'.'<pre>';print_r($data);
        //echo 'dashboard'.'<pre>';print_r($result_array);exit;
        //echo 'action'.'<pre>';print_r($action_items);
        //echo 'info_exoport'.'<pre>';print_r($info);
        //echo 'bu_info'.'<pre>';print_r($business_unit);exit;
        //print_r($action_items);die;
        ///////////////////////

        $this->load->library('excel');
        //activate worksheet number 1
        $excelRowstartsfrom=2;
        $excelColumnstartsFrom=1;
        $columnBegin =$excelColumnstartsFrom;
        $excelstartsfrom=$excelRowstartsfrom;
        $merge0 = $this->getkey($excelColumnstartsFrom).$excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom).($excelstartsfrom+2);
        $this->excel->setActiveSheetIndex(0)->mergeCells($merge0);


        $file_img = './uploads/'.$logo;
        if($logo=='')
            $file_img = './images/company-logo.png';
        if (file_exists($file_img)) {
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Customer Signature');
            $objDrawing->setDescription('Customer Signature');
            //Path to signature .jpg file
            $signature = $file_img;
            $objDrawing->setPath($signature);
            $objDrawing->setOffsetX(50);                     //setOffsetX works properly
            $objDrawing->setCoordinates($this->getkey($excelColumnstartsFrom).$excelstartsfrom);             //set image to cell E38
            $objDrawing->setHeight(61);                     //signature height
            $objDrawing->setWorksheet($this->excel->getActiveSheet());  //save
        }


        $excelRowstartsfrom=$excelRowstartsfrom+4;
        $excelColumnstartsFrom=1;
        $columnBegin =$excelColumnstartsFrom;
        $excelstartsfrom=$excelRowstartsfrom;
        $merge1 = $this->getkey($excelColumnstartsFrom).$excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1).($excelstartsfrom+1);
        $merge2 = $this->getkey($excelColumnstartsFrom+2).$excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+2).($excelstartsfrom+1);
        ///wrapping text

        //Action Items Date set width
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+2))->setWidth(12);

        ///

        /////contract info starts
        $this->excel->setActiveSheetIndex(0)->mergeCells($merge1);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'REVIEW DATE:');
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(31);
        $this->excel->getActiveSheet()->getStyle($merge1)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($merge2)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $this->excel->getActiveSheet()->getStyle($merge1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle($merge1)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($merge2);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['last_review']==''?'N/A':$info['last_review']);

        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+2))->setWidth('20');
        $excelstartsfrom+=4;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Provider');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['provider_name']);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Internal LOB or Function');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$business_unit[0]['bu_name']);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Contract Start-date');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['contract_start_date']);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Contract End-date');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['contract_end_date']);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        //$this->excel->setActiveSheetIndex()->getRowDimension($excelstartsfrom)->setRowHeight(30);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Short Scope Description');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        //$this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+6) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['description']);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+5) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        //$excelstartsfrom++;
        //
        $excelColumnstartsFrom=5;
        $excelstartsfrom = $excelRowstartsfrom;

        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Contract Name');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['contract_name']);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Provider contract sponsor');
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(30);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['provider_contract_sponsor']==''?'N/A':$info['provider_contract_sponsor']);
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+2))->setWidth('20');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Internal contract sponsor');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['internal_contract_sponsor']==''?'N/A':$info['internal_contract_sponsor']);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Internal Relationship Manager');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['internal_partner_relationship_manager']==''?'N/A':$info['internal_partner_relationship_manager']);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Internal Contract Governance');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['contract_responsible']['internal']==''?'N/A':$info['contract_responsible']['internal']);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Annual Contract Value');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $number = $info['contract_value'];
        $currency = number_format($number, 2, '.', ',');


        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->getNumberFormat()->setFormatCode("##,##,###.##");
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['currency_name'].' '.currencyFormat($currency,$info['currency_name']));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Classification');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['classification_name']);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'Relationship type');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,$info['relationship_category_name']);
        //$this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+1))->setAutoSize(true);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        //$this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+1))->getAlignment()->setWrapText(true);

        //$excelstartsfrom++;
        //
        /*$this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'OOC Reporting periodicity');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        //$this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom,'---');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'NBB Reporting');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom,'---');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,'CLIENT Group Reporting');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('D4E8F3FF');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom,'---');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        $excelstartsfrom++;*/
        //
        //finalising contract info
        $excelstartsfrom=$excelRowstartsfrom;
        $excelColumnstartsFrom=1;
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom).$excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+6).($excelstartsfrom+8))->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ))
        );

        /////contract info ends
        $excelColumnstartsFrom=1;
        $img_start=$excelstartsfrom=$excelRowstartsfrom+10;
        $exceleven = $excelstartsfrom;
        $excelodd = $exceleven;

        foreach($res as $i =>$v) {
            if($i%2!=0) {
                $excelColumnstartsFrom += 4;
                $excelstartsfrom = $excelodd;
            }
            else {
                $excelColumnstartsFrom = 1;
                $excelstartsfrom = $exceleven;
            }

            if($i%2!=0)
                $excelstartsfrom--;
            $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom, $res[$i]['module_name']);
            //$this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setAutosize(true);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
                array('borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'font'  => array(
                        'bold'  => true
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


            $score = $res[$i]['module_score'];
           /* $file_img_path = './images/'.$score.'.png';
            if (file_exists($file_img_path)) {
                $objDrawing = new PHPExcel_Worksheet_Drawing();
                $objDrawing->setName('Customer Signature');
                $objDrawing->setDescription('Customer Signature');
                //Path to signature .jpg file
                $signature = $file_img_path;
                $objDrawing->setPath($signature);
                $objDrawing->setOffsetX(65);                     //setOffsetX works properly
                $objDrawing->setCoordinates($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom);             //set image to cell E38
                $objDrawing->setHeight(15);                     //signature height
                $objDrawing->setWorksheet($this->excel->getActiveSheet());  //save
            }*/
			if(in_array(trim(strtolower($score)),array('red','amber','green'))){
				if(strtolower($score) == 'red')
					$color = 'FF0000';
				if(strtolower($score) == 'amber')
					$color = 'ff9900';
				if(strtolower($score) == 'green')
					$color = '5bb166';

				$this->excel->setActiveSheetIndex(0)
				->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,'•');
				$this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
				array('alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
        ),'borders' => array(
					'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					)
					),'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => $color),
					'size'  => 15,
					'name'  => 'Verdana'
				)));
			}
            else{
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom, $score);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->getAlignment()->setHorizontal(
                    PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                );

            }

            $headersel = $this->getkey($excelColumnstartsFrom) . $excelstartsfrom . ':' . $this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E8F3FF');
            $this->excel->getActiveSheet()->getStyle($headersel)->applyFromArray(
                array('borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )));
            //$rowsStratsFrom = $head_starts_from + 1;*/
            $excelstartsfrom++;

            foreach($res[$i]['topics'] as $j => $v1){
                //echo $res[$i]['module_name'];
                if(strtolower($res[$i]['module_name'])=='contract completeness'){
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom) .$excelstartsfrom , $res[$i]['topics'][$j]['topic_name']);
                    $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
                        array('borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )));

                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom+1) .$excelstartsfrom , ' '.(($res[$i]['topics'][$j]['topic_progress']==NULL)?0:round($res[$i]['topics'][$j]['topic_progress'],2)).' %');
                    $this->excel->getActiveSheet(0)->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
                        array('borders' => array(
                            'allboarders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )));


                }else{
                    $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);

                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom) .$excelstartsfrom , $res[$i]['topics'][$j]['topic_name']);
                    //$this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setAutosize(true);
                    $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
                        array('borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )));
                }

                //$this->excel->setActiveSheetIndex()->getRowDimension($excelstartsfrom)->setRowHeight(15);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $score = $res[$i]['topics'][$j]['topic_score'];
                //echo $score.' topic';
                /*$file_img_path = './images/'.$score.'.png';
                if (file_exists($file_img_path)) {
                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                    $objDrawing->setName('Customer Signature');
                    $objDrawing->setDescription('Customer Signature');
                    //Path to signature .jpg file
                    $signature = $file_img_path;
                    $objDrawing->setPath($signature);
                    $objDrawing->setOffsetX(65);                     //setOffsetX works properly
                    $objDrawing->setCoordinates($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom);             //set image to cell E38
                    $objDrawing->setHeight(15);                     //signature height
                    $objDrawing->setWorksheet($this->excel->getActiveSheet());  //save
                }*/
				if(in_array(trim(strtolower($score)),array('red','amber','green'))){
				if(strtolower($score) == 'red')
					$color = 'FF0000';
				if(strtolower($score) == 'amber')
					$color = 'ff9900';
				if(strtolower($score) == 'green')
					$color = '5bb166';

				$this->excel->setActiveSheetIndex(0)
				->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom,'•');
				$this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
				array('alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),'borders' => array(
					'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					)
					),'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => $color),
					'size'  => 15,
					'name'  => 'Verdana'
				)));
			}
                else{
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom, $score);
                    $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->getAlignment()->setHorizontal(
                        PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->applyFromArray(
                    array('borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )));
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
                    array('borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )));

                $excelstartsfrom++;
                if($i%2==0){
                    $exceleven++;
                }else{
                    $excelodd++;
                }
            }
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom, '');
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, '');
            $excelstartsfrom++;
            $exceleven++;
            $excelodd++;

        }
        $excelstartsfrom++;
        $excelColumnstartsFrom=1;
        $merge = $this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+6) . $excelstartsfrom;
        $this->excel->setActiveSheetIndex(0)->mergeCells($merge);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom, 'Action and remediation items');
        $this->excel->getActiveSheet()->getStyle($merge)->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'font'  => array(
                    'bold'  => true
                )
            )
        );
        $excelstartsfrom++;
        $action_items_row_starts= $excelstartsfrom;
        $this->excel->getActiveSheet()->getStyle($merge)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle($merge)->getFill()->getStartColor()->setARGB('D1D1D1d1');
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom, 'Action');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom, 'Target Date');
        //
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+3) . $excelstartsfrom, 'Status');
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+3))->setWidth(11);
        //
        $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom+4) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+6) . $excelstartsfrom);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+4) . $excelstartsfrom, 'Comments');

        $merge = $this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+6) . $excelstartsfrom;
        $this->excel->getActiveSheet()->getStyle($merge)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle($merge)->getFill()->getStartColor()->setARGB('D1D1D1d1');
        $this->excel->getActiveSheet()->getStyle($merge)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            )
        );
        //$this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+2))->setAutoSize(true);
        $excelstartsfrom++;
        foreach($action_items as $i => $v){
            //print_r($action_items);die;
            $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom, $action_items[$i]['action_item']);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom)->applyFromArray(
                array('borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )));
            //
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom, $action_items[$i]['due_date']);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . $excelstartsfrom)->applyFromArray(
                array('borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )));
            //
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+3) . $excelstartsfrom, $action_items[$i]['status']);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+3) . $excelstartsfrom)->applyFromArray(
                array('borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )));
            //
            $this->excel->setActiveSheetIndex(0)->mergeCells($this->getkey($excelColumnstartsFrom+4) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+6) . $excelstartsfrom);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+4) . $excelstartsfrom, $action_items[$i]['comments']);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+4) . $excelstartsfrom.':'.$this->getkey($excelColumnstartsFrom+6) . $excelstartsfrom)->applyFromArray(
                array('borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )));
            $excelstartsfrom++;
        }
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom).$excelRowstartsfrom.':'.$this->getkey($excelColumnstartsFrom+6).$excelstartsfrom)
            ->getAlignment()->setWrapText(true);
        /*$this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom).$action_items_row_starts.':'.$this->getkey($excelColumnstartsFrom+4).$excelstartsfrom)
            ->getAlignment()->setWrapText(true);*/

        $this->excel->getActiveSheet()->setSelectedCells('A0');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('CONTRACT DASHBOARD');
        $filename = $info['contract_name'].'_'.date("d-m-Y",strtotime($review_created_date)).'.xls'; //save our workbook as this file name
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $file_path = FILE_SYSTEM_PATH.'downloads/' . $filename;
        $objWriter->save($file_path);
        $view_path='downloads/' . $filename;
        $file_path = REST_API_URL.$view_path;
        $file_path = str_replace('::1','localhost',$file_path);

        $insert_id = $this->Download_model->addDownload(array('path'=>$view_path,'filename'=>$filename,'user_id'=>pk_decrypt($_SERVER['HTTP_USER']),'access_token'=>substr($_SERVER['HTTP_AUTHORIZATION'],7),'status'=>0,'created_date_time'=>currentDate()));

        $response = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>pk_encrypt($insert_id));
        $this->response($response, REST_Controller::HTTP_OK);


    }

    public function report_get(){
        //this function generates a report in excel.

        $data = $this->input->get();
        $header = array('last_review'=>'yes',
            'modules'=>array('selection_process'=>'Selection process','performance_plan'=>'Performance plan','contract_completeness'=>'contract completeness'),
            'rag'=>'yes','action_items'=>'yes','comments'=>'yes');
        $data = array();
        $data[0]=array(
            'business_unit'=>1,
            'supplier_contract'=>'abcd',
            'selection_process'=>'red',
            'amber_cnt'=>3,
            'red_cnt'=>4,
            'green_cnt'=>5,
            'action_items'=>8,
            'decision_required'=>'yes',
            'comments'=>'asfaf',
            'last_review_date'=>'21-04-2017',
            'performance_plan'=>'amber',
            'contract_completeness'=>'green',
            'classification'=>'abc'
        );
        $data[1]=array(
            'business_unit'=>2,
            'supplier_contract'=>'efgh',
            'selection_process'=>'red',
            'amber_cnt'=>3,
            'red_cnt'=>4,
            'green_cnt'=>5,
            'action_items'=>8,
            'decision_required'=>'ye1s',
            'comments'=>'asfaf',
            'last_review_date'=>'28-05-2017',
            'performance_plan'=>'green',
            'contract_completeness'=>'amber',
            'classification'=>'abc'
        );
        $report_data = array('report_name'=>'report1','customer_name'=>'Valued Customer','data'=>$data);

        $this->load->library('excel');
        //activate worksheet number 1
        $excelRowstartsfrom=3;
        $excelColumnstartsFrom=1;
        $columnBegin =$excelColumnstartsFrom;
        $excelstartsfrom=$excelRowstartsfrom;

        $count =$excelColumnstartsFrom+3;
        if(isset($header['last_review']) && $header['last_review']=='yes')
            $count++;
        if(isset($header['modules']))
            $count = $count + count($header['modules']);
        if(isset($header['rag']) && $header['rag']=='yes')
            $count = $count+3;
        if(isset($header['action_items']) && $header['action_items']=='yes')
            $count++;
        if(isset($header['comments']) && $header['comments']=='yes')
            $count++;

        $merge1 = $this->getkey($excelColumnstartsFrom).$excelstartsfrom.':'.$this->getkey($count).($excelstartsfrom);
               $this->excel->setActiveSheetIndex(0)->mergeCells($merge1);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . $excelstartsfrom,$report_data['customer_name']);
        $this->excel->getActiveSheet()->getStyle($merge1)->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),'font'  => array('bold'  => true,'size' =>36)));
        $this->excel->getActiveSheet()->getStyle($merge1)->getFont()->getColor()->setRGB('FFFFFF');

        $file_img_path1 = './images/report_img_3.png';
        if (file_exists($file_img_path1)) {
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Customer Signature');
            $objDrawing->setDescription('Customer Signature');
            //Path to signature .jpg file
            $signature = $file_img_path1;
            $objDrawing->setPath($signature);
            $objDrawing->setOffsetX(5);
            $objDrawing->setOffsetY(25);//setOffsetX works properly
            $objDrawing->setCoordinates($this->getkey($count-4) . $excelstartsfrom);             //set image to cell E38
            $objDrawing->setHeight(110);                     //signature height
            $objDrawing->setWorksheet($this->excel->getActiveSheet());  //save
        }
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $excelstartsfrom)->getFill()->getStartColor()->setARGB('d4b8cce4');

        $this->excel->getActiveSheet()->getRowDimension($excelstartsfrom)->setRowHeight(105);

        $this->excel->getActiveSheet()->getRowDimension($excelstartsfrom+1)->setRowHeight(115);
        $this->excel->getActiveSheet()->getRowDimension($excelstartsfrom+2)->setRowHeight(10);
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1),'LOB / Function');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom+1) . ($excelstartsfrom+2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom+1) . ($excelstartsfrom+2))->getFill()->getStartColor()->setARGB('d4376091');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom+1) . ($excelstartsfrom+2))->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(15);


        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+1) . ($excelstartsfrom+1),'SUPPLIER / CONTRACT');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+1) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom+1) . ($excelstartsfrom+2))->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+1))->setWidth(20);


        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom+2) . ($excelstartsfrom+1),'Classification');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom+2) . ($excelstartsfrom+2))->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),'font'  => array('bold'  => true)));
        $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+2))->setWidth(5);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . ($excelstartsfrom+1))->getAlignment()->setTextRotation(45);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom+2) . ($excelstartsfrom+2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+2) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom+2) . ($excelstartsfrom+2))->getFill()->getStartColor()->setARGB('d4D8D8D8');

        $excelColumnstartsFrom+=3;
        if(isset($header['last_review']) && $header['last_review']=='yes'){
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1),'Latest review date');
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->applyFromArray(
                array('borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),'font'  => array('bold'  => true)));
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1))->getAlignment()->setTextRotation(45);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->getStartColor()->setARGB('d4D8D8D8');
            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(10);

            $excelColumnstartsFrom++;
        }

        if(isset($header['modules'])){
            foreach($header['modules'] as $k=>$v){
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1),$v);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->applyFromArray(
                    array('borders' => array(
                        'outline' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),'font'  => array('bold'  => true)));
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1))->getAlignment()->setTextRotation(45);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->getStartColor()->setARGB('d4b8cce4');
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(5);

                $excelColumnstartsFrom++;

            }
        }
        if(isset($header['rag']) && $header['rag']=='yes'){
            $color = array('Red (#)','Amber (#)','Green (#)');
            $hash = array('d4FF0000','d4FFC000','d492D050');
            for($i=0;$i<3;$i++){
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1),$color[$i]);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->applyFromArray(
                    array('borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),'font'  => array('bold'  => true)));
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1))->getAlignment()->setTextRotation(45);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1))->getFill()->getStartColor()->setARGB('d4dbe5f1');
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->getStartColor()->setARGB($hash[$i]);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(5);


                $excelColumnstartsFrom++;
            }
        }
        $this->excel->setActiveSheetIndex(0)
            ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1),'Decision required');
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->applyFromArray(
            array('borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),'font'  => array('bold'  => true)));
        if(isset($header['action_items']) && $header['action_items']=='yes') {
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1))->getAlignment()->setTextRotation(45);
            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(5);
        }else if( isset($header['comments']) && $header['comments']=='yes'){
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1))->getAlignment()->setTextRotation(45);
            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(5);
        }else{
            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(35);
        }


        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->getStartColor()->setARGB('d4CCC0DA');

        $excelColumnstartsFrom++;

        if(isset($header['action_items']) && $header['action_items']=='yes'){
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1),'Action items');
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->applyFromArray(
                array('borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),'font'  => array('bold'  => true)));
            if(isset($header['comments']) && $header['comments']=='yes')
            {
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1))->getAlignment()->setTextRotation(45);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(5);

            }else{
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(35);
            }
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->getStartColor()->setARGB('d4CCC0DA');

            $excelColumnstartsFrom++;

        }
        if(isset($header['comments']) && $header['comments']=='yes'){
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1),'Comments');
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->applyFromArray(
                array('borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),'font'  => array('bold'  => true)));
            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setWidth(40);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+1).':'.$this->getkey($excelColumnstartsFrom) . ($excelstartsfrom+2))->getFill()->getStartColor()->setARGB('d4b8cce4');

            $excelColumnstartsFrom++;
        }

        $excelstartsfrom+=3;
        $dataRow=$excelstartsfrom;


        foreach($report_data['data'] as $k=>$v){
            $excelColumnstartsFrom = $columnBegin;
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom),$v['business_unit']);
            $excelColumnstartsFrom++;
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom),$v['supplier_contract']);
            $excelColumnstartsFrom++;
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom),$v['classification']);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom))->getFill()->getStartColor()->setARGB('d4D8D8D8');

            $excelColumnstartsFrom++;
            if(isset($header['last_review']) && $header['last_review']=='yes')
                if(isset($v['last_review_date'])) {
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom), $v['last_review_date']);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom))->getFill()->getStartColor()->setARGB('d4D8D8D8');

                    $excelColumnstartsFrom++;
                }
            foreach($header['modules'] as $mk=>$vk){
                if(isset($v[$mk])){
                    $file_img_path = './images/'.$v[$mk].'.png';
                    if (file_exists($file_img_path)) {
                        $objDrawing = new PHPExcel_Worksheet_Drawing();
                        $objDrawing->setName('Customer Signature');
                        $objDrawing->setDescription('Customer Signature');
                        //Path to signature .jpg file
                        $signature = $file_img_path;
                        $objDrawing->setPath($signature);
                        $objDrawing->setOffsetX(12);                     //setOffsetX works properly
                        $objDrawing->setCoordinates($this->getkey($excelColumnstartsFrom) . $excelstartsfrom);             //set image to cell E38
                        $objDrawing->setHeight(15);                     //signature height
                        $objDrawing->setWorksheet($this->excel->getActiveSheet());  //save
                    }
                }
                $excelColumnstartsFrom++;
            }


            if(isset($header['rag']) && $header['rag']=='yes') {
                if (isset($v['red_cnt'])) {
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom), $v['red_cnt']);
                    $excelColumnstartsFrom++;
                }
                if (isset($v['amber_cnt'])) {
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom), $v['amber_cnt']);
                    $excelColumnstartsFrom++;
                }
                if (isset($v['green_cnt'])) {
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom), $v['green_cnt']);
                    $excelColumnstartsFrom++;
                }
            }
            if($v['decision_required']=='yes')
                $file = 'flag.png';
            else
                $file = '123';
            $file_img_path = './images/'.$file;
            if (file_exists($file_img_path)) {
                $objDrawing = new PHPExcel_Worksheet_Drawing();
                $objDrawing->setName('Customer Signature');
                $objDrawing->setDescription('Customer Signature');
                //Path to signature .jpg file
                $signature = $file_img_path;
                $objDrawing->setPath($signature);
                $objDrawing->setOffsetX(10);
                $objDrawing->setOffsetY(3);//setOffsetX works properly
                $objDrawing->setCoordinates($this->getkey($excelColumnstartsFrom) . $excelstartsfrom);             //set image to cell E38
                $objDrawing->setHeight(15);                     //signature height
                $objDrawing->setWorksheet($this->excel->getActiveSheet());  //save

            }
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom))->getFill()->getStartColor()->setARGB('d4CCC0DA');

            $excelColumnstartsFrom++;


            if(isset($header['action_items']) && $header['action_items']=='yes')
                if(isset($v['action_items'])){
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom), $v['action_items']);
                    $excelColumnstartsFrom++;
                }
            if(isset($header['comments']) && $header['comments']=='yes')
                if(isset($v['comments'])){
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom) . ($excelstartsfrom), $v['comments']);
                $excelColumnstartsFrom++;
                }
            $columnEnd=$excelColumnstartsFrom-1;
         $excelstartsfrom++;
        }
        $this->excel->getActiveSheet()->getStyle($this->getkey($columnBegin).($dataRow).':'.$this->getkey($columnEnd).($excelstartsfrom-1))->applyFromArray(
            array('borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),'alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));







        $this->excel->getActiveSheet()->setSelectedCells('A0');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('REPORT');
        $filename = $report_data['report_name'].'_'.date("d-m-Y",strtotime(currentDate())).'.xls';
       // echo $filename;exit;//save our workbook as this file name
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $file_path = FILE_SYSTEM_PATH.'downloads/' . $filename;
        $objWriter->save($file_path);
        $file_path = REST_API_URL.'downloads/' . $filename;
        $file_path = str_replace('::1','localhost',$file_path);
        $response = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array("file_path" => $file_path,"file_name"=>$filename));
        $this->response($response, REST_Controller::HTTP_OK);


    }


    public function contractListExport_get(){
        //this function generates a report in excel.
        $data = $this->input->get();

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id']) && strtolower($data['business_unit_id'])!='all') {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['business_unit_id'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['delegate_id'])) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_owner_id'])) {
            $data['contract_owner_id'] = pk_decrypt($data['contract_owner_id']);
            if($data['contract_owner_id']!=$this->session_user_customer_all_users){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_user'])) {
            $data['customer_user'] = pk_decrypt($data['customer_user']);
            if($data['customer_user']!=$this->session_user_customer_all_users){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id']) && isset($data['id_user'])){
            if(in_array($data['user_role_id'],array(3))){
                $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $data['id_user'],'status' => '1'));
                $data['business_unit_id'] = array_map(function($i){ return $i['id_business_unit']; },$business_unit);
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            if($data['user_role_id']==4){
                $data['delegate_id'] = $data['id_user'];
                if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            if($data['user_role_id']==5){
                $data['customer_user'] = $data['id_user'];
                if(!in_array($data['customer_user'],$this->session_user_contributors)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['user_role_id']==6){
                $data['business_unit_id'] = $this->session_user_business_units;
            }
        }

        $result = $this->Contract_model->getContractList($data);
        for($s=0;$s<count($result['data']);$s++)
        {
            preg_match_all('/[A-Z]/', ucwords(strtolower($result['data'][$s]['relationship_category_name'])), $matches);
            $result['data'][$s]['relationship_category_short_name'] = implode('',$matches[0]);
            $result['data'][$s]['review_by'] = '---';$result['data'][$s]['last_review']=NULL;
            $last_finalized_review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result['data'][$s]['id_contract'],'order' => 'DESC','contract_review_status'=>'finished'));
            if(!empty($last_finalized_review) && isset($last_finalized_review[0]['id_contract_review']) && $last_finalized_review[0]['id_contract_review']!='' && $last_finalized_review[0]['id_contract_review']!=0) {
                $result['data'][$s]['review_by'] = $last_finalized_review[0]['review_by'];
                if($last_finalized_review[0]['review_on']!='---')
                    $result['data'][$s]['last_review'] = date('Y-m-d',strtotime($last_finalized_review[0]['review_on']));
            }
            $review = $this->Contract_model->getLastReviewByContractId(array('contract_id' => $result['data'][$s]['id_contract'],'order' => 'DESC'));
            if(!empty($review) && isset($review[0]['id_contract_review']) && $review[0]['id_contract_review']!='' && $review[0]['id_contract_review']!=0) {
                //getting score of recent review
                $result['data'][$s]['id_contract_review'] = $review[0]['id_contract_review'];
                $module_score = $this->Contract_model->getContractReviewModuleScore(array('contract_review_id' => $review[0]['id_contract_review']));
                for($sr=0;$sr<count($module_score);$sr++)
                {
                    $module_score[$sr]['score'] = getScoreByCount($module_score[$sr]);
                }
                $result['data'][$s]['score'] = getScore($scope = array_map(function($i){ return strtolower($i['score']); },$module_score));
            }

            $result['data'][$s]['contract_start_date'] = date('Y-m-d',strtotime($result['data'][$s]['contract_start_date']));
            $result['data'][$s]['contract_end_date'] = date('Y-m-d',strtotime($result['data'][$s]['contract_end_date']));
            //getting action items of a recent review based on user role
        }
        //preparing headers
        $headers=array('Provider','Contract Name','Category','Status','Review By','Last Review','Actual Score','Contract Start Date',
            'Contract End Date','Annual Contract Value (incl. VAT)','Classification','Automatic Prolongation','Contract Description',
            'Contract Sponsor (internal)','Contract Sponsor (provider)','Relation Manager (internal)','Relation Manager (provider)',
            'Contract Responsible (internal)','Contract Responsible (provider)');
        if(isset($result['data']))
            $result = $result['data'];

        $this->load->library('excel');
        //activate worksheet number 1
        $excelRowstartsfrom=1;
        $excelColumnstartsFrom=0;
        $columnBegin =$excelColumnstartsFrom;
        $excelstartsfrom=$excelRowstartsfrom;

        //writing headers
        foreach($headers as $k=>$v){
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($columnBegin) . $excelstartsfrom,$v);
            $this->excel->getActiveSheet()->getStyle($this->getkey($columnBegin) . $excelstartsfrom)->applyFromArray(
                array('borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),'alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),'font'  => array('bold'  => true,'size'=>12)));
            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin))->setAutoSize(true);
            $columnBegin++;
        }
        $excelstartsfrom++;

        $excel_data=array();
        //arranging data in required format
        foreach($result as $k => $v){
            $excel_data[$k]['provider_name']=$v['provider_name'];
            $excel_data[$k]['contract_name']=$v['contract_name'];
            $excel_data[$k]['relationship_category_short_name']=$v['relationship_category_short_name'];
            $excel_data[$k]['contract_status']=$v['contract_status'];
            $excel_data[$k]['review_by']=$v['review_by'];
            $excel_data[$k]['last_review']=$v['last_review'];
            $excel_data[$k]['score']=isset($v['score'])?$v['score']:'';
            $excel_data[$k]['contract_start_date']=$v['contract_start_date'];
            $excel_data[$k]['contract_end_date']=$v['contract_end_date'];
            $currency = number_format($v['contract_value'], 2, '.', ',');
            $excel_data[$k]['contract_value']=$v['currency_name'].' '.currencyFormat($currency,$v['currency_name']);
            $excel_data[$k]['classification_name']=$v['classification_name'];
            $excel_data[$k]['auto_renewal']=$v['auto_renewal']==1?'Yes':'No';
            $excel_data[$k]['description']=$v['description'];
            $excel_data[$k]['internal_contract_sponsor']=$v['internal_contract_sponsor'];
            $excel_data[$k]['provider_contract_sponsor']=$v['provider_contract_sponsor'];
            $excel_data[$k]['internal_partner_relationship_manager']=$v['internal_partner_relationship_manager'];
            $excel_data[$k]['provider_partner_relationship_manager']=$v['provider_partner_relationship_manager'];
            $excel_data[$k]['internal_contract_responsible']=$v['internal_contract_responsible'];
            $excel_data[$k]['provider_contract_responsible']=$v['provider_contract_responsible'];
        }
        ///writing data row by row
        foreach($excel_data as $k => $v){
            $columnBegin =$excelColumnstartsFrom;
            foreach($v as $v1){
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($columnBegin) . $excelstartsfrom,$v1);
                $this->excel->getActiveSheet()->getStyle($this->getkey($columnBegin) . $excelstartsfrom)->applyFromArray(
                    array('borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),'alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),'font'  => array('size'=>12)));
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($columnBegin))->setAutoSize(true);
                $columnBegin++;
            }
            $excelstartsfrom++;
        }
        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom).$excelRowstartsfrom.':'.$this->getkey($columnBegin).$excelstartsfrom)
            ->getAlignment()->setWrapText(true);



        $this->excel->getActiveSheet()->setSelectedCells('A0');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Blad1');
        $filename = 'contracts'.'_'.date("d-m-Y",strtotime(currentDate())).'.xls';
        // echo $filename;exit;//save our workbook as this file name
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $file_path = FILE_SYSTEM_PATH.'downloads/' . $filename;
        $objWriter->save($file_path);
        $view_path='downloads/' . $filename;
        $file_path = REST_API_URL.$view_path;
        $file_path = str_replace('::1','localhost',$file_path);

        $insert_id = $this->Download_model->addDownload(array('path'=>$view_path,'filename'=>$filename,'user_id'=>pk_decrypt($_SERVER['HTTP_USER']),'access_token'=>substr($_SERVER['HTTP_AUTHORIZATION'],7),'status'=>0,'created_date_time'=>currentDate()));

        $response = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>pk_encrypt($insert_id));
        $this->response($response, REST_Controller::HTTP_OK);

    }

    function getkey($pos){
        //this function used to return ascii value based on position used int export_get function
        $numeric = $pos % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($pos / 26);
        if ($num2 > 0) {
            return $this->getkey($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }
    public function reviewdiscussion_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        //$this->form_validator->add_rules('contract_review_id', array('required'=>$this->lang->line('contract_review_id_req')));
        /*$this->form_validator->add_rules('module_id', array('required'=>$this->lang->line('module_id_req')));*/
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract'])) {
            $data['id_contract'] = pk_decrypt($data['id_contract']);
            if(!in_array($data['id_contract'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review'])) {
            $data['id_contract_review'] = pk_decrypt($data['id_contract_review']);
            if(!in_array($data['id_contract_review'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_user'])) {
            $data['contract_user'] = pk_decrypt($data['contract_user']);
            if(!in_array($data['contract_user'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_contract_review_discussion'])) {
            $data['id_contract_review_discussion'] = pk_decrypt($data['id_contract_review_discussion']);
            if(!in_array($data['id_contract_review_discussion'],$this->session_user_contract_review_discussions)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $actions_allowed=0;//0 - dont allow ,1- allowed
        $logged_user='other user';
        if(isset($data['id_user']) && isset($data['user_role_id']) && $data['user_role_id']==5){
            $data['contract_user'] = $data['id_user'];
            $actions_allowed=1;
            $logged_user='contributor';
            if(!in_array($data['contract_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else{
            $actions_allowed=0;
        }
        if(!isset($data['contract_review_id'])){
            $pending_discussions=$this->Contract_model->getContractDiscussion(array('id_contract'=>$data['contract_id'],'discussion_status'=>1));
            if(isset($pending_discussions[0]['contract_review_id'])){
                $data['contract_review_id']=$pending_discussions[0]['contract_review_id'];
            }
            else{
                $recent_reviews=$this->Contract_model->getContractReview(array('contract_id' => $data['contract_id'],'order'=>'DESC'));
                if(isset($recent_reviews[0]['id_contract_review'])){
                    $data['contract_review_id']=$recent_reviews[0]['id_contract_review'];
                }
            }
        }
        if(isset($data['id_contract_review_discussion'])){
            $pending_discussions=$this->Contract_model->getContractDiscussion(array('id_contract_review_discussion'=>$data['id_contract_review_discussion']));
            if(isset($pending_discussions[0]['contract_review_id'])){
                $data['contract_review_id']=$pending_discussions[0]['contract_review_id'];
            }
        }
        $idaadi=$this->Contract_model->checkContributorForContractReview(array('contract_review_id'=>$data['contract_review_id'],'id_user'=>$data['id_user']));
        if($idaadi===true){
            $data['contract_user'] = $data['id_user'];
            $actions_allowed=1;
            $logged_user='contributor';
        }
        $review_information=$this->Contract_model->getContractReview(array('id_contract_review'=>$data['contract_review_id']));
        $result = $this->Contract_model->getContractReviewDisucussionData($data);
        $result_parsed=array();
        $contract_review_discussion_id=NULL;
        foreach($result as $k=>$v){

            $result_parsed[$v['id_module']]['module_name']=$v['module_name'];
            $result_parsed[$v['id_module']]['id_module']=$v['id_module'];
            $result_parsed[$v['id_module']]['is_auto_close']=$v['is_auto_close'];

            $result_parsed[$v['id_module']]['topics'][$v['id_topic']]['topic_name']=$v['topic_name'];
            $result_parsed[$v['id_module']]['topics'][$v['id_topic']]['id_topic']=$v['id_topic'];
            $result_parsed[$v['id_module']]['topics'][$v['id_topic']]['questions'][]=$v;
            if($v['id_contract_review_discussion']!='' || $v['id_contract_review_discussion']!=NULL)

            if($v['id_contract_review_discussion']!='' || $v['id_contract_review_discussion']!=NULL) {
                $contract_review_discussion_id=$v['id_contract_review_discussion'];
                $result_parsed[$v['id_module']]['id_contract_review_discussion']=$contract_review_discussion_id;
                $result_parsed[$v['id_module']]['discussion_created_by'] = $v['discussion_created_by'];
                $result_parsed[$v['id_module']]['discussion_created_on'] = $v['discussion_created_on'];
                $result_parsed[$v['id_module']]['discussion_closed_by'] = $v['discussion_closed_by'];
                $result_parsed[$v['id_module']]['discussion_closed_on'] = $v['discussion_closed_on'];
                $result_parsed[$v['id_module']]['discussion_status'] = $v['discussion_status'];
                $result_parsed[$v['id_module']]['diaaid']='annus';
                $result_parsed[$v['id_module']]['dcmaamcd']='annus';
                $result_parsed[$v['id_module']]['dcaacd']='annus';
                $result_parsed[$v['id_module']]['dclaalcd']='annus';
                $result_parsed[$v['id_module']]['dsaasd']='annus';
                if(isset($review_information[0]['contract_review_status']) && $review_information[0]['contract_review_status']=='review in progress') {
                    $result_parsed[$v['id_module']]['diaaid'] = 'annus';
                    if ($v['discussion_status'] == 1) {
                        if ($logged_user == 'contributor') {
                            $result_parsed[$v['id_module']]['dcaacd']='itako';
                        }
                        $result_parsed[$v['id_module']]['dcmaamcd'] = 'itako';
                        $result_parsed[$v['id_module']]['dclaalcd'] = 'itako';
                        $result_parsed[$v['id_module']]['dsaasd']='itako';
                    } else {
                        $result_parsed[$v['id_module']]['dcmaamcd'] = 'annus';
                        $result_parsed[$v['id_module']]['dclaalcd'] = 'annus';
                    }
                }


            }

            if(!isset($result_parsed[$v['id_module']]['id_contract_review_discussion'])) {
                $result_parsed[$v['id_module']]['id_contract_review_discussion']=NULL;
                $result_parsed[$v['id_module']]['discussion_created_by'] = NULL;
                $result_parsed[$v['id_module']]['discussion_created_on'] = NULL;
                $result_parsed[$v['id_module']]['discussion_closed_by'] = NULL;
                $result_parsed[$v['id_module']]['discussion_closed_on'] = NULL;
                $result_parsed[$v['id_module']]['discussion_status'] = NULL;
                $result_parsed[$v['id_module']]['diaaid']='annus';
                $result_parsed[$v['id_module']]['dcaacd']='annus';
                $result_parsed[$v['id_module']]['dcmaamcd']='annus';
                $result_parsed[$v['id_module']]['dclaalcd']='annus';
                $result_parsed[$v['id_module']]['dsaasd']='annus';
                if(isset($review_information[0]['contract_review_status']) && $review_information[0]['contract_review_status']=='review in progress') {
                    if ($logged_user == 'contributor') {
                        $result_parsed[$v['id_module']]['diaaid'] = 'itako';
                        $result_parsed[$v['id_module']]['dcmaamcd'] = 'itako';
                        $result_parsed[$v['id_module']]['dcaacd'] = 'itako';
                    }
                    else
                        $result_parsed[$v['id_module']]['diaaid'] = 'annus';

                }
            }
        }
        $result=array_values($result_parsed);
        foreach($result as $k=>$v){
            $result[$k]['id_contract_review_discussion']=pk_encrypt($result[$k]['id_contract_review_discussion']);
            $result[$k]['id_module']=pk_encrypt($result[$k]['id_module']);
            foreach($result[$k]['topics'] as $kt=>$vt){
                $result[$k]['topics'][$kt]['id_topic']=pk_encrypt($result[$k]['topics'][$kt]['id_topic']);
                foreach($result[$k]['topics'][$kt]['questions'] as $kq=>$vq){
                    $result[$k]['topics'][$kt]['questions'][$kq]['contract_review_id']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['contract_review_id']);
                    $result[$k]['topics'][$kt]['questions'][$kq]['id_contract_review_discussion']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['id_contract_review_discussion']);
                    $result[$k]['topics'][$kt]['questions'][$kq]['id_contract_review_discussion_question']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['id_contract_review_discussion_question']);
                    $result[$k]['topics'][$kt]['questions'][$kq]['id_module']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['id_module']);
                    $result[$k]['topics'][$kt]['questions'][$kq]['id_question']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['id_question']);
                    $result[$k]['topics'][$kt]['questions'][$kq]['id_topic']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['id_topic']);
                    foreach($result[$k]['topics'][$kt]['questions'][$kq]['change_log'] as $kc=>$vc){
                        $result[$k]['topics'][$kt]['questions'][$kq]['change_log'][$kc]['contract_review_discussion_question_id']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['change_log'][$kc]['contract_review_discussion_question_id']);
                        $result[$k]['topics'][$kt]['questions'][$kq]['change_log'][$kc]['created_by']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['change_log'][$kc]['created_by']);
                        $result[$k]['topics'][$kt]['questions'][$kq]['change_log'][$kc]['id_contract_review_discussion_question_log']=pk_encrypt($result[$k]['topics'][$kt]['questions'][$kq]['change_log'][$kc]['id_contract_review_discussion_question_log']);
                    }
                }
            }
        }
        foreach($review_information as $kr=>$vr){
            $review_information[$kr]['contract_id']=pk_encrypt($review_information[$kr]['contract_id']);
            $review_information[$kr]['created_by']=pk_encrypt($review_information[$kr]['created_by']);
            $review_information[$kr]['id_contract_review']=pk_encrypt($review_information[$kr]['id_contract_review']);
            $review_information[$kr]['relationship_category_id']=pk_encrypt($review_information[$kr]['relationship_category_id']);
            $review_information[$kr]['updated_by']=pk_encrypt($review_information[$kr]['updated_by']);
        }

        $is_discussion_exist=(count($this->Contract_model->getContractReviewDiscussionModuleCount(array('id_contract_review'=>$data['contract_review_id'],'discussion_status'=>1)))>0)?"itako":'annus';
        $existing_discussions=$this->Contract_model->getContractDiscussion(array('id_contract'=>$data['contract_id'],'discussion_status'=>2,'contract_review_status'=>'finished'));
        foreach($existing_discussions as $ke=>$ve){
            $existing_discussions[$ke]['id_contract_review_discussion']=pk_encrypt($existing_discussions[$ke]['id_contract_review_discussion']);
            $existing_discussions[$ke]['contract_review_id']=pk_encrypt($existing_discussions[$ke]['contract_review_id']);
            $existing_discussions[$ke]['module_id']=pk_encrypt($existing_discussions[$ke]['module_id']);
            $existing_discussions[$ke]['created_by']=pk_encrypt($existing_discussions[$ke]['created_by']);
            $existing_discussions[$ke]['updated_by']=pk_encrypt($existing_discussions[$ke]['updated_by']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('contract_review_discussion_id'=>pk_encrypt($contract_review_discussion_id),'contract_review_id'=>pk_encrypt($data['contract_review_id']),'closed_discussions'=>$existing_discussions,'review_discussion'=>$result,'actions_allowed'=>$actions_allowed,'ideedi'=>$is_discussion_exist,'idieeidi'=>($contract_review_discussion_id!=NULL && $contract_review_discussion_id>0)?"itako":'annus','review_information'=>$review_information));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function reviewdiscussion_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $this->form_validator->add_rules('contract_review_id', array('required'=>$this->lang->line('contract_review_id_req')));
        $this->form_validator->add_rules('review_discussion', array('required'=>$this->lang->line('contract_review_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $contract_review_discussion_id=NULL;
        if(isset($data['id_user']) && isset($data['user_role_id']) && $data['user_role_id']==5){
            $data['contract_user'] = $data['id_user'];
            if(!in_array($data['contract_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        /*if(isset($data['contract_review_discussion_id']) && $data['contract_review_discussion_id']>0){
            $contract_review_discussion_id = $data['contract_review_discussion_id'];
        }
        else{
            $insert=['contract_review_id'=>$data['contract_review_id'],'created_on'=>currentDate(),'created_by'=>$data['created_by']];
            $contract_review_discussion_id=$this->Contract_model->addContractReviewDiscussion($insert);
        }*/
        $result_parsed=array();
        $insert=$update=array();
        $recent_module_id=NULL;
        //$data['review_discussion']=json_decode($data['review_discussion'],true);
        $message=$this->lang->line('review_discussion_save_success');
        $mail_type='';
        foreach($data['review_discussion'] as $k=>$v){

            $data['review_discussion'][$k]['id_module']=pk_decrypt($data['review_discussion'][$k]['id_module']);
            if($data['review_discussion'][$k]['id_module']>0 && !in_array($data['review_discussion'][$k]['id_module'],$this->session_user_contract_review_modules)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $data['review_discussion'][$k]['id_topic']=pk_decrypt($data['review_discussion'][$k]['id_topic']);
            if($data['review_discussion'][$k]['id_topic']>0 && !in_array($data['review_discussion'][$k]['id_topic'],$this->session_user_contract_review_topics)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $data['review_discussion'][$k]['id_contract_review_discussion']=pk_decrypt($data['review_discussion'][$k]['id_contract_review_discussion']);
            if($data['review_discussion'][$k]['id_contract_review_discussion']>0 && !in_array($data['review_discussion'][$k]['id_contract_review_discussion'],$this->session_user_contract_review_discussions)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $data['review_discussion'][$k]['contract_review_id']=pk_decrypt($data['review_discussion'][$k]['contract_review_id']);
            if($data['review_discussion'][$k]['contract_review_id']>0 && !in_array($data['review_discussion'][$k]['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $data['review_discussion'][$k]['id_contract_review_discussion_question']=pk_decrypt($data['review_discussion'][$k]['id_contract_review_discussion_question']);
            if($data['review_discussion'][$k]['id_contract_review_discussion_question']>0 && !in_array($data['review_discussion'][$k]['id_contract_review_discussion_question'],$this->session_user_contract_review_discussion_questions)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $data['review_discussion'][$k]['id_question']=pk_decrypt($data['review_discussion'][$k]['id_question']);
            if($data['review_discussion'][$k]['id_question']>0 && !in_array($data['review_discussion'][$k]['id_question'],$this->session_user_contract_review_questions)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        foreach($data['review_discussion'] as $k=>$v){
            $recent_module_id=$v['id_module'];
            $id_contract_review_discussion=NULL;
            $discussion_initiated_user_info = $this->User_model->getUserInfo(array('user_id' => $data['created_by']));
            if($v['id_contract_review_discussion']=='' || $v['id_contract_review_discussion']==NULL){
                $modulediscussion=$this->Contract_model->getContractReviewDiscussion(array('id_module'=>$v['id_module'],'contract_review_id'=>$v['contract_review_id']));
                if(isset($modulediscussion['id_contract_review_discussion']) && $modulediscussion['id_contract_review_discussion']>0){
                    $id_contract_review_discussion=$modulediscussion['id_contract_review_discussion'];
                }
                else{
                    $insertModuleDiscussion=['contract_review_id'=>$v['contract_review_id'],'created_on'=>currentDate(),'created_by'=>$data['created_by'],'module_id'=>$v['id_module']];
                    $id_contract_review_discussion=$this->Contract_model->addContractReviewDiscussion($insertModuleDiscussion);
                    $message=$this->lang->line('review_discussion_initiate_success');
                    $mail_type='insert';
                }
            }
            else{
                $id_contract_review_discussion=$v['id_contract_review_discussion'];
                $mail_type='update';
            }
            if($v['id_contract_review_discussion_question']=='' || $v['id_contract_review_discussion_question']==NULL)
                $insert[]=['contract_review_discussion_id'=>$id_contract_review_discussion,'question_id'=>$v['id_question'],'remarks'=>$v['remarks'],'created_by'=>$data['created_by'],'created_on'=>currentDate()];
            else
                $update[]=['id_contract_review_discussion_question'=>$v['id_contract_review_discussion_question'],'contract_review_discussion_id'=>$id_contract_review_discussion,'question_id'=>$v['id_question'],'remarks'=>$v['remarks'],'updated_by'=>$data['created_by'],'updated_on'=>currentDate(),'status'=>$v['status']];

        }

        /*echo "<pre>insert:";print_r($insert);echo "</pre>";
        echo "<pre>update:";print_r($update);echo "</pre>";exit;*/
        if(count($insert)>0){
            $this->Contract_model->addContractReviewDiscussionQuestion($insert);
        }
        if(count($update)>0){
            $this->Contract_model->updateContractReviewDiscussionQuestion($update);
        }
        $result_parsed['recent_module_id']=pk_encrypt($recent_module_id);
        $result=($result_parsed);
        /*echo "<pre>";print_r($result);echo "</pre>";
        echo "<pre>";print_r($insert);echo "</pre>";
        echo "<pre>";print_r($update);echo "</pre>";
        exit;*/
        if($mail_type!=''){
            $template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $discussion_initiated_user_info->customer_id,'language_id' =>1,'module_key'=>($mail_type=='insert'?'CONTRACT_REVIEW_DISCUSSION_INITIATE':'CONTRACT_REVIEW_DISCUSSION_UPDATE')));
            if($template_configurations_parent['total_records']>0){
                $module_info = $this->Module_model->getModuleName(array('language_id'=>1,'module_id'=>$recent_module_id));
                $contract_info = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
                $bu_owner_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['contract_owner_id']));
                $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $discussion_initiated_user_info->customer_id));
                if(isset($contract_info[0]['delegate_id']) && $contract_info[0]['delegate_id']!=NULL && $contract_info[0]['delegate_id']>0){
                    $delegate_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['delegate_id']));
                }
                /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
                $cust_admin = $cust_admin['data'][0];*/
                $customer_admin_list=$this->Customer_model->getCustomerAdminList(array('customer_id'=>$discussion_initiated_user_info->customer_id));
                if($customer_details[0]['company_logo']=='') {
                    $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
                }
                else{
                    $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

                }
                if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }

                //mail to customer admins
                foreach($customer_admin_list['data'] as $kd=>$vd){
                    $template_configurations=$template_configurations_parent['data'][0];
                    $wildcards=$template_configurations['wildcards'];
                    $wildcards_replaces=array();
                    $wildcards_replaces['first_name']=$vd['first_name'];
                    $wildcards_replaces['last_name']=$vd['last_name'];
                    $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                    if($mail_type=='insert') {
                        $wildcards_replaces['discussion_initiated_user_name'] = $discussion_initiated_user_info->first_name . ' ' . $discussion_initiated_user_info->last_name . ' (' . $discussion_initiated_user_info->user_role_name . ')';
                        $wildcards_replaces['discussion_initiated_date'] = dateFormat(currentDate());
                    }else{
                        $wildcards_replaces['discussion_updated_user_name'] = $discussion_initiated_user_info->first_name . ' ' . $discussion_initiated_user_info->last_name . ' (' . $discussion_initiated_user_info->user_role_name . ')';
                        $wildcards_replaces['discussion_updated_date'] = dateFormat(currentDate());
                    }
                    $wildcards_replaces['contract_review_module_name']=$module_info[0]['module_name'];
                    $wildcards_replaces['logo']=$customer_logo;
                    $wildcards_replaces['url']=WEB_BASE_URL.'html';
                    $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                    $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                    /*$from_name=SEND_GRID_FROM_NAME;
                    $from=SEND_GRID_FROM_EMAIL;
                    $from_name=$cust_admin['name'];
                    $from=$cust_admin['email'];*/
                    $from_name=$template_configurations['email_from_name'];
                    $from=$template_configurations['email_from'];
                    $to=$vd['email'];
                    $to_name=$vd['first_name'].' '.$vd['last_name'];
                    $mailer_data['mail_from_name']=$from_name;
                    $mailer_data['mail_to_name']=$to_name;
                    $mailer_data['mail_from']=$from;
                    $mailer_data['mail_to']=$to;
                    $mailer_data['mail_to_user_id']=$vd['id_user'];
                    $mailer_data['mail_subject']=$subject;
                    $mailer_data['mail_message']=$body;
                    $mailer_data['status']=0;
                    $mailer_data['send_date']=currentDate();
                    $mailer_data['is_cron']=0;
                    $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                    //print_r($mailer_data);
                    $mailer_id=$this->Customer_model->addMailer($mailer_data);
                    //sending mail to bu owner
                    if($mailer_data['is_cron']==0) {
                        //$mail_sent_status=sendmail($to, $subject, $body, $from);
                        $this->load->library('sendgridlibrary');
                        $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                        if($mail_sent_status==1)
                            $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                    }
                }
                //mail to bu owner
                if(isset($bu_owner_info->first_name)){
                    $template_configurations=$template_configurations_parent['data'][0];
                    $wildcards=$template_configurations['wildcards'];
                    $wildcards_replaces=array();
                    $wildcards_replaces['first_name']=$bu_owner_info->first_name;
                    $wildcards_replaces['last_name']=$bu_owner_info->last_name;
                    $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                    if($mail_type=='insert') {
                        $wildcards_replaces['discussion_initiated_user_name'] = $discussion_initiated_user_info->first_name . ' ' . $discussion_initiated_user_info->last_name . ' (' . $discussion_initiated_user_info->user_role_name . ')';
                        $wildcards_replaces['discussion_initiated_date'] = dateFormat(currentDate());
                    }else{
                        $wildcards_replaces['discussion_updated_user_name'] = $discussion_initiated_user_info->first_name . ' ' . $discussion_initiated_user_info->last_name . ' (' . $discussion_initiated_user_info->user_role_name . ')';
                        $wildcards_replaces['discussion_updated_date'] = dateFormat(currentDate());
                    }
                    $wildcards_replaces['contract_review_module_name']=$module_info[0]['module_name'];
                    $wildcards_replaces['logo']=$customer_logo;
                    $wildcards_replaces['url']=WEB_BASE_URL.'html';
                    $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                    $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                    /*$from_name=SEND_GRID_FROM_NAME;
                    $from=SEND_GRID_FROM_EMAIL;
                    $from_name=$cust_admin['name'];
                    $from=$cust_admin['email'];*/
                    $from_name=$template_configurations['email_from_name'];
                    $from=$template_configurations['email_from'];
                    $to=$bu_owner_info->email;
                    $to_name=$bu_owner_info->first_name.' '.$bu_owner_info->last_name;
                    $mailer_data['mail_from_name']=$from_name;
                    $mailer_data['mail_to_name']=$to_name;
                    $mailer_data['mail_from']=$from;
                    $mailer_data['mail_to']=$to;
                    $mailer_data['mail_to_user_id']=$bu_owner_info->id_user;
                    $mailer_data['mail_subject']=$subject;
                    $mailer_data['mail_message']=$body;
                    $mailer_data['status']=0;
                    $mailer_data['send_date']=currentDate();
                    $mailer_data['is_cron']=0;
                    $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                    //print_r($mailer_data);
                    $mailer_id=$this->Customer_model->addMailer($mailer_data);
                    //sending mail to bu owner
                    if($mailer_data['is_cron']==0) {
                        //$mail_sent_status=sendmail($to, $subject, $body, $from);
                        $this->load->library('sendgridlibrary');
                        $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                        if($mail_sent_status==1)
                            $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                    }
                }
                //mail to delegate
                if(isset($delegate_info) && isset($delegate_info->first_name)){
                    $template_configurations=$template_configurations_parent['data'][0];
                    $wildcards=$template_configurations['wildcards'];
                    $wildcards_replaces=array();
                    $wildcards_replaces['first_name']=$delegate_info->first_name;
                    $wildcards_replaces['last_name']=$delegate_info->last_name;
                    $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                    if($mail_type=='insert') {
                        $wildcards_replaces['discussion_initiated_user_name'] = $discussion_initiated_user_info->first_name . ' ' . $discussion_initiated_user_info->last_name . ' (' . $discussion_initiated_user_info->user_role_name . ')';
                        $wildcards_replaces['discussion_initiated_date'] = dateFormat(currentDate());
                    }else{
                        $wildcards_replaces['discussion_updated_user_name'] = $discussion_initiated_user_info->first_name . ' ' . $discussion_initiated_user_info->last_name . ' (' . $discussion_initiated_user_info->user_role_name . ')';
                        $wildcards_replaces['discussion_updated_date'] = dateFormat(currentDate());
                    }
                    $wildcards_replaces['contract_review_module_name']=$module_info[0]['module_name'];
                    $wildcards_replaces['logo']=$customer_logo;
                    $wildcards_replaces['url']=WEB_BASE_URL.'html';
                    $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                    $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                    /*$from_name=SEND_GRID_FROM_NAME;
                    $from=SEND_GRID_FROM_EMAIL;
                    $from_name=$cust_admin['name'];
                    $from=$cust_admin['email'];*/
                    $from_name=$template_configurations['email_from_name'];
                    $from=$template_configurations['email_from'];
                    $to=$delegate_info->email;
                    $to_name=$delegate_info->first_name.' '.$delegate_info->last_name;
                    $mailer_data['mail_from_name']=$from_name;
                    $mailer_data['mail_to_name']=$to_name;
                    $mailer_data['mail_from']=$from;
                    $mailer_data['mail_to']=$to;
                    $mailer_data['mail_to_user_id']=$delegate_info->id_user;
                    $mailer_data['mail_subject']=$subject;
                    $mailer_data['mail_message']=$body;
                    $mailer_data['status']=0;
                    $mailer_data['send_date']=currentDate();
                    $mailer_data['is_cron']=0;
                    $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                    //print_r($mailer_data);
                    $mailer_id=$this->Customer_model->addMailer($mailer_data);
                    //sending mail to bu owner
                    if($mailer_data['is_cron']==0) {
                        //$mail_sent_status=sendmail($to, $subject, $body, $from);
                        $this->load->library('sendgridlibrary');
                        $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                        if($mail_sent_status==1)
                            $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                    }
                }
            }
        }
        $result = array('status'=>TRUE, 'message' => $message, 'data'=>($result));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function reviewdiscussionclose_post(){
       // updateContractReviewDiscussion
        $data = $this->input->post();
        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=>$this->lang->line('module_id_req')));
        $this->form_validator->add_rules('contract_review_id', array('required'=>$this->lang->line('contract_review_id_req')));
        $this->form_validator->add_rules('contract_review_discussion_id', array('required'=>$this->lang->line('contract_review_discussion_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['module_id'])) {
            $data['module_id'] = pk_decrypt($data['module_id']);
            if(!in_array($data['module_id'],$this->session_user_contract_review_modules)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
            if(!in_array($data['contract_review_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $modules=explode(',',$data['contract_review_discussion_id']);
        foreach ($modules as $k=>$v) {
            $v=pk_decrypt($v);
            if($v!=''){
                if(!in_array($v,$this->session_user_contract_review_discussions)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
                $this->Contract_model->updateContractReviewDiscussion(array('id_contract_review_discussion'=>$v,'updated_by'=>$data['created_by'],'updated_on'=>currentDate(),'discussion_status'=>2));
                $discussion_initiated_user_info = $this->User_model->getUserInfo(array('user_id' => $data['created_by']));
                $template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $discussion_initiated_user_info->customer_id,'language_id' =>1,'module_key'=>'CONTRACT_REVIEW_DISCUSSION_CLOSE'));
                if($template_configurations_parent['total_records']>0){
                    $module_info = $this->Module_model->getModuleName(array('language_id'=>1,'module_id'=>$data['module_id']));
                    $contract_info = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
                    $bu_owner_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['contract_owner_id']));
                    $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $discussion_initiated_user_info->customer_id));
                    if(isset($contract_info[0]['delegate_id']) && $contract_info[0]['delegate_id']!=NULL && $contract_info[0]['delegate_id']>0){
                        $delegate_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['delegate_id']));
                    }
                    /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
                    $cust_admin = $cust_admin['data'][0];*/
                    $customer_admin_list=$this->Customer_model->getCustomerAdminList(array('customer_id'=>$discussion_initiated_user_info->customer_id));
                    if($customer_details[0]['company_logo']=='') {
                        $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
                    }
                    else{
                        $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

                    }
                    if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }

                    //mail to customer admins
                    foreach($customer_admin_list['data'] as $kd=>$vd){
                        $template_configurations=$template_configurations_parent['data'][0];
                        $wildcards=$template_configurations['wildcards'];
                        $wildcards_replaces=array();
                        $wildcards_replaces['first_name']=$vd['first_name'];
                        $wildcards_replaces['last_name']=$vd['last_name'];
                        $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                        $wildcards_replaces['discussion_closed_user_name']=$discussion_initiated_user_info->first_name.' '.$discussion_initiated_user_info->last_name.' ('.$discussion_initiated_user_info->user_role_name.')';
                        $wildcards_replaces['discussion_closed_date']=dateFormat(currentDate());
                        $wildcards_replaces['contract_review_module_name']=$module_info[0]['module_name'];
                        $wildcards_replaces['logo']=$customer_logo;
                        $wildcards_replaces['url']=WEB_BASE_URL.'html';
                        $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                        $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                        /*$from_name=SEND_GRID_FROM_NAME;
                        $from=SEND_GRID_FROM_EMAIL;
                        $from_name=$cust_admin['name'];
                        $from=$cust_admin['email'];*/
                        $from_name=$template_configurations['email_from_name'];
                        $from=$template_configurations['email_from'];
                        $to=$vd['email'];
                        $to_name=$vd['first_name'].' '.$vd['last_name'];
                        $mailer_data['mail_from_name']=$from_name;
                        $mailer_data['mail_to_name']=$to_name;
                        $mailer_data['mail_to_user_id']=$vd['id_user'];
                        $mailer_data['mail_from']=$from;
                        $mailer_data['mail_to']=$to;
                        $mailer_data['mail_subject']=$subject;
                        $mailer_data['mail_message']=$body;
                        $mailer_data['status']=0;
                        $mailer_data['send_date']=currentDate();
                        $mailer_data['is_cron']=0;
                        $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                        //print_r($mailer_data);
                        $mailer_id=$this->Customer_model->addMailer($mailer_data);
                        //sending mail to bu owner
                        if($mailer_data['is_cron']==0) {
                            //$mail_sent_status=sendmail($to, $subject, $body, $from);
                            $this->load->library('sendgridlibrary');
                            $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                            if($mail_sent_status==1)
                                $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                        }
                    }
                    //mail to bu owner
                    if(isset($bu_owner_info->first_name)){
                        $template_configurations=$template_configurations_parent['data'][0];
                        $wildcards=$template_configurations['wildcards'];
                        $wildcards_replaces=array();
                        $wildcards_replaces['first_name']=$bu_owner_info->first_name;
                        $wildcards_replaces['last_name']=$bu_owner_info->last_name;
                        $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                        $wildcards_replaces['discussion_closed_user_name']=$discussion_initiated_user_info->first_name.' '.$discussion_initiated_user_info->last_name.' ('.$discussion_initiated_user_info->user_role_name.')';
                        $wildcards_replaces['discussion_closed_date']=dateFormat(currentDate());
                        $wildcards_replaces['contract_review_module_name']=$module_info[0]['module_name'];
                        $wildcards_replaces['logo']=$customer_logo;
                        $wildcards_replaces['url']=WEB_BASE_URL.'html';
                        $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                        $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                        /*$from_name=SEND_GRID_FROM_NAME;
                        $from=SEND_GRID_FROM_EMAIL;
                        $from_name=$cust_admin['name'];
                        $from=$cust_admin['email'];*/
                        $from_name=$template_configurations['email_from_name'];
                        $from=$template_configurations['email_from'];
                        $to=$bu_owner_info->email;
                        $to_name=$bu_owner_info->first_name.' '.$bu_owner_info->last_name;
                        $mailer_data['mail_from_name']=$from_name;
                        $mailer_data['mail_to_name']=$to_name;
                        $mailer_data['mail_to_user_id']=$bu_owner_info->id_user;
                        $mailer_data['mail_from']=$from;
                        $mailer_data['mail_to']=$to;
                        $mailer_data['mail_subject']=$subject;
                        $mailer_data['mail_message']=$body;
                        $mailer_data['status']=0;
                        $mailer_data['send_date']=currentDate();
                        $mailer_data['is_cron']=0;
                        $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                        //print_r($mailer_data);
                        $mailer_id=$this->Customer_model->addMailer($mailer_data);
                        //sending mail to bu owner
                        if($mailer_data['is_cron']==0) {
                            //$mail_sent_status=sendmail($to, $subject, $body, $from);
                            $this->load->library('sendgridlibrary');
                            $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                            if($mail_sent_status==1)
                                $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                        }
                    }
                    //mail to delegate
                    if(isset($delegate_info) && isset($delegate_info->first_name)){
                        $template_configurations=$template_configurations_parent['data'][0];
                        $wildcards=$template_configurations['wildcards'];
                        $wildcards_replaces=array();
                        $wildcards_replaces['first_name']=$delegate_info->first_name;
                        $wildcards_replaces['last_name']=$delegate_info->last_name;
                        $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                        $wildcards_replaces['discussion_closed_user_name']=$discussion_initiated_user_info->first_name.' '.$discussion_initiated_user_info->last_name.' ('.$discussion_initiated_user_info->user_role_name.')';
                        $wildcards_replaces['discussion_closed_date']=dateFormat(currentDate());
                        $wildcards_replaces['contract_review_module_name']=$module_info[0]['module_name'];
                        $wildcards_replaces['logo']=$customer_logo;
                        $wildcards_replaces['url']=WEB_BASE_URL.'html';
                        $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                        $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                        /*$from_name=SEND_GRID_FROM_NAME;
                        $from=SEND_GRID_FROM_EMAIL;
                        $from_name=$cust_admin['name'];
                        $from=$cust_admin['email'];*/
                        $from_name=$template_configurations['email_from_name'];
                        $from=$template_configurations['email_from'];
                        $to=$delegate_info->email;
                        $to_name=$delegate_info->first_name.' '.$delegate_info->last_name;
                        $mailer_data['mail_from_name']=$from_name;
                        $mailer_data['mail_to_name']=$to_name;
                        $mailer_data['mail_to_user_id']=$delegate_info->id_user;
                        $mailer_data['mail_from']=$from;
                        $mailer_data['mail_to']=$to;
                        $mailer_data['mail_subject']=$subject;
                        $mailer_data['mail_message']=$body;
                        $mailer_data['status']=0;
                        $mailer_data['send_date']=currentDate();
                        $mailer_data['is_cron']=0;
                        $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                        //print_r($mailer_data);
                        $mailer_id=$this->Customer_model->addMailer($mailer_data);
                        //sending mail to bu owner
                        if($mailer_data['is_cron']==0) {
                            //$mail_sent_status=sendmail($to, $subject, $body, $from);
                            $this->load->library('sendgridlibrary');
                            $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                            if($mail_sent_status==1)
                                $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                        }
                    }
                }
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('review_discussion_close_success'), 'data'=>array('recent_module_id'=>pk_encrypt($data['module_id'])));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    protected function closereviewdiscussion($data){
        if($data['id_contract_review_discussion']!=''){
            $this->Contract_model->updateContractReviewDiscussion(array('id_contract_review_discussion'=>$data['id_contract_review_discussion'],'updated_by'=>$data['created_by'],'updated_on'=>currentDate(),'discussion_status'=>2,'is_auto_close'=>1));
            $discussion_initiated_user_info = $this->User_model->getUserInfo(array('user_id' => $data['created_by']));
            $template_configurations_parent=$this->Customer_model->EmailTemplateList(array('customer_id' => $discussion_initiated_user_info->customer_id,'language_id' =>1,'module_key'=>'CONTRACT_REVIEW_DISCUSSION_CLOSE'));
            if($template_configurations_parent['total_records']>0){
                $module_info = $this->Module_model->getModuleName(array('language_id'=>1,'module_id'=>$data['module_id']));
                $contract_info = $this->Contract_model->getContractDetails(array('id_contract' => $data['contract_id']));
                $bu_owner_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['contract_owner_id']));
                $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $discussion_initiated_user_info->customer_id));
                if(isset($contract_info[0]['delegate_id']) && $contract_info[0]['delegate_id']!=NULL && $contract_info[0]['delegate_id']>0){
                    $delegate_info = $this->User_model->getUserInfo(array('user_id' => $contract_info[0]['delegate_id']));
                }
                /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
                $cust_admin = $cust_admin['data'][0];*/
                $customer_admin_list=$this->Customer_model->getCustomerAdminList(array('customer_id'=>$discussion_initiated_user_info->customer_id));
                if($customer_details[0]['company_logo']=='') {
                    $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
                }
                else{
                    $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);

                }
                if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }

                //mail to customer admins
                foreach($customer_admin_list['data'] as $kd=>$vd){
                    $template_configurations=$template_configurations_parent['data'][0];
                    $wildcards=$template_configurations['wildcards'];
                    $wildcards_replaces=array();
                    $wildcards_replaces['first_name']=$vd['first_name'];
                    $wildcards_replaces['last_name']=$vd['last_name'];
                    $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                    $wildcards_replaces['discussion_closed_user_name']=$discussion_initiated_user_info->first_name.' '.$discussion_initiated_user_info->last_name.' ('.$discussion_initiated_user_info->user_role_name.')';
                    $wildcards_replaces['discussion_closed_date']=dateFormat(currentDate());
                    $wildcards_replaces['contract_review_module_name']=$module_info[0]['module_name'];
                    $wildcards_replaces['logo']=$customer_logo;
                    $wildcards_replaces['url']=WEB_BASE_URL.'html';
                    $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                    $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                    /*$from_name=SEND_GRID_FROM_NAME;
                    $from=SEND_GRID_FROM_EMAIL;
                    $from_name=$cust_admin['name'];
                    $from=$cust_admin['email'];*/
                    $from_name=$template_configurations['email_from_name'];
                    $from=$template_configurations['email_from'];
                    $to=$vd['email'];
                    $to_name=$vd['first_name'].' '.$vd['last_name'];
                    $mailer_data['mail_from_name']=$from_name;
                    $mailer_data['mail_to_name']=$to_name;
                    $mailer_data['mail_to_user_id']=$vd['id_user'];
                    $mailer_data['mail_from']=$from;
                    $mailer_data['mail_to']=$to;
                    $mailer_data['mail_subject']=$subject;
                    $mailer_data['mail_message']=$body;
                    $mailer_data['status']=0;
                    $mailer_data['send_date']=currentDate();
                    $mailer_data['is_cron']=0;
                    $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                    //print_r($mailer_data);
                    $mailer_id=$this->Customer_model->addMailer($mailer_data);
                    //sending mail to bu owner
                    if($mailer_data['is_cron']==0) {
                        //$mail_sent_status=sendmail($to, $subject, $body, $from);
                        $this->load->library('sendgridlibrary');
                        $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                        if($mail_sent_status==1)
                            $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                    }
                }
                //mail to bu owner
                if(isset($bu_owner_info->first_name)){
                    $template_configurations=$template_configurations_parent['data'][0];
                    $wildcards=$template_configurations['wildcards'];
                    $wildcards_replaces=array();
                    $wildcards_replaces['first_name']=$bu_owner_info->first_name;
                    $wildcards_replaces['last_name']=$bu_owner_info->last_name;
                    $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                    $wildcards_replaces['discussion_closed_user_name']=$discussion_initiated_user_info->first_name.' '.$discussion_initiated_user_info->last_name.' ('.$discussion_initiated_user_info->user_role_name.')';
                    $wildcards_replaces['discussion_closed_date']=dateFormat(currentDate());
                    $wildcards_replaces['contract_review_module_name']=$module_info[0]['module_name'];
                    $wildcards_replaces['logo']=$customer_logo;
                    $wildcards_replaces['url']=WEB_BASE_URL.'html';
                    $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                    $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                    /*$from_name=SEND_GRID_FROM_NAME;
                    $from=SEND_GRID_FROM_EMAIL;
                    $from_name=$cust_admin['name'];
                    $from=$cust_admin['email'];*/
                    $from_name=$template_configurations['email_from_name'];
                    $from=$template_configurations['email_from'];
                    $to=$bu_owner_info->email;
                    $to_name=$bu_owner_info->first_name.' '.$bu_owner_info->last_name;
                    $mailer_data['mail_from_name']=$from_name;
                    $mailer_data['mail_to_name']=$to_name;
                    $mailer_data['mail_to_user_id']=$bu_owner_info->id_user;
                    $mailer_data['mail_from']=$from;
                    $mailer_data['mail_to']=$to;
                    $mailer_data['mail_subject']=$subject;
                    $mailer_data['mail_message']=$body;
                    $mailer_data['status']=0;
                    $mailer_data['send_date']=currentDate();
                    $mailer_data['is_cron']=0;
                    $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                    //print_r($mailer_data);
                    $mailer_id=$this->Customer_model->addMailer($mailer_data);
                    //sending mail to bu owner
                    if($mailer_data['is_cron']==0) {
                        //$mail_sent_status=sendmail($to, $subject, $body, $from);
                        $this->load->library('sendgridlibrary');
                        $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                        if($mail_sent_status==1)
                            $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                    }
                }
                //mail to delegate
                if(isset($delegate_info) && isset($delegate_info->first_name)){
                    $template_configurations=$template_configurations_parent['data'][0];
                    $wildcards=$template_configurations['wildcards'];
                    $wildcards_replaces=array();
                    $wildcards_replaces['first_name']=$delegate_info->first_name;
                    $wildcards_replaces['last_name']=$delegate_info->last_name;
                    $wildcards_replaces['contract_name']=$contract_info[0]['contract_name'];
                    $wildcards_replaces['discussion_closed_user_name']=$discussion_initiated_user_info->first_name.' '.$discussion_initiated_user_info->last_name.' ('.$discussion_initiated_user_info->user_role_name.')';
                    $wildcards_replaces['discussion_closed_date']=dateFormat(currentDate());
                    $wildcards_replaces['contract_review_module_name']=$module_info[0]['module_name'];
                    $wildcards_replaces['logo']=$customer_logo;
                    $wildcards_replaces['url']=WEB_BASE_URL.'html';
                    $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                    $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                    /*$from_name=SEND_GRID_FROM_NAME;
                    $from=SEND_GRID_FROM_EMAIL;
                    $from_name=$cust_admin['name'];
                    $from=$cust_admin['email'];*/
                    $from_name=$template_configurations['email_from_name'];
                    $from=$template_configurations['email_from'];
                    $to=$delegate_info->email;
                    $to_name=$delegate_info->first_name.' '.$delegate_info->last_name;
                    $mailer_data['mail_from_name']=$from_name;
                    $mailer_data['mail_to_name']=$to_name;
                    $mailer_data['mail_to_user_id']=$delegate_info->id_user;
                    $mailer_data['mail_from']=$from;
                    $mailer_data['mail_to']=$to;
                    $mailer_data['mail_subject']=$subject;
                    $mailer_data['mail_message']=$body;
                    $mailer_data['status']=0;
                    $mailer_data['send_date']=currentDate();
                    $mailer_data['is_cron']=0;
                    $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                    //print_r($mailer_data);
                    $mailer_id=$this->Customer_model->addMailer($mailer_data);
                    //sending mail to bu owner
                    if($mailer_data['is_cron']==0) {
                        //$mail_sent_status=sendmail($to, $subject, $body, $from);
                        $this->load->library('sendgridlibrary');
                        $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                        if($mail_sent_status==1)
                            $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                    }
                }
            }
        }
    }
    public function actionitemresponsibleusers_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_review_id'])) {
            $data['contract_review_id'] = pk_decrypt($data['contract_review_id']);
        }
        $result = $this->Contract_model->getActionItemResponsibleUsers($data);
        foreach($result as $k=>$v){
            $result[$k]->id_user=pk_encrypt($result[$k]->id_user);
            $result[$k]->user_role_id=pk_encrypt($result[$k]->user_role_id);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function emailTemplateList_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = tableOptions($data);
        $this->form_validator->add_rules('user_role_id', array('required'=>$this->lang->line('user_role_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=>$this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!in_array($this->session_user_info->user_role_id,array(1,2))){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_id'])) {
            $data['user_id'] = pk_decrypt($data['user_id']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['user_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_email_template'])) {
            $data['id_email_template'] = pk_decrypt($data['id_email_template']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['id_email_template'],$this->session_user_customer_email_templates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && !in_array($data['id_email_template'],$this->session_user_wadmin_email_templates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
         if($data['user_role_id']==1) {
             $data['customer_id'] = 0;
             $customer_logo = getImageUrlSendEmail('', 'company');
         }
        else {
            $customer_info = $this->User_model->getUserInfo(array('user_id'=>$data['user_id']));
            $data['customer_id'] = $customer_info->customer_id;
            $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $customer_info->customer_id));
            if($customer_details[0]['company_logo']=='') {
                $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
            }
            else{
                $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company', SMALL_IMAGE);
            }

        }
        $result = $this->Contract_model->getEmailTemplate($data);
        foreach($result['data'] as $k=>$v){
            $result['data'][$k]['created_by']=pk_encrypt($result['data'][$k]['created_by']);
            $result['data'][$k]['customer_id']=pk_encrypt($result['data'][$k]['customer_id']);
            $result['data'][$k]['email_template_id']=pk_encrypt($result['data'][$k]['email_template_id']);
            $result['data'][$k]['id_email_template']=pk_encrypt($result['data'][$k]['id_email_template']);
            $result['data'][$k]['id_email_template_language']=pk_encrypt($result['data'][$k]['id_email_template_language']);
            $result['data'][$k]['language_id']=pk_encrypt($result['data'][$k]['language_id']);
            $result['data'][$k]['parent_email_template_id']=pk_encrypt($result['data'][$k]['parent_email_template_id']);
            $result['data'][$k]['updated_by']=pk_encrypt($result['data'][$k]['updated_by']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result,'customer_logo'=>$customer_logo);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function emailTemplateUpdate_post(){
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('user_id', array('required'=>$this->lang->line('user_id_req')));
        $this->form_validator->add_rules('id_email_template', array('required'=>$this->lang->line('id_email_template_req')));
        $this->form_validator->add_rules('status', array('required'=>$this->lang->line('email_status_req')));
        $this->form_validator->add_rules('id_email_template_language', array('required'=>$this->lang->line('id_email_template_language_req')));
        $this->form_validator->add_rules('template_name', array('required'=>$this->lang->line('email_template_name_req')));
        $this->form_validator->add_rules('template_subject', array('required'=>$this->lang->line('email_template_subject_req')));
        $this->form_validator->add_rules('template_content', array('required'=>$this->lang->line('email_template_content_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!in_array($this->session_user_info->user_role_id,array(1,2))){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['user_id'])) {
            $data['user_id'] = pk_decrypt($data['user_id']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['user_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_email_template'])) {
            $data['id_email_template'] = pk_decrypt($data['id_email_template']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['id_email_template'],$this->session_user_customer_email_templates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && !in_array($data['id_email_template'],$this->session_user_wadmin_email_templates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_email_template_language'])) $data['id_email_template_language']=pk_decrypt($data['id_email_template_language']);
        $email_template = array();
        $email_template_lang = array();
        $email_template['id_email_template']=$data['id_email_template'];
        $email_template['status']=$data['status'];
        $email_template['updated_by']=$data['user_id'];
        $email_template['updated_on']=currentDate();
        $email_template_lang['id_email_template_language']=$data['id_email_template_language'];
        $email_template_lang['template_name']=$data['template_name'];
        $email_template_lang['template_subject']=$data['template_subject'];
        $email_template_lang['template_content']=$data['template_content'];

        $this->Contract_model->updateEmailTemplate($email_template);
        $this->Contract_model->updateEmailTemplate($email_template_lang);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);

    }
    public function emailTemplateUpdateStatus_post(){
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('user_id', array('required'=>$this->lang->line('user_id_req')));
        $this->form_validator->add_rules('id_email_template', array('required'=>$this->lang->line('id_email_template_req')));
        $this->form_validator->add_rules('status', array('required'=>$this->lang->line('email_status_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!in_array($this->session_user_info->user_role_id,array(1,2))){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['user_id'])) {
            $data['user_id'] = pk_decrypt($data['user_id']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['user_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_email_template'])) {
            $data['id_email_template'] = pk_decrypt($data['id_email_template']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['id_email_template'],$this->session_user_customer_email_templates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && !in_array($data['id_email_template'],$this->session_user_wadmin_email_templates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $email_template = array();
        $email_template['id_email_template']=$data['id_email_template'];
        $email_template['status']=$data['status'];
        $email_template['updated_by']=$data['user_id'];
        $email_template['updated_on']=currentDate();


        $this->Contract_model->updateEmailTemplate($email_template);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function insertEmailTemplate_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_email_template', array('required'=>$this->lang->line('user_role_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=>$this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!in_array($this->session_user_info->user_role_id,array(1,2))){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['user_id'])) {
            $data['user_id'] = pk_decrypt($data['user_id']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['user_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_email_template'])) {
            $data['id_email_template'] = pk_decrypt($data['id_email_template']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['id_email_template'],$this->session_user_customer_email_templates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && !in_array($data['id_email_template'],$this->session_user_wadmin_email_templates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $email_template_data = $this->Contract_model->getEmailTemplate(array('id_email_template'=>$data['id_email_template']));
        if(empty($email_template_data))
        {
            $result = array('status'=>TRUE, 'message' => $this->lang->line('invalid_id_email_template'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $email_template_data = $email_template_data[0];

        $email_template['module_name'] = $email_template_data['module_name'];
        $email_template['module_key'] = $email_template_data['module_key'];
        $email_template['wildcards'] = $email_template_data['wildcards'];
        $email_template['created_by'] = $data['user_id'];
        $email_template['created_on'] = currentDate();
        $email_template['status'] = $email_template_data['status'];
        $email_template['parent_email_template_id'] = $email_template_data['id_email_template'];

        $email_template_language['template_name']=$email_template_data['template_name'];
        $email_template_language['template_subject']=$email_template_data['template_subject'];
        $email_template_language['template_content']=$email_template_data['template_content'];
        $email_template_language['language_id']=$email_template_data['language_id'];

        if(isset($data['customer_id']))
            $this->Contract_model->insertEmailTemplate(array('email_template'=>$email_template,'email_template_language'=>$email_template_language,'customer_id'=>$data['customer_id']));
        else
            $this->Contract_model->insertEmailTemplate(array('email_template'=>$email_template,'email_template_language'=>$email_template_language));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function delecteContract_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($this->session_user_info->user_role_id!=2 && $this->session_user_info->user_role_id!=3){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'2');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $this->User_model->update_data('contract',array('is_deleted'=>1,'updated_by'=>$data['id_user'],'updated_on'=>currentDate()),array('id_contract'=>$data['contract_id']));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('contract_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function undoDelContract_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('contract_id', array('required'=>$this->lang->line('contract_id_req')));
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($this->session_user_info->user_role_id!=2){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'2');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'2');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $this->User_model->update_data('contract',array('is_deleted'=>0,'updated_by'=>$data['id_user']),array('id_contract'=>$data['contract_id']));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('contract_undo'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }


}
