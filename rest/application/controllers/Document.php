<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Document extends REST_Controller
{
    public $session_user_id=NULL;
    public $session_user_info=NULL;
    public $session_user_business_units=NULL;
    public $session_user_business_units_user=NULL;
    public $session_user_contracts=NULL;
    public $session_user_contract_reviews=NULL;
    public $session_user_contract_documents=NULL;
    public $session_user_contract_action_items=NULL;
    public $session_user_delegates=NULL;
    public $session_user_contributors=NULL;
    public $session_user_reporting_owners=NULL;
    public $session_user_bu_owners=NULL;
    public $session_user_customer_admins=NULL;
    public $session_user_customer_all_users=NULL;
    public $session_user_customer_relationship_categories=NULL;
    public $session_user_customer_relationship_classifications=NULL;
    public $session_user_customer_calenders=NULL;
    public $session_user_master_currency=NULL;
    public $session_user_master_language=NULL;
    public $session_user_master_countries=NULL;
    public $session_user_master_templates=NULL;
    public $session_user_master_customers=NULL;
    public $session_user_master_users=NULL;
    public $session_user_master_user_roles=NULL;
    public $session_user_contract_review_modules=NULL;
    public $session_user_contract_review_topics=NULL;
    public $session_user_contract_review_questions=NULL;
    public $session_user_contract_review_question_options=NULL;
    public $session_user_wadmin_relationship_categories=NULL;
    public $session_user_wadmin_relationship_classifications=NULL;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Validation_model');
        //$this->session_user_id=!empty($this->session->userdata('session_user_id_acting'))?($this->session->userdata('session_user_id_acting')):($this->session->userdata('session_user_id'));
        $getLoggedUserId=$this->User_model->getLoggedUserId();
        $this->session_user_id=$getLoggedUserId[0]['id'];
        $this->session_user_info=$this->User_model->getUserInfo(array('user_id'=>$this->session_user_id));
        if($this->session_user_info->user_role_id<3 || $this->session_user_info->user_role_id==6 || $this->session_user_info->user_role_id==5)
            $this->session_user_business_units=$this->Validation_model->getBusinessUnitList(array('customer_id'=>$this->session_user_info->customer_id));
        else if($this->session_user_info->user_role_id>=3)
            $this->session_user_business_units=$this->Validation_model->getBusinessUnitListByUser(array('user_id'=>$this->session_user_info->id_user));
        if($this->session_user_info->user_role_id==5)
            $this->session_user_contracts=$this->Validation_model->getContributorContract(array('business_unit_id'=>$this->session_user_business_units,'customer_user'=>$this->session_user_info->id_user));
        else
            $this->session_user_contracts=$this->Validation_model->getContracts(array('business_unit_id'=>$this->session_user_business_units));
        //$this->session_user_contracts=$this->Validation_model->getContracts(array('business_unit_id'=>$this->session_user_business_units_user));
        $assigned_contracts=$this->Validation_model->getContributorContract(array('customer_user'=>$this->session_user_info->id_user));
        $this->session_user_contracts=array_merge($this->session_user_contracts,$assigned_contracts);
        $this->session_user_contract_reviews=$this->Validation_model->getContractReviews(array('contract_id'=>$this->session_user_contracts));
        $review_documents=$this->Validation_model->getContractReviewDocuments(array('contract_review_id'=>$this->session_user_contract_reviews));
        $documents=$this->Validation_model->getContractDocuments(array('contract_id'=>$this->session_user_contracts));
        $this->session_user_contract_documents=array_merge($review_documents,$documents);
        
        $this->session_user_delegates=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id),'user_role_id'=>4));
        $this->session_user_contributors=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id),'user_role_id'=>5));
        $this->session_user_customer_all_users=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id)));
        $this->session_user_master_customers=$this->Validation_model->getCustomers();
        $this->session_user_contract_review_modules=$this->Validation_model->getContractReviewModules(array('contract_review_id'=>$this->session_user_contract_reviews));
        $this->session_user_contract_review_topics=$this->Validation_model->getContractReviewTopics(array('module_id'=>$this->session_user_contract_review_modules));
        $this->session_user_contract_review_questions=$this->Validation_model->getContractReviewQuestions(array('topic_id'=>$this->session_user_contract_review_topics));

    }

    public function list_get()
    {
        $data = $this->input->get();

        $data = tableOptions($data);
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['reference_id'])) {
            $data['reference_id'] = pk_decrypt($data['reference_id']);
            if(!in_array($data['reference_id'],$this->session_user_contracts) && !in_array($data['reference_id'],$this->session_user_contract_review_questions)  && !in_array($data['reference_id'],$this->session_user_contract_review_topics) ){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['module_id'])) {
            $data['module_id'] = pk_decrypt($data['module_id']);
            if(!in_array($data['module_id'],$this->session_user_master_customers) && !in_array($data['module_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['contract_id']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(isset($data['id_user']) && isset($data['user_role_id']) && $data['user_role_id']==5){
            $data['contract_user'] = $data['id_user'];
            if(!in_array($data['contract_user'],$this->session_user_contributors)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['deleted'])){

        }else{
            $data['document_status']=1;
        }
        //$data['document_status']=isset($data['deleted'])?0:1;
        if(isset($data['updated_by'])){
            unset($data['updated_by']);
            $result = $this->Document_model->getDocLogList($data);
            foreach($result['data'] as $ka=>$va) {
                $result['data'][$ka]['updated_by']=0;
            }
            $data['updated_by']=isset($data['updated_by'])?$data['updated_by']:1;
            $result2 = $this->Document_model->getDocLogList($data);
            $result['total_records']+=$result2['total_records'];
            $result['data'] = array_merge($result['data'],$result2['data']);
            foreach($result['data'] as $ka=>$va){
                if($va['updated_by']>0){
                    $result['data'][$ka]['uploaded_on']=null;
                    $result['data'][$ka]['datetime']=$result['data'][$ka]['updated_on'];
                    $result['data'][$ka]['action']='Added';
                }
                if($va['updated_by']==0){
                    $result['data'][$ka]['updated_on']=null;
                    $result['data'][$ka]['datetime']=$result['data'][$ka]['uploaded_on'];
                    $result['data'][$ka]['action']='Deleted';
                }
            }
            function date_compare1($a, $b)
            {
                $t1 = strtotime($a['datetime']);
                $t2 = strtotime($b['datetime']);
                return $t2 - $t1;
            }
            usort($result['data'], 'date_compare1');
            //echo '<pre>'.print_r($data).'</pre>';
            if(isset($data['sort']['predicate']) && $data['sort']['predicate']=='first_name'){
                if($data['sort']['reverse']=='ASC'){
                    function sortByOrder($a, $b) {
                        return strcmp($a['first_name'] , $b['first_name']);
                    }
                    usort($result['data'], 'sortByOrder');
                }else{
                    function sortByOrder($a, $b) {
                        return strcmp($b['first_name'] , $a['first_name']);
                    }
                    usort($result['data'], 'sortByOrder');
                }
            }
            if(isset($data['sort']['predicate']) && $data['sort']['predicate']=='module_name'){
                if($data['sort']['reverse']=='ASC'){
                    function sortByOrder($a, $b) {
                        return strcmp($a['module_name'] , $b['module_name']);
                    }
                    usort($result['data'], 'sortByOrder');
                }else{
                    function sortByOrder($a, $b) {
                        return strcmp($b['module_name'] , $a['module_name']);
                    }
                    usort($result['data'], 'sortByOrder');
                }
            }
            if(isset($data['sort']['predicate']) && $data['sort']['predicate']=='action'){
                if($data['sort']['reverse']=='ASC'){
                    function sortByOrder($a, $b) {
                        return strcmp($a['action'] , $b['action']);
                    }
                    usort($result['data'], 'sortByOrder');
                }else{
                    function sortByOrder($a, $b) {
                        return strcmp($b['action'] , $a['action']);
                    }
                    usort($result['data'], 'sortByOrder');
                }
            }
            if(isset($data['sort']['predicate']) && $data['sort']['predicate']=='datetime' ){
                if($data['sort']['reverse'] == 'ASC'){
                    function date_compare($a, $b)
                    {
                        $t1 = strtotime($a['datetime']);
                        $t2 = strtotime($b['datetime']);
                        return $t1 - $t2;
                    }
                    usort($result['data'], 'date_compare');
                }else{
                    function date_compare($a, $b)
                    {
                        $t1 = strtotime($a['datetime']);
                        $t2 = strtotime($b['datetime']);
                        return $t2 - $t1;
                    }
                    usort($result['data'], 'date_compare');
                }
            }

            foreach($result['data'] as $ka=>$va){
                $result['data'][$ka]['document_source_exactpath']=($va['document_source']);
                $result['data'][$ka]['id_module']=pk_encrypt($va['id_module']);
                $result['data'][$ka]['id_document']=pk_encrypt($va['id_document']);
                $result['data'][$ka]['module_id']=pk_encrypt($va['module_id']);
                $result['data'][$ka]['reference_id']=pk_encrypt($va['reference_id']);
                $result['data'][$ka]['user_role_id']=pk_encrypt($va['user_role_id']);
                $result['data'][$ka]['contract_owner_id']=pk_encrypt($va['contract_owner_id']);
                $result['data'][$ka]['delegate_id']=pk_encrypt($va['delegate_id']);
                $result['data'][$ka]['updated_by']=(int)($va['updated_by']);
            }
         } else{
                $result = $this->Document_model->getDocList($data);
                foreach($result['data'] as $ka=>$va){
                    $result['data'][$ka]['document_source_exactpath']=($va['document_source']);
                    $result['data'][$ka]['id_module']=pk_encrypt($va['id_module']);
                    $result['data'][$ka]['id_document']=pk_encrypt($va['id_document']);
                    $result['data'][$ka]['module_id']=pk_encrypt($va['module_id']);
                    $result['data'][$ka]['reference_id']=pk_encrypt($va['reference_id']);
                    $result['data'][$ka]['user_role_id']=pk_encrypt($va['user_role_id']);
                    $result['data'][$ka]['contract_owner_id']=pk_encrypt($va['contract_owner_id']);
                    $result['data'][$ka]['delegate_id']=pk_encrypt($va['delegate_id']);
                }
        }

        //print_r($result);die;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    /*public function documentlist_get()
    {          // to bring documents of all reviews, in contract review page
        $data = $this->input->get();

        $data = tableOptions($data);
        $data['document_status']=1;
        if(isset($data['id_user']) && isset($data['user_role_id']) && $data['user_role_id']==5){
            $data['contract_user'] = $data['id_user'];
        }
        $result = $this->Document_model->getAllDoccumentList($data);
        foreach($result as $ka=>$va){
            $result[$ka]['document_source_exactpath']=getExactImageUrl($va['document_source']);
        }
        //print_r($result);die;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);

    }*/

    public function add_post()
    {

        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($_FILES['file']) || empty($_FILES['file'])) {
            $result = array('status' => FALSE, 'error' => array('document' => $this->lang->line('document_error').$this->lang->line('allowed_formats')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=>$this->lang->line('module_id_req')));
        $this->form_validator->add_rules('module_type', array('required'=>$this->lang->line('module_type_req')));
        $this->form_validator->add_rules('reference_id', array('required'=>$this->lang->line('reference_id_req')));
        $this->form_validator->add_rules('reference_type', array('required'=>$this->lang->line('reference_type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['reference_id'])) {
            $data['reference_id'] = pk_decrypt($data['reference_id']);
            if($data['reference_type']=='contract'){
                if($data['reference_id']>0 && !in_array($data['reference_id'],$this->session_user_contracts)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['reference_type']=='question'){
                if($data['reference_id']>0 && !in_array($data['reference_id'],$this->session_user_contract_review_questions)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }

        }
        if(isset($data['module_id'])) {
            $data['module_id'] = pk_decrypt($data['module_id']);
            if($data['module_type']=='customer'){
                if($data['module_id']>0 && !in_array($data['module_id'],$this->session_user_master_customers)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['module_type']=='contract_review'){
                if($data['module_id']>0 && !in_array($data['module_id'],$this->session_user_contract_reviews)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }

        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['uploaded_by'])) {
            $data['uploaded_by'] = pk_decrypt($data['uploaded_by']);
            if($data['uploaded_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $totalFilesCount = count($_FILES['file']['name']);
        $document_data = array();
        $path=FILE_SYSTEM_PATH.'uploads/';
        if(!is_dir($path.$data['customer_id'])){ mkdir($path.$data['customer_id']); }
        if(!is_dir($path.$data['customer_id'].'/deleted')){ mkdir($path.$data['customer_id'].'/deleted'); }
        if(isset($_FILES) && count($totalFilesCount)>0)
        {
            for($i_attachment=0;$i_attachment<$totalFilesCount;$i_attachment++) {

                $imageName = doUpload(array(
                    'temp_name' => $_FILES['file']['tmp_name'][$i_attachment],
                    'image' => $_FILES['file']['name'][$i_attachment],
                    'upload_path' => $path,
                    'folder' => $data['customer_id']));
                $document_data[$i_attachment]['module_id'] = $data['module_id'];
                $document_data[$i_attachment]['module_type'] = $data['module_type'];
                $document_data[$i_attachment]['reference_id'] = $data['reference_id'];
                $document_data[$i_attachment]['reference_type'] = $data['reference_type'];
                $document_data[$i_attachment]['document_name'] = $_FILES['file']['name'][$i_attachment];
                $document_data[$i_attachment]['document_source'] = $imageName;
                $document_data[$i_attachment]['document_mime_type'] = $_FILES['file']['type'][$i_attachment];
                $document_data[$i_attachment]['uploaded_by'] = $data['uploaded_by'];
                $document_data[$i_attachment]['uploaded_on'] = currentDate();
                $document_data[$i_attachment]['updated_on'] = currentDate();
            }
        }


        if(count($document_data)>0){
            $this->Document_model->addBulkDocuments($document_data);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('document_add'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function delete_delete()
    {
        $data = $this->input->get();
        $result='';
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_document', array('required'=>$this->lang->line('document_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_document'])) {
            $data['id_document'] = pk_decrypt($data['id_document']);
            if(!in_array($data['id_document'],$this->session_user_contract_documents)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['module_id'])) {
            $data['module_id'] = pk_decrypt($data['module_id']);
            if(!in_array($data['module_id'],$this->session_user_master_customers) && !in_array($data['module_id'],$this->session_user_contract_reviews)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['reference_id'])) {
            $data['reference_id'] = pk_decrypt($data['reference_id']);
            if(!in_array($data['reference_id'],$this->session_user_contracts) && !in_array($data['reference_id'],$this->session_user_contract_review_questions)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_owner_id'])) {
            $data['contract_owner_id'] = pk_decrypt($data['contract_owner_id']);
            if(!in_array($data['contract_owner_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['delegate_id'])) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if(!in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $details=$this->Document_model->getDocument($data);
        if($details[0]['module_type']=='contract_review'){
            $contract_id=$details[0]['module_id'];
            $cnt_details=$this->Contract_model->getContractDetails(array('contract_review_id'=>$contract_id));
        }
        if($details[0]['reference_type']=='contract'){
            $contract_id=$details[0]['reference_id'];
            $cnt_details=$this->Contract_model->getContractDetails(array('id_contract'=>$contract_id));
        }
        if(isset($cnt_details[0]['id_contract'])) {
            $user_info=$this->User_model->getUserInfo(array('user_id'=>$details[0]['uploaded_by']));
            $delete_access = 0;
            if (isset($this->session_user_id) && isset($this->session_user_info->user_role_id)) {
                if ($this->session_user_info->user_role_id == 6) {
                    $delete_access = 0;
                    if ($details[0]['uploaded_by'] == $this->session_user_id) {
                        $delete_access = 1;
                    }
                }
                else if ($this->session_user_info->user_role_id == 5) {
                    $delete_access = 1;
                    if ($details[0]['uploaded_by'] == $this->session_user_id) {
                        $delete_access = 1;
                    }
                } else if ($this->session_user_info->user_role_id == 4 || $this->session_user_info->user_role_id == 3 || $this->session_user_info->user_role_id == 2 || $this->session_user_info->user_role_id == 1) {
                    if ($details[0]['uploaded_by'] == $this->session_user_id || $user_info->user_role_id > $this->session_user_info->user_role_id) {
                        $delete_access = 1;
                    }
                }
                if ($this->session_user_id == $cnt_details[0]['contract_owner_id'] || $this->session_user_id == $cnt_details[0]['delegate_id']) {
                    $delete_access = 1;
                }
            } else {
                $delete_access = 1;
            }
            if($delete_access==0){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $path=getExactImageDirectoryUrl($details[0]['document_source']);
        if(file_exists($path)) {
            if(copy($path, str_replace($details[0]['document_source'], str_replace('/', '/deleted/', $details[0]['document_source']), $path)))
                unlink($path);
        }
        $this->Document_model->deleteDocument($this->session_user_id,$data,$details[0]['document_source']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('document_delete'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }



}