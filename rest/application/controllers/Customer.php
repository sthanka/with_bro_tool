<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Customer extends REST_Controller
{
    public $user_id = 0 ;
    public $session_user_id=NULL;
    public $session_user_info=NULL;
    public $session_user_business_units=NULL;
    public $session_user_business_units_user=NULL;
    public $session_user_contracts=NULL;
    public $session_user_contract_reviews=NULL;
    public $session_user_contract_documents=NULL;
    public $session_user_contract_action_items=NULL;
    public $session_user_delegates=NULL;
    public $session_user_contributors=NULL;
    public $session_user_reporting_owners=NULL;
    public $session_user_bu_owners=NULL;
    public $session_user_customer_admins=NULL;
    public $session_user_customer_all_users=NULL;
    public $session_user_customer_relationship_categories=NULL;
    public $session_user_customer_relationship_classifications=NULL;
    public $session_user_customer_calenders=NULL;
    public $session_user_master_currency=NULL;
    public $session_user_master_language=NULL;
    public $session_user_master_countries=NULL;
    public $session_user_master_templates=NULL;
    public $session_user_master_customers=NULL;
    public $session_user_master_users=NULL;
    public $session_user_master_user_roles=NULL;
    public $session_user_contract_review_modules=NULL;
    public $session_user_contract_review_topics=NULL;
    public $session_user_contract_review_questions=NULL;
    public $session_user_contract_review_question_options=NULL;
    public $session_user_wadmin_relationship_categories=NULL;
    public $session_user_wadmin_relationship_classifications=NULL;
    public $session_user_wadmin_email_templates=NULL;
    public $session_user_customer_email_templates=NULL;
    public $session_user_customer_providers=NULL;
    public $session_user_own_business_units=NULL;
    public $session_user_review_business_units=NULL;
    public function __construct()
    {
        parent::__construct();
        if(isset($_SERVER['HTTP_USER'])){
            $this->user_id = pk_decrypt($_SERVER['HTTP_USER']);
        }
        $this->load->model('Validation_model');
        $getLoggedUserId=$this->User_model->getLoggedUserId();
        //$this->session_user_id=!empty($this->session->userdata('session_user_id_acting'))?($this->session->userdata('session_user_id_acting')):($this->session->userdata('session_user_id'));
        $this->session_user_id=$getLoggedUserId[0]['id'];
        $this->session_user_info=$this->User_model->getUserInfo(array('user_id'=>$this->session_user_id));
        if($this->session_user_info->user_role_id<3 || $this->session_user_info->user_role_id==5)
            $this->session_user_business_units=$this->Validation_model->getBusinessUnitList(array('customer_id'=>$this->session_user_info->customer_id));
        else if($this->session_user_info->user_role_id==3 || $this->session_user_info->user_role_id==4)
            $this->session_user_business_units=$this->Validation_model->getBusinessUnitListByUser(array('user_id'=>$this->session_user_info->id_user));
        else if($this->session_user_info->user_role_id==6){
            if($this->session_user_info->is_allow_all_bu==1)
                $this->session_user_business_units=$this->Validation_model->getBusinessUnitList(array('customer_id'=>$this->session_user_info->customer_id));
            else
                $this->session_user_business_units=$this->Validation_model->getBusinessUnitListByUser(array('user_id'=>$this->session_user_info->id_user));
        }
        $this->session_user_own_business_units=$this->session_user_business_units;
        $this->session_user_review_business_units=$this->Validation_model->getReviewBusinessUnits(array('id_user'=>$this->session_user_id));
        $this->session_user_business_units=array_merge($this->session_user_business_units,$this->session_user_review_business_units);
        if($this->session_user_info->user_role_id==5)
            $this->session_user_contracts=$this->Validation_model->getContributorContract(array('business_unit_id'=>$this->session_user_business_units,'customer_user'=>$this->session_user_info->id_user));
        else
            $this->session_user_contracts=$this->Validation_model->getContracts(array('business_unit_id'=>$this->session_user_business_units));
        //$this->session_user_contracts=$this->Validation_model->getContracts(array('business_unit_id'=>$this->session_user_business_units_user));
        $this->session_user_delegates=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id),'user_role_id'=>4));
        $this->session_user_contributors=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id),'user_role_id'=>5));
        $this->session_user_customer_all_users=$this->Validation_model->getCustomerUsers(array('customer_id'=>array($this->session_user_info->customer_id)));
        $this->session_user_customer_relationship_categories=$this->Validation_model->getCustomerRelationshipCategories(array('customer_id'=>array($this->session_user_info->customer_id)));
        $this->session_user_customer_calenders=$this->Validation_model->getCustomerCalenders(array('customer_id'=>array($this->session_user_info->customer_id)));
        $this->session_user_master_countries=$this->Validation_model->getCountries();
        $this->session_user_master_templates=$this->Validation_model->getTemplates();
        $this->session_user_master_customers=$this->Validation_model->getCustomers();
        $this->session_user_master_users=$this->Validation_model->getUsers();
        $this->session_user_master_user_roles=$this->Validation_model->getUserRoles();


        $this->session_user_wadmin_relationship_categories=$this->Validation_model->getCustomerRelationshipCategories(array('customer_id'=>array(0)));
    }

    public function list_get()
    {
        $data = $this->input->get();
        if($this->session_user_info->user_role_id!=1){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        /*helper function for ordering smart table grid options*/
        $data = tableOptions($data);
        $result = $this->Customer_model->customerList($data);
        for($s=0;$s<count($result['data']);$s++)
        {
            /*getImageUrl helper function for getting image usrl*/
            $result['data'][$s]['company_logo'] = getImageUrl($result['data'][$s]['company_logo'],'company',SMALL_IMAGE);
            $result['data'][$s]['country_id'] = pk_encrypt($result['data'][$s]['country_id']);
            $result['data'][$s]['created_by'] = pk_encrypt($result['data'][$s]['created_by']);
            $result['data'][$s]['id_customer'] = pk_encrypt($result['data'][$s]['id_customer']);
            $result['data'][$s]['template_id'] = pk_encrypt($result['data'][$s]['template_id']);
            $result['data'][$s]['updated_by'] = pk_encrypt($result['data'][$s]['updated_by']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function info_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_customer', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_customer'])) {
            $data['id_customer'] = pk_decrypt($data['id_customer']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['id_customer']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $result = $this->Customer_model->getCustomer($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['company_logo_medium'] = getImageUrl($result[$s]['company_logo'],'company',MEDIUM_IMAGE);
            $result[$s]['company_logo_small'] = getImageUrl($result[$s]['company_logo'],'company',SMALL_IMAGE);
            $result[$s]['company_logo'] = getImageUrl($result[$s]['company_logo'],'company','');
            $result[$s]['country_id'] = pk_encrypt($result[$s]['country_id']);
            $result[$s]['created_by'] = pk_encrypt($result[$s]['created_by']);
            $result[$s]['id_customer'] = pk_encrypt($result[$s]['id_customer']);
            $result[$s]['template_id'] = pk_encrypt($result[$s]['template_id']);
            $result[$s]['updated_by'] = pk_encrypt($result[$s]['updated_by']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function details_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_customer', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_customer'])) {
            $data['id_customer'] = pk_decrypt($data['id_customer']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['id_customer']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $result = $this->Customer_model->getCustomer($data);
        $result_array = array();
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['company_logo_medium'] = getImageUrl($result[$s]['company_logo'],'company',MEDIUM_IMAGE);
            $result[$s]['company_logo_small'] = getImageUrl($result[$s]['company_logo'],'company',SMALL_IMAGE);
            $result[$s]['company_logo'] = getImageUrl($result[$s]['company_logo'],'company','');
        }
        if(!empty($result)) {
            if (isset($result[0])) {
                $result = $result[0];
            }
            $result_array = array(
                'company_name' => $result['company_name'],
                'company_address' => $result['company_address'],
                'postal_code' => $result['postal_code'],
                'city' => $result['city'],
                'vat_number' => $result['vat_number'],
                'template_id' => pk_encrypt($result['template_id']),
                'country_id' => pk_encrypt($result['country_id']),
                'company_logo_medium' => $result['company_logo_medium'],
                'company_logo_small' => $result['company_logo_small'],
                'company_logo' => $result['company_logo'],
            );
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result_array);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function add_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['customer'])){ $data = $data['customer']; }

        $this->form_validator->add_rules('company_name', array('required'=>$this->lang->line('company_name_req')));
        $this->form_validator->add_rules('postal_code', array('required'=>$this->lang->line('postal_code_req')));
        //$this->form_validator->add_rules('vat_number', array('required'=>$this->lang->line('vat_number_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        //$this->form_validator->add_rules('template_id', array('required'=>$this->lang->line('template_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($this->session_user_info->user_role_id!=1){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['template_id'])) {
            $data['template_id'] = pk_decrypt($data['template_id']);
            if(!in_array($data['template_id'],$this->session_user_master_templates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['country_id'])) {
            $data['country_id'] = pk_decrypt($data['country_id']);
            if(!in_array($data['country_id'],$this->session_user_master_countries)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $customer_data = array(
            'company_name' => $data['company_name'],
            'company_address' => isset($data['company_address'])?$data['company_address']:'',
            'postal_code' => $data['postal_code'],
            'city' => isset($data['city'])?$data['city']:'',
            'vat_number' => isset($data['vat_number'])?$data['vat_number']:'',
            'country_id' => isset($data['country_id'])?$data['country_id']:'',
            'created_by' => $data['created_by'],
            'created_on' => currentDate()
        );

        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['customer']['name']['company_logo']))
        {
            $imageName = doUpload(array(
                'temp_name' => $_FILES['customer']['tmp_name']['company_logo'],
                'image' => $_FILES['customer']['name']['company_logo'],
                'upload_path' => $path,
                'folder' => ''));
            $customer_data['company_logo'] = $imageName;
        }
        else{
            unset($customer_data['company_logo']);
        }

        $customer_id = $this->Customer_model->addCustomer($customer_data);
        if(isset($imageName)){
            if(!is_dir($path.$customer_id)){ mkdir($path.$customer_id); }
            rename($path.$imageName, $path.$customer_id.'/'.$imageName);
            imageResize($path.$customer_id.'/'.$imageName);
            $this->Customer_model->updateCustomer(array('id_customer' => $customer_id,'company_logo' => $customer_id.'/'.$imageName));
        }

        /* updating relationship category */
        $relationship_category = $this->Relationship_category_model->RelationshipCategoryList(array('customer_id' => 0,'relationship_category_status' =>1));
        $relationship_category = $relationship_category['data'];

        for($s=0;$s<count($relationship_category);$s++)
        {
            $inserted_id = $this->Relationship_category_model->addRelationshipCategory(array(
                'relationship_category_quadrant' => $relationship_category[$s]['relationship_category_quadrant'],
                'relationship_category_status' => 1,
                'parent_relationship_category_id' => $relationship_category[$s]['id_relationship_category'],
                'customer_id' => $customer_id,
                'created_by' => $data['created_by'],
                'created_on' => currentDate()
            ));

            $this->Relationship_category_model->addRelationshipCategoryLanguage(array(
                'relationship_category_id' => $inserted_id,
                'relationship_category_name' => $relationship_category[$s]['relationship_category_name'],
                'language_id' => $relationship_category[$s]['language_id']
            ));
        }

        /* updating relationship classification */
        $relationship_classification = $this->Relationship_category_model->RelationshipClassificationList(array('customer_id' => 0,'parent_classification_id' => 0,'classification_status' =>1));
        $relationship_classification = $relationship_classification['data'];
        /*$relationship_classification1 = $this->Relationship_category_model->RelationshipClassificationList(array('customer_id' => 0,'parent_classification_id_not' => 0,'classification_status' =>1));
        $relationship_classification1 = $relationship_classification1['data'];
        $relationship_classification = array_merge($relationship_classification,$relationship_classification1);*/
        for($s=0;$s<count($relationship_classification);$s++)
        {
            $parent_inserted_id = $this->Relationship_category_model->addRelationshipClassification(array(
                'classification_key' => $relationship_classification[$s]['classification_key'],
                'classification_position' => $relationship_classification[$s]['classification_position'],
                'parent_classification_id' => $relationship_classification[$s]['parent_classification_id'],
                'parent_relationship_classification_id' => $relationship_classification[$s]['id_relationship_classification'],
                'customer_id' => $customer_id,
                'is_visible' => $relationship_classification[$s]['is_visible'],
                'created_by' => $data['created_by'],
                'created_on' => currentDate()
            ));
            $this->Relationship_category_model->addRelationshipClassificationLanguage(array(
                'relationship_classification_id' => $parent_inserted_id,
                'classification_name' => $relationship_classification[$s]['classification_name'],
                'language_id' => $relationship_classification[$s]['language_id']
            ));

            $relationship_classification1 = $this->Relationship_category_model->RelationshipClassificationList(array('customer_id' => 0,'parent_classification_id' => $relationship_classification[$s]['id_relationship_classification'],'classification_status' =>1));
            $relationship_classification1 = $relationship_classification1['data'];
            for($sr=0;$sr<count($relationship_classification1);$sr++)
            {
                $inserted_id = $this->Relationship_category_model->addRelationshipClassification(array(
                    'classification_key' => $relationship_classification1[$sr]['classification_key'],
                    'classification_position' => $relationship_classification1[$sr]['classification_position'],
                    'parent_classification_id' => $parent_inserted_id,
                    'parent_relationship_classification_id' => $relationship_classification[$s]['id_relationship_classification'],
                    'customer_id' => $customer_id,
                    'is_visible' => $relationship_classification1[$sr]['is_visible'],
                    'created_by' => $data['created_by'],
                    'created_on' => currentDate()
                ));
                $this->Relationship_category_model->addRelationshipClassificationLanguage(array(
                    'relationship_classification_id' => $inserted_id,
                    'classification_name' => $relationship_classification1[$sr]['classification_name'],
                    'language_id' => $relationship_classification1[$sr]['language_id']
                ));
            }


        }

        /* updating email templates */
        $email_template = $this->Customer_model->EmailTemplateList(array('customer_id' => 0,'language_id' =>1,'status'=>'0,1'));
        $email_template = $email_template['data'];

        for($s=0;$s<count($email_template);$s++)
        {
            $inserted_id = $this->Customer_model->addEmailTemplate(array(
                'module_name' => $email_template[$s]['module_name'],
                'module_key' => $email_template[$s]['module_key'],
                'wildcards' => $email_template[$s]['wildcards'],
                'email_from_name' => $email_template[$s]['email_from_name'],
                'email_from' => $email_template[$s]['email_from'],
                'status' => $email_template[$s]['status'],
                'parent_email_template_id' => $email_template[$s]['id_email_template'],
                'customer_id' => $customer_id,
                'created_by' => $data['created_by'],
                'recipients' => $email_template[$s]['recipients'],
                'created_on' => currentDate()
            ));

            $this->Customer_model->addEmailTemplateLanguage(array(
                'email_template_id' => $inserted_id,
                'template_name' => $email_template[$s]['template_name'],
                'template_subject' => $email_template[$s]['template_subject'],
                'template_content' => $email_template[$s]['template_content'],
                'language_id' => $email_template[$s]['language_id']
            ));
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('customer_add'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function update_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer'])){ $data = $data['customer']; }

        $this->form_validator->add_rules('id_customer', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('company_name', array('required'=>$this->lang->line('company_name_req')));
        $this->form_validator->add_rules('postal_code', array('required'=>$this->lang->line('postal_code_req')));
        //$this->form_validator->add_rules('vat_number', array('required'=>$this->lang->line('vat_number_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        //$this->form_validator->add_rules('template_id', array('required'=>$this->lang->line('template_id_req')));
        //$this->form_validator->add_rules('company_status', array('required'=>$this->lang->line('company_status_req')));
        $validated = $this->form_validator->validate($data);
        $error = '';
        if($validated != 1)
        {
            if($error!=''){ $validated = array_merge($error,$validated); }
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_customer'])) {
            $data['id_customer'] = pk_decrypt($data['id_customer']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['id_customer']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['template_id'])) {
            $data['template_id'] = pk_decrypt($data['template_id']);
            if(!in_array($data['template_id'],$this->session_user_master_templates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['country_id'])) {
            $data['country_id'] = pk_decrypt($data['country_id']);
            if(!in_array($data['country_id'],$this->session_user_master_countries)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $update_data = array(
            'id_customer' => $data['id_customer'],
            'company_name' => $data['company_name'],
            'company_address' => isset($data['company_address'])?$data['company_address']:'',
            'postal_code' => $data['postal_code'],
            'city' => isset($data['city'])?$data['city']:'',
            'vat_number' => isset($data['vat_number'])?$data['vat_number']:'',
            'country_id' => isset($data['country_id'])?$data['country_id']:'',
            'updated_by' => $data['created_by'],
            'updated_on' => currentDate(),
            'company_status' => ''
        );
        if(isset($data['company_status'])){ $update_data['company_status'] = $data['company_status']; }
        else{ unset($update_data['company_status']); }

        $customer_id = $data['id_customer'];
        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['customer']['name']['company_logo']))
        {
            $imageName = doUpload(array(
                'temp_name' => $_FILES['customer']['tmp_name']['company_logo'],
                'image' => $_FILES['customer']['name']['company_logo'],
                'upload_path' => $path,
                'folder' => $data['id_customer']));
            $update_data['company_logo'] = $imageName;

            imageResize($path.$imageName);
            /* getting previous image to delete*/
            $customer_data = $this->Customer_model->getCustomer(array('id_customer' => $data['id_customer']));
            if(!empty($customer_data)){
                deleteImage($customer_data[0]['company_logo']);
            }
        }
        else{
            unset($update_data['company_logo']);
        }
        if(isset($data['is_delete_logo']) && $data['is_delete_logo'] == 1){
            $update_data['company_logo'] = null;
        }

        $this->Customer_model->updateCustomer($update_data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('customer_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function adminList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('customer_id', array('required' => $this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $data = tableOptions($data);
        $result = $this->Customer_model->getCustomerAdminList($data);
        foreach($result['data'] as $k=>$v){
            $result['data'][$k]['id_user']=pk_encrypt($v['id_user']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function admin_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data

        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('customer_id', array('required'=> $this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['user_id'])) $data['user_id']=pk_decrypt($data['user_id']);
        if(isset($data['customer_id'])) $data['customer_id']=pk_decrypt($data['customer_id']);
        if(isset($data['user_role_id'])) $data['user_role_id']=pk_decrypt($data['user_role_id']);
        if($this->session_user_info->user_role_id!=1){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['user_role_id'] = 2;
        $result = $this->User_model->getUserInfo($data);
        if(isset($result->id_user))
            $result->id_user=pk_encrypt($result->id_user);
        if(isset($result->customer_id))
            $result->customer_id=pk_encrypt($result->customer_id);
        if(isset($result->user_role_id))
            $result->user_role_id=pk_encrypt($result->user_role_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function admin_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $firstNameRules               = array(
            'required'=> $this->lang->line('first_name_req'),
            'max_len-100' => $this->lang->line('first_name_len'),
        );
        $lastNameRules               = array(
            'required'=> $this->lang->line('last_name_req'),
            'max_len-100' => $this->lang->line('last_name_len'),
        );
        $emailRules = array(
            'required'=> $this->lang->line('email_req'),
            'valid_email' => $this->lang->line('email_invalid')
        );
        $is_manual_passwordRules = array(
            'required'=> $this->lang->line('is_manual_password_req')
        );
        $passwordRules               = array(
            'required'=> $this->lang->line('password_req'),
            'min_len-8' => $this->lang->line('password_num_min_len'),
            'max_len-20' => $this->lang->line('password_num_max_len'),
        );
        if($this->session_user_info->user_role_id!=1){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('customer_id', array('required' => $this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('first_name', $firstNameRules);
        $this->form_validator->add_rules('last_name', $lastNameRules);
        $this->form_validator->add_rules('email', $emailRules);
        if(!isset($data['id_user'])) {
            $this->form_validator->add_rules('is_manual_password', $is_manual_passwordRules);
            if(isset($data['is_manual_password']) && $data['is_manual_password']==1){
                $this->form_validator->add_rules('password', $passwordRules);
            }
        }
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])){
            $data['customer_id']=pk_decrypt($data['customer_id']);
            if(!in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if(!in_array($data['id_user'],$this->session_user_master_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $user_id=0;
        if(isset($data['id_user'])){ $user_id = $data['id_user']; }
        /*checking for email uniqueness*/
        $email_check = $this->User_model->check_email(array('email' => $data['email'],'id' => $user_id));
        $result = $email_check;

        if(!empty($email_check)){
            $result = array('status'=>FALSE,'error'=>array('email' => $this->lang->line('email_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['id_user'])) {
            if($data['is_manual_password']==1){
                $password = $data['password'];
            }
            else if($data['is_manual_password']==0){
                $password = generatePassword(8);//helper function for generating password
            }

            $user_data = array(
                'user_role_id' => 2,
                'customer_id' => $data['customer_id'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => md5($password),
                'gender' => isset($data['gender']) ? $data['gender'] : '',
                'language_id' => 1,
                'created_by' => $data['created_by'],
                'created_on' => currentDate(),
                'user_status' => 1
            );

            $this->User_model->createUser($user_data);
            /*getting data for mail --start */
            $customer_name = $user_role_name = '';
            $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $data['customer_id']));
            $user_role_details = $this->User_model->getUserRole(array('user_role_id' => 2));

            if(!empty($user_role_details)){ $user_role_name = $user_role_details[0]['user_role_name']; }
            if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }

            /*sending mail for newly created admin*/
            /*$message = str_replace(array('{first_name}','{last_name}','{role}','{customer_name}','{email}','{password}'),array($data['first_name'],$data['last_name'],$user_role_name,$customer_name,$data['email'],$password),$this->lang->line('customer_admin_create_message'));
            $template_data = array(
                'web_base_url' => WEB_BASE_URL,
                'message' => $message,
                'mail_footer' => $this->lang->line('mail_footer')
            );
            $subject = $this->lang->line('customer_admin_create_subject');
            $template_data = $this->parser->parse('templates/notification.html',$template_data);
            sendmail($data['email'],$subject,$template_data);

            $msg = $this->lang->line('customer_admin_add');*/
            $user_info = $this->User_model->check_email(array('email' => $data['email']));
            $msg = $this->lang->line('customer_admin_add');
            $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $user_info->customer_id));
            /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
            $cust_admin = $cust_admin['data'][0];*/
            $cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
            $cust_admin = $cust_admin['data'][0];
            //echo 'cust_detail'.'<pre>';print_r($customer_details);exit;
            if($customer_details[0]['company_logo']=='') {
                $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
                /*$result->customer_logo_small = getImageUrl($customer_details[0]['company_logo'], 'company');
                $result->customer_logo = getImageUrl($customer_details[0]['company_logo'], 'company');*/
            }
            else{
                $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);
                /*$result->customer_logo_small = getImageUrl($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);
                $result->customer_logo = getImageUrl($customer_details[0]['company_logo'], 'profile');*/
            }

            $user_info = $this->User_model->getUserInfo(array('user_id' => $user_info->id_user));
            $user_role_name = $this->User_model->getUserRole(array('user_role_id' => $user_info->user_role_id));
            $template_configurations=$this->Customer_model->EmailTemplateList(array('customer_id' => $user_info->customer_id,'language_id' =>1,'module_key'=>'USER_CREATION'));
            //echo 'cust_detail'.'<pre>';print_r($user_info);exit;
            if($template_configurations['total_records']>0){
                $template_configurations=$template_configurations['data'][0];
                $wildcards=$template_configurations['wildcards'];
                $wildcards_replaces=array();
                $wildcards_replaces['first_name']=$user_info->first_name;
                $wildcards_replaces['last_name']=$user_info->last_name;
                $wildcards_replaces['customer_name']=$customer_details[0]['company_name'];
                $wildcards_replaces['logo']=$customer_logo;
                $wildcards_replaces['email']=$user_info->email;
                $wildcards_replaces['role']=$user_role_name[0]['user_role_name'];
                $wildcards_replaces['url']=WEB_BASE_URL.'html';
                $wildcards_replaces['password']=$password;
                $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                /*$from_name=SEND_GRID_FROM_NAME;
                $from=SEND_GRID_FROM_EMAIL;
                $from_name=$cust_admin['name'];
                $from=$cust_admin['email'];*/
                $from_name=$template_configurations['email_from_name'];
                $from=$template_configurations['email_from'];
                $to=$user_info->email;
                $to_name=$user_info->first_name.' '.$user_info->last_name;
                $mailer_data['mail_from_name']=$from_name;
                $mailer_data['mail_to_name']=$to_name;
                $mailer_data['mail_to_user_id']=$user_info->id_user;
                $mailer_data['mail_from']=$from;
                $mailer_data['mail_to']=$to;
                $mailer_data['mail_subject']=$subject;
                $mailer_data['mail_message']=$body;
                $mailer_data['status']=0;
                $mailer_data['send_date']=currentDate();
                $mailer_data['is_cron']=0;
                $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                $mailer_id=$this->Customer_model->addMailer($mailer_data);
                if($mailer_data['is_cron']==0) {
                    //$mail_sent_status=sendmail($to, $subject, $body, $from);
                    $this->load->library('sendgridlibrary');
                    $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                    if($mail_sent_status==1)
                        $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                }
            }
        }
        else{
            $user_data = array(
                'customer_id' => $data['customer_id'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'gender' => isset($data['gender']) ? $data['gender'] : '',
                'language_id' => 1,
                'updated_by' => $data['created_by'],
                'updated_on' => currentDate(),
                'user_status' => $data['user_status']
            );

            $this->User_model->updateUser($user_data,$data['id_user']);
            $msg = $this->lang->line('customer_admin_update');
        }

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function admin_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($this->session_user_info->user_role_id!=1){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if(!in_array($data['id_user'],$this->session_user_master_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $this->User_model->updateUser(array('user_status' => 0),$data['id_user']);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('customer_admin_inactive'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function userList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('customer_id', array('required' => $this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('user_role_id', array('required' => $this->lang->line('user_role_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->user_role_id!=$data['user_role_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id'])) {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['business_unit_id'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->id_user!=$data['id_user']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['current_user_not']))
            $data['current_user_not']=pk_decrypt($data['current_user_not']);
        if(isset($data['user_role_id']) && in_array($data['user_role_id'],array(3))){
            //3 means bu owner, can able to get own business unit users
            $user_id=isset($data['id_user'])?$data['id_user']:$this->user_id;
            $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $user_id,'status'=>1));
            $business_unit_id = array_map(function($i){ return $i['business_unit_id']; },$business_unit);
            $data['business_unit_array'] = $business_unit_id;
            $data['user_role_not'] = array(3);
        }

        $data = tableOptions($data);//helper function ordering smart table grid option
        $data['user_role_not']=array();
        if($data['user_role_id']==1){
            $data['user_role_not']=array(1);
        }
        if($data['user_role_id']==2){
            $data['user_role_not']=array(1,2);
        }
        if($data['user_role_id']==3){
            $data['user_role_not']=array(1,2,3,6);
        }
        if($data['user_role_id']==4){
            $data['user_role_not']=array(1,2,3,4,6);
        }
        if($data['user_role_id']==5){
            $data['user_role_not']=array(1,2,3,4,5,6);
        }
        if($data['user_role_id']==6){
            $data['user_role_not']=array(1,2,3,4,5,6);
        }
        $result = $this->Customer_model->getCustomerUserList($data);
        for($s=0;$s<count($result['data']);$s++)
        {
            $result['data'][$s]['id_user']=pk_encrypt($result['data'][$s]['id_user']);
            if($result['data'][$s]['bu_name']!='')
                $result['data'][$s]['bu_name'] = explode(',',$result['data'][$s]['bu_name']);
            if($result['data'][$s]['business_unit_id']!='')
                $result['data'][$s]['business_unit_id'] = explode(',',$result['data'][$s]['business_unit_id']);
            for($si=0;$si<count($result['data'][$s]['business_unit_id']);$si++){
                $result['data'][$s]['business_unit_id'][$si]=pk_encrypt($result['data'][$s]['business_unit_id'][$si]);
            }

        }
        //echo $this->db->last_query(); exit;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function user_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data

        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('customer_id', array('required'=> $this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['user_id'])) {
            $data['user_id'] = pk_decrypt($data['user_id']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['user_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id']))
            $data['user_role_id']=pk_decrypt($data['user_role_id']);
        $data['user_role_id_not'] = array(1,2);
        $result = $this->User_model->getUserInfo($data);
        $result->business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $result->id_user,'status' =>1));
        if(isset($result->id_user))
            $result->id_user=pk_encrypt($result->id_user);
        if(isset($result->customer_id))
            $result->customer_id=pk_encrypt($result->customer_id);
        if(isset($result->user_role_id))
            $result->user_role_id=pk_encrypt($result->user_role_id);
        foreach($result->business_unit as $k=>$v){
            $result->business_unit[$k]['business_unit_id']=pk_encrypt($result->business_unit[$k]['business_unit_id']);
            $result->business_unit[$k]['country_id']=pk_encrypt($result->business_unit[$k]['country_id']);
            $result->business_unit[$k]['created_by']=pk_encrypt($result->business_unit[$k]['created_by']);
            $result->business_unit[$k]['customer_id']=pk_encrypt($result->business_unit[$k]['customer_id']);
            $result->business_unit[$k]['id_business_unit']=pk_encrypt($result->business_unit[$k]['id_business_unit']);
            $result->business_unit[$k]['id_business_unit_user']=pk_encrypt($result->business_unit[$k]['id_business_unit_user']);
            $result->business_unit[$k]['updated_by']=pk_encrypt($result->business_unit[$k]['updated_by']);
            $result->business_unit[$k]['user_id']=pk_encrypt($result->business_unit[$k]['user_id']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function user_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $firstNameRules               = array(
            'required'=> $this->lang->line('first_name_req'),
            'max_len-100' => $this->lang->line('first_name_len'),
        );
        $lastNameRules               = array(
            'required'=> $this->lang->line('last_name_req'),
            'max_len-100' => $this->lang->line('last_name_len'),
        );
        $emailRules = array(
            'required'=> $this->lang->line('email_req'),
            'valid_email' => $this->lang->line('email_invalid')
        );
        $is_manual_passwordRules = array(
            'required'=> $this->lang->line('is_manual_password_req')
        );
        $passwordRules               = array(
            'required'=> $this->lang->line('password_req'),
            'min_len-8' => $this->lang->line('password_num_min_len'),
            'max_len-20' => $this->lang->line('password_num_max_len'),
        );

        $this->form_validator->add_rules('customer_id', array('required' => $this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('first_name', $firstNameRules);
        $this->form_validator->add_rules('last_name', $lastNameRules);
        $this->form_validator->add_rules('email', $emailRules);
        $this->form_validator->add_rules('user_role_id', array('required' => $this->lang->line('user_role_id_req')));
        if(!isset($data['id_user'])) {
            $this->form_validator->add_rules('is_manual_password', $is_manual_passwordRules);
            if(isset($data['is_manual_password']) && $data['is_manual_password']==1){
                $this->form_validator->add_rules('password', $passwordRules);
            }
        }
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])){
            $data['customer_id']=pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['id_user'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if(!in_array($data['user_role_id'],$this->session_user_master_user_roles)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit'])) {
            foreach($data['business_unit'] as $k=>$v){
                $data['business_unit'][$k]=pk_decrypt($v);
                if($this->session_user_info->user_role_id!=1 && !in_array($data['business_unit'][$k],$this->session_user_business_units)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
        }
        $user_id = 0;
        if(isset($data['id_user'])){ $user_id = $data['id_user']; }
        $email_check = $this->User_model->check_email(array('email' => $data['email'],'id' => $user_id));
        if(!empty($email_check)){
            $result = array('status'=>FALSE,'error'=>array('email' => $this->lang->line('email_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($data['user_role_id']!=6)
            $data['is_allow_all_bu']=0;

        if(!isset($data['id_user'])) {
            if($data['is_manual_password']==1){
                $password = $data['password'];
            }
            else if($data['is_manual_password']==0){
                $password = generatePassword(8);//helper function for generating password
            }
            $user_data = array(
                'user_role_id' => $data['user_role_id'],
                'customer_id' => $data['customer_id'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => md5($password),
                'gender' => isset($data['gender']) ? $data['gender'] : '',
                'language_id' => 1,
                'created_by' => $data['created_by'],
                'created_on' => currentDate(),
                'user_status' => 1,
                'is_allow_all_bu' => isset($data['is_allow_all_bu'])?$data['is_allow_all_bu']:0
            );
            $user_id = $this->User_model->createUser($user_data);

            $customer_name = $user_role_name = '';
            $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $data['customer_id']));
            /*$cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
            $cust_admin = $cust_admin['data'][0];*/
            if($customer_details[0]['company_logo']=='') {
                $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
                /*$result->customer_logo_small = getImageUrl($customer_details[0]['company_logo'], 'company');
                $result->customer_logo = getImageUrl($customer_details[0]['company_logo'], 'company');*/
            }
            else{
                $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);
                /*$result->customer_logo_small = getImageUrl($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);
                $result->customer_logo = getImageUrl($customer_details[0]['company_logo'], 'profile');*/
            }
            $user_role_details = $this->User_model->getUserRole(array('user_role_id' => 2));
            $user_role_details_user = $this->User_model->getUserRole(array('user_role_id' => $data['user_role_id']));
            if(!empty($user_role_details)){ $user_role_name = $user_role_details[0]['user_role_name']; }
            if(!empty($user_role_details_user)){ $user_role_name_user = $user_role_details_user[0]['user_role_name']; }
            if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }

            /*sending mail for newly created customer user*/
            /*$message = str_replace(array('{first_name}','{last_name}','{role}','{customer_name}','{email}','{password}'),array($data['first_name'],$data['last_name'],$user_role_name,$customer_name,$data['email'],$password),$this->lang->line('customer_user_create_message'));
            $template_data = array(
                'web_base_url' => WEB_BASE_URL,
                'message' => $message,
                'mail_footer' => $this->lang->line('mail_footer')
            );
            $subject = $this->lang->line('customer_user_create_subject');
            $template_data = $this->parser->parse('templates/notification.html',$template_data);
            sendmail($data['email'],$subject,$template_data);*/


            $template_configurations=$this->Customer_model->EmailTemplateList(array('customer_id' => $data['customer_id'],'language_id' =>1,'module_key'=>'USER_CREATION'));
            if($template_configurations['total_records']>0){
                $template_configurations=$template_configurations['data'][0];
                $wildcards=$template_configurations['wildcards'];
                $wildcards_replaces=array();
                $wildcards_replaces['first_name']=$data['first_name'];
                $wildcards_replaces['last_name']=$data['last_name'];
                $wildcards_replaces['customer_name']=$customer_name;
                $wildcards_replaces['logo']=$customer_logo;
                $wildcards_replaces['email']=$data['email'];
                $wildcards_replaces['role']=$user_role_name_user;
                $wildcards_replaces['password']=$password;
                $wildcards_replaces['url']=WEB_BASE_URL.'html';
                $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
                $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
                /*$from_name=SEND_GRID_FROM_NAME;
                $from=SEND_GRID_FROM_EMAIL;
                $from_name=$cust_admin['name'];
                $from=$cust_admin['email'];*/
                $from_name=$template_configurations['email_from_name'];
                $from=$template_configurations['email_from'];
                $to=$data['email'];
                $to_name=$data['first_name'].' '.$data['last_name'];
                $mailer_data['mail_from_name']=$from_name;
                $mailer_data['mail_to_name']=$to_name;
                $mailer_data['mail_to_user_id']=$user_id;
                $mailer_data['mail_from']=$from;
                $mailer_data['mail_to']=$to;
                $mailer_data['mail_subject']=$subject;
                $mailer_data['mail_message']=$body;
                $mailer_data['status']=0;
                $mailer_data['send_date']=currentDate();
                $mailer_data['is_cron']=0;//0-immediate mail,1-through cron job
                $mailer_data['email_template_id']=$template_configurations['id_email_template'];
                //echo '<pre>';print_r($customer_logo);exit;
                $mailer_id=$this->Customer_model->addMailer($mailer_data);
                if($mailer_data['is_cron']==0) {
                    //$mail_sent_status=sendmail($to, $subject, $body, $from);
                    $this->load->library('sendgridlibrary');
                    $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                    if($mail_sent_status==1)
                        $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
                }
            }

            $msg = $this->lang->line('user_add');
        }
        else{
            $user_data = array(
                'user_role_id' => $data['user_role_id'],
                'customer_id' => $data['customer_id'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'gender' => isset($data['gender']) ? $data['gender'] : '',
                'language_id' => 1,
                'updated_by' => $data['created_by'],
                'updated_on' => currentDate(),
                'user_status' => $data['user_status'],
                'is_allow_all_bu' => isset($data['is_allow_all_bu'])?$data['is_allow_all_bu']:0
            );
            $user_id = $data['id_user'];
            $this->User_model->updateUser($user_data,$data['id_user']);
            $msg = $this->lang->line('user_update');
        }

        //mapping user to business unit
        if(isset($data['business_unit'])) {
            $previous_data = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $user_id));
            $previous_data = array_map(function ($i) {
                return $i['business_unit_id'];
            }, $previous_data);

            $add = array_values(array_diff($data['business_unit'], $previous_data));
            $delete = array_values(array_diff($previous_data, $data['business_unit']));

            if (!empty($add)) {
                for ($s = 0; $s < count($add); $s++) {
                    if($add[$s]!='') {
                        $business_unit_user[] = array(
                            'business_unit_id' => $add[$s],
                            'user_id' => $user_id,
                            'created_by' => $data['created_by'],
                            'created_on' => currentDate(),
                        );
                    }
                }

                if (!empty($business_unit_user)) {
                    $this->Business_unit_model->mapBusinessUnitUser($business_unit_user);
                }
            }
            $this->Business_unit_model->updateBusinessUnitUser(array(
                'user_id' => $user_id,
                'status' => 1
            ));
            if (!empty($delete)) {
                for ($s = 0; $s < count($delete); $s++) {
                    if($delete[$s]!='') {
                        $this->Business_unit_model->updateBusinessUnitUser(array(
                            'business_unit_id' => $delete[$s],
                            'user_id' => $user_id,
                            'status' => 0
                        ));
                    }
                }
            }
        }

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function user_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['id_user'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $this->User_model->updateUser(array('user_status' => 0),$data['id_user']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('customer_user_inactive'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function resetPassword_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $passwordRules               = array(
            'required'=> $this->lang->line('password_req'),
            'min_len-8' => $this->lang->line('password_num_min_len'),
            'max_len-12' => $this->lang->line('password_num_max_len'),
        );
        $confirmPasswordRules        = array(
            'required'=>$this->lang->line('confirm_password_req'),
            'match_field-password'=>$this->lang->line('password_match')
        );

        $this->form_validator->add_rules('customer_id', array('required' => $this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('user_id', array('required' => $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('password', $passwordRules);
        $this->form_validator->add_rules('cpassword', $confirmPasswordRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['user_id'])) {
            $data['user_id'] = pk_decrypt($data['user_id']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['user_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $customer_details = $this->Customer_model->getCustomer(array('id_customer' => $data['customer_id']));
        $cust_admin = $this->Customer_model->getCustomerAdminList(array('customer_id' => $customer_details[0]['id_customer']));
        $cust_admin = $cust_admin['data'][0];
        if($customer_details[0]['company_logo']=='') {
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'company');
            /*$result->customer_logo_small = getImageUrl($customer_details[0]['company_logo'], 'company');
            $result->customer_logo = getImageUrl($customer_details[0]['company_logo'], 'company');*/
        }
        else{
            $customer_logo = getImageUrlSendEmail($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);
            /*$result->customer_logo_small = getImageUrl($customer_details[0]['company_logo'], 'profile', SMALL_IMAGE);
            $result->customer_logo = getImageUrl($customer_details[0]['company_logo'], 'profile');*/
        }
        if(!empty($customer_details)){ $customer_name = $customer_details[0]['company_name']; }
        $this->User_model->changePassword(array('user_id' => $data['user_id'],'password' => $data['password']));

        /*mail content --start */
        $user_info = $this->User_model->getUserInfo(array('user_id' => $data['user_id']));
        /*$message = str_replace(array('{first_name}','{last_name}','{password}'),array($user_info->first_name,$user_info->last_name,$data['password']),$this->lang->line('reset_password_mail'));
        $template_data = array(
            'web_base_url' => WEB_BASE_URL,
            'message' => $message,
            'mail_footer' => $this->lang->line('mail_footer')
        );
        $subject = $this->lang->line('reset_password_subject');
        $template_data = $this->parser->parse('templates/notification.html',$template_data);
        sendmail($user_info->email,$subject,$template_data);*/

        $template_configurations=$this->Customer_model->EmailTemplateList(array('customer_id' => $data['customer_id'],'language_id' =>1,'module_key'=>'RESET_PASSWORD'));
        if($template_configurations['total_records']>0){
            $template_configurations=$template_configurations['data'][0];
            $wildcards=$template_configurations['wildcards'];
            $wildcards_replaces=array();
            $wildcards_replaces['first_name']=$user_info->first_name;
            $wildcards_replaces['last_name']=$user_info->last_name;
            $wildcards_replaces['customer_name']=$customer_name;
            $wildcards_replaces['logo']=$customer_logo;
            $wildcards_replaces['email']=$user_info->email;
            $wildcards_replaces['role']=$user_info->user_role_name;
            $wildcards_replaces['password']=$data['password'];
            $wildcards_replaces['url']=WEB_BASE_URL.'html';
            $body = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_content']);
            $subject = wildcardreplace($wildcards,$wildcards_replaces,$template_configurations['template_subject']);
            /*$from_name=SEND_GRID_FROM_NAME;
            $from=SEND_GRID_FROM_EMAIL;
            $from_name=$cust_admin['name'];
            $from=$cust_admin['email'];*/
            $from_name=$template_configurations['email_from_name'];
            $from=$template_configurations['email_from'];
            $to=$user_info->email;
            $to_name=$user_info->first_name.' '.$user_info->last_name;
            $mailer_data['mail_from_name']=$from_name;
            $mailer_data['mail_to_name']=$to_name;
            $mailer_data['mail_to_user_id']=$user_info->id_user;
            $mailer_data['mail_from']=$from;
            $mailer_data['mail_to']=$to;
            $mailer_data['mail_subject']=$subject;
            $mailer_data['mail_message']=$body;
            $mailer_data['status']=0;
            $mailer_data['send_date']=currentDate();
            $mailer_data['is_cron']=0;
            $mailer_data['email_template_id']=$template_configurations['id_email_template'];
            $mailer_id=$this->Customer_model->addMailer($mailer_data);
            if($mailer_data['is_cron']==0) {
                //$mail_sent_status=sendmail($to, $subject, $body, $from);
                $this->load->library('sendgridlibrary');
                $mail_sent_status=$this->sendgridlibrary->sendemail($from_name,$from,$subject,$body,$to_name,$to,array(),$mailer_id);
                if($mail_sent_status==1)
                    $this->Customer_model->updateMailer(array('status'=>1,'mailer_id'=>$mailer_id));
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('password_changed'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function delete_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_customer', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_customer'])) {
            $data['id_customer'] = pk_decrypt($data['id_customer']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['id_customer']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['id_customer'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $this->Customer_model->updateCustomer(array('id_customer' => $data['id_customer'],'company_status' => 0));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('customer_inactive'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function calender_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('month', array('required'=>$this->lang->line('month_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        unset($data['date']);
        $data['status'] = 1;
        $result = $this->Customer_model->getCalender($data);
        foreach($result as $k=>$v){
            $result[$k]['customer_id']=pk_encrypt($v['customer_id']);
            $result[$k]['id_calender']=pk_encrypt($v['id_calender']);
            $result[$k]['relationship_category_id']=pk_encrypt($v['relationship_category_id']);
            $result[$k]['created_by']=pk_encrypt($v['created_by']);
            $result[$k]['updated_by']=pk_encrypt($v['updated_by']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function calenderYearView_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('year', array('required'=>$this->lang->line('year_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $data['status'] = 1;
        $result = $this->Customer_model->getYearCalender($data);
        $relationships = $this->Relationship_category_model->getRelationshipCategory(array('customer_id'=>$data['customer_id']));
        $output = array();
        foreach($result as $arg)
        {
            $output[$arg['month_id']][] = $arg;
            $rel_cat[$arg['relationship_category_id']] = $arg['relationship_category_id'];
        }

        foreach($output as $k=>$v){//changing 0,1 indexes to month and relationship id's
            foreach ($v as $k1=>$v1) {
                $output[$k][$v1['relationship_category_id']]=$v1;

                unset($output[$k][$k1]);
            }
        }
        $final_result=array();
        foreach($output as $k=>$v){
            foreach($v as $k1=>$v1){

                foreach($relationships as $r=>$s){

                    if($k1==$s['relationship_category_id']){

                        $final_result[$k][pk_encrypt($s['relationship_category_id'])]=$v1;
                        $final_result[$k][pk_encrypt($s['relationship_category_id'])]['month_id']=pk_encrypt($v1['month_id']);
                        $final_result[$k][pk_encrypt($s['relationship_category_id'])]['relationship_category_id']=pk_encrypt($v1['relationship_category_id']);
                    }
                    else{
                        if(!isset($final_result[$k][pk_encrypt($s['relationship_category_id'])]))
                        {
                            $final_result[$k][pk_encrypt($s['relationship_category_id'])]=array('relationship_category_id'=>pk_encrypt($s['relationship_category_id']),
                                'relationship_category_name'=>$s['relationship_category_name'],
                                'month'=>date("F", mktime(0, 0, 0, $k, 10)),
                                'month_id'=>pk_encrypt($k),
                                'relationship_category_quadrant'=>'',
                                'relationship_count'=>0);
                        }
                    }
                }
            }
        }


        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$final_result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function calender_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('date', array('required'=>$this->lang->line('date_req')));
        $this->form_validator->add_rules('relationship_category_id', array('required'=>$this->lang->line('relationship_category_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['relationship_category_id']) && is_array($data['relationship_category_id'])) {
            foreach($data['relationship_category_id'] as $k=>$v){
                $data['relationship_category_id'][$k]=pk_decrypt($v);
                if($this->session_user_info->user_role_id!=1 && !in_array($data['relationship_category_id'][$k],$this->session_user_customer_relationship_categories)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
                if($this->session_user_info->user_role_id==1 && !in_array($data['relationship_category_id'][$k],$this->session_user_wadmin_relationship_categories)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
        }
        $conflicts=array();
        if(isset($data['relationship_category_id']) && is_array($data['relationship_category_id'])) {
            $existing_records = $this->Customer_model->getCalender(array('customer_id' => $data['customer_id'], 'date' => $data['date']));
            $existing_records_inactive = $this->Customer_model->getCalender(array('customer_id' => $data['customer_id'], 'date' => $data['date'],'status'=>0));
            $existing_records_relationship_id = array_map(function ($i) {
                return $i['relationship_category_id'];
            }, $existing_records);
            $existing_records_relationship_inactive_id = array_map(function ($i) {
                return $i['relationship_category_id'];
            }, $existing_records_inactive);
            $data['new_relationship_category_id'] = array_values(array_diff($data['relationship_category_id'], $existing_records_relationship_id));
            $data['new_relationship_category_id_tobeactive'] = array_values(array_intersect($data['relationship_category_id'], $existing_records_relationship_inactive_id));
            $data['old_relationship_category_id'] = array_values(array_diff($existing_records_relationship_id, $data['relationship_category_id']));
            $add = array();
            $update = array();

            //$this->Customer_model->updateCalender(array('date' => $data['date'], 'status' => 1));

            for ($s = 0; $s < count($data['new_relationship_category_id']); $s++) {
                $alreadyExist=$this->Customer_model->checkAlreadyExist(array('relationship_category_id'=>$data['new_relationship_category_id'][$s],'date'=>$data['date']));
                if(count($alreadyExist)==0) {
                    $add[] = array(
                        'customer_id' => $data['customer_id'],
                        'date' => $data['date'],
                        'relationship_category_id' => $data['new_relationship_category_id'][$s],
                        'created_by' => $data['created_by'],
                        'created_on' => currentDate()
                    );
                }
                else{
                    foreach($alreadyExist as $k=>$v){
                        //$conflicts[]=array('category'=>$v['relationship_category_name'],'date'=>$v['date']);
                        $conflicts[]=$v['relationship_category_name'];
                    }

                }
            }
            for ($s = 0; $s < count($data['new_relationship_category_id_tobeactive']); $s++) {
                $alreadyExist=$this->Customer_model->checkAlreadyExist(array('relationship_category_id'=>$data['new_relationship_category_id_tobeactive'][$s],'date'=>$data['date']));
                if(count($alreadyExist)==0) {
                    /*$this->Customer_model->updateCalenderByCategory(array(
                        'customer_id' => $data['customer_id'],
                        'date' => $data['date'],
                        'relationship_category_id' => $data['new_relationship_category_id_tobeactive'][$s],
                        'status' => 1,
                        'updated_by' => $data['created_by'],
                        'updated_on' => currentDate()
                    ));*/
                    $update[]=array(
                        'customer_id' => $data['customer_id'],
                        'date' => $data['date'],
                        'relationship_category_id' => $data['new_relationship_category_id_tobeactive'][$s],
                        'status' => 1,
                        'updated_by' => $data['created_by'],
                        'updated_on' => currentDate()
                    );
                }
                else{
                    foreach($alreadyExist as $k=>$v){
                        //$conflicts[]=array('category'=>$v['relationship_category_name'],'date'=>$v['date']);
                        $conflicts[]=$v['relationship_category_name'];
                    }

                }
            }
            for ($s = 0; $s < count($data['old_relationship_category_id']); $s++) {
                /*$this->Customer_model->updateCalenderByCategory(array(
                    'customer_id' => $data['customer_id'],
                    'date' => $data['date'],
                    'relationship_category_id' => $data['old_relationship_category_id'][$s],
                    'status' => 0,
                    'updated_by' => $data['created_by'],
                    'updated_on' => currentDate()
                ));*/
                $update[]=array(
                    'customer_id' => $data['customer_id'],
                    'date' => $data['date'],
                    'relationship_category_id' => $data['old_relationship_category_id'][$s],
                    'status' => 0,
                    'updated_by' => $data['created_by'],
                    'updated_on' => currentDate()
                );
            }
            if(count($conflicts)==0) {
                if (!empty($add))
                    $this->Customer_model->addCalender($add);
                if (!empty($update)){
                    foreach($update as $ku=>$vu){
                        $this->Customer_model->updateCalenderByCategory($vu);
                    }
                }
            }
            else{
                $result = array('status'=>FALSE,'message'=>implode(',',array_unique($conflicts)).' has a conflict.','data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else{
            $this->Customer_model->updateCalender(array('date' => $data['date'], 'status' => 0));
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_save'), 'data'=>array('warning'=>implode(',',$conflicts)));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function calender_delete()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_calender', array('required'=>$this->lang->line('calender_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_calender'])) {
            $data['id_calender'] = pk_decrypt($data['id_calender']);
            if(!in_array($data['id_calender'],$this->session_user_customer_calenders)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $this->Customer_model->updateCalender(array('id_calender' => $data['id_calender'],'status' =>1));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('relationship_category_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function relationshipCategoryRemainder_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $result = $this->Customer_model->getRelationshipCategoryRemainder($data);
        foreach($result as $k=>$v){
            $result[$k]['customer_id']=pk_encrypt($v['customer_id']);
            $result[$k]['id_relationship_category']=pk_encrypt($v['id_relationship_category']);
            $result[$k]['id_relationship_category_remainder']=pk_encrypt($v['id_relationship_category_remainder']);
            $result[$k]['relationship_category_id']=pk_encrypt($v['relationship_category_id']);
            $result[$k]['updated_by']=pk_encrypt($v['updated_by']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function relationshipCategoryRemainder_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        //$this->form_validator->add_rules('days', array('required'=>$this->lang->line('days_req')));
        $this->form_validator->add_rules('relationship_category_id', array('required'=>$this->lang->line('relationship_category_id_req')));
        $this->form_validator->add_rules('updated_by', array('required'=>$this->lang->line('updated_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['updated_by'])) {
            $data['updated_by'] = pk_decrypt($data['updated_by']);
            if($data['updated_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }


        if(isset($data['relationship_category_id']) && is_array($data['relationship_category_id'])) {
            foreach($data['relationship_category_id'] as $k=>$v){
                $data['relationship_category_id'][$k]['id']=pk_decrypt($v['id']);
                if($this->session_user_info->user_role_id!=1 && !in_array($data['relationship_category_id'][$k]['id'],$this->session_user_customer_relationship_categories)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
                if($this->session_user_info->user_role_id==1 && !in_array($data['relationship_category_id'][$k]['id'],$this->session_user_wadmin_relationship_categories)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
        }
        $existing_categories = $this->Customer_model->getRelationshipCategoryRemainder(array('customer_id' => $data['customer_id']));
        $add = $update = array();
        for($s=0;$s<count($data['relationship_category_id']);$s++)
        {
            for($sr=0;$sr<count($existing_categories);$sr++)
            {
                if($data['relationship_category_id'][$s]['id']==$existing_categories[$sr]['id_relationship_category'] && $existing_categories[$sr]['id_relationship_category_remainder']!=''){
                    $update[] = array(
                        'id_relationship_category_remainder' => $existing_categories[$sr]['id_relationship_category_remainder'],
                        'days' => $data['relationship_category_id'][$s]['days'],
                        'r2_days' => $data['relationship_category_id'][$s]['r2_days'],
                        'r3_days' => $data['relationship_category_id'][$s]['r3_days'],
                        'updated_by' => $data['updated_by'],
                        'updated_on' => currentDate()
                    );
                }
                else if($data['relationship_category_id'][$s]['id']==$existing_categories[$sr]['id_relationship_category']){
                    $add[] = array(
                        'customer_id' => $data['customer_id'],
                        'days' => $data['relationship_category_id'][$s]['days'],
                        'r2_days' => $data['relationship_category_id'][$s]['r2_days'],
                        'r3_days' => $data['relationship_category_id'][$s]['r3_days'],
                        'relationship_category_id' => $data['relationship_category_id'][$s]['id'],
                        'updated_by' => $data['updated_by'],
                        'updated_on' => currentDate()
                    );
                }
            }
        }

        if(!empty($add))
            $this->Customer_model->addRelationshipRemainder($add);

        if(!empty($update))
            $this->Customer_model->updateRelationshipRemainder($update);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_save'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function dashboard_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(isset($data['delegate_id'])) {
            $data['delegate_id'] = pk_decrypt($data['delegate_id']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['delegate_id'],$this->session_user_delegates)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_owner_id'])) {
            $data['contract_owner_id'] = pk_decrypt($data['contract_owner_id']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['contract_owner_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['contract_id'])) {
            $data['contract_id'] = pk_decrypt($data['id_contract']);
            if(!in_array($data['contract_id'],$this->session_user_contracts)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['responsible_user_id'])) {
            $data['responsible_user_id'] = pk_decrypt($data['responsible_user_id']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['responsible_user_id'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['created_by'])) {
            $data['created_by'] = pk_decrypt($data['created_by']);
            if($data['created_by']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['business_unit_id']) && !is_array($data['business_unit_id'])) {
            $data['business_unit_id'] = pk_decrypt($data['business_unit_id']);
            if(!in_array($data['business_unit_id'],$this->session_user_business_units)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $data['business_unit_id']=array();
        if(isset($data['user_role_id']) && isset($data['id_user'])){
            if(in_array($data['user_role_id'],array(3))){
                $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $data['id_user'],'status' => '1'));
                $data['business_unit_id'] = array_map(function($i){ return $i['id_business_unit']; },$business_unit);
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            /*if($data['user_role_id']==3){
                $data['contract_owner_id'] = $data['id_user'];
            }*/
            if($data['user_role_id']==4){
                $data['delegate_id'] = $data['id_user'];
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
                if($this->session_user_info->user_role_id!=1 && !in_array($data['delegate_id'],$this->session_user_delegates)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['user_role_id']==5){
                $data['customer_user'] = $data['id_user'];
                if($this->session_user_info->user_role_id!=1 && !in_array($data['customer_user'],$this->session_user_contributors)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            if($data['user_role_id']==6){
                $data['business_unit_id'] = $this->session_user_business_units;
            }
        }

        $result_array = array();

        $result_array['contracts_count'] = $this->Contract_model->getContractListCount($data);
        $data['contract_status'] = 'pending review,review in progress';
        /*$pending_reviews = $this->Contract_model->getContractList($data);
        $result_array['contracts'] = $pending_reviews;*/
        $result_array['to_be_reviewed'] = $this->Contract_model->getContractListCount($data);
        $data['contract_status'] = 'pending review';
        $result_array['pending_reviews'] = $this->Contract_model->getContractListCount($data);

        unset($data['business_unit_id']);
        unset($data['delegate_id']);
        unset($data['responsible_user_id']);
        unset($data['customer_user']);
        $data['business_unit_id']=array();
        if(isset($data['user_role_id']) && isset($data['id_user'])){
            if($data['user_role_id']==2){
                unset($data['id_user']);
            }
            else if($data['user_role_id']==3){
                $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $data['id_user'],'status' => '1'));
                $data['business_unit_id'] = array_map(function($i){ return $i['id_business_unit']; },$business_unit);
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
                unset($data['id_user']);
            }
            else if($data['user_role_id']==4) {
                /*$data['delegate_id'] = $data['id_user']; //commented on 7/6/18 reg. action items
                if($this->session_user_info->user_role_id!=1 && !in_array($data['delegate_id'],$this->session_user_delegates)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }*/
                $data['session_user_role']=$this->session_user_info->user_role_id;
                $data['session_user_id']=$this->session_user_id;
            }
            else if($data['user_role_id']==5) {
                $data['responsible_user_id'] = $data['id_user'];
                $data['created_by'] = $data['id_user'];
                if($this->session_user_info->user_role_id!=1 && !in_array($data['created_by'],$this->session_user_contributors)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
                if($this->session_user_info->user_role_id!=1 && !in_array($data['responsible_user_id'],$this->session_user_contributors)){
                    $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            else if($data['user_role_id']==6){
                $data['business_unit_id'] = $this->session_user_business_units;
            }
        }
        /*$action_items = $this->Contract_model->getActionItems($data);
        $result_array['action_items'] = $action_items;*/
        unset($data['contract_status']);
        $data['contract_review_action_item_status']='open';
        $data['item_status']=1;
        //unset($data['responsible_user_id']);
        $result_array['action_items_count'] = $this->Contract_model->getActionItemsCount($data);
        $user_role_id_not=array();
        if($data['user_role_id']==1){
            $user_role_id_not=array(1);
        }
        if($data['user_role_id']==2){
            $user_role_id_not=array(1,2);
        }
        if($data['user_role_id']==3){
            $user_role_id_not=array(1,2,3,6);
        }
        if($data['user_role_id']==4){
            $user_role_id_not=array(1,2,3,4,6);
        }
        if($data['user_role_id']==5){
            $user_role_id_not=array(1,2,3,4,5,6);
        }
        if($data['user_role_id']==6){
            $user_role_id_not=array(1,2);
        }

        $result_array['co_workers'] = $this->Customer_model->getUserCount(array('customer_id' => $data['customer_id'],'user_role_id_not' => $user_role_id_not,'business_unit_array'=>$data['business_unit_id']));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result_array);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function testemailtemplate_post(){

        //type = preview,testmail
        $data = $this->input->post();
        //echo '<pre>';print_r($data);exit;
        //$wildcards_replaces = json_decode($data['wildcards'],true);
        //$wildcards=json_encode(array_keys($wildcards_replaces));
        $wildcards_replaces=$wildcards=array();
        /*echo '<pre>';print_r($wildcards_replaces);
        echo '<pre>';print_r($wildcards);*/
        //$data['content']=EMAIL_HEADER_CONTENT.$data['content'].EMAIL_FOOTER_CONTENT;
        //$body = wildcardreplace($wildcards, $wildcards_replaces, $data['content']);
        //$subject = wildcardreplace($wildcards, $wildcards_replaces, $data['subject']);
        $body=$data['content'];
        $subject=$data['subject'];
        $from_name = SEND_GRID_FROM_NAME;
        $from = SEND_GRID_FROM_EMAIL;
        $to = $data['to_email'];
        $to_name = $data['to_name'];

        if($data['type']=='testmail'){

            $this->load->library('sendgridlibrary');
            $this->sendgridlibrary->sendemail($from_name, $from, $subject, $body, $to_name, $to, array());
        }

        $result_array = array('body'=>$body,'subject'=>$subject,'from_name'=>$from_name,'from'=>$from,'to'=>$to,'to_name'=>$to_name);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result_array);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    function getDirectorySize($path)
    {
        $totalsize = 0;
        $totalcount = 0;
        $dircount = 0;
        if(is_dir($path)) {
            if ($handle = opendir($path)) {
                while (false !== ($file = readdir($handle))) {
                    $nextpath = $path . '/' . $file;
                    if ($file != '.' && $file != '..' && !is_link($nextpath)) {
                        if (is_dir($nextpath)) {
                            $dircount++;
                            $result = $this->getDirectorySize($nextpath);
                            $totalsize += $result;

                        } elseif (is_file($nextpath)) {
                            $totalsize += filesize($nextpath);
                            $totalcount++;
                        }
                    }
                }
                closedir($handle);
            }

        }
        //$total['size'] =     $this->sizeFormat($totalsize);

        return $totalsize;
    }

    function sizeFormat($size)
    {
        if($size<1024)
        {
            return $size." bytes";
        }
        else if($size<(1024*1024))
        {
            $size=round($size/1024,1);
            return $size." KB";
        }
        else if($size<(1024*1024*1024))
        {
            $size=round($size/(1024*1024),1);
            return $size." MB";
        }
        else
        {
            $size=round($size/(1024*1024*1024),1);
            return $size." GB";
        }

    }
    public function userListHistory_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('customer_id', array('required' => $this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('user_role_id', array('required' => $this->lang->line('user_role_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user_role_id'])) {
            $data['user_role_id'] = pk_decrypt($data['user_role_id']);
            if($data['user_role_id']!=$this->session_user_info->user_role_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['current_user_not'])) $data['current_user_not']=pk_decrypt($data['current_user_not']);
        if(isset($data['user_role_id']) && in_array($data['user_role_id'],array(3))){
            //3 means bu owner, can able to get own business unit users
            $user_id=isset($data['id_user'])?$data['id_user']:$this->user_id;
            $business_unit = $this->Business_unit_model->getBusinessUnitUser(array('user_id' => $user_id,'status'=>1));
            $business_unit_id = array_map(function($i){ return $i['business_unit_id']; },$business_unit);
            $data['business_unit_array'] = $business_unit_id;
            $data['user_role_not'] = array(3);
        }

        $data = tableOptions($data);//helper function ordering smart table grid option
        $data['user_role_not']=array();
        if($data['user_role_id']==1){
            $data['user_role_not']=array(1);
        }
        if($data['user_role_id']==2){
            $data['user_role_not']=array(1);
        }
        if($data['user_role_id']==3){
            $data['user_role_not']=array(1,2,3,6);
        }
        if($data['user_role_id']==4){
            $data['user_role_not']=array(1,2,3,4,6);
        }
        if($data['user_role_id']==5){
            $data['user_role_not']=array(1,2,3,4,5,6);
        }
        if($data['user_role_id']==6){
            $data['user_role_not']=array(1,2,3,4,5,6);
        }
        $result = $this->Customer_model->getCustomerUserListHistory($data);
        for($s=0;$s<count($result['data']);$s++)
        {
            if($result['data'][$s]['bu_name']!='')
                $result['data'][$s]['bu_name'] = explode(',',$result['data'][$s]['bu_name']);
            if($result['data'][$s]['business_unit_id']!='') {
                $result['data'][$s]['business_unit_id'] = explode(',', $result['data'][$s]['business_unit_id']);
                foreach($result['data'][$s]['business_unit_id'] as $k=>$v){
                    $result['data'][$s]['business_unit_id'][$k]=pk_encrypt($v);
                }
            }

                $result['data'][$s]['id_user'] = pk_encrypt($result['data'][$s]['id_user']);

        }
        //echo $this->db->last_query(); exit;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function userHistory_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_user', array('required' => $this->lang->line('id_user_req')));
        $this->form_validator->add_rules('type', array('required' => $this->lang->line('id_user_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_user'])){
            $data['id_user']=pk_decrypt($data['id_user']);
            if($this->session_user_info->user_role_id!=1 && !in_array($data['id_user'],$this->session_user_customer_all_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && !in_array($data['id_user'],$this->session_user_master_users)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if($data['type']=='detail'){
            $this->form_validator->add_rules('from_date', array('required' => $this->lang->line('from_date_req')));
            $this->form_validator->add_rules('to_date', array('required' => $this->lang->line('to_date_req')));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
            $data = tableOptions($data);//helper function ordering smart table grid option
            $result['history'] = $this->Customer_model->getUserLoginHistory($data);
            foreach($result['history']['data'] as $k=>$v){
                $result['history']['data'][$k]['id_user']=pk_encrypt($result['history']['data'][$k]['id_user']);
            }
            $result['user_info'] = $this->User_model->getUserInfo(array('user_id'=>$data['id_user']));
            $result['user_info']->customer_id=pk_encrypt($result['user_info']->customer_id);
            $result['user_info']->id_user=pk_encrypt($result['user_info']->id_user);
            $result['user_info']->user_role_id=pk_encrypt($result['user_info']->user_role_id);


        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function listCustomers_get(){
        $data = $this->input->get();
        if($this->session_user_info->user_role_id!=1){
            $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        /*helper function for ordering smart table grid options*/
        $data = tableOptions($data);
        $result = $this->Customer_model->customerList($data);
        for($s=0;$s<count($result['data']);$s++)
        {
            /*getImageUrl helper function for getting image usrl*/
            $result['data'][$s]['company_logo'] = getImageUrl($result['data'][$s]['company_logo'],'company',SMALL_IMAGE);
            $result['data'][$s]['users_count']=$this->User_model->getUserCount(array('customer_id'=>$result['data'][$s]['id_customer']));
            $result['data'][$s]['bu_owners_count']=$this->User_model->getUserCount(array('customer_id'=>$result['data'][$s]['id_customer'],'role'=>3));
            $result['data'][$s]['bu_delegates_count']=$this->User_model->getUserCount(array('customer_id'=>$result['data'][$s]['id_customer'],'role'=>4));
            $result['data'][$s]['contributors_count']=$this->User_model->getUserCount(array('customer_id'=>$result['data'][$s]['id_customer'],'role'=>5));
            $result['data'][$s]['reporting_owners_count']=$this->User_model->getUserCount(array('customer_id'=>$result['data'][$s]['id_customer'],'role'=>6));
            $business_unit = $this->Business_unit_model->getBusinessUnitList(array('customer_id'=>$result['data'][$s]['id_customer']));
            $result['data'][$s]['business_unit_count']=$business_unit['total_records'];
            $path = FCPATH.'uploads/'.$result['data'][$s]['id_customer'];
            $path_storage=$this->getDirectorySize($path);
            $path_new = FILE_SYSTEM_PATH.'uploads/'.$result['data'][$s]['id_customer'];
            $path_new_storage=$this->getDirectorySize($path_new);
            $result['data'][$s]['storage']['size'] = $this->sizeFormat($path_storage+$path_new_storage);
            $result['data'][$s]['country_id']=pk_encrypt($result['data'][$s]['country_id']);
            $result['data'][$s]['created_by']=pk_encrypt($result['data'][$s]['created_by']);
            $result['data'][$s]['id_customer']=pk_encrypt($result['data'][$s]['id_customer']);
            $result['data'][$s]['template_id']=pk_encrypt($result['data'][$s]['template_id']);
            $result['data'][$s]['updated_by']=pk_encrypt($result['data'][$s]['updated_by']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function actionList_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('access_token', array('required'=>$this->lang->line('access_token_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = tableOptions($data);
        $result = $this->User_model->getActionList($data);
        foreach($result['data'] as $k=>$v){
            $result['data'][$k]['acting_user_id']=pk_encrypt($v['acting_user_id']);
            $result['data'][$k]['id']=pk_encrypt($v['id']);
            $result['data'][$k]['id_access_log']=pk_encrypt($v['id_access_log']);
            $result['data'][$k]['user_id']=pk_encrypt($v['user_id']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function insertEmailTemplatesManually_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('created_by', array('required'=>$this->lang->line('created_by_req')));
        $this->form_validator->add_rules('id_customer', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['created_by'])) $data['created_by']=pk_decrypt($data['created_by']);
        if(isset($data['id_customer']) && $data['id_customer']!='all') $data['id_customer']=pk_decrypt($data['id_customer']);
        $email_template = $this->Customer_model->EmailTemplateList(array('customer_id' => 0,'language_id' =>1,'status'=>'0,1'));
        $email_template = $email_template['data'];
        if($data['id_customer']=='all') {
            $customers = $this->Customer_model->customerList(array());
            $customers=$customers['data'];
        }
        else{
            $customers[0]['id_customer']=$data['id_customer'];

        }
        for($s=0;$s<count($email_template);$s++)
        {
            foreach($customers as $kc=>$vc) {
                $check_email_template_already_exist = $this->Customer_model->EmailTemplateList(array('customer_id' => $vc['id_customer'], 'language_id' => 1, 'status' => '0,1', 'parent_email_template_id' => $email_template[$s]['id_email_template']));
                $check_email_template_already_exist = $check_email_template_already_exist['total_records'];
                if ($check_email_template_already_exist == 0) {
                    $inserted_id = $this->Customer_model->addEmailTemplate(array(
                        'module_name' => $email_template[$s]['module_name'],
                        'module_key' => $email_template[$s]['module_key'],
                        'wildcards' => $email_template[$s]['wildcards'],
                        'email_from_name' => $email_template[$s]['email_from_name'],
                        'email_from' => $email_template[$s]['email_from'],
                        'status' => $email_template[$s]['status'],
                        'parent_email_template_id' => $email_template[$s]['id_email_template'],
                        'customer_id' => $vc['id_customer'],
                        'created_by' => $data['created_by'],
                        'created_on' => currentDate()
                    ));

                    $this->Customer_model->addEmailTemplateLanguage(array(
                        'email_template_id' => $inserted_id,
                        'template_name' => $email_template[$s]['template_name'],
                        'template_subject' => $email_template[$s]['template_subject'],
                        'template_content' => $email_template[$s]['template_content'],
                        'language_id' => $email_template[$s]['language_id']
                    ));
                }
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function dailyupdates_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('date', array('required'=>$this->lang->line('date_req')));
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        //$this->form_validator->add_rules('to_date', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $result = $this->Customer_model->getDailyUpdates($data);
        foreach($result as $k=>$v){
            $result[$k]['id_daily_update_customer']=pk_encrypt($v['id_daily_update_customer']);
            $result[$k]['customer_id']=pk_encrypt($v['customer_id']);
        }
        $this->Customer_model->updatedailynotificationcount($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function addprovider_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('email', array('required'=>$this->lang->line('email_req')));
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $this->form_validator->add_rules('provider_name', array('required'=>$this->lang->line('provider_name_req')));
        $this->form_validator->add_rules('division', array('required'=>$this->lang->line('division_req')));
        $this->form_validator->add_rules('country_id', array('required'=>$this->lang->line('country_id_req')));
        $this->form_validator->add_rules('address', array('required'=>$this->lang->line('address_req')));
        $this->form_validator->add_rules('contact_no', array('required'=>$this->lang->line('contact_no_req')));
        $this->form_validator->add_rules('description', array('required'=>$this->lang->line('description_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['country_id'])) {
            $data['country_id'] = pk_decrypt($data['country_id']);
            if(!in_array($data['country_id'],$this->session_user_master_countries)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $add_data = array(
            'provider_name' => $data['provider_name'],
            'address' => $data['address'],
            'email' => $data['email'],
            'contact_no' => $data['contact_no'],
            'division' => $data['division'],
            'country_id' => $data['country_id'],
            'customer_id' => $data['customer_id'],
            'description' => $data['description'],
            'status' => 1,
            'created_on' => currentDate(),
            'created_by' => $data['id_user']
        );

        $insert_id = $this->Customer_model->addprovider($add_data);
        if($insert_id){
            $result = array('status'=>TRUE, 'message' => $this->lang->line('provider_add'), 'data'=>' ');
            $this->response($result, REST_Controller::HTTP_OK);
        }else{
            $result = array('status'=>FALSE, 'message' => '', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function updateprovider_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_provider', array('required'=>$this->lang->line('provider_id_req')));
        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $this->form_validator->add_rules('email', array('required'=>$this->lang->line('email_req')));
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $this->form_validator->add_rules('division', array('required'=>$this->lang->line('division_req')));
        $this->form_validator->add_rules('country_id', array('required'=>$this->lang->line('country_id_req')));
        $this->form_validator->add_rules('provider_name', array('required'=>$this->lang->line('provider_name_req')));
        $this->form_validator->add_rules('address', array('required'=>$this->lang->line('address_req')));
        $this->form_validator->add_rules('contact_no', array('required'=>$this->lang->line('contact_no_req')));
        $this->form_validator->add_rules('description', array('required'=>$this->lang->line('description_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>' ');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_provider'])) {
            $data['id_provider'] = pk_decrypt($data['id_provider']);
            if(!in_array($data['id_provider'],$this->session_user_customer_providers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['country_id'])) {
            $data['country_id'] = pk_decrypt($data['country_id']);
            if(!in_array($data['country_id'],$this->session_user_master_countries)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $update_data = array(
            'provider_name' => $data['provider_name'],
            'address' => $data['address'],
            'email' => $data['email'],
            'contact_no' => $data['contact_no'],
            'division' => $data['division'],
            'country_id' => $data['country_id'],
            'customer_id' => $data['customer_id'],
            'description' => $data['description'],
            'status' => $data['status'],
            'updated_on' => currentDate(),
            'updated_by' => $data['id_user']
        );

        $update = $this->Customer_model->updateprovider($update_data,$data['id_provider']);

        if($update){
            $result = array('status'=>TRUE, 'message' => $this->lang->line('provider_update'), 'data'=>' ');
            $this->response($result, REST_Controller::HTTP_OK);
        }else{
            $result = array('status'=>FALSE, 'message' => '', 'data'=>' ');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function provider_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('customer_id', array('required'=>$this->lang->line('customer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['customer_id'])) {
            $data['customer_id'] = pk_decrypt($data['customer_id']);
            if($this->session_user_info->user_role_id!=1 && $this->session_user_info->customer_id!=$data['customer_id']){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($this->session_user_info->user_role_id==1 && $data['customer_id']!='' && $data['customer_id']>0 && !in_array($data['customer_id'],$this->session_user_master_customers)){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        $data = tableOptions($data);
        $result = $this->Customer_model->getproviderlist($data);
        foreach($result['data'] as $k=>$v){
            $result['data'][$k]['id_provider']=pk_encrypt($v['id_provider']);
            $result['data'][$k]['customer_id']=pk_encrypt($v['customer_id']);
            $result['data'][$k]['country_id']=pk_encrypt($v['country_id']);
            $result['data'][$k]['created_by']=pk_encrypt($v['created_by']);
            $result['data'][$k]['updated_by']=pk_encrypt($v['updated_by']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);

    }
    public function dailynotificationcount_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $data = tableOptions($data);
        $result = $this->Customer_model->dailynotificationcount($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function notification_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $data = tableOptions($data);
        $result = $this->Customer_model->dailyNotificationList($data);
        foreach($result['data'] as $k=>$v){
            $content = $v['content'];
            $content = json_decode($content, true);
            $result['data'][$k]['content'] = $content;
            $result['data'][$k]['mailer_id'] = pk_encrypt($v['mailer_id']);
            $result['data'][$k]['mail_to_user_id'] = pk_encrypt($v['mail_to_user_id']);
            $result['data'][$k]['email_template_id'] = pk_encrypt($v['email_template_id']);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function dailynotificationcount_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_user', array('required'=>$this->lang->line('user_id_req')));
        $this->form_validator->add_rules('date', array('required'=>$this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_user'])) {
            $data['id_user'] = pk_decrypt($data['id_user']);
            if($data['id_user']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $data = tableOptions($data);
        $result = $this->Customer_model->updatedailynotificationcount($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function checkAD_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('email_id', array('required'=> $this->lang->line('email_req')));
        $this->form_validator->add_rules('password', array('required'=> $this->lang->line('password_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $params=array('host'=>$data['host'],'port'=>$data['port'],'dc'=>$data['dc']);
        $this->load->library('LdapAuthentication',$params);
        $is_login=$this->ldapauthentication->login($data['email_id'],$data['password']);
        if($is_login['status']===true){
            $result = array('status'=>TRUE,'message'=>$this->lang->line('success'),'data'=>'');
        }
        else{
            $result = array('status'=>FALSE,'message'=>$is_login['message'],'data'=>'');
        }
        $this->response($result, REST_Controller::HTTP_OK);

    }
}