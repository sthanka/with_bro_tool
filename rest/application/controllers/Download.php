<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/mailer/mailer.php';

class Download extends CI_Controller
{
    public $session_user_id=NULL;
    public function __construct()
    {
        parent::__construct();
        if(isset($_SERVER['HTTP_USER'])){
            $this->user_id = pk_decrypt($_SERVER['HTTP_USER']);
        }
        $this->load->model('Download_model');
        $this->load->model('Validation_model');
        //$getLoggedUserId=$this->User_model->getLoggedUserId();
        //$this->session_user_id=$getLoggedUserId[0]['id'];
        //$this->session_user_id=!empty($this->session->userdata('session_user_id_acting'))?($this->session->userdata('session_user_id_acting')):($this->session->userdata('session_user_id'));

    }

    /*function downloadReport($file='') {
        if(DATA_ENCRYPT)
        {
            $aesObj = new AES();
            $_GET['path']=rawurldecode(urlencode($_GET['path']));
            $_GET['name']=rawurldecode(urlencode($_GET['name']));
            $_GET['path'] = $aesObj->decrypt($_GET['path'],AES_KEY);
            $_GET['name'] = $aesObj->decrypt($_GET['name'],AES_KEY);
        }
        $file=$_GET['path'];
        $file =str_replace(REST_API_URL,"./",$_GET['path']);
        $file_name=$_GET['name'];
        //echo basename($file);exit;
        //$file ='./uploads/38/Schermafbeelding_2017-05-11_om_15_1494582538_1495601217.png';
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            if (readfile($file))
            {
            }

    }*/
    function downloadReportNew(){
        /*if(DATA_ENCRYPT)
        {
            $aesObj = new AES();
            $_GET['user_id'] = $aesObj->decrypt($_GET['user_id'],AES_KEY);
            $_GET['id_download'] = $aesObj->decrypt($_GET['id_download'],AES_KEY);
            $_GET['access_token'] = $aesObj->decrypt($_GET['access_token'],AES_KEY);
            $_GET['id_download'] = $aesObj->decrypt($_GET['id_download'],AES_KEY);
        }*/
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            echo json_encode($result);exit;
        }
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('id_download', array('required'=> $this->lang->line('document_id_req')));
        $this->form_validator->add_rules('access_token', array('required'=> $this->lang->line('access_token_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            echo json_encode($result);exit;
        }
        if(isset($data['user_id'])) {
            //$data['user_id'] = pk_decrypt($data['user_id']);
            $data['user_id'] = pk_decrypt(rawurldecode(urlencode($data['user_id'])));
            /*if($data['user_id']!=$this->session_user_id){
                $result = array('status'=>FALSE, 'error' =>array('message'=>$this->lang->line('permission_not_allowed')), 'data'=>'');
                $this->load->view('errors/html/error_404',array('heading'=>'Not Allowed','message'=>'Not allowed to access.'));
            }*/
        }
        if(isset($data['id_download']))
            $data['id_download'] = pk_decrypt(rawurldecode(urlencode($data['id_download'])));


        $check=$this->Download_model->checkDownload(array('id_download'=>$data['id_download'],'user_id'=>$data['user_id'],'access_token'=>$data['access_token'],'status'=>0));
        if($check===false){
            $this->load->view('errors/html/error_404',array('heading'=>'Not Allowed','message'=>'Not allowed to access.'));
        }
        else{
            if(isset($check['id_download'])){
                $file=$check['path'];
                $file =FILE_SYSTEM_PATH.$check['path'];
                if(is_file($file) && !is_dir($file) && file_exists($file) && is_writable(dirname($file))){
                    $ext = pathinfo($file, PATHINFO_EXTENSION);
                    if(!in_array($ext,array('php'))){
                        $file_name=$check['filename'];
                        //echo basename($file);exit;
                        //$file ='./uploads/38/Schermafbeelding_2017-05-11_om_15_1494582538_1495601217.png';
                        $this->Download_model->updateDownload(array('id_download'=>$data['id_download'],'status'=>1));
                        header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
                        header('Content-Transfer-Encoding: binary');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize($file));
                        ob_clean();
                        flush();
                        if (readfile($file))
                        {
                        }
                    }
                    else{
                        $this->load->view('errors/html/error_404',array('heading'=>'File Not Found','message'=>'The file you requested are not found.'));
                    }
                }
                else{
                    $this->load->view('errors/html/error_404',array('heading'=>'File Not Found','message'=>'The file you requested are not found.'));
                }
            }
        }
    }
    /*function downloadReport($file='') {
        if(DATA_ENCRYPT)
        {
            $aesObj = new AES();
            $_GET['path']=rawurldecode(urlencode($_GET['path']));
            $_GET['name']=rawurldecode(urlencode($_GET['name']));
            $_GET['path'] = $aesObj->decrypt($_GET['path'],AES_KEY);
            $_GET['name'] = $aesObj->decrypt($_GET['name'],AES_KEY);
        }
        $file=$_GET['path'];
        $file =str_replace(REST_API_URL,"./",$_GET['path']);
        if(is_file($file) && !is_dir($file) && file_exists($file) && is_writable(dirname($file))){
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            if(!in_array($ext,array('php'))){
                $file_name=$_GET['name'];
                //echo basename($file);exit;
                //$file ='./uploads/38/Schermafbeelding_2017-05-11_om_15_1494582538_1495601217.png';
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                if (readfile($file))
                {
                }
            }
        }
    }*/
    function downloadFile($file='') {
        $file ='./uploads/inventory_invoice/'.$file;
        if ($file and file_exists($file))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
        }
        if (readfile($file))
        {

        }
    }
    function test(){
        $data=$this->input->get();

        $this->Customer_model->updatedailynotificationcount($data);
    }


}