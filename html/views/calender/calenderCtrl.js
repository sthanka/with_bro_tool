angular.module('app')
    .controller('calenderCtrl', function ($state, $rootScope, $scope, uiCalendarConfig, $window, $uibModal, relationCategoryService, calenderService, dateFilter, $location) {
        $rootScope.module = '';
        $rootScope.displayName = '';
        $scope.eventSources = [];
        $scope.categoryAdded = [];
        $scope.quadrantColor = {'Q1': '#DA4421', 'Q2': '#5bb166', 'Q3': '#ff9900', 'Q4': '#F00000'};
        $scope.relCat = {};
        $scope.selected = [];
        $scope.calenderData = [];
        var gobalParams = {};
        gobalParams.customer_id = $scope.user1.customer_id;
        relationCategoryService.list(gobalParams).then(function (result) {
            $scope.relationCategory = result.data.data;
            $scope.loadCalenderData();
        });
        $scope.loadCalenderData = function () {
            $scope.calenderData = [];
            var moment = $.fullCalendar.moment(new Date(), 'yyyy-MM-dd');    //Not required even at start stage
            if ($scope.currentSelectedMonthDateObj) {
                var moment = $.fullCalendar.moment($scope.currentSelectedMonthDateObj, 'yyyy-MM-dd');
            }
            gobalParams.date = moment.format('YYYY-MM-DD');
            gobalParams.month = moment.format('M');
            calenderService.get(gobalParams).then(function (result) {
                for (var a in result.data) {
                    if (!$scope.calenderData[result.data[a].date] && $scope.calenderData[result.data[a].date] == undefined) {
                        $scope.calenderData[result.data[a].date] = [];
                    }
                    if (!$scope.calenderData[result.data[a].date][result.data[a].relationship_category_id] && $scope.calenderData[result.data[a].date][result.data[a].relationship_category_id] == undefined) {
                        $scope.calenderData[result.data[a].date][result.data[a].relationship_category_id] = [];
                    }
                    $scope.calenderData[result.data[a].date][result.data[a].relationship_category_id].push(result.data[a].relationship_category_id);
                }
                for (var a in $scope.calenderData) {
                    $scope.addRelationCat($scope.relationCategory, Object.keys($scope.calenderData[a]), a);
                }
            });
        }

        $scope.currentYear = new Date();
        $scope.currentYear = $scope.currentYear.getFullYear();
        $scope.nextYear = $scope.currentYear + 1;

        $scope.uiConfig = {
            calendar: {
                editable: true,
                header: {
                    left: 'title',
                    center: '',
                    right: 'currentYear,nextYear,prev,today,next'
                },
                customButtons: {
                    currentYear: {
                        text: $scope.currentYear,
                        click: function() {
                            $state.go('app.calender.year',{year:$scope.currentYear});
                        }
                    },
                    nextYear: {
                        text: $scope.nextYear,
                        click: function() {
                            $state.go('app.calender.year',{year:$scope.nextYear});
                        }
                    }
                },
                dayClick: function (date, jsEvent, view) {
                    var check = $.fullCalendar.moment(date, 'yyyy-MM-dd');
                    var today = $.fullCalendar.moment(new Date(), 'yyyy-MM-dd');
                    if (check < today) {
                        $scope.openCategoryModal(date, 'old');
                    }
                    else {
                        $scope.openCategoryModal(date, 'future');
                    }
                },
                viewRender: function (view, element) {
                    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    var month1 = months.indexOf(view.title.trim().split(" ")[0]) + 1;
                    if (month1 > 9)month1 = month1;
                    else month1 = 0 + "" + month1;
                    $scope.currentSelectedMonthDateObj = view.title.split(" ")[1] + "-" + month1 + "-" + 0 + "" + 1;
                    $('#calendar').fullCalendar('option', 'height', ($window.innerHeight + 160));
                    $scope.loadCalenderData();
                }
            }
        };
        $scope.addRelationCat = function (relationCategory, data, item) {
            var html = '';
            var selected = [];
            /*$scope.selected = [];*/
            var element = angular.element('td.fc-day[data-date="' + item + '"]');
            element.html('');
            for (var a in relationCategory) {
                for (var b in data) {
                    if (data[b] && data[b] == relationCategory[a].id_relationship_category) {
                        selected.push(relationCategory[a].id_relationship_category);
                        html += '<p class="claender_style" style="background-color:' + $scope.quadrantColor[relationCategory[a].relationship_category_quadrant] + '">' +
                            '<span title="' + relationCategory[a].relationship_category_name + '" tooltip-append-to-body="true" class="display-block f11"' +
                            'uib-tooltip="' + relationCategory[a].relationship_category_name + '">' +
                            relationCategory[a].relationship_category_name +
                            '</span>' +
                            '</p>';
                    }
                }
            }
            $scope.selected[item] = [];
            $scope.selected[item] = selected;
            element.html(html);
            return selected;
        }
        $scope.openCategoryModal = function (date, tense) {
            $scope.relCat = {};
            $scope.tense = tense;
            $scope.selectedRow = date;
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'views/calender/addCategory.html',
                controller: function ($uibModalInstance, $scope, $filter, item, tense) {
                    $scope.tense = tense;
                    $scope.update = false;
                    $scope.title = 'general.create';
                    $scope.bottom = 'general.save';
                    $scope.isEdit = false;
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };

                    for (var a in $scope.selected[item.format()]) {
                        $scope.relCat[$scope.selected[item.format()][a]] = true;
                    }

                    $scope.setRelationCategory = function (data) {
                        var selected = [];
                        for (var a in data) {
                            if (data[a]) {
                                selected.push(a);
                            }
                        }
                        if (selected.length == 0) {
                            selected = 0;
                            var element = angular.element('td.fc-day[data-date="' + item.format() + '"]');
                            element.html('');
                            delete $scope.selected[item.format()];
                            delete $scope.relCat[$scope.selected[item.format()]];
                        }
                        var params = {};
                        params.customer_id = $scope.user1.customer_id;
                        params.date = item.format();
                        params.relationship_category_id = selected;
                        params.created_by = $scope.user.id_user;
                        calenderService.post(params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message, 'Success');
                                $scope.loadCalenderData();
                                var obj = {};
                                obj.action_name = 'update';
                                obj.action_description = 'update$$Calendar$$Relationship$$Category';
                                obj.module_type = $state.current.activeLink;
                                obj.action_url = $location.$$absUrl;
                                $rootScope.confirmNavigationForSubmit(obj);
                                $scope.cancel();
                            } else {
                                $rootScope.toast('Error', result.message, 'l-error');
                            }
                        });
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    },
                    tense: function () {
                        if ($scope.tense) {
                            return $scope.tense;
                        }
                    }
                }
            });
            modalInstance.result.then(function ($data) {
            }, function () {
            });
        };
        $scope.settingModel = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'views/calender/change-setting.html',
                controller: function ($uibModalInstance, $scope, $filter) {
                    $scope.update = false;
                    $scope.title = 'general.create';
                    $scope.bottom = 'general.save';
                    $scope.isEdit = false;
                    $scope.setting = {};
                    var params = {};
                    params.customer_id = $scope.user1.customer_id;
                    relationCategoryService.list(params).then(function (result) {
                        $scope.relationCategory = result.data.data;
                    });
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                    relationCategoryService.getSettingsData({'customer_id': $scope.user1.customer_id}).then(function (result) {
                        angular.forEach(result.data, function (i, o) {
                            $scope.setting[i.id_relationship_category] = [];
                            $scope.setting[i.id_relationship_category]['days'] = i.days;
                            $scope.setting[i.id_relationship_category]['r2_days'] = i.r2_days;
                            $scope.setting[i.id_relationship_category]['r3_days'] = i.r3_days;
                        });
                    });
                    $scope.addSetting = function (setting) {
                        var params = {};
                        params.updated_by = $scope.user.id_user;
                        params.customer_id = $scope.user1.customer_id;
                        params.relationship_category_id = [];
                        angular.forEach($scope.relationCategory, function (item, key) {
                            var obj = {};
                            obj.id = item.id_relationship_category;
                            obj.days = setting[item.id_relationship_category]['days'];
                            obj.r2_days = setting[item.id_relationship_category]['r2_days'];
                            obj.r3_days = setting[item.id_relationship_category]['r3_days'];
                            params.relationship_category_id[key] = obj;
                        });
                        relationCategoryService.updateSettings(params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                var obj = {};
                                obj.action_name = 'update';
                                obj.action_description = 'update$$Calendar$$reminders';
                                obj.module_type = $state.current.activeLink;
                                obj.action_url = $location.$$absUrl;
                                $rootScope.confirmNavigationForSubmit(obj);
                                $scope.cancel();
                            } else $rootScope.toast('Error', result.error, 'error');
                        });
                    }
                },
                resolve: {}
            });
            modalInstance.result.then(function ($data) {
            }, function () {
            });
        };
    })
    .controller('addCategoryCtrl', function ($state, $rootScope, $scope, uiCalendarConfig) {
    })
    .controller('fullcalenderCtrl', function ($state, $rootScope, $scope, uiCalendarConfig, calenderService) {
        $rootScope.module = 'Calendar';
        $rootScope.displayName = $state.params.year;
        $scope.currentYear = $state.params.year;
        $scope.monthsList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        $scope.quadrantColor = {'Q1': '#DA4421', 'Q2': '#5bb166', 'Q3': '#ff9900', 'Q4': '#F00000'};
        $scope.months = [];
        var params = {};
        params.customer_id = $scope.user1.customer_id;
        params.year = $scope.currentYear;
        calenderService.yearly(params).then(function (result) {
            for(var mon in $scope.monthsList){
                $scope.months[mon] = {};
                $scope.months[mon]['monthName'] = $scope.monthsList[mon];
                $scope.months[mon]['relation_category_name'] = [];
                $scope.months[mon]['relation_category_name'] = result.data[parseInt(mon)+1];
            }
            /*console.log(result.data);
            console.log(result.data.length);
            for(var mon in $scope.monthsList){
                if(result.data && result.data.length!=0){
                    for(var a in result.data){
                        console.log(parseInt(mon)+1);
                        console.log(a);
                        console.log(result.data[a]);
                        if((parseInt(mon)+1) == a){
                            console.log('1',$scope.monthsList[mon]);
                            $scope.months[mon] = {};
                            $scope.months[mon]['monthName'] = $scope.monthsList[mon];
                            $scope.months[mon]['relation_category_name'] = [];
                            $scope.months[mon]['relation_category_name'].push(result.data[a]);
                            console.log('final',$scope.months);
                        }else {
                            console.log('2',$scope.monthsList[mon]);
                            if($scope.months[mon] && $scope.months[mon]['monthName'] == undefined){
                                $scope.months[mon]['monthName'] = $scope.monthsList[mon];
                            }else{
                                $scope.months[mon] = {};
                                $scope.months[mon]['relation_category_name'] = [];
                            }
                        }
                    }
                } else {
                    console.log('3',$scope.monthsList[mon]);
                    if(typeof $scope.months[mon]['monthName' == undefined]){
                        $scope.months[mon]['monthName'] = $scope.monthsList[mon];
                    }else{
                        $scope.months[mon] = {};
                        $scope.months[mon]['relation_category_name'] = [];
                    }
                }
                console.log('final data',$scope.months);
            }*/
        });
    })