angular.module('app',['ng-fusioncharts'])
.controller('dashboardCtrl', function($scope, $rootScope, $state, $stateParams, dashboardService, contractService, encode, decode, $uibModal,$location,AuthService,userService){
    $scope.del=0;
    if($rootScope.access == 'ca' || $rootScope.access == 'bo'){
        $scope.del=1;
    }
    $scope.deleteContract = function (row) {
        var r=confirm("Do you want to continue?");
        if(r==true){
            var params = {};
            params.contract_id = row.id_contract;
            params.user_role_id = $scope.user1.user_role_id;
            params.id_user = $scope.user1.id_user;
            contractService.delete(params).then(function (result) {
                if(result.status){
                    var obj = {};
                    obj.action_name = 'Delete';
                    obj.action_description = 'contract delete $$('+result.data.file_name+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = location.href;
                    if(AuthService.getFields().data.parent){
                        obj.user_id = AuthService.getFields().data.parent.id_user;
                        obj.acting_user_id = AuthService.getFields().data.data.id_user;
                    }
                    else obj.user_id = AuthService.getFields().data.data.id_user;
                    if(AuthService.getFields().access_token != undefined){
                        var s = AuthService.getFields().access_token.split(' ');
                        obj.access_token = s[1];
                    }
                    else obj.access_token = '';                   
                    $rootScope.toast('Success',result.message);
                    if($state.current.name=="app.dashboard2")
                        $state.go('app.dashboard');
                    else $state.go('app.dashboard2');
                }
            });
        }        
    }  

    $rootScope.module = '';
    $rootScope.displayName = '';
    $scope.dashboardData = {};
    $scope.your_action_items = 0;
    $scope.widgetinfo = function(){
        var params ={};
        params.id_user = $scope.user1.id_user;
        params.user_role_id = $scope.user1.user_role_id;
        params.customer_id = $scope.user1.customer_id;
        dashboardService.info(params).then(function(result){
            $scope.dashboardData = result.data;
        })
    };
    $scope.widgetinfo();
    $scope.myDataSource = {};
    $scope.contractOverallDetails = function(tableState){
        var params ={};
        params.customer_id = $scope.user1.customer_id;
        params.id_user  = $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        params.contract_status  = 'pending review,review in progress';
        contractService.contractOverallDetails(params).then(function(result){
            if(result.status)
            $scope.myDataSource = result.data;
        });
    };
    $scope.contractOverallDetails('');

    $scope.getPendingReviews = function(tableState){
        $scope.tableStateRef = tableState;
        $scope.isLoading = true;
        var pagination = tableState.pagination;
        tableState.customer_id = $scope.user1.customer_id;
        tableState.business_unit_id = $scope.business_unit_id;
        tableState.id_user  = $scope.user1.id_user;
        tableState.user_role_id  = $scope.user1.user_role_id;
        tableState.contract_status  = 'pending review,review in progress';
        contractService.list(tableState).then(function(result){
            //$scope.contractOverallDetails(tableState);
            $scope.contractReviews = result.data.data;
            $scope.emptyTable=false;
            tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
            $scope.isLoading = false;
            if(result.data.total_records < 1)
                $scope.emptyTable=true;
        })
    }
    $scope.getActionItems = function(tableState1){
        $scope.isLoading1 = true;
        var pagination1 = tableState1.pagination;
        $scope.tableStateRef1 = tableState1;
        tableState1.id_user  = $scope.user1.id_user;
        tableState1.user_role_id  = $scope.user1.user_role_id;
        tableState1.customer_id = $scope.user1.customer_id;
        tableState1.contract_review_action_item_status = 'open';
        tableState1.page_type = 'dashboard';
        contractService.getAllActionItems(tableState1).then(function(result){
            $scope.actionItemsList = result.data.data;
            $scope.emptyTable1=false;
            $scope.your_action_items = result.data.total_records;
            tableState1.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState1.pagination.number);
            $scope.isLoading1 = false;
            if(result.data.total_records < 1)
                $scope.emptyTable1=true;
        })
    }
    $scope.getMyContracts = function(tableState){
        $scope.tableStateRef = tableState;
        $scope.isLoading = true;
        var pagination = tableState.pagination;
        tableState.customer_id = $scope.user1.customer_id;
        tableState.business_unit_id = $scope.business_unit_id;
        tableState.created_by  = $scope.user1.id_user;
        tableState.id_user  = $scope.user1.id_user;
        // tableState.user_role_id  = $scope.user1.user_role_id;
        contractService.list(tableState).then(function(result){
            //$scope.contractOverallDetails(tableState);
            $scope.myContract = result.data.data;
            $scope.myContractCount = result.data.total_records;
            $scope.emptyMyContractTable=false;
            tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
            $scope.isMyContractLoading = false;
            if(result.data.total_records < 1)
                $scope.emptyMyContractTable=true;
        })
    }
    $scope.getContributingToContracts = function(tableState){
        $scope.tableStateRef = tableState;
        $scope.isLoading = true;
        var pagination = tableState.pagination;
        tableState.customer_id = $scope.user1.customer_id;
        tableState.business_unit_id = $scope.business_unit_id;
        tableState.id_user  = $scope.user1.id_user;
        // tableState.user_role_id  = $scope.user1.user_role_id;
        tableState.customer_user  = $scope.user1.id_user;
        contractService.list(tableState).then(function(result){
            $scope.contributingToContract = result.data.data;
            $scope.contributingToContractCount = result.data.total_records;
            $scope.emptyContributingToTable=false;
            tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
            $scope.isContributingToLoading = false;
            if(result.data.total_records < 1)
                $scope.emptyContributingToTable=true;
        })
    }
    $scope.goToOverview = function(){
        $state.go('app.contract.contract-overview');
    }
    $scope.goToActionItems = function(){
        $state.go('app.actionItems',{status:encode('open')});
    }
    $scope.goToUsers = function(){
        $state.go('app.customer-user.list');
    }

    $scope.goToContractDashboard = function (row) {
        $state.go('app.contract.contract-dashboard',{name:row.contract_name,id:encode(row.id_contract)});
    }
    $scope.exportContractReview = function (row) {
        var params={};
        params.contract_id = params.id_contract= row.id_contract;
        params.id_user=  $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        contractService.exportReviewData(params).then(function(result){
            if(result.status){
                var obj = {};
                obj.action_name = 'export';
                obj.action_description = 'export$$contract$$review$$('+ row.contract_name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';
                $rootScope.toast('Success',result.message);
                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.file_path =  GibberishAES.enc(result.data.file_path, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file_name =  GibberishAES.enc(result.data.file_name, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.file_path+'&name='+result.data.file_name;
                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;

                    }
                });
            }else{$rootScope.toast('Error',result.error,'l-error');}
        })
    }
    $scope.goToContractReview = function(row){
        $state.go('app.contract.contract-review',{name:row.contract_name,id:encode(row.id_contract),rId:encode(row.id_contract_review)});
    }
    $scope.goToPendingContracts = function (){
        $state.go('app.contract.contract-overview',{'status' : 'pending review'});
    }
    $scope.updateContractReview = function (row, type) {
        $scope.review_access = true;
        $scope.type = type;
        $scope.isActionItem = true;
        $scope.selectedRow = row;
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'right-panel-modal modal-open',
            templateUrl: 'views/Manage-Users/contracts/create-edit-contract-review.html',
            controller: function ($uibModalInstance, $scope, item) {
                $scope.update = false;
                //$scope.bottom = 'general.save';
                $scope.isEdit = false;
                if (item) {
                    $scope.isEdit = true;
                    $scope.submitStatus = true;
                    //angular.copy(item,$scope.data);

                    $scope.data = angular.copy(item);
                    $scope.data.due_date = new Date($scope.data.due_date);
                    $scope.title = '';
                    $scope.update = true;
                    $scope.bottom = 'general.update';
                }
                if($scope.type == 'view'){
                    $scope.bottom = 'contract.finish';
                }
                if($scope.type == 'add'){
                    $scope.bottom = 'general.update';
                }
                var param ={};
                param.contract_id = row.contract_id;
                param.customer_id = $scope.user1.customer_id;
                param.user_role_id = $scope.user1.user_role_id;
                param.contract_review_id = row.contract_review_id;
                contractService.getActionItemResponsibleUsers(param).then(function(result){
                    $scope.userList = result.data;
                });
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
                $scope.goToEdit = function(data){
                    $scope.data.due_date = new Date(data.due_date);
                }
                var params ={};
                $scope.getActionItemById = function(id){
                    contractService.getActionItemDetails({'id_contract_review_action_item':id}).then(function(result){
                        $scope.data = result.data[0];
                    });
                }
                $scope.addReviewActionItem=function(data){
                    if($scope.type == 'view'){
                        params.id_contract_review_action_item = data.id_contract_review_action_item;
                        params.comments = data.comments;
                        params.is_finish = data.is_finish;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id  = row.contract_id;
                        if(params.is_finish == 1){
                            var r=confirm("Are you sure that you want to finish this action item ?");
                            $scope.deleConfirm = r;
                            if(r==true){
                                var obj = {};
                                obj.action_name = 'update';
                                obj.action_description = 'Finish Action Item$$('+data.action_item+')';
                                obj.module_type = $state.current.activeLink;
                                obj.action_url = $location.$$absUrl;
                                $rootScope.confirmNavigationForSubmit(obj);
                                contractService.reviewActionItemUpdate(params).then(function (result) {
                                    if (result.status) {
                                        $rootScope.toast('Success', result.message);
                                        $scope.getActionItems($scope.tableStateRef1);
                                        $scope.cancel();
                                    } else {
                                        $rootScope.toast('Error', result.error,'error');
                                    }
                                });
                            }
                        }else{
                            contractService.reviewActionItemUpdate(params).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $scope.getActionItemById(data.id_contract_review_action_item);
                                    $scope.getActionItems($scope.tableStateRef1);
                                    var obj = {};
                                    obj.action_name = 'save';
                                    obj.action_description = 'save Action Item$$('+data.action_item+')';
                                    obj.module_type = $state.current.activeLink;
                                    obj.action_url = $location.$$absUrl;
                                    $rootScope.confirmNavigationForSubmit(obj);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error,'error');
                                }
                            });
                        }
                    }
                    else if(data != 0 && data.hasOwnProperty('id_contract_review_action_item')){
                        delete data.comments;
                        params = data;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id = params.id_contract =  row.contract_id;
                        contractService.addReviewActionItemList(params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                $scope.getActionItemById(data.id_contract_review_action_item);
                                $scope.getActionItems($scope.tableStateRef1);
                                var obj = {};
                                obj.action_name = 'update';
                                obj.action_description = 'update Action Item$$('+data.action_item+')';
                                obj.module_type = $state.current.activeLink;
                                obj.action_url = $location.$$absUrl;
                                $rootScope.confirmNavigationForSubmit(obj);
                                $scope.type= 'view';
                                $scope.bottom='contract.finish'
                            } else {
                                $rootScope.toast('Error', result.error,'error');
                            }
                        });
                    }
                }
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
            },
            resolve: {
                item: function () {
                    if ($scope.selectedRow) {
                        return $scope.selectedRow;
                    }
                }
            }
        });
        modalInstance.result.then(function ($data) {
        }, function () {
        });
    }
    $scope.goToContratDiscussion = function(row){
        $state.go('app.contract.review-design',{name:row.contract_name,id:encode(row.id_contract),rId:encode(row.id_contract_review)});
    }
    $scope.createContract = function(row){
        $state.go('app.contract.create-contract');
    }
})