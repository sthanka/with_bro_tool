angular.module('app')
    .controller('loginCtrl',function($state,$rootScope,$scope,$localStorage,AuthService,$http,encode,userService,$location){
        $localStorage.curUser ={};
        $scope.menu ={};
        $scope.showBtn = false;
        $scope.showLoading = false;
        $scope.submitLogin = function(loginForm,user, type){
            if (loginForm.$valid) {
                $scope.showBtn = true;
                var param = {};
                param.password = encode(user.password);
                param.email_id = user.email_id;
                param.session_exceed = 0;
                param.login_with_ldap = type;
                $scope.userData = {};
                userService.post(param).then(function(result){
                    if (result.status) {
                        $rootScope.access = result.data.data.access;
                        $scope.userData = result;
                        $localStorage.curUser = $scope.userData;
                        $scope.showLoading = true;
                        if($rootScope.returnToState){
                            var state = $rootScope.returnToState;
                            var stateParam = $rootScope.returnToStateParams;
                            delete $rootScope.returnToState;
                            delete $rootScope.returnToStateParams;
                            if(stateParam.id)
                                $state.go(state,{id:stateParam.id});
                            else $state.go(state);
                        }
                        else{
                            $location.path('/');
                        }
                        //$location.path('/')
                        // $state.go('app.main');
                    }else {
                        if(result.error.message)$rootScope.toast('Error', result.error.message, 'l-error');
                        else{$rootScope.toast('Error', result.error, 'error');}
                        $scope.showLoading = false;
                        $scope.showBtn = false;
                    }
                });
            }
        }
        $scope.forgotPassword = function(user){
            var param = {};
            param = user;
            userService.forgotPassword(param).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $state.go('appSimple.login');
                }else{
                    $rootScope.toast('Error',result.error.email);
                }
            });
        }
    })