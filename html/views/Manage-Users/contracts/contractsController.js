angular.module('app',['ng-fusioncharts','localytics.directives'])
.controller('contractOverviewCtrl', function($scope, $rootScope, $state, encode, businessUnitService, contractService, AuthService,userService){
    $scope.bussinessUnit = {};
    var param ={};
    $scope.del=0;
    if($rootScope.access == 'ca' || $rootScope.access == 'bo'){
        $scope.del=1;
    }
    param.user_role_id=$rootScope.user_role_id;
    param.id_user=$rootScope.id_user;
    param.customer_id = $scope.user1.customer_id;
    param.status = 1;
    businessUnitService.list(param).then(function(result){
        /*result.data.data.push({'id_business_unit':'All', 'bu_name':'All'});
        $scope.bussinessUnit = result.data.data.reverse();*/
        $scope.bussinessUnit = result.data.data;
    });
    $scope.createContract = function(row){
        if(row)
            $state.go('app.contract.edit-contract',{name:row.contract_name,id:encode(row.id_contract)});
        else
            $state.go('app.contract.create-contract');
    }
    $scope.goToContractDashboard = function (row) {
        $state.go('app.contract.contract-dashboard',{name:row.contract_name,id:encode(row.id_contract)});
    }
    $scope.exportContractReview = function (row) {
        var params={};
        params.contract_id = params.id_contract= row.id_contract;
        params.id_user=  $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        contractService.exportReviewData(params).then(function(result){
            if(result.status){
                var obj = {};
                obj.action_name = 'export';
                obj.action_description = 'export$$contract$$review$$('+ row.contract_name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';
                $rootScope.toast('Success',result.message);
                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.file_path =  GibberishAES.enc(result.data.file_path, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file_name =  GibberishAES.enc(result.data.file_name, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.file_path+'&name='+result.data.file_name;
                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;

                    }
                });
            }else{$rootScope.toast('Error',result.error,'l-error');}
        })
    }
    $scope.getDownloadUrl = function(objData){
        var d = {};
        d.id_document = objData.id_document;
        contractService.getUrl(d).then(function (result) {
            if(result.status){
                var obj = {};
                obj.action_name = 'download';
                obj.action_description = 'download$$attachment$$('+ objData.document_name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';
                $rootScope.toast('Success',result.message);
                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.url =  GibberishAES.enc(result.data.url, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file =  GibberishAES.enc(result.data.file, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.url+'&name='+result.data.file;
                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;
                    }
                });
            }
        });
    };
})
.controller('contractListCtrl', function($scope, $rootScope, $state, $stateParams, contractService, businessUnitService, encode, $location,AuthService,userService){
    var param ={};
    $scope.del=0;
    if($rootScope.access == 'ca' || $rootScope.access == 'bo'){
        $scope.del=1;
    }
    param.user_role_id=$rootScope.user_role_id;
    param.id_user=$rootScope.id_user;
    param.customer_id = $scope.user1.customer_id;
    param.status = 1;
    businessUnitService.list(param).then(function(result){
        result.data.data.unshift({'id_business_unit':'All', 'bu_name':'All'});
        $scope.bussinessUnit = result.data.data;
    });
    $scope.getStatuses = function () {
        contractService.getContractStatus().then(function(result){
            if(result.status){
                var obj = {key:'all',value:'All'};
                result.data = result.data.reverse();
                result.data.push(obj);
                $scope.statusList = result.data.reverse();
            }
        })
    }
    $scope.getStatuses();
    $scope.contractsList = {};
    $scope.myDataSource = {};
    $scope.getProviderList = function(){
        $scope.provider_name = null;
        var params = {};
        if($scope.business_unit_id) {
            params.business_unit_id = $scope.business_unit_id;
        }
        params.customer_id = $scope.user1.customer_id;
        params.id_user  = $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        contractService.providerList(params).then(function(result){
            result.data.unshift({'provider_name':'All'});
            $scope.providerList = result.data;
        });
    };
    $scope.getProviderList();
    $scope.contractOverallDetails = function(data){
        var params = {};
        params.customer_id = $scope.user1.customer_id;
        params.id_user =  $scope.user1.id_user;
        params.user_role_id = $scope.user1.user_role_id;
        if(!angular.isUndefined(data.business_unit_id)) params.business_unit_id = data.business_unit_id;
        if(!angular.isUndefined(data.provider_name)) params.provider_name = data.provider_name;
        if(!angular.isUndefined(data.contract_status)) params.contract_status = data.contract_status;
        contractService.contractOverallDetails(params).then(function(result){
            $scope.myDataSource = result.data;
        });
    };
    //$scope.contractOverallDetails('');
    $scope.callServer = function (tableState){
        $scope.filtersData = {};
        $rootScope.module = '';
        $rootScope.displayName = '';
        $scope.tableStateRef = tableState;
        $scope.isLoading = true;
        var pagination = tableState.pagination;
        tableState.customer_id = $scope.user1.customer_id;
        tableState.business_unit_id = $scope.business_unit_id;
        tableState.id_user  = $scope.user1.id_user;
        tableState.user_role_id  = $scope.user1.user_role_id;
        if($scope.provider_name && $scope.provider_name != null){
            tableState.provider_name  = $scope.provider_name;
        }else{
            delete tableState.provider_name;
            $scope.provider_name = '';
        }
        if($scope.contract_status && $scope.contract_status != null){
            if($scope.contract_status !='all')
                tableState.contract_status  = $scope.contract_status;
            else delete tableState.contract_status;
        }else{
            delete tableState.contract_status;
            $scope.contract_status = '';
        }
        if($scope.contractType && $scope.contractType != null){
            if($scope.contractType =='my_contracts'){
                tableState.created_by  = $scope.user1.id_user;
            } else {
                delete tableState.created_by;
            }
            if($scope.contractType =='contributing_to'){
                tableState.customer_user  = $scope.user1.id_user;
            } else {
                delete tableState.customer_user;
            }
            delete tableState.user_role_id;
        }else{
            delete tableState.created_by;
            delete tableState.customer_user;
            tableState.user_role_id = $scope.user1.user_role_id;
        }
        $scope.filtersData.provider_name = tableState.provider_name;
        $scope.filtersData.business_unit_id = tableState.business_unit_id;
        $scope.filtersData.contract_status = tableState.contract_status;
        contractService.list(tableState).then (function(result){
            if($scope.filtersData && (tableState.pagination.start == 0)){
                $scope.contractOverallDetails($scope.filtersData);
            }else{}
            $scope.contractsList = result.data.data;
            $scope.emptyTable=false;
            tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
            $scope.isLoading = false;
            if(result.data.total_records < 1)
                $scope.emptyTable=true;
        })
    }
    $scope.goToContractDetails = function(row){
        $state.go('app.contract.view',{name:row.contract_name,id:encode(row.id_contract)});
    }
    $scope.goToDashboard = function (row) {
        $state.go('app.contract.contract-dashboard',{name:row.contract_name,id:encode(row.id_contract)});
    }
    $scope.exportContract = function (row) {
        var params={};
        params.contract_id = params.id_contract= row.id_contract;
        params.id_user=  $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        contractService.exportReviewData(params).then(function (result) {
            if(result.status){
                var obj = {};
                obj.action_name = 'export';
                obj.action_description = 'export$$contract$$('+row.contract_name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';
                $rootScope.toast('Success',result.message);
                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.file_path =  GibberishAES.enc(result.data.file_path, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file_name =  GibberishAES.enc(result.data.file_name, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.file_path+'&name='+result.data.file_name;
                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;
                    }
                });
            }
        });
    }
    $scope.goToContractReview = function(row){
        $state.go('app.contract.contract-review',{name:row.contract_name,id:encode(row.id_contract),rId:encode(row.id_contract_review)});
    }
    $scope.deleteContract = function (row) {
        var r=confirm("Do you want to continue?");
        if(r==true){
            var params = {};
            params.contract_id = row.id_contract;
            params.user_role_id = $scope.user1.user_role_id;
            params.id_user = $scope.user1.id_user;
            contractService.delete(params).then(function (result) {
                if(result.status){
                    var obj = {};
                    obj.action_name = 'Delete';
                    obj.action_description = 'contract delete $$('+result.data.file_name+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = location.href;
                    if(AuthService.getFields().data.parent){
                        obj.user_id = AuthService.getFields().data.parent.id_user;
                        obj.acting_user_id = AuthService.getFields().data.data.id_user;
                    }
                    else obj.user_id = AuthService.getFields().data.data.id_user;
                    if(AuthService.getFields().access_token != undefined){
                        var s = AuthService.getFields().access_token.split(' ');
                        obj.access_token = s[1];
                    }
                    else obj.access_token = '';
                    $rootScope.toast('Success',result.message);
                    setTimeout(function(){
                        $scope.callServer($scope.tableStateRef);
                    },300);
                }
            });
        }
        
    }
    if($stateParams.pname){
        $scope.provider_name = $stateParams.pname;
        setTimeout(function(){
            $scope.callServer($scope.tableStateRef);
        },500);
    }
    if($stateParams.status){
        $scope.contract_status = $stateParams.status;
        setTimeout(function(){
            $scope.callServer($scope.tableStateRef);
        },300);
    }
    $scope.exportContractsList = function(){
        var params = {};
        params.customer_id = $scope.user1.customer_id;
        params.user_role_id = $scope.user1.user_role_id;
        params.id_user = $scope.user1.id_user;
        contractService.exportContracts(params).then(function (result) {
            if(result.status){
                var obj = {};
                obj.action_name = 'export';
                obj.action_description = 'export$$contracts list$$('+result.data.file_name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';
                $rootScope.toast('Success',result.message);
                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.file_path =  GibberishAES.enc(result.data.file_path, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file_name =  GibberishAES.enc(result.data.file_name, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.file_path+'&name='+result.data.file_name;

                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;
                    }
                });
            }
        });
    }
    $scope.goToContratDiscussion = function(row){
        $state.go('app.contract.review-design',{name:row.contract_name,id:encode(row.id_contract),rId:encode(row.id_contract_review)});
    }
})
.controller('deletedContractListCtrl', function($scope, $rootScope, $state, $stateParams, contractService, businessUnitService, encode, $location,AuthService,userService){
        //console.log('deletedContractListCtrl');
        var param ={};
        param.user_role_id=$rootScope.user_role_id;
        param.id_user=$rootScope.id_user;
        param.customer_id = $scope.user1.customer_id;
        param.status = 1;

        $scope.getStatuses = function () {
            contractService.getContractStatus().then(function(result){
                if(result.status){
                    var obj = {key:'all',value:'All'};
                    result.data = result.data.reverse();
                    result.data.push(obj);
                    $scope.statusList = result.data.reverse();
                }
            })
        }
        $scope.getStatuses();
        $scope.deletedContractsList = {};
        $scope.myDataSource = {};

        $scope.callServer = function (tableState){
            $scope.filtersData = {};
            $rootScope.module = '';
            $rootScope.displayName = '';
            $scope.tableStateRef = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            tableState.customer_id = $scope.user1.customer_id;
            tableState.business_unit_id = $scope.business_unit_id;
            tableState.id_user  = $scope.user1.id_user;
            tableState.user_role_id  = $scope.user1.user_role_id;
            if($scope.provider_name && $scope.provider_name != null){
                tableState.provider_name  = $scope.provider_name;
            }else{
                delete tableState.provider_name;
                $scope.provider_name = '';
            }
            if($scope.contract_status && $scope.contract_status != null){
                if($scope.contract_status !='all')
                    tableState.contract_status  = $scope.contract_status;
                else delete tableState.contract_status;
            }else{
                delete tableState.contract_status;
                $scope.contract_status = '';
            }
            $scope.filtersData.provider_name = tableState.provider_name;
            $scope.filtersData.business_unit_id = tableState.business_unit_id;
            $scope.filtersData.contract_status = tableState.contract_status;
            contractService.listDelete(tableState).then (function(result){
                /*if($scope.filtersData && (tableState.pagination.start == 0)){
                    $scope.contractOverallDetails($scope.filtersData);
                }else{*/
                $scope.deletedContractsList = result.data.data;
                $scope.emptyTable=false;
                tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
                $scope.isLoading = false;
                if(result.data.total_records < 1)
                    $scope.emptyTable=true;
            })
        }

        $scope.goToDashboard = function (row) {
            $state.go('app.contract.contract-dashboard',{name:row.contract_name,id:encode(row.id_contract)});
        }

        if($stateParams.pname){
            $scope.provider_name = $stateParams.pname;
            setTimeout(function(){
                $scope.callServer($scope.tableStateRef);
            },500);
        }
        if($stateParams.status){
            $scope.contract_status = $stateParams.status;
            setTimeout(function(){
                $scope.callServer($scope.tableStateRef);
            },300);
        }

        $scope.undoDeleteContract = function(row){
            var r=confirm("Do you want to continue?");
            if(r==true){
                var params = {};
                params.contract_id = row.id_contract;
                params.user_role_id = $scope.user1.user_role_id;
                params.id_user = $scope.user1.id_user;
                contractService.undoDelete(params).then(function (result) {
                    if(result.status){
                        var obj = {};
                        obj.action_name = 'Undo Delete';
                        obj.action_description = 'undo $$ contract delete $$('+result.data.file_name+')';
                        obj.module_type = $state.current.activeLink;
                        obj.action_url = location.href;
                        if(AuthService.getFields().data.parent){
                            obj.user_id = AuthService.getFields().data.parent.id_user;
                            obj.acting_user_id = AuthService.getFields().data.data.id_user;
                        }
                        else obj.user_id = AuthService.getFields().data.data.id_user;
                        if(AuthService.getFields().access_token != undefined){
                            var s = AuthService.getFields().access_token.split(' ');
                            obj.access_token = s[1];
                        }
                        else obj.access_token = '';
                        $rootScope.toast('Success',result.message);
                        setTimeout(function(){
                            $scope.callServer($scope.tableStateRef);
                        },300);
                    }
                });
            }
            
        }
    })
.controller('createContractCtrl', function($scope, $rootScope, $state,$stateParams,$location, decode, contractService, customerService, masterService,Upload, $window, dateFilter){
    $scope.currencyList = [];
    $scope.relationshipCategoryList = {};
    $scope.relationshipClassificationList = {};
    $scope.contract = {};
    $rootScope.module = '';
    $rootScope.displayName = '';
    $scope.del=0;
    if($rootScope.access == 'ca' || $rootScope.access == 'bo'){
        $scope.del=1;
    }
    masterService.currencyList().then(function(result){
        $scope.currencyList = result.data;
    });
    contractService.getRelationshipCategory({'customer_id': $scope.user1.customer_id}).then(function(result){
        $scope.relationshipCategoryList = result.data;
    });
    contractService.getRelationshipClassiffication({'customer_id': $scope.user1.customer_id}).then(function(result){
        $scope.relationshipClassificationList = result.data;
    });
    $scope.title = 'general.create';
    $scope.bottom = 'general.save';
    if($stateParams.id){
        $scope.title = 'general.edit';
        $scope.bottom = 'general.update';
        $rootScope.module = 'Contract';
        $rootScope.displayName = $stateParams.name;
        var params = {};
        params.id_contract = decode($stateParams.id);
        params.id_user  = $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        contractService.getContractById(params).then(function(result){
            $scope.contract = result.data[0];
            $scope.contract.contract_start_date = new Date($scope.contract.contract_start_date);
            $scope.contract.contract_end_date = new Date($scope.contract.contract_end_date);
            $scope.contract['auto_renewal'] = $scope.contract['auto_renewal']==1?true:false;
            $scope.getContractDelegates($scope.contract.business_unit_id,$scope.contract.id_contract);
        });
    }else{
        setTimeout(function(){
            $scope.contract['auto_renewal'] = true;
        });
    }
    $scope.getContractDelegates = function (id,contractId){
        contractService.getDelegates({'id_business_unit': id}).then(function(result){
            $scope.delegates = result.data;
        });
        var params = {};
        params.business_unit_id = id;
        params.contract_id = contractId;
        params.type = "buowner";
        contractService.getbuOwnerUsers(params).then(function(result){
            $scope.buOwnerUsers = result.data;
        });
        /* $scope.contract.contract_owner_id = '';
        $scope.contract.delegate_id = ''; */
    }
    $scope.addContract = function (contract){
        //delete contract.attachments;
        contract.created_by = $scope.user.id_user;
        contract.customer_id = $scope.user1.customer_id;
        if($scope.user.access =='bo')
            contract.contract_owner_id = $scope.user.id_user;
        else contract.contract_owner_id = contract.contract_owner_id;
        $scope.contract['auto_renewal'] = $scope.contract['auto_renewal']==true?'1':'0';
        contract.attachment_delete = [];
        angular.forEach($scope.file.delete, function(i,o){
            var obj = {};
            obj.id_document = i.id_document;
            contract.attachment_delete.push(obj) ;
        });
        var params = {};
        angular.copy(contract,params);
        params.updated_by = $scope.user.id_user;
        params.contract_end_date = dateFilter(params.contract_end_date,'yyyy-MM-dd');
        params.contract_start_date = dateFilter(params.contract_start_date,'yyyy-MM-dd');
        if(new Date( params.contract_end_date) <= new Date( params.contract_start_date)){
            alert('Start date should be less than End date');
        }else{
            if(contract.id_contract){
                Upload.upload({
                    url: API_URL+'Contract/update',
                    data: {
                        'file' : $scope.file.attachment,
                        'contract': params
                    }
                }).then(function(resp){
                    if(resp.data.status){
                        $state.go('app.contract.view',{name:contract.contract_name,id:$stateParams.id});
                        $rootScope.toast('Success',resp.data.message);
                        var obj = {};
                        obj.action_name = 'update';
                        obj.action_description = 'update$$contract$$'+contract.contract_name;
                        obj.module_type = $state.current.activeLink;
                        obj.action_url = $location.$$absUrl;
                        $rootScope.confirmNavigationForSubmit(obj);
                    }else{
                        $rootScope.toast('Error',resp.data.error,'error',$scope.contract);
                    }
                },function(resp){
                    $rootScope.toast('Error',resp.error);
                },function(evt){
                    var progressPercentage=parseInt(100.0*evt.loaded/evt.total);
                });
            }
            else{
                 Upload.upload({
                    url: API_URL+'Contract/add',
                     data: {
                         'file' : $scope.file.attachment,
                         'contract': contract
                     }
                 }).then(function(resp){
                 if(resp.data.status){
                    $state.go('app.contract.contract-overview');
                    $rootScope.toast('Success',resp.data.message);
                     var obj = {};
                     obj.action_name = 'add';
                     obj.action_description = 'add$$contract$$'+contract.contract_name;
                     obj.module_type = $state.current.activeLink;
                     obj.action_url = $location.$$absUrl;
                     $rootScope.confirmNavigationForSubmit(obj);
                 }else{
                    $rootScope.toast('Error',resp.data.error,'error',$scope.contract);
                 }
                 },function(resp){
                    $rootScope.toast('Error',resp.error);
                 },function(evt){
                    var progressPercentage=parseInt(100.0*evt.loaded/evt.total);
                 });
            }
        }
    }
    $scope.cancel = function(){
        //$window.history.back();
        $state.go('app.contract.contract-overview');
    }
})
.controller('contractReviewActionItemCtrl', function($scope, $rootScope, $state, $stateParams, contractService, decode, $uibModal){
    if($stateParams.id){
        $scope.reviewList = {};
        $scope.callServer = function (tableState){
            $rootScope.module = 'Contract';
            $rootScope.displayName = $stateParams.name;
            $scope.tableStateRef = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            $scope.contract_id = tableState.contract_id  = decode($stateParams.id);
            tableState.id_user  = $scope.user1.id_user;
            tableState.user_role_id  = $scope.user1.user_role_id;
            contractService.reviewActionItemList(tableState).then (function(result){
                $scope.reviewList = result.data.data;
                $scope.emptyTable=false;
                tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
                $scope.isLoading = false;
                if(result.data.total_records < 1)
                    $scope.emptyTable=true;
            })
        }

        $scope.createContractReview = function (id) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'views/Manage-Users/contracts/create-edit-contract-review.html',
                controller: function ($uibModalInstance, $scope, item) {
                    $scope.update = false;
                    $scope.title = 'general.create';
                    $scope.bottom = 'general.save';
                    $scope.isEdit = false;
                    if (item != 0 &&  item.hasOwnProperty('id_contract_review_action_item')) {
                        $scope.isEdit = true;
                        $scope.submitStatus = true;
                        $scope.data = angular.copy(item);
                        delete $scope.data.comments;
                        $scope.data.due_date = new Date($scope.data.due_date);
                        $scope.update = true;
                        $scope.title = 'general.edit';
                        $scope.bottom = 'general.update';
                    }
                    //console.log("$scope1 -",$scope);
                    contractService.getActionItemResponsibleUsers({'contract_id': $scope.contract_id}).then(function(result){
                        $scope.userList = result.data;
                    });
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                    var params ={};
                    $scope.addReviewActionItem=function(data){
                        if(data != 0 && data.hasOwnProperty('id_contract_review_action_item')){
                            delete data.comments;
                            params = data;
                            params.updated_by = $scope.user.id_user;
                            params.contract_id = params.id_contract = $scope.contract_id;
                            params.id_user  = $scope.user1.id_user;
                            params.user_role_id  = $scope.user1.user_role_id;
                            contractService.addReviewActionItemList(params).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $scope.callServer($scope.tableStateRef);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error,'error');
                                }
                            });
                        }else{
                            params = data ;
                            params.contract_id = $scope.contract_id;
                            params.created_by = $scope.user.id_user;
                            params.contract_id = params.id_contract = $scope.contract_id;
                            params.id_user  = $scope.user1.id_user;
                            params.user_role_id  = $scope.user1.user_role_id;
                            contractService.addReviewActionItemList(params).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $scope.callServer($scope.tableStateRef);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error,'error');
                                }
                            });
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
            modalInstance.result.then(function ($data) {
            }, function () {
            });
        };

    }
})
.controller('contractViewCtrl', function($timeout,$scope, $rootScope, $state, $stateParams, contractService, decode, encode, attachmentService, Upload,$location, $uibModal, userService, AuthService){
    $rootScope.module = 'Contract Details';
    $rootScope.displayName = $stateParams.name;
    $scope.isSubLoading = true;
    var obj = {};
    obj.action_name = 'view';
    obj.action_description = 'view Contract$$('+$stateParams.name+')';
    obj.module_type = $state.current.activeLink;
    obj.action_url = $location.$$absUrl;
    $rootScope.confirmNavigationForSubmit(obj);
        if($stateParams.id){
        $scope.init = function (tableState){
            $scope.contract_id = decode($stateParams.id);
            $scope.contract_id_encoded = $stateParams.id;
            var params = {};
            params.id_contract  = $scope.contract_id;
            params.id_user  = $scope.user1.id_user;
            params.user_role_id  = $scope.user1.user_role_id;
            $scope.loading = false;
            contractService.getContractById(params).then (function(result){
                if(result.data.length>0){
                    if(result.data[0].contract_end_date=="1970-01-01")
                        result.data[0].contract_end_date='';
                    $scope.contractInfo = $scope.data = result.data[0];
                    $scope.loading = true;
                    $scope.review_access = true;
                    if($scope.contractInfo.reaaer != "itako"){$scope.review_access = false;}
                }else{
                    $state.go('app.contract.contract-overview');
                }
            });
            // params.parent_contract_id = $scope.contract_id;
            contractService.list(params).then (function (restult) {
                if(result.status){
                    $scope.child_contracts = result.data.data;
                }else{

                }
            });
        }
        $scope.goToContractDetails = function(row){
           $state.go('app.contract.view',{name:row.contract_name,id:encode(row.id_contract)});
        }
        $scope.callServerSubContract = function (tableState){
            $scope.filtersData = {};
            $rootScope.module = '';
            $rootScope.displayName = '';
            $scope.tableStateRef = tableState;
            $scope.isSubLoading = true;
            var pagination = tableState.pagination;
            tableState.customer_id = $scope.user1.customer_id;
            tableState.business_unit_id = $scope.business_unit_id;
            tableState.id_user  = $scope.user1.id_user;
            tableState.user_role_id  = $scope.user1.user_role_id;
            tableState.parent_contract_id  = decode($stateParams.id);
            if($scope.provider_name && $scope.provider_name != null){
                tableState.provider_name  = $scope.provider_name;
            }else{
                delete tableState.provider_name;
                $scope.provider_name = '';
            }
            if($scope.contract_status && $scope.contract_status != null){
                if($scope.contract_status !='all')
                    tableState.contract_status  = $scope.contract_status;
                else delete tableState.contract_status;
            }else{
                delete tableState.contract_status;
                $scope.contract_status = '';
            }
            $scope.filtersData.provider_name = tableState.provider_name;
            $scope.filtersData.business_unit_id = tableState.business_unit_id;
            $scope.filtersData.contract_status = tableState.contract_status;
            contractService.list(tableState).then (function(result){
                $scope.subContractsList = result.data.data;
                $scope.emptySubTable=false;
                tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
                $scope.isSubLoading = false;
                if(result.data.total_records < 1)
                    $scope.emptySubTable=true;
            })
        }
        $scope.init();
        $scope.reviewAction = function (tableState){
            setTimeout(function(){
                $scope.tableStateRef = tableState;
                $scope.isLoading = true;
                var pagination = tableState.pagination;
                tableState.contract_id  = $scope.contract_id;
                tableState.id_user  = $scope.user1.id_user;
                tableState.user_role_id  = $scope.user1.user_role_id;
                //tableState.id_contract_review  = $scope.data.id_contract_review;
                contractService.reviewActionItemList(tableState).then (function(result){
                    $scope.reviewList = result.data.data;
                    $scope.emptyTable=false;
                    tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
                    $scope.isLoading = false;
                    if(result.data.total_records < 1)
                        $scope.emptyTable=true;
                })
            },700);
        }
        $scope.deleteAttachment = function(id,name){
            var r=confirm("Do you want to continue?");
            $scope.deleConfirm = r;
            if(r==true){
                var params = {};
                params.id_document = id;
                attachmentService.deleteAttachments(params).then (function(result){
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        var obj = {};
                        obj.action_name = 'delete';
                        obj.action_description = 'delete$$Attachment$$('+name+')';
                        obj.module_type = $state.current.activeLink;
                        obj.action_url = $location.$$absUrl;
                        $rootScope.confirmNavigationForSubmit(obj);
                        $scope.init();
                    }else{$rootScope.toast('Error',result.error,'error');}
                })
            }
        }
    }
    $scope.goToEdit = function (){
        $state.go('app.contract.edit-contract',{name:$stateParams.name,id:$stateParams.id});
    }
    $scope.initializeReview = function (val){
        var params ={};
        params.created_by = $scope.user.id_user;
        params.customer_id = $scope.user1.customer_id;
        params.contract_id = decode($stateParams.id);
        if(val == true) params.contract_review_type = 'adhoc_review';
        contractService.initializeReview(params).then(function(result){
            if(result.status){
                $rootScope.toast('Success', result.message);
                var obj = {};
                obj.action_name = 'initiate';
                obj.action_description = 'initiate$$Review$$('+$stateParams.name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = $location.$$absUrl;
                $rootScope.confirmNavigationForSubmit(obj);
                $state.go('app.contract.contract-review',{name:$stateParams.name,id:$stateParams.id,rId:encode(result.data)});
            }else  $rootScope.toast('Error', result.error,'error',$scope.user);
        });
    }
    $scope.goToContractReview = function(id){
        $state.go('app.contract.contract-review',{name:$stateParams.name,id:$stateParams.id,rId:encode(id)});
    }
    $scope.goToContractLogs = function(id){
        $state.go('app.contract.contract-log',{name:$stateParams.name,id:$stateParams.id});
    }
    $scope.showUpload = false;
    $scope.uploadDoc = function(){
        $scope.showUpload = true;
    }
    $scope.uploadAttachment = function(attachments,data){
        if(attachments.length>0){
            Upload.upload({
                url: API_URL+'Document/add',
                data:{
                    file:attachments,
                    customer_id: $scope.user1.customer_id,
                    module_id: data.id_contract_review,
                    module_type: 'contract_review',
                    reference_id: decode($stateParams.id),
                    reference_type: 'contract',
                    uploaded_by: $scope.user1.id_user
                }
            }).then(function (resp) {
                $scope.showUpload = false;
                if(resp.data.status){
                    $rootScope.toast('Success',resp.data.message);
                    var obj = {};
                    obj.action_name = 'upload';
                    obj.action_description = 'upload$$attachments$$for$$contract$$('+$stateParams.name+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = window.location.href;
                    $rootScope.confirmNavigationForSubmit(obj);
                }
                else $rootScope.toast('Error',resp.data.error,'error',$scope.user);
                $scope.init();
            }, function (resp) {
                $rootScope.toast('Error',resp.data.error,'error');
            }, function (evt) {
                $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
            });

        }else{
            $rootScope.toast('Error','No file selected','image-error');
        }
    }
    $scope.cancelAttachments = function(){$scope.showUpload = false;}
    $scope.updateContractReview = function (row, type) {
		$scope.type = type;
        $scope.selectedRow = row;
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'right-panel-modal modal-open',
            templateUrl: 'views/Manage-Users/contracts/create-edit-contract-review.html',
            controller: function ($uibModalInstance, $scope, item) {
                $scope.update = false;
                $scope.bottom = 'general.save';
                $scope.isEdit = false;
                if (item) {
                    $scope.isEdit = true;
                    $scope.submitStatus = true;
                    $scope.data = angular.copy(item);
                    delete $scope.data.comments;
                    $scope.data.due_date = new Date($scope.data.due_date);
                    $scope.title = '';
                    $scope.update = true;
                    $scope.bottom = 'general.update';
                }
                if($scope.type == 'view')
                    $scope.bottom = 'contract.finish';

                /*contractService.responsibleUserList({'contract_id': decode($stateParams.id)}).then(function(result){
                    $scope.userList = result.data;
                });*/
                var respUserParams = {};
                respUserParams.contract_review_id  = $scope.data.contract_review_id?$scope.data.contract_review_id:'0';
                respUserParams.contract_id  = decode($stateParams.id);
                respUserParams.module_id  = $scope.data.module_id;
                contractService.getActionItemResponsibleUsers(respUserParams).then(function(result){
                    $scope.userList = result.data;
                });
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
                $scope.goToEdit = function(data){
                    $scope.data.due_date = new Date(data.due_date);
                }
                var params ={};
                $scope.getActionItemById = function(id){
                    contractService.getActionItemDetails({'id_contract_review_action_item':id}).then(function(result){
                        $scope.data = result.data[0];
                    });
                }
                $scope.addReviewActionItem=function(data){
                    if($scope.type == 'view'){
                        params.id_contract_review_action_item = data.id_contract_review_action_item;
                        params.comments = data.comments;
                        params.is_finish = data.is_finish;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id  = decode($stateParams.id);
                        if(data.is_finish  == 1){
                            var r=confirm("Are you sure that you want to finish this action item ?");
                            $scope.deleConfirm = r;
                            if(r==true){
                                contractService.reviewActionItemUpdate(params).then(function (result) {
                                    if (result.status) {
                                        var obj = {};
                                        obj.action_name = 'Finish';
                                        obj.action_description = 'Finish$$Action$$Item$$('+data.action_item+')';
                                        obj.module_type = $state.current.activeLink;
                                        obj.action_url = $location.$$absUrl;
                                        $rootScope.confirmNavigationForSubmit(obj);
                                        $rootScope.toast('Success', result.message);
                                        $scope.reviewAction($scope.tableStateRef);
                                        $scope.cancel();
                                    } else {
                                        $rootScope.toast('Error', result.error,'error');
                                    }
                                });
                            }
                        }else{
                            contractService.reviewActionItemUpdate(params).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    var obj = {};
                                    obj.action_name = 'Save';
                                    obj.action_description = 'Save$$Action$$Item$$('+data.action_item+')';
                                    obj.module_type = $state.current.activeLink;
                                    obj.action_url = $location.$$absUrl;
                                    $rootScope.confirmNavigationForSubmit(obj);
                                    $scope.reviewAction($scope.tableStateRef);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error,'error');
                                }
                            });
                        }

                    }
                    else if(data != 0 && data.hasOwnProperty('id_contract_review_action_item')){
                        delete data.comments;
                        params = data;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id = params.id_contract = decode($stateParams.id);
                        contractService.addReviewActionItemList(params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                var obj = {};
                                obj.action_name = 'update';
                                obj.action_description = 'update$$Action$$Item$$('+data.action_item+')';
                                obj.module_type = $state.current.activeLink;
                                obj.action_url = $location.$$absUrl;
                                $rootScope.confirmNavigationForSubmit(obj);
                                $scope.getActionItemById(data.id_contract_review_action_item);
                                $scope.cancel();
                                $scope.reviewAction($scope.tableStateRef);
                            } else {
                                $rootScope.toast('Error', result.error,'error');
                            }
                        });
                    }
                }
            },
            resolve: {
                item: function () {
                    if ($scope.selectedRow) {
                        return $scope.selectedRow;
                    }
                }
            }
        });
        modalInstance.result.then(function ($data) {
        }, function () {
        });
    }
    $scope.deleteContractActionItem = function(row){
        var r=confirm("Do you want to continue?");
        $scope.deleConfirm = r;
        if(r==true){
            var params ={};
            params.id_contract_review_action_item  = row.id_contract_review_action_item ;
            params.updated_by  = $rootScope.id_user ;
            contractService.deleteActionItem(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success', result.message);
                    var obj = {};
                    obj.action_name = 'delete';
                    obj.action_description = 'delete$$Action$$Item$$('+row.action_item+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    $scope.reviewAction($scope.tableStateRef);
                }else $rootScope.toast('Error', result.error, 'error',$scope.user);
            });
        }
    }
    $scope.goToDashboard = function (data) {
        $state.go('app.contract.contract-dashboard',{name:$stateParams.name,id:$stateParams.id});
    }
    $scope.goToDesign = function(data){
        $state.go('app.contract.review-design',{name:$stateParams.name,id:$stateParams.id,rId:encode(data.id_contract_review)});
    }
    $scope.getDownloadAttachmentUrl = function(objData){
        var d = {};
        d.id_document = objData.id_document;
        contractService.getUrl(d).then(function (result) {
            if(result.status){
                var obj = {};
                obj.action_name = 'download';
                obj.action_description = 'download$$attachment$$('+ objData.document_name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';
                $rootScope.toast('Success',result.message);
                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.url =  GibberishAES.enc(result.data.url, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file =  GibberishAES.enc(result.data.file, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.url+'&name='+result.data.file;
                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;
                    }
                });
            }
        });
    };
})
.controller('contractReviewCtrl', function($scope, $rootScope, $state, $stateParams, contractService, decode, encode, $uibModal, attachmentService,$location){
    //contractModules
    $rootScope.module = 'Contract';
    $rootScope.displayName = $stateParams.name;
    var params = {};
    $scope.loading = false;
    params.contract_review_id = decode($stateParams.rId);
    params.contract_id = params.id_contract = decode($stateParams.id);
    params.id_user  = $scope.user1.id_user;
    params.user_role_id  = $scope.user1.user_role_id;
    contractService.contractModule(params).then(function(result){
        $scope.contractModules = result.data;
        $scope.no_modules = false;
        if(result.data == ''){$scope.no_modules = true;}
        $scope.loading = false;
        if($scope.contractModules)$scope.loading = true;
    });
    contractService.getContractById(params).then (function(result){
        $scope.progress = result.data.progress;
        $scope.contractData = result.data[0];
        if( $scope.contractData){
            $scope.review_access = true;
            if($scope.contractData.reaaer != "itako"){$scope.review_access = false;}
            if($scope.contractData.ideedi != 'annus'){$scope.discussion_access = true;}}
    })
    $scope.goToDetails = function(){
        $state.go('app.contract.view',{name:$stateParams.name,id:$stateParams.id});
    }
    $scope.goToModuleQuestions = function (module) {
        var reviewId = module.contract_review_id;
        var moduleId = module.id_module;
        var module_name = module.module_name;
        var topic_name = module.default_topic.topic_name;
        var topic_id = encode(module.default_topic.id_topic);
        $state.go('app.contract.contract-module-review',
            {name:$stateParams.name,id:$stateParams.id,rId:encode(reviewId),mName:module_name,moduleId:encode(moduleId),tName:topic_name,tId:topic_id});
    }
    $scope.reviewAction = function (tableState){
            $scope.tableStateRef = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            //tableState.contract_review_id = decode($stateParams.rId);
            tableState.contract_id = decode($stateParams.id);
            tableState.id_user  = $scope.user1.id_user;
            tableState.user_role_id  = $scope.user1.user_role_id;
            contractService.reviewActionItemList(tableState).then (function(result){
                $scope.reviewList = result.data.data;
                $scope.emptyTable=false;
                tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
                $scope.isLoading = false;
                if(result.data.total_records < 1)
                    $scope.emptyTable=true;
            })
    }
    $scope.updateContractAction = function (row, type) {
        $scope.type = type;
        $scope.selectedRow = row;
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'right-panel-modal modal-open',
            templateUrl: 'views/Manage-Users/contracts/create-edit-contract-review.html',
            controller: function ($uibModalInstance, $scope, item) {
                $scope.update = false;
                $scope.title = item.action_item;
                $scope.bottom = 'general.save';
                $scope.isEdit = false;
                if (item != 0 &&  item.hasOwnProperty('id_contract_review_action_item')) {
                    $scope.isEdit = true;
                    $scope.submitStatus = true;
                    $scope.data = angular.copy(item);
                    delete $scope.data.comments;
                    $scope.data.due_date = new Date($scope.data.due_date);
                    $scope.update = true;
                    $scope.bottom = 'general.update';
                    $scope.addaction = false;
                }

                if($scope.type == 'view')
                    $scope.bottom = 'contract.finish';
                contractService.getActionItemResponsibleUsers({'contract_id': decode($stateParams.id),'contract_review_id': decode($stateParams.rId)}).then(function(result){
                    $scope.userList = result.data;
                });
                $scope.getActionItemById = function(id){
                    contractService.getActionItemDetails({'id_contract_review_action_item':id}).then(function(result){
                        $scope.data = result.data[0];
                    });
                }
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
                $scope.goToEdit = function(data){
                    $scope.data.due_date = new Date(data.due_date);
                }
                var params ={};
                $scope.addReviewActionItem=function(data){
                    if($scope.type == 'view'){
                        params.id_contract_review_action_item = data.id_contract_review_action_item;
                        params.comments = data.comments;
                        params.is_finish = data.is_finish;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id  = decode($stateParams.id);
                        if(data.is_finish  == 1) {
                            var r = confirm("Are you sure that you want to finish this action item ?");
                            $scope.deleConfirm = r;
                            if (r == true) {
                                contractService.reviewActionItemUpdate(params).then(function (result) {
                                    if (result.status) {
                                        $rootScope.toast('Success', result.message);
                                        var obj = {};
                                        obj.action_name = 'Finish';
                                        obj.action_description = 'Finish$$Action$$Item$$('+data.action_item+')';
                                        obj.module_type = $state.current.activeLink;
                                        obj.action_url = $location.$$absUrl;
                                        $rootScope.confirmNavigationForSubmit(obj);
                                        $scope.reviewAction($scope.tableStateRef);
                                        $scope.cancel();
                                    } else {
                                        $rootScope.toast('Error', result.error,'error');
                                    }
                                });
                            }
                        }else{
                            contractService.reviewActionItemUpdate(params).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    var obj = {};
                                    obj.action_name = 'save';
                                    obj.action_description = 'save$$Action$$Item$$('+data.action_item+')';
                                    obj.module_type = $state.current.activeLink;
                                    obj.action_url = $location.$$absUrl;
                                    $rootScope.confirmNavigationForSubmit(obj);
                                    $scope.reviewAction($scope.tableStateRef);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error,'error');
                                }
                            });
                        }
                    }
                    else if(data != 0 && data.hasOwnProperty('id_contract_review_action_item')){
                        delete data.comments;
                        params = data;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id  = decode($stateParams.id);
                        contractService.addReviewActionItemList(params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                var obj = {};
                                obj.action_name = 'update';
                                obj.action_description = 'update$$Action$$Item$$('+data.action_item+')';
                                obj.module_type = $state.current.activeLink;
                                obj.action_url = $location.$$absUrl;
                                $rootScope.confirmNavigationForSubmit(obj);
                                $scope.getActionItemById(data.id_contract_review_action_item);
                                $scope.cancel();
                                $scope.reviewAction($scope.tableStateRef);
                            } else {
                                $rootScope.toast('Error', result.error,'error');
                            }
                        });
                    }
                }
            },
            resolve: {
                item: function () {
                    if ($scope.selectedRow) {
                        return $scope.selectedRow;
                    }
                }
            }
        });
        modalInstance.result.then(function ($data) {
        }, function () {
        });
    }
    $scope.deleteContractActionItem = function(row){
        var r=confirm("Do you want to continue?");
        $scope.deleConfirm = r;
        if(r==true){
            var params ={};
            params.id_contract_review_action_item  = row.id_contract_review_action_item ;
            params.updated_by  = $rootScope.id_user ;
            contractService.deleteActionItem(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success', result.message);
                    var obj = {};
                    obj.action_name = 'delete';
                    obj.action_description = 'delete$$Action$$Item$$('+row.action_item+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    $scope.reviewAction($scope.tableStateRef);
                }else $rootScope.toast('Error', result.error, 'error',$scope.user);
            });
        }
    }
    $scope.finalizeReviewList = function(){
       /* var r = confirm('Do you want to finish the review ?');
        if(r == true){
            params.created_by  = $rootScope.id_user ;
            contractService.finalizeReviewList(params).then(function(result){
                if(result.status){
                    $state.go('app.contract.view',{name:$stateParams.name,id:$stateParams.id});
                    $rootScope.toast('Success', result.message);
                    $scope.reviewAction($scope.tableStateRef);
                }else $rootScope.toast('Error', result.error, 'error',$scope.user);
            });
        }*/
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'right-panel-modal modal-open',
            templateUrl: 'contract-review-finalize.html',
            controller: function ($uibModalInstance, $scope) {
                $scope.allowToFinish = false;
                $scope.finalizeReview = function (finalize,exists) {
                    $scope.allowToFinish = true;
                    var params = {};
                    params.contract_review_id = decode($stateParams.rId);
                    params.contract_id = params.id_contract = decode($stateParams.id);
                    params.id_user  = $scope.user1.id_user;
                    params.user_role_id  = $scope.user1.user_role_id;
                    if(exists != 'annus'){
                        if(finalize.finalize_without_discussion == 1)
                            params.finalize_without_discussion = finalize.finalize_without_discussion;
                        params.finalize_comments = finalize.finalize_comments;
                    }
                    params.created_by  = $rootScope.id_user ;
                    contractService.finalizeReviewList(params).then(function(result){
                        if(result.status){
                            $state.go('app.contract.view',{name:$stateParams.name,id:$stateParams.id});
                            $rootScope.toast('Success', result.message);
                            var obj = {};
                            obj.action_name = 'finalize';
                            obj.action_description = 'finalize$$Contract$$Review$$('+$stateParams.name+')';
                            obj.module_type = $state.current.activeLink;
                            obj.action_url = $location.$$absUrl;
                            $rootScope.confirmNavigationForSubmit(obj);
                            $scope.reviewAction($scope.tableStateRef);
                        }else $rootScope.toast('Error', result.error, 'error',$scope.user);
                    });
                }
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
            },
            resolve: {}
        });
        modalInstance.result.then(function ($data) {
        }, function () {
        });
    }
    $scope.getModuleAttachmentList = function(){
        var params ={};
        params.module_id = decode($stateParams.rId);
        params.module_type =  'contract_review';
        params.id_user  = $rootScope.id_user;
        params.user_role_id  = $rootScope.user_role_id;
        params.page_type  = 'contract_overview';
        params.contract_id  = decode($stateParams.id);
        contractService.getAttachments(params).then(function(result){
            $scope.attachmentList = result.data.data;
        });
    }
    $scope.getModuleAttachmentList();
    $scope.deleteModuleDocument = function(id,name){
        var r=confirm("Do you want to continue?");
        $scope.deleConfirm = r;
        if(r==true){
            attachmentService.deleteAttachments({'id_document': id}).then (function(result){
                if(result.status){
                    var obj = {};
                    obj.action_name = 'delete';
                    obj.action_description = 'delete$$Review$$Attachment$$('+name+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    $scope.getModuleAttachmentList();
                    $rootScope.toast('Success',result.data.message);
                    $scope.getModuleAttachmentList($scope.tableDocStateRef2);
                }
            })
        }
    }
    $scope.goToDashboard = function () {
        $state.go('app.contract.contract-dashboard',{name:$stateParams.name,id:$stateParams.id,rId:$stateParams.rId});
    }
    $scope.goToDesign = function(){
        $state.go('app.contract.review-design',{name:$stateParams.name,id:$stateParams.id,rId:$stateParams.rId});
    }

    $scope.goToChangeLog = function(){
        $state.go('app.contract.review-change-log', {'name': $stateParams.name,id:$stateParams.id,rId:$stateParams.rId});
    }
    $scope.initializeReview = function (val){
        var params ={};
        params.created_by = $scope.user.id_user;
        params.customer_id = $scope.user1.customer_id;
        params.contract_id = decode($stateParams.id);
        if(val == true) params.contract_review_type = 'adhoc_review';
        contractService.initializeReview(params).then(function(result){
            if(result.status){
                $rootScope.toast('Success', result.message);
                var obj = {};
                obj.action_name = 'initiate';
                obj.action_description = 'initiate$$Review$$('+$stateParams.name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = $location.$$absUrl;
                $rootScope.confirmNavigationForSubmit(obj);
                $state.transitionTo('app.contract.contract-review',{name:$stateParams.name,id:$stateParams.id,rId:encode(result.data)});
            }else  $rootScope.toast('Error', result.error,'error',$scope.user);
        });
    }
})
.controller('contractModuleReviewCtrl', function($scope, $rootScope, $state, $stateParams, businessUnitService, contractService, userService, attachmentService, decode, encode, Upload, $uibModal,$location){
    $scope.topic_count = 1;
    $rootScope.module = 'Contract';
    $rootScope.displayName = $stateParams.name +' - '+ $stateParams.mName ;
    $scope.optionsData = {};
    $scope.optionsData.feedback = {};
    $scope.optionsData.answers = {};
    $scope.showFileToAttach = false;
    $scope.contract_review_id =  decode($stateParams.rId);
    $scope.module_id = decode($stateParams.moduleId);
    $scope.contract_id = decode($stateParams.id);

    var Globalparams= {};
    Globalparams.contract_review_id = decode($stateParams.rId);
    Globalparams.module_id = decode($stateParams.moduleId);
    Globalparams.contract_id = decode($stateParams.id);
    Globalparams.id_topic = decode($stateParams.tId);

    $scope.getTopicQuestions = function(params){
        contractService.getcontractReviewModules(params).then(function(result){
            $scope.contractModuleTopics = result.data[0];
            $scope.topic_no = $scope.contractModuleTopics.topic_pagination.current?$scope.contractModuleTopics.topic_pagination.current:'';
            $scope.next = $scope.contractModuleTopics.topic_pagination.next;
            $scope.previous = $scope.contractModuleTopics.topic_pagination.previous;
            $scope.present_count = $scope.contractModuleTopics.topic_pagination.count!=0?$scope.contractModuleTopics.topic_pagination.current_count:0;
            $scope.contributors = $scope.contractModuleTopics.contributors;
            if($scope.contractModuleTopics.topics.length>0)
                $rootScope.topicName = $scope.contractModuleTopics.topics[0].topic_name;
            $scope.attachment = [];
            if($scope.contractModuleTopics.topics.length>0){
                angular.forEach($scope.contractModuleTopics.topics[0].questions, function(item,key){
                    $scope.optionsData.answers[item.id_question] = item.parent_question_answer;
                    $scope.optionsData.feedback[item.id_question] = item.question_feedback;
                });
            }
            $scope.review_access = true;
            if($scope.contractModuleTopics.contract_details[0].reaaer!= "itako"){$scope.review_access = false;}
        });
    }
    $scope.reset = function(val){
        $scope.optionsData.answers[val]='';
    }
    $scope.getTopicQuestions(Globalparams);

    $scope.reviewAction = function (tableState){
        setTimeout(function(){
            $scope.tableStateRef = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            tableState.contract_review_id = decode($stateParams.rId);
            tableState.module_id = decode($stateParams.moduleId);
            tableState.contract_id = decode($stateParams.id);
            tableState.topic_id = decode($stateParams.tId);
            tableState.id_user  = $scope.user1.id_user;
            tableState.user_role_id  = $scope.user1.user_role_id;
            tableState.page_type='contract_review';
            contractService.reviewActionItemList(tableState).then (function(result){
                $scope.reviewList = result.data.data;
                $scope.isLoading = false;
                tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
                $scope.isLoading = false;
                if(result.data.total_records < 1)
                    $scope.emptyTable=true;
            })
        },500);
    }

    $scope.goToNextTopic = function(next) {
       $state.transitionTo('app.contract.contract-module-review',
            {name:$stateParams.name,id:$stateParams.id,rId:$stateParams.rId,mName:$stateParams.mName,moduleId:$stateParams.moduleId,
                tName:next.next_text,tId:encode(next.next)});
    }

    $scope.goToPreviousTopic = function(previous) {
        $state.transitionTo('app.contract.contract-module-review',
            {name:$stateParams.name,id:$stateParams.id,rId:$stateParams.rId,mName:$stateParams.mName,moduleId:$stateParams.moduleId,
                tName:previous.previous_text,tId:encode(previous.previous)});
    }

    $scope.getAttachmentList = function(tableState){
        $scope.tableDocStateRef = tableState;
        var pagination = tableState.pagination;
        tableState.module_id = $scope.contract_review_id;
        tableState.module_type =  'contract_review';
        tableState.page_type= 'contract_review';
        tableState.reference_id = decode($stateParams.tId);
        tableState.reference_type = 'topic';
        tableState.id_user  = $rootScope.id_user;
        tableState.user_role_id  = $rootScope.user_role_id;
        contractService.getAttachments(tableState).then(function(result){
            $scope.attachmentList = result.data.data;
            $scope.emptyTable=false;
            tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
            $scope.isLoading = false;
            if(result.data.total_records < 1)
                $scope.emptyTable=true;
        });
    }
    $scope.createContractReview = function (row, type, question) {
        $scope.question_id = question;
        $scope.type = type;
        $scope.selectedRow = row;
        $scope.contract = {};
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'right-panel-modal modal-open',
            templateUrl: 'views/Manage-Users/contracts/create-edit-contract-review.html',
            controller: function ($uibModalInstance, $scope, item) {
                $scope.update = false;
                $scope.title = 'contract.create_action_item';
                $scope.bottom = 'general.save';
                $scope.isEdit = false;
                if (typeof item != 'undefined') {
                    $scope.isEdit = true;
                    $scope.submitStatus = true;
                    $scope.title = '';
                    $scope.data = angular.copy(item);
                    delete $scope.data.comments;
                    $scope.data.due_date = new Date($scope.data.due_date);
                    $scope.update = true;
                    $scope.bottom = 'general.update';
                    $scope.addaction = false;
                }
                else{
                    $scope.addaction = true;
                }
                $scope.getActionItemById = function(id){
                    contractService.getActionItemDetails({'id_contract_review_action_item':id}).then(function(result){
                        $scope.data = result.data[0];
                        $scope.data.due_date = new Date($scope.data.due_date);
                    });
                }
                if($scope.type == 'view') $scope.bottom = 'contract.finish';
                contractService.getActionItemResponsibleUsers({'contract_id': $scope.contract_id,'contract_review_id': $scope.contract_review_id}).then(function(result){
                    $scope.userList = result.data;
                });
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
                $scope.goToEdit = function(data){
                    $scope.data.due_date = new Date(data.due_date);
                }
                var params ={};
                $scope.addReviewActionItem=function(data){
                    if($scope.type == 'view'){
                        var params = [];
                        params = data;
                        params.id_contract_review_action_item = data.id_contract_review_action_item;
                        delete params.description;
                        params.comments = data.comments;
                        params.is_finish = data.is_finish;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id  = decode($stateParams.id);
                        if(data.is_finish  == 1) {
                            var r = confirm("Are you sure that you want to finish this action item ?");
                            $scope.deleConfirm = r;
                            if (r == true) {
                                contractService.reviewActionItemUpdate(params).then(function (result) {
                                    if (result.status) {
                                        $rootScope.toast('Success', result.message);
                                        var obj = {};
                                        obj.action_name = 'Finish';
                                        obj.action_description = 'Finish$$Action$$Item$$('+data.action_item+')';
                                        obj.module_type = $state.current.activeLink;
                                        obj.action_url = $location.$$absUrl;
                                        $rootScope.confirmNavigationForSubmit(obj);
                                        $scope.reviewAction($scope.tableStateRef);
                                        $scope.cancel();
                                    } else {
                                        $rootScope.toast('Error', result.error,'error');
                                    }
                                });
                            }
                        }else{
                            contractService.reviewActionItemUpdate(params).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    var obj = {};
                                    obj.action_name = 'Save';
                                    obj.action_description = 'Save$$Action$$Item$$('+data.action_item+')';
                                    obj.module_type = $state.current.activeLink;
                                    obj.action_url = $location.$$absUrl;
                                    $rootScope.confirmNavigationForSubmit(obj);
                                    $scope.reviewAction($scope.tableStateRef);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error,'error');
                                }
                            });
                        }
                    }
                    else if(typeof data == undefined){
                        var params ={};
                        delete data.comments;
                        params = data;
                        params.description = data.description;
                        params.contract_review_id = $scope.contract_review_id;
                        params.module_id = $scope.module_id;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id = params.id_contract = $scope.contract_id;
                        params.topic_id = $scope.topic_no;
                        params.id_user  = $scope.user1.id_user;
                        params.user_role_id  = $scope.user1.user_role_id;
                        contractService.addReviewActionItemList(params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                var obj = {};
                                obj.action_name = 'update';
                                obj.action_description = 'update$$Action$$Item$$('+data.action_item+')';
                                obj.module_type = $state.current.activeLink;
                                obj.action_url = $location.$$absUrl;
                                $rootScope.confirmNavigationForSubmit(obj);
                                $scope.reviewAction($scope.tableStateRef);
                                $scope.cancel();
                            } else {
                                $rootScope.toast('Error', result.error,'error');
                            }
                        });
                    }
                    else {
                        var params ={};
                        delete data.comments;
                        params = data ;
                        params.description = data.description;
                        delete params.comments;
                        params.module_id = $scope.module_id;
                        params.contract_id = $scope.contract_id;
                        params.created_by = $scope.user.id_user;
                        params.contract_id = params.id_contract = $scope.contract_id;
                        params.topic_id = $scope.topic_no;
                        params.id_user  = $scope.user1.id_user;
                        params.user_role_id  = $scope.user1.user_role_id;
                        params['contract_review_id'] = $scope.contract_review_id;
                        params.question_id= $scope.question_id;
                        contractService.addReviewActionItemList(params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                var obj = {};
                                obj.action_name = 'add';
                                obj.action_description = 'add$$Action$$Item$$('+data.action_item+')';
                                obj.module_type = $state.current.activeLink;
                                obj.action_url = $location.$$absUrl;
                                $rootScope.confirmNavigationForSubmit(obj);
                                if(data.id_contract_review_action_item){
                                    $scope.getActionItemById(data.id_contract_review_action_item);
                                    $scope.cancel();
                                }else{$scope.cancel();}
                                $scope.reviewAction($scope.tableStateRef);
                            } else {
                                $rootScope.toast('Error', result.error,'error');
                            }
                        });
                    }
                }
            },
            resolve: {
                item: function () {
                    if ($scope.selectedRow) {
                        return $scope.selectedRow;
                    }
                }
            }
        });
        modalInstance.result.then(function ($data) {
        }, function () {
        });
    }
    /*$scope.viewContractReview = function (row, type) {
        $scope.type = type;
        $scope.selectedRow = row;
        $scope.contract = {};
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'right-panel-modal modal-open',
            templateUrl: 'views/Manage-Users/contracts/create-edit-contract-review.html',
            controller: function ($uibModalInstance, $scope, item) {
                $scope.update = false;
                $scope.title = 'contract.create_action_item';
                $scope.bottom = 'general.save';
                $scope.isEdit = false;
                if (typeof item != 'undefined') {
                    $scope.isEdit = true;
                    $scope.submitStatus = true;
                    $scope.title = '';
                    $scope.data = angular.copy(item);
                    delete $scope.data.comments;
                    $scope.data.due_date = new Date($scope.data.due_date)
                    $scope.update = true;
                    $scope.bottom = 'general.update';
                    $scope.addaction = false;
                }
                else{
                    $scope.addaction = true;
                }

                if($scope.type == 'view')
                    $scope.bottom = 'contract.finish';

                contractService.getbuOwnerUsers({'contract_id': $scope.contract_id}).then(function(result){
                    $scope.userList = result.data;
                });
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
                var params ={};
                $scope.addReviewActionItem=function(data){
                    if($scope.type == 'view'){
                        var params = [];
                        params = data;
                        params.id_contract_review_action_item = data.id_contract_review_action_item;
                        delete params.description;
                        params.comments = data.comments;
                        params.is_finish = data.is_finish;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id  = decode($stateParams.id);
                        if(data.is_finish  == 1) {
                            var r = confirm("Do you want finish this action item ?");
                            $scope.deleConfirm = r;
                            if (r == true) {
                                contractService.reviewActionItemUpdate(params).then(function (result) {
                                    if (result.status) {
                                        $rootScope.toast('Success', result.message);
                                        $scope.reviewAction($scope.tableStateRef);
                                        $scope.cancel();
                                    } else {
                                        $rootScope.toast('Error', result.error,'error');
                                    }
                                });
                            }
                        }else{
                            contractService.reviewActionItemUpdate(params).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $scope.reviewAction($scope.tableStateRef);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error,'error');
                                }
                            });
                        }
                    }
                    else if(typeof data == undefined){
                        var params ={};
                        params = data;
                        params.description = data.description;
                        delete params.comments;
                        params.contract_review_id = $scope.contract_review_id;
                        params.module_id = $scope.module_id;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id = params.id_contract = $scope.contract_id;
                        params.topic_id = $scope.topic_no;
                        params.id_user  = $scope.user1.id_user;
                        params.user_role_id  = $scope.user1.user_role_id;
                        contractService.addReviewActionItemList(params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                $scope.reviewAction($scope.tableStateRef);
                                $scope.cancel();
                            } else {
                                $rootScope.toast('Error', result.error,'error');
                            }
                        });
                    }
                    else {
                        var params ={};
                        params = data ;
                        params.description = data.description;
                        delete params.comments;
                        params.module_id = $scope.module_id;
                        params.contract_id = $scope.contract_id;
                        params.created_by = $scope.user.id_user;
                        params.contract_id = params.id_contract = $scope.contract_id;
                        params.topic_id = $scope.topic_no;
                        params.id_user  = $scope.user1.id_user;
                        params.user_role_id  = $scope.user1.user_role_id;
                        params['contract_review_id'] = $scope.contract_review_id;
                        contractService.addReviewActionItemList(params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                $scope.reviewAction($scope.tableStateRef);
                                $scope.cancel();
                            } else {
                                $rootScope.toast('Error', result.error,'error');
                            }
                        });
                    }
                }
            },
            resolve: {
                item: function () {
                    if ($scope.selectedRow) {
                        return $scope.selectedRow;
                    }
                }
            }
        });
        modalInstance.result.then(function ($data) {
        }, function () {
        });
    }*/
    $scope.addContributors = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'right-panel-modal modal-open',
            templateUrl: 'add-edit-contributors.html',
            controller: function ($uibModalInstance, $scope) {
                $scope.title = 'general.create';
                $scope.bottom = 'general.save';
                $scope.isEdit = false;
                $scope.showContractList = false;
                $scope.businessUnitList = [];
                $scope.data = {};
                var params = {};
                params.user_role_id = $scope.user1.user_role_id;
                params.customer_id  = $scope.user1.customer_id;
                params.user_id = $scope.user.id_user;
                businessUnitService.bulist(params).then(function(result){
                    $scope.businessUnitList = result.data.data;
                });
                $scope.getContributorsByBusinessUnit = function(selectedBU) {
                    var params = {};
                    params.contract_id = $scope.contract_id;
                    params.type = 'contributor';
                    params.user_role_id = $scope.user1.user_role_id;
                    params.customer_id  = $scope.user1.customer_id;
                    params.user_id = $scope.user.id_user;
                    if (selectedBU !== '') {
                        params.business_unit_id = selectedBU;
                    }
                    contractService.reviewUsers(params).then(function(result){
                        // $("#contributor").empty();
                        $scope.userList = result.data;
                        // $("#contributor").trigger("chosen:updated");
                        if (selectedBU == '') {
                            $scope.contributors1 = [];
                            if (!$scope.data.contributors) {
                                $scope.data.contributors = [];
                            }
                            else{
                                $scope.data.contributors = [];
                            }
                            //console.log($scope.contributors);
                            //$scope.data.contributors = $scope.contributors;
                            angular.forEach($scope.contributors, function(i,o){
                                angular.forEach(result.data, function(i1,o1){
                                    if(i.user_id==i1.id_user)
                                        $scope.data.contributors.push(i1);
                                });
                            });
                            angular.forEach($scope.data.contributors, function(i,o){
                                $scope.contributors1.push(i.id_user);
                            });
                        } else {

                            //$scope.userList.forEach
                        }
                    });
                };
                $scope.getContributorsByBusinessUnit('');
                $scope.addContributer = function(data){
                    if (!$scope.data.contributors) {
                        $scope.data.contributors = [];
                    }                    
                    var isPresent = false;
                    angular.forEach($scope.data.contributors, function(i,o){
                        if (i.id_user && i.id_user === data.id_user)
                            isPresent = true;
                    });
                    if (!isPresent) {
                        $scope.data.contributors.push(data);
                        $scope.showContractList = false;
                    } else {
                        $rootScope.toast('Error', 'Contributor already added.');
                    }
                }
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
                var params ={};
                $scope.showBtn = false;
                $scope.saveContributors=function(data){
                    $scope.showBtn = true;
                    params.contract_review_id = $scope.contract_review_id;
                    params.module_id = $scope.module_id;
                    params.created_by = $scope.user.id_user;
                    params.contract_id = params.id_contract = $scope.contract_id;
                    params.topic_id = $scope.topic_no;
                    params.user_role_id  = $scope.user1.user_role_id;
                    params.customer_id  = $scope.user1.customer_id;
                    var a = [], diff = [];
                    var a1 = $scope.contributors1;
                    var a2 = [];
                    angular.forEach($scope.data.contributors, function(i,o){
                        a2.push(i.id_user);
                    });
                    params.contributors_add = a2.join(',');
                    var diff = a1.diff(a2);
                    if(diff.length<=0){
                        params.contributors_remove = '';
                    } else {
                        for (var i = 0; i < diff.length; i++) {
                            if(diff[i] == undefined){
                                delete diff[i];
                            }
                        }
                        params.contributors_remove = diff.join(',');
                        params.contributors_remove = params.contributors_remove.replace(/,\s*$/, "");   /*remove last comma which is getting appended*/
                    }
                    $scope.showBtn = false;
                    contractService.addContributors(params).then(function (result) {
                        if (result.status) {
                            $rootScope.toast('Success', result.message);
                            var obj = {};
                            obj.action_name = 'update';
                            obj.action_description = 'update$$Cotributors$$('+$stateParams.name +' - '+ $stateParams.mName+')';
                            obj.module_type = $state.current.activeLink;
                            obj.action_url = $location.$$absUrl;
                            $rootScope.confirmNavigationForSubmit(obj);
                            $scope.getTopicQuestions(params); //not required
                            $scope.cancel();
                        } else {
                            $rootScope.toast('Error', result.error,'error');
                        }
                    });
                }
                $scope.remove = function(index){
                    $scope.data.contributors.splice(index, 1);
                };
            }
        });
        modalInstance.result.then(function ($data) {
        }, function () {
        });
    };
    Array.prototype.diff = function(a) {
        return this.filter(function(i) {return a.indexOf(i) < 0;});
    };
    $scope.addFile = function (){ $scope.showFileToAttach = true;}
    $scope.removeFile = function (){ $scope.showFileToAttach = false;}
    $scope.addAttachment = function(file){
        var file = file;
        if(file){
            Upload.upload({
                url: API_URL+'Document/add',
                data:{
                    file:file,
                    customer_id : $scope.user1.customer_id,
                    module_id : $scope.contract_review_id,
                    module_type : 'contract_review',
                    reference_id : decode($stateParams.tId),
                    reference_type : 'topic',
                    uploaded_by : $scope.user1.id_user,
                    user_role_id  : $scope.user1.user_role_id,
                }
            }).then(function (resp) {
                if(resp.data.status){
                    $rootScope.toast('Success',resp.data.message);
                    var obj = {};
                    obj.action_name = 'upload';
                    obj.action_description = 'upload$$Attachments$$('+$stateParams.name +' - '+ $stateParams.mName+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    $scope.removeFile();
                    $scope.getAttachmentList($scope.tableDocStateRef);
                }
                else $rootScope.toast('Error',resp.data.error,'error',$scope.user);
            }, function (resp) {
                $rootScope.toast('Error',resp.data.error,'error');
            }, function (evt) {
                $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
            });

        }else{
            $rootScope.toast('Error','invalid format','image-error');
        }
    }

    $scope.deleteDocument = function(row,name){
        var r=confirm("Do you want to continue?");
        $scope.deleConfirm = r;
        if(r==true){
            attachmentService.deleteAttachments({'id_document': row.id_document}).then (function(result){
                if(result.status){
                    var obj = {};
                    obj.action_name = 'delete';
                    obj.action_description = 'delete$$Attachment$$('+name+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    $rootScope.toast('Success',result.data.message);
                    $scope.getAttachmentList($scope.tableDocStateRef);
                    $scope.getTopicQuestions(Globalparams);
                }
            })
        }
    }

    $scope.deleteContractActionItem = function(row){
        var r=confirm("Do you want to continue?");
        $scope.deleConfirm = r;
        if(r==true){
            var params ={};
            params.id_contract_review_action_item  = row.id_contract_review_action_item ;
            params.updated_by  = $rootScope.id_user ;
            contractService.deleteActionItem(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success', result.message);
                    var obj = {};
                    obj.action_name = 'delete';
                    obj.action_description = 'delete$$Action Item$$('+row.action_item+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    $scope.reviewAction($scope.tableStateRef);
                }else $rootScope.toast('Error', result.error, 'error',$scope.user);
            });
        }
    }

    $scope.cancel = function(){
        $state.go('app.contract.contract-review', {name:$stateParams.name,id: $stateParams.id, rId:$stateParams.rId});
    }

    $scope.goToSave = function(data, options, opt){
        if(data){
            var params = {};
            params.contract_review_id = decode($stateParams.rId);
            params.created_by = $rootScope.id_user;
            $scope.options = {};
            angular.forEach($scope.contractModuleTopics.topics[0].questions, function(i,o){
                $scope.options[o] = {};
                $scope.options[o].question_id = i.id_question;
                $scope.options[o].parent_question_id = i.parent_question_id;
                if(data.answers[i.id_question])
                    $scope.options[o].question_answer = data.answers[i.id_question];
                else $scope.options[o].question_answer = '';
                if(data.feedback[i.id_question])
                    $scope.options[o].question_feedback = data.feedback[i.id_question];
                else $scope.options[o].question_feedback = '';
            });
            params.data = $scope.options;
            contractService.answerQuestion(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success', result.message);
                    var obj = {};
                    obj.action_name = 'save';
                    obj.action_description = 'save$$module$$questions$$('+$stateParams.mName+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    params.module_id = $scope.module_id;
                    params.contract_id = $scope.contract_id;
                    params.id_topic = decode($stateParams.tId);
                    if(options!=''){
                        if(opt == 'next'){
                            $state.go('app.contract.contract-module-review', options);
                        } else if(opt == 'exit') {
                            $state.go('app.contract.contract-review', options);
                        }

                    }
                    $scope.getTopicQuestions(Globalparams);
                    //$scope.getTopicQuestions(params); //not required
                } else $rootScope.toast('Error', result.error, 'error');
            });
        }
    }
    $scope.save = function(options){
        $scope.goToSave(options,'','');
        var params = {};
        params.contract_review_id = decode($stateParams.rId);
        params.module_id = $scope.module_id;
        params.contract_id = $scope.contract_id;
        params.id_topic = decode($stateParams.tId);
        //$scope.getTopicQuestions(params); //not required
    }
    $scope.saveAndNext = function(obj,options){
        $scope.goToSave(options, {name:$stateParams.name,
            id:$stateParams.id,
            rId:$stateParams.rId,
            mName:$stateParams.mName,
            moduleId:$stateParams.moduleId,
            tName:obj.next_text,tId:encode(obj.next)}, 'next');
        var params = {};
        params.contract_review_id = decode($stateParams.rId);
        params.module_id = $scope.module_id;
        params.contract_id = $scope.contract_id;
        params.id_topic = decode($stateParams.tId);
        //$scope.getTopicQuestions(params); //not required
        //$scope.goToNextTopic(obj);
        /*$state.go('app.contract.contract-module-review',
            {name:$stateParams.name,id:$stateParams.id,rId:$stateParams.rId,mName:$stateParams.mName,moduleId:$stateParams.moduleId,
                tName:obj.next_text,tId:encode(obj.next)});*/

    }
    $scope.saveAndExit = function(options){
        $scope.goToSave(options, {name:$stateParams.name,id: $stateParams.id, rId:$stateParams.rId}, 'exit');
        /*$state.go('app.contract.contract-review', {name:$stateParams.name,id: $stateParams.id, rId:$stateParams.rId});*/
    }

    $scope.addQuestionAttachemnts = function(row) {
        // row.question_text
        // row.id_question
        $scope.selectedRow = row;
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'right-panel-modal modal-open',
            templateUrl: 'views/Manage-Users/contracts/question-level-attachments.html',
            controller: function ($uibModalInstance, $scope, item) {
                $scope.update = false;
                $scope.bottom = 'general.save';
                $scope.isEdit = false;
                if (item) {
                    $scope.isEdit = true;
                    $scope.getQuestionAttachmentsList = function(id){
                        var param ={};
                        param. customer_id = $scope.user1.customer_id;
                        param.module_id = decode($stateParams.rId);
                        param.module_type =  'contract_review';
                        param.page_type = 'contract_review';
                        param.reference_id = id;
                        param.reference_type = 'question';
                        param.id_user  = $rootScope.id_user;
                        param.user_role_id  = $rootScope.user_role_id;
                        contractService.getAttachments(param).then(function(result){
                            $scope.questionAttachmentList = result.data.data;
                        });
                    }
                    $scope.getQuestionAttachmentsList(row.id_question);
                }
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
                $scope.deleteQuestionAttachment = function(id,name){
                    var r=confirm("Do you want to continue?");
                    $scope.deleConfirm = r;
                    if(r==true){
                        var params = {};
                        params.id_document = id;
                        attachmentService.deleteAttachments(params).then (function(result){
                            $rootScope.toast('Success',result.data.message);
                            var obj = {};
                            obj.action_name = 'delete';
                            obj.action_description = 'delete$$module$$question$$attachement$$('+name+')';
                            obj.module_type = $state.current.activeLink;
                            obj.action_url = $location.$$absUrl;
                            $rootScope.confirmNavigationForSubmit(obj);
                            $scope.getQuestionAttachmentsList(row.id_question);
                            $scope.getAttachmentList($scope.tableDocStateRef);
                            contractService.getcontractReviewModules(Globalparams).then(function(result){
                                angular.forEach(result.data[0].topics[0].questions, function(i,o){
                                    $scope.contractModuleTopics.topics[0].questions[o].attachment_count = i.attachment_count;
                                });
                            });
                        })
                    }
                }
                var params ={};
                $scope.addQuestionAttachemts=function(data){
                    var file = data;
                    if(file){
                        Upload.upload({
                            url: API_URL+'Document/add',
                            data:{
                                file:file,
                                customer_id: $scope.user1.customer_id,
                                module_id: decode($stateParams.rId),
                                module_type: 'contract_review',
                                reference_id: row.id_question,
                                reference_type: 'question',
                                uploaded_by: $scope.user1.id_user
                            }
                        }).then(function (resp) {
                            if(resp.data.status){
                                $scope.getAttachmentList($scope.tableDocStateRef);
                                $rootScope.toast('Success',resp.data.message);
                                var obj = {};
                                obj.action_name = 'upload';
                                obj.action_description = 'upload$$module$$question$$attachement$$('+$stateParams.mName+')';
                                obj.module_type = $state.current.activeLink;
                                obj.action_url = $location.$$absUrl;
                                $rootScope.confirmNavigationForSubmit(obj);
                                $scope.removeFile();
                                contractService.getcontractReviewModules(Globalparams).then(function(result){
                                    angular.forEach(result.data[0].topics[0].questions, function(i,o){
                                        $scope.contractModuleTopics.topics[0].questions[o].attachment_count = i.attachment_count;
                                    });
                                });
                                $scope.cancel();
                            }
                            else $rootScope.toast('Error',resp.data.error,'error',$scope.user);
                        }, function (resp) {
                            $rootScope.toast('Error',resp.data.error,'error');
                        }, function (evt) {
                            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                        });

                    }else{
                        $rootScope.toast('Error','invalid format','image-error');
                    }
                }
            },
            resolve: {
                item: function () {
                    if ($scope.selectedRow) {
                        return $scope.selectedRow;
                    }
                }
            }
        });
        modalInstance.result.then(function ($data) {
        }, function () {
        });
    }
})
.controller('contractDashboardCtrl', function($scope, $rootScope, $state, $stateParams, contractService, decode, encode, $uibModal,$location,userService,AuthService){
    $scope.dashboardData = {};
    $rootScope.module = 'Contract Dashboard';
    $rootScope.displayName = $stateParams.name;
    var params={};
    params.contract_id = decode($stateParams.id);
    params.contract_review_id = decode($stateParams.rId);
    params.id_user  = $scope.user1.id_user;
    params.user_role_id  = $scope.user1.user_role_id;
    $scope.showData={};
    contractService.getDashboard(params).then(function(result){
        if(result.status)
            $scope.dashboardData =  result.data;
       // $scope.showData = $scope.dashboardData.modules;
    })
    $scope.init = function (tableState){
        $scope.contract_id = decode($stateParams.id);
        var params = {};
        params.id_contract  = $scope.contract_id;
        params.id_user  = $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        contractService.getContractById(params).then (function(result){
            $scope.contractInfo = $scope.data = result.data[0];
        })
    }
    $scope.init();
    $scope.goToNext = function(data){
        $state.go('app.contract.contract-dashboard', {name:$stateParams.name,id: $stateParams.id, rId:encode(data.next)});
    }
    $scope.goToPrev = function(data){
        $state.go('app.contract.contract-dashboard', {name:$stateParams.name,id: $stateParams.id, rId:encode(data.prev)});
    }

    $scope.showTopicQuestions = function(data,topic) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'modal-open questions-modal',
            templateUrl: 'view-topic-questions.html',
            size: 'lg',
            controller: function ($uibModalInstance, $scope) {
                $scope.load = true;
                $scope.title = topic.topic_name;
                var params= {};
                params.contract_review_id = decode($stateParams.rId);
                params.module_id = data.module_id;
                params.contract_id = decode($stateParams.id);
                params.id_topic = topic.topic_id;
                    contractService.getTopicQuestionsById(params).then(function(result){
                        $scope.contractModuleTopics = result.data.questions;
                        $scope.load = false;
                    });
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };

            }
        });
        modalInstance.result.then(function ($data) {
        }, function () {
        });
    }

    $scope.goToChangeLog = function(){
        $state.go('app.contract.review-change-log', {'name': $stateParams.name,id:$stateParams.id,rId:encode($scope.dashboardData.contract_review_id)});
    }
    $scope.exportReview = function (){
        var params={};
        params.contract_id = params.id_contract= decode($stateParams.id);
        params.id_user=  $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        params.contract_review_id = $scope.dashboardData.contract_review_id;
        contractService.exportReviewData(params).then(function(result){            
            if(result.status){
                var obj = {};
                obj.action_name = 'export';
                obj.action_description = 'export$$contract$$review$$('+$stateParams.name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';
                $rootScope.toast('Success',result.message);
                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.file_path =  GibberishAES.enc(result.data.file_path, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file_name =  GibberishAES.enc(result.data.file_name, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.file_path+'&name='+result.data.file_name;
                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;
                    }
                });
            }
            else $rootScope.toast('Error',result.error,'error');
        })
    }
    $scope.exportDashboardData = function (){
        var params={};
        params.contract_id = decode($stateParams.id);
        params.contract_review_id = decode($stateParams.rId);
        params.id_user  = $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        contractService.exportDashboardData(params).then(function(result){            
            if(result.status){
                var obj = {};
                obj.action_name = 'export';
                obj.action_description = 'export$$contract$$review$$('+$stateParams.name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';
                $rootScope.toast('Success',result.message);
                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.file_path =  GibberishAES.enc(result.data.file_path, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file_name =  GibberishAES.enc(result.data.file_name, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.file_path+'&name='+result.data.file_name;
                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;
                    }
                });
            }
            else $rootScope.toast('Error',result.error,'error');
        })
    }
    $scope.downloadAttachment = function(objData){
        var d = {};
        d.id_document = objData.id_document;
        contractService.getUrl(d).then(function (result) {
            if(result.status){
                var obj = {};
                obj.action_name = 'download';
                obj.action_description = 'download$$contract$$review$$question$$attachment$$('+objData.document_name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';
                $rootScope.toast('Success',result.message);
                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.url =  GibberishAES.enc(result.data.url, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file =  GibberishAES.enc(result.data.file, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.url+'&name='+result.data.file;
                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;
                    }
                });
            }
        });
    };
    $scope.isNaN= function (n) {
        return isNaN(n);
    }
})
.controller('ReviewDesign', function($scope, $rootScope,$state, $stateParams, encode, decode, contractService, $window, $location){
    $rootScope.module = 'Contract Review Discussion';
    $rootScope.displayName = $stateParams.name;

    $scope.init = function (tableState){
        $scope.loading = false;
        $scope.contract_id = decode($stateParams.id);
        var params = {};
        params.id_contract  = $scope.contract_id;
        params.id_user  = $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        contractService.getContractById(params).then (function(result){
            $scope.contractInfo = $scope.data = result.data[0];
            $scope.loading = true;
        })
    }
    $scope.init();

    $scope.getDiscussionQuestions = function (open_module){
        var params ={};
        params.contract_id = params.id_contract = decode($stateParams.id);
        params.id_user = $scope.user1.id_user;
        params.user_role_id = $scope.user1.user_role_id;
        contractService.discussDetails(params).then(function(result){
            $scope.loading = false;
            if(result.status){
                $scope.discussionData = result.data;
                $scope.loading = true;
                angular.forEach($scope.discussionData.review_discussion, function(item,key){
                    $scope.discussionData.review_discussion[key].open = false;
                    if(open_module){
                        if($scope.discussionData.review_discussion[key].id_module == open_module){
                            $scope.discussionData.review_discussion[key].open = true;
                        }
                    }else{$scope.discussionData.review_discussion[key].open = false;}
                });
            }
        });
    }
    $scope.getDiscussionQuestions();
  /*  $scope.initiateQuestionComments = function(questionData){
        var params ={};
        params.review_discussion = [];
        params.contract_id = params.id_contract = decode($stateParams.id);
        params.contract_review_id =  $scope.discussionData.contract_review_id;
        params.created_by = $scope.user.id_user;
        params.user_role_id = $scope.user.user_role_id;
        angular.forEach(questionData, function(item,key){
            if(key == 'topics'){
                angular.forEach(item,function(it,k){
                    angular.forEach(it.questions, function(row,val){
                        if(row.id_contract_review_discussion_question){
                            if(row.remarks){
                                params.review_discussion.push(row);
                            }
                        }else{
                            if(row.status == 1){
                                if(row.remarks){
                                    params.review_discussion.push(row);
                                }
                            }
                        }
                    });
                });
            }
        });
        if(params.review_discussion.length >0){
            contractService.postdiscussion(params).then(function(result){
                if(result.status){
                    setTimeout(function(){
                        $scope.getDiscussionQuestions(result.data.recent_module_id);
                    },500);
                    $rootScope.toast('Success', result.message);
                }
                else $rootScope.toast('Error', result.error,'error');
            });
        }else{$rootScope.toast('Warning','please enter comments for atleast one question', 'warning');}
    }*/
    $scope.saveQuestionComments = function(questionData,type){
        var params ={};
        params.review_discussion = [];
        params.contract_id = params.id_contract = decode($stateParams.id);
        params.contract_review_id =  $scope.discussionData.contract_review_id;
        params.created_by = $scope.user.id_user;
        params.user_role_id = $scope.user.user_role_id;
        angular.forEach(questionData, function(item,key){
            if(key == 'topics'){
                angular.forEach(item,function(it,k){
                    angular.forEach(it.questions, function(row,val){
                        if(row.id_contract_review_discussion_question){
                            if(row.remarks && row.status == 1){
                                params.review_discussion.push(row);
                            }
                            if(row.status == 0){
                                params.review_discussion.push(row);
                            }
                        }else{
                            if(row.status == 1){
                                if(row.remarks){
                                    params.review_discussion.push(row);
                                }
                            }
                        }
                    });
                });
            }
        });
        if(params.review_discussion.length >0){
            contractService.postdiscussion(params).then(function(result){
                if(result.status){
                    setTimeout(function(){
                        $scope.getDiscussionQuestions(result.data.recent_module_id);
                    },500);
                    $rootScope.toast('Success', result.message);
                    if(type == 'init') $scope.action = 'Initiate';
                    else $scope.action= 'Save';
                    var obj = {};
                    obj.action_name = $scope.action;
                    obj.action_description = $scope.action+'$$Discussion$$('+questionData.module_name+'-'+$stateParams.name+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);

                }
                else $rootScope.toast('Error', result.error,'error');
            });
        }else{$rootScope.toast('Warning','Please enter comments for at least one question', 'warning');}
    }
    $scope.closeDiscussion = function (data,$event){
        var params1 = {};
        params1.contract_review_discussion_id = data.id_contract_review_discussion;
        params1.contract_review_id =  $scope.discussionData.contract_review_id;
        params1.created_by = $scope.user.id_user;
        params1.module_id = data.id_module;
        params1.contract_id = params1.id_contract= decode($stateParams.id);
        var r=confirm("Do you want to close discussion for this module ?");
        $scope.deleConfirm = r;
        if(r==true){
            contractService.closediscussion(params1).then(function(result){
                if(result.status){
                    $scope.getDiscussionQuestions();
                    $rootScope.toast('Success',result.message);
                    var obj = {};
                    obj.action_name = 'close';
                    obj.action_description = 'close$Discussion$$('+data.module_name+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    //$state.go('app.contract.contract-review',{name:$stateParams.name,id:$stateParams.id,rId:$stateParams.rId});
                }else $rootScope.toast('Error',result.error,'error');
            });
        }
    }
    $scope.getClosedDiscussions = function(closedId){
        var params ={};
        params.contract_id = params.id_contract = decode($stateParams.id);
        //params.contract_review_id =  decode($stateParams.rId);
        params.id_user = $scope.user1.id_user;
        params.user_role_id = $scope.user1.user_role_id;
        if(closedId) params.id_contract_review_discussion = closedId;
        contractService.discussDetails(params).then(function(result){
            $scope.loading = false;
            if(result.status){
                $scope.discussionData = result.data;
                $scope.loading = true;
                angular.forEach($scope.discussionData.review_discussion, function(item,key){
                    $scope.discussionData.review_discussion[key].open = false;
                });
            }
        });
        //$scope.getDiscussionQuestions();
    }
    $scope.reloadDiscussions = function (){
        $window.location.reload();
    }
})
.controller('ReviewChangeLogCtrl', function($scope, $rootScope, $state, $stateParams, encode, decode, contractService, userService, AuthService){
    $scope.getFileList = function(tableState){
        $scope.tableStateRef = tableState;
        $scope.isLoading = true;
        var pagination = tableState.pagination;
        tableState.module_id = decode($stateParams.rId);
        tableState.module_type =  'contract_review';
        tableState.id_user  = $rootScope.id_user;
        tableState.user_role_id  = $rootScope.user_role_id;
        tableState.page_type  = 'contract_overview';
        tableState.contract_id  = decode($stateParams.id);
        tableState.deleted  = 0;
        tableState.updated_by = 1;
        contractService.getFileLogs(tableState).then(function(result){
            $scope.FileList = result.data.data;
            console.log($scope.FileList);
            $scope.emptyTable=false;
            tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
            $scope.isLoading = false;
            if(result.data.total_records < 1)
                $scope.emptyTable=true;
        })
    }
    $rootScope.module = 'Change Log';
    $rootScope.displayName = $stateParams.name;
    $scope.contract_name = $stateParams.name;
    $scope.getTopics = function(id,type){
        var param ={};
        param.contract_review_id = decode($stateParams.rId);
        param.id_user  = $scope.user1.id_user;
        param.user_role_id  = $scope.user1.user_role_id;
        if(id){
            if($scope.change.module) param.id_module  = $scope.change.module;
            if($scope.change.topic) param.id_topic  = $scope.change.topic;
        }else {
            param.id_module  = 'all';
           if(type == 'module') param.id_module  = 'all';
           if(type == 'topic') param.id_module  = $scope.change.module;
        }
        contractService.getchangeLogs(param).then(function(result){
            $scope.modules = result.data.modules;
            if(param.id_module != undefined)
                $scope.topics = result.data.topics;
            $scope.questionsList = result.data.questions;
            $scope.contractInfo = result.data.review_information;
            $scope.noQuestions = false ;
            if(result.data.questions.length == 0) $scope.noQuestions = true;
        });
    }
    $scope.getTopics(0,'all');
    $scope.getDownloadUrl = function(objData){
        var d = {};
        d.id_document = objData.id_document;
        contractService.getUrl(d).then(function (result) {
            if(result.status){
                var obj = {};
                obj.action_name = 'download';
                obj.action_description = 'download$$attachment$$('+ objData.document_name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';

                $rootScope.toast('Success',result.message);

                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.url =  GibberishAES.enc(result.data.url, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file =  GibberishAES.enc(result.data.file, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.url+'&name='+result.data.file;
                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;
                    }
                });
                $scope.getFileList($scope.tableStateRef);
            }
        });
    };
})
.controller('contractLogCtrl', function($state, $scope, $rootScope, $stateParams, encode, decode, contractService,userService,AuthService){
    $rootScope.module = 'Contract Logs';
    $rootScope.displayName = $stateParams.name;
    contractService.getLogs({'contract_id':decode($stateParams.id)}).then (function(result){
        if(result.status){
            $scope.currentContract = result.data.current_cotract_detailis;
            $scope.contractLogOptions = result.data.contract_log_options;
        }
    });
    $scope.getContractLogs = function(logId) {
        var param = {};
        param.contract_log_id = logId;
        param.contract_id = decode($stateParams.id);
        contractService.getLogs(param).then (function(result){
            if(result.status){
                $scope.currentContract = result.data.current_cotract_detailis;
                $scope.contractLogOptions = result.data.contract_log_options;
                $scope.contractLogDetails = result.data.contract_log_details;
            }
        });
    }
    $scope.FileList= [];
    $scope.getFileList = function(){
        $scope.isLoading = true;
        var params = {};
            params.id_contract = decode($stateParams.id);
            params.id_user  = $scope.user1.id_user;
            params.user_role_id  = $scope.user1.user_role_id;
            params.deleted = 0;
            params.updated_by = 1;
        contractService.getContractById(params).then(function(result){
            if(result.data.length > 0){
                if(result.data[0]['attachment']){
                    $scope.FileList = result.data[0].attachment;
                }
            }
            $scope.isLoading = false;
        });
    }
    $scope.getFileList();
    $scope.getDownloadUrl = function(objData){
        var d = {};
        d.id_document = objData.id_document;
        contractService.getUrl(d).then(function (result) {
            if(result.status){
                var obj = {};
                obj.action_name = 'download';
                obj.action_description = 'download$$attachment$$('+ objData.document_name+')';
                obj.module_type = $state.current.activeLink;
                obj.action_url = location.href;
                if(AuthService.getFields().data.parent){
                    obj.user_id = AuthService.getFields().data.parent.id_user;
                    obj.acting_user_id = AuthService.getFields().data.data.id_user;
                }
                else obj.user_id = AuthService.getFields().data.data.id_user;
                if(AuthService.getFields().access_token != undefined){
                    var s = AuthService.getFields().access_token.split(' ');
                    obj.access_token = s[1];
                }
                else obj.access_token = '';

                $rootScope.toast('Success',result.message);

                userService.accessEntry(obj).then(function(result1){
                    if(result1.status){
                        if(DATA_ENCRYPT){
                            result.data.url =  GibberishAES.enc(result.data.url, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                            result.data.file =  GibberishAES.enc(result.data.file, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                        }
                        //window.location = API_URL+'download/downloadreport?path='+result.data.url+'&name='+result.data.file;
                        window.location = API_URL+'download/downloadreportnew?id_download='+result.data+'&user_id='+obj.user_id+'&access_token='+obj.access_token;
                    }
                });
            }
        });
    };
})
.controller('subContractCreateCtrl', function($scope, $rootScope, $state,$stateParams,$location, encode, decode, contractService, customerService, masterService,Upload, $window, dateFilter){
        $scope.currencyList = [];
        $scope.relationshipCategoryList = {};
        $scope.relationshipClassificationList = {};
        $scope.contract = {};
        $rootScope.module = '';
        $rootScope.displayName = '';
        masterService.currencyList().then(function(result){
            $scope.currencyList = result.data;
        });
        contractService.getRelationshipCategory({'customer_id': $scope.user1.customer_id}).then(function(result){
            $scope.relationshipCategoryList = result.data;
        });
        contractService.getRelationshipClassiffication({'customer_id': $scope.user1.customer_id}).then(function(result){
            $scope.relationshipClassificationList = result.data;
        });
        if($stateParams.id){
            $rootScope.module = 'Contract';
            $rootScope.displayName = $stateParams.name;
            var params = {};
            params.id_contract = decode($stateParams.id);
            params.id_user  = $scope.user1.id_user;
            params.user_role_id  = $scope.user1.user_role_id;
            contractService.getContractById(params).then(function(result){
                $scope.contract = result.data[0];
                $scope.contract['contract_name'] = '';
                $scope.contract.contract_start_date = new Date($scope.contract.contract_start_date);
                $scope.contract.contract_end_date = new Date($scope.contract.contract_end_date);
                $scope.contract['auto_renewal'] = false;
                $scope.contract['contract_value'] = '';
                $scope.contract['classification_id'] = '';
                $scope.contract['relationship_category_id'] = '';
                $scope.contract['delegate_id'] = '';
                $scope.contract['description'] = '';
                $scope.contract['internal_contract_sponsor'] = '';
                $scope.contract['provider_contract_sponsor'] = '';
                $scope.contract['internal_partner_relationship_manager'] = '';
                $scope.contract['provider_partner_relationship_manager'] = '';
                $scope.contract['internal_contract_responsible'] = '';
                $scope.contract['provider_contract_responsible'] = '';
                $scope.contract['attachment'] = '';
                //$scope.file['attachment'] = '';
                $scope.getContractDelegates($scope.contract.business_unit_id,$scope.contract.id_contract);
                $scope.contract['parent_contract_id'] = params.id_contract;
                $scope.dat = ($scope.contract['contract_end_date'] - new Date("1970-01-01"))/1000/60/60/24;
                if($scope.dat==0)
                    $scope.contract['contract_end_date']='';
            });
        }
        $scope.title = 'general.create';
        $scope.bottom = 'general.save';
        $scope.getContractDelegates = function (id,contractId){
            contractService.getDelegates({'id_business_unit': id}).then(function(result){
                $scope.delegates = result.data;
            });
            var params = {};
            params.business_unit_id = id;
            params.contract_id = contractId;
            params.type = "buowner";
            contractService.getbuOwnerUsers(params).then(function(result){
                $scope.buOwnerUsers = result.data;
            });
        }
        $scope.subContractCreate = function (contract){
            //delete contract.attachments;
            contract.created_by = $scope.user.id_user;
            contract.customer_id = $scope.user1.customer_id;
            if($scope.user.access =='bo')
                contract.contract_owner_id = $scope.user.id_user;
            else contract.contract_owner_id = contract.contract_owner_id;
            $scope.contract['auto_renewal'] = $scope.contract['auto_renewal']==true?'1':'0';
            contract.attachment_delete = [];
            angular.forEach($scope.file.delete, function(i,o){
                var obj = {};
                obj.id_document = i.id_document;
                contract.attachment_delete.push(obj) ;
            });
            var params = {};
            angular.copy(contract,params);
            params.updated_by = $scope.user.id_user;
            params.contract_end_date = dateFilter(params.contract_end_date,'yyyy-MM-dd');
            params.contract_start_date = dateFilter(params.contract_start_date,'yyyy-MM-dd');
            if(new Date( params.contract_end_date) <= new Date( params.contract_start_date)){
                alert('Start date should be less than End date');
            }else{
                if(!params.parent_contract_id){
                    params.parent_contract_id = $scope.contract.id_contract;
                }
                if (params.id_contract) {
                    delete params.id_contract;
                }
                Upload.upload({
                    url: API_URL+'Contract/add',
                    data: {
                        'file' : $scope.file.attachment,
                        'contract': params
                    }
                }).then(function(resp){
                    if(resp.data.status){
                        $state.go('app.contract.view',{name: resp.data.contract_data.contract_name,
                                                        id: encode(resp.data.contract_data.contract_id)});
                        $rootScope.toast('Success',resp.data.message);
                        var obj = {};
                        obj.action_name = 'update';
                        obj.action_description = 'update$$contract$$'+contract.contract_name;
                        obj.module_type = $state.current.activeLink;
                        obj.action_url = $location.$$absUrl;
                        $rootScope.confirmNavigationForSubmit(obj);
                    }else{
                        $rootScope.toast('Error',resp.data.error,'error',$scope.contract);
                    }
                },function(resp){
                    $rootScope.toast('Error',resp.error);
                },function(evt){
                    var progressPercentage=parseInt(100.0*evt.loaded/evt.total);
                });
            }
        }
        $scope.cancel = function(){
            //$window.history.back();
            $state.go('app.contract.contract-overview');
        }
    })
.controller('contractFileLogCtrl', function($state, $scope, $rootScope, $stateParams, encode, decode, contractService){
    $scope.FileList = [];
    $scope.getFileList = function(tableState){
        $scope.tableStateRef = tableState;
        $scope.isLoading = true;
        var pagination = tableState.pagination;
        tableState.module_id = decode($stateParams.rId);
        tableState.module_type =  'contract_review';
        tableState.id_user  = $rootScope.id_user;
        tableState.user_role_id  = $rootScope.user_role_id;
        tableState.page_type  = 'contract_overview';
        tableState.contract_id  = decode($stateParams.id);
        tableState.deleted  = 0;
        contractService.getFileLogs(tableState).then(function(result){
            $scope.FileList = result.data.data;
            $scope.emptyTable=false;
            tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
            $scope.isLoading = false;
            if(result.data.total_records < 1)
                $scope.emptyTable=true;
        })
    }
})