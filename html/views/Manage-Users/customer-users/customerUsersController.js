angular.module('app')
.controller('UserCtrl', function($timeout, $scope, $rootScope, $stateParams, $state, $localStorage,encode, decode, customerService, masterService, businessUnitService, userService, $location,$window){
    $scope.dynamicPopover = {templateUrl: 'myPopoverTemplate.html'};
    $scope.customer_user = $localStorage.curUser.data.data;
    $scope.usersList = {};
    businessUnitService.list({'user_role_id':$scope.user1.user_role_id,'customer_id': $scope.customer_user.customer_id, status: 1,id_user:$scope.user1.id_user}).then(function(result){
        $scope.bussinessUnit = result.data.data;
        if($stateParams.buId){
            $scope.business_unit_id = decode($stateParams.buId);
        }
    });
    $scope.showForm = function(row){

        if(row){
            var user_id = encode(row.id_user);
            $state.go('app.customer-user.edit-customer-user',{id:encode($scope.customer_user.customer_id),userId:user_id});
        }
        else
            $state.go('app.customer-user.create-customer-user',{id:encode($scope.customer_user.customer_id)});
    }
    $scope.callServer = function callServer(tableState) {
        $rootScope.module = '';
        $rootScope.displayName = '';
        $scope.tableStateRef = tableState;
        $scope.isLoading = true;
        var pagination = tableState.pagination;
        tableState.customer_id = $scope.customer_user.customer_id;
        tableState.user_role_id = $scope.user1.user_role_id;
        tableState.id_user = $scope.user1.id_user;
        if(tableState.search && tableState.search.predicateObject && tableState.search.predicateObject.business_unit_id && $scope.business_unit_id!=''){
            tableState.business_unit_id = $scope.business_unit_id;
        }
        else if($stateParams.buId){
            $scope.business_unit_id = decode($stateParams.buId);
            tableState.business_unit_id = decode($stateParams.buId);
        }
        else{
            delete tableState.business_unit_id;
        }
        customerService.getUserList(tableState).then(function (result){
            $scope.usersList = result.data.data;
            $scope.emptyTable=false;
            tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
            $scope.isLoading = false;
            if(result.data.total_records < 1)
                $scope.emptyTable=true;
        });
    };
    $scope.delete = function(row){
        var params = {};
        params.id_user = row.id_user;
        customerService.deleteUser(params).then(function (result){
            if (result.status) {
                $rootScope.toast('Success', result.message);
                $scope.callServer($scope.tableStateRef);
            } else {
                $rootScope.toast('Error', result.error,'error');
            }
        });
    }

    $scope.loginAsAdmin = function (row) {
        userService.loginAs({'id_user':row.id_user}).then(function(result){
            if(result.status){
                $localStorage.curUser.data.parent = $localStorage.curUser.data.data;
                $localStorage.curUser.data.data = result.data.data;
                $localStorage.curUser.data.menu = result.data.menu;
                //$window.location.href = APP_DIR;
                $timeout(function(){
                    window.location.href = APP_DIR;
                },2000);
            }
        });
    }
    $scope.unblock = function (row) {
        var params ={};
        params.email = row.email;
        userService.unBlock(params).then(function(result){
            if(result.status){
                $rootScope.toast('User unblocked', result.message);
                $scope.callServer($scope.tableStateRef);
            } else {
                $rootScope.toast('Error', result.error,'error');
            }
        });
    }

})
.controller('manageUserCtrl', function($scope,$rootScope, $state, decode,masterService ){
    $scope.userRoles = {};
    var param = {};
    param.user_role_id = $scope.user1.user_role_id;
    masterService.getUserRole(param).then(function(result){
        $scope.userRoles = result.data;
    });
})
.controller('addCustomUserCtrl', function($scope, $rootScope, $state, $window, $localStorage,  encode, decode, customerService ,$stateParams, businessUnitService, $location){
        $scope.customer_user = $localStorage.curUser.data.data;
        $scope.customer_id = decode($stateParams.id);
        //$scope.customerId = decode($stateParams.id);
        $scope.user_id = decode($stateParams.userId);
        $scope.customUser = {};
        $scope.title = "general.create";
        $scope.bottom="general.save";
        if($scope.user_id){
            $scope.title = "general.edit";
            $scope.bottom="general.update";
            var param ={};
            param.customer_id = $scope.customer_id;
            param.user_id = $scope.user_id;
            customerService.getUserById(param).then(function(result){
                $scope.customUser = result.data;
                if(result.data.user_role_id){
                    console.log(result.data.user_role_id);
                    for(var a in $scope.userRoles){
                        if($scope.userRoles[a].id_user_role == result.data.user_role_id)
                            $scope.customUser.user_role_id = $scope.userRoles[a];
                    }
                }
                $rootScope.module = 'User';
                $rootScope.displayName = $scope.customUser.first_name+" "+ $scope.customUser.last_name;
                var bussiness_unit = [];
                for(var a in $scope.customUser.business_unit){
                    bussiness_unit.push($scope.customUser.business_unit[a].business_unit_id);
                }
                $scope.customUser.business_unit = bussiness_unit;
            });
        }

        businessUnitService.list({'user_role_id':$scope.user1.user_role_id, 'customer_id': $scope.customer_user.customer_id, status: 1,id_user:$scope.user1.id_user}).then(function(result){
            $scope.bussinessUnit = result.data.data;
        });

        $scope.addUser =  function (customUser){
            var params ={};
            params = customUser;
            /*params.id_user_role = params.user_role_id.id_user_role;*/
            params.user_role_id = params.user_role_id.id_user_role;
            params.created_by = $scope.user.id_user;
            params.customer_id = $scope.customer_id;
            if(customUser.is_manual == 0){
                delete customUser.password;
                customUser.is_manual_password = 0;
            }else{
                customUser.is_manual_password = 1;
            }
            /*if(customUser.is_allow_all_bu == 1){
                customUser.business_unit = '';
            }*/
            customerService.postUser(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    var obj = {};
                    if(customUser.id_user>0)$scope.action = 'update';
                    else $scope.action = 'create';
                    obj.action_name = $scope.action;
                    obj.action_description = $scope.action+'$$user$$('+customUser.first_name+' '+customUser.last_name+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    $state.go('app.customer-user.list');
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.user);
                }

            });
        }
        $scope.resetPassword = function(userPwd){
            var params ={};
            params.customer_id = $scope.customer_id;
            params.user_id = $scope.user_id;
            params.password = userPwd.npassword;
            params.cpassword = userPwd.cpassword;
            customerService.resetPassword(params).then (function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    var obj = {};
                    obj.action_name = 'update';
                    obj.action_description = 'update$$user$$password$$('+$scope.customUser.first_name+' '+$scope.customUser.last_name+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    $state.go('app.manage-user.user-list',{name:$stateParams.name,id:encode($scope.customer_id)});
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.user);
                }
            });
        }
        $scope.cancel = function(){
            //$window.history.back();
            $state.go('app.customer-user.list');
        }
        $scope.changeBussinessUnit = function(){
            console.log('$scope.customUser.is_allow_all_bu',$scope.customUser.is_allow_all_bu);
        }
    })