angular.module('app',['ng-fusioncharts'])
    .controller('customerRelationCategoryCtrl', function ($state, $rootScope, $scope, $uibModal, relationCategoryService,$location) {
        $scope.myDataSource = {};
        $scope.callServer = function callServer(tableState)
        {
            $rootScope.displayName ='';
            $rootScope.module = '';
            $scope.tableStateRef=tableState;
            $rootScope.bredCrumbLabel = '';
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            tableState.customer_id = $scope.user1.customer_id;
            relationCategoryService.list(tableState).then(function (result){
                $scope.displayed = result.data.data;
                $scope.data = result.data.data;
                tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
                $scope.isLoading = false;
                $scope.myDataSource = result.data.graph;
            });
        };
        $scope.goToClassifications = function (){
            $state.go('app.customer-relationship_category.customer-relationship_classification');
        }
        $scope.updateCategory = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'views/Manage-Users/relationship-category/create-edit-customer-category.html',
                controller: function ($uibModalInstance, $scope, item) {
                    if (item) {
                        $scope.isEdit = true;
                        $scope.submitStatus = true;
                        $scope.category = angular.copy(item);
                        $scope.update = true;
                        $scope.title = 'general.edit';
                        $scope.bottom = 'general.update';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                    var params ={};
                    $scope.update=function(category){
                        if(typeof category.id_relationship_category!='undefined' && ((isNaN(category.id_relationship_category)===false && category.id_relationship_category > 0) || (isNaN(category.id_relationship_category)===true && category.id_relationship_category.length > 0))){
                            params = category;
                            params.created_by = $scope.user.id_user;
                            params.updated_by = $scope.user.id_user;
                            params.customer_id = $scope.user1.customer_id;
                            relationCategoryService.update(params).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    var obj = {};
                                    obj.action_name = 'update';
                                    obj.action_description = 'update$$relationship category$$('+item.relationship_category_name+')';
                                    obj.module_type = $state.current.activeLink;
                                    obj.action_url = $location.$$absUrl;
                                    $rootScope.confirmNavigationForSubmit(obj);
                                    $scope.cancel();
                                    $scope.callServer($scope.tableStateRef);
                                } else {
                                    $rootScope.toast('Error', result.error,'error');
                                }
                            });
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
            modalInstance.result.then(function ($data) {
            }, function () {
            });
        };
    })