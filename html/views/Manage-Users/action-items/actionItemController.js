angular.module('app')
.controller('actionItemCtrl' , function($scope, $rootScope,$state, $stateParams, contractService, actionItemsService, encode, decode, $uibModal, $location){
    $rootScope.module = '';
    $rootScope.displayName = '';
    $scope.ActionItemsList = {};
    $scope.providersList = {};
    $scope.show_my_action_items = '';
    $scope.provider_name = '';
    $scope.contract_id = '';
    if($stateParams.cId){$scope.contract_id = decode($stateParams.cId);}
    if($stateParams.status){$scope.item_status = decode($stateParams.status);}
    $scope.ActionItemsTable = function(tableState,id){
        $scope.isLoading = true;
        var pagination = tableState.pagination;
        $scope.tableStateRef = tableState;
        tableState.id_user  = $scope.user1.id_user;
        if(typeof id != 'object')
            tableState.contract_id  = id;
        if($scope.item_status){tableState.contract_review_action_item_status = $scope.item_status;}
        else {tableState.contract_review_action_item_status = 'all';}
        tableState.user_role_id  = $scope.user1.user_role_id;
        tableState.customer_id = $scope.user1.customer_id;
        if($scope.show_my_action_items  && $scope.show_my_action_items != null){
            tableState.show_my_action_items = $scope.show_my_action_items;
        }else{
            delete tableState.show_my_action_items;
            $scope.show_my_action_items = '';
        }
        if($scope.provider_name  && $scope.provider_name != null && $scope.provider_name != 'All'){
            tableState.provider_name = $scope.provider_name;
        }else{
            delete tableState.provider_name;
            $scope.provider_name = '';
        }
        if($scope.contract_id  && $scope.contract_id != null && $scope.contract_id != 'all'){
            tableState.contract_id = $scope.contract_id;
        }else if(typeof id != 'object') {
            tableState.contract_id = id;
            $scope.contract_id = id;
        }else{
            delete tableState.contract_id;
            $scope.contract_id = '';
        }
        contractService.getAllActionItems(tableState).then(function(result){
            $scope.ActionItemsList = result.data.data;
            $scope.isLoading = false;
            $scope.emptyTable=false;
            tableState.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState.pagination.number);
            $scope.isLoading = false;
            if(result.data.total_records < 1)
                $scope.emptyTable=true;
        });
    }
    $scope.goToQuestion = function(row) {

        console.log('row data', row);
        var obj = {};
        obj.action_name = 'view';
        obj.action_description = 'view$$module$$questions$$('+row.module_name+')';
        obj.module_type = $state.current.activeLink;
        obj.action_url = $location.$$absUrl;
        $rootScope.confirmNavigationForSubmit(obj);
        $state.transitionTo('app.contract.contract-module-review',
            {name:row.contract_name,id:encode(row.contract_id),rId:encode(row.contract_review_id),mName:row.module_name,moduleId:encode(row.module_id),
                tName:row.topic_name,tId:encode(row.topic_id)});
    }
    $scope.getFiltersData = function () {
        var params = {};
        if($scope.business_unit_id) {
            params.business_unit_id = $scope.business_unit_id;
        }
        params.customer_id = $scope.user1.customer_id;
        params.id_user  = $scope.user1.id_user;
        params.user_role_id  = $scope.user1.user_role_id;
        actionItemsService.getActionItemFilters(params).then(function(result){
            if(result.status){
                result.data.providers.unshift({'provider_name':'All'});
                result.data.contracts.unshift({'contract_id' : 'all', 'contract_name' : 'All'});
            /*    result.data.providers.push({'provider_name':'All'});
                var obj = {'contract_id' : 'all', 'contract_name' : 'All'};
                console.log('obj',obj);
                result.data.contracts.push(obj);
                $scope.providers = result.data.providers.reverse();
                $scope.contracts = result.data.contracts.reverse();*/
                $scope.providers = result.data.providers;
                $scope.contracts = result.data.contracts;
            }
        });
    }
    $scope.getFiltersData();
    $scope.showMyActionItems = function (val) {
        $scope.show_my_action_items = val;
        setTimeout(function(){
            $scope.ActionItemsTable($scope.tableStateRef);
        },500);
    }
    $scope.providersListTable = function(tableState1){
        $scope.isLoading1 = true;
        var pagination = tableState1.pagination;
        $scope.tableStateRef1 = tableState1;
        tableState1.id_user  = $scope.user1.id_user;
        tableState1.user_role_id  = $scope.user1.user_role_id;
        tableState1.customer_id = $scope.user1.customer_id;
        contractService.contractProviders(tableState1).then(function(result){
            $scope.providersList = result.data;
            $scope.isLoading1 = false;
            $scope.emptyTable1=false;
            tableState1.pagination.numberOfPages =  Math.ceil(result.data.total_records / tableState1.pagination.number);
            $scope.isLoading1 = false;
            if(result.data.length < 1)
                $scope.emptyTable1=true;
        });
    }
    $scope.updateContractReview = function (row, type) {
        $scope.review_access = true;
        $scope.type = type;
        $scope.isActionItem = true;
        $scope.selectedRow = row;
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'right-panel-modal modal-open',
            templateUrl: 'views/Manage-Users/contracts/create-edit-contract-review.html',
            controller: function ($uibModalInstance, $scope, item) {
                $scope.update = false;
                //$scope.bottom = 'general.save';
                $scope.isEdit = false;
                if (item) {
                    $scope.isEdit = true;
                    $scope.submitStatus = true;
                    $scope.data = angular.copy(item);
                    $scope.data.due_date = new Date($scope.data.due_date);
                    $scope.title = '';
                    $scope.update = true;
                    $scope.bottom = 'general.update';
                }
                if($scope.type == 'view'){
                    $scope.bottom = 'contract.finish';
                }
                if($scope.type == 'add'){
                    $scope.bottom = 'general.update';
                }
                var param ={};
                param.contract_id = row.contract_id;
                param.customer_id = $scope.user1.customer_id;
                param.user_role_id = $scope.user1.user_role_id;
                param.contract_review_id = row.contract_review_id;
                contractService.getActionItemResponsibleUsers(param).then(function(result){
                    $scope.userList = result.data;
                });
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
                $scope.goToEdit = function(data){
                    $scope.data.due_date = new Date(data.due_date);
                }
                var params ={};
                $scope.getActionItemById = function(id){
                    contractService.getActionItemDetails({'id_contract_review_action_item':id}).then(function(result){
                        $scope.data = result.data[0];
                    });
                }
                $scope.addReviewActionItem=function(data){
                    if($scope.type == 'view'){
                        params.id_contract_review_action_item = data.id_contract_review_action_item;
                        params.comments = data.comments;
                        params.is_finish = data.is_finish;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id  = row.contract_id;
                        if(params.is_finish == 1){
                            var r=confirm("Are you sure that you want to finish this action item ?");
                            $scope.deleConfirm = r;
                            if(r==true){
                                contractService.reviewActionItemUpdate(params).then(function (result) {
                                    if (result.status) {
                                        var obj = {};
                                        obj.action_name = 'update';
                                        obj.action_description = 'finish$$action item$$('+data.action_item+')';
                                        obj.module_type = $state.current.activeLink;
                                        obj.action_url = $location.$$absUrl;
                                        $rootScope.confirmNavigationForSubmit(obj);
                                        $rootScope.toast('Success', result.message);
                                        $scope.ActionItemsTable($scope.tableStateRef);
                                        $scope.cancel();
                                    } else {
                                        $rootScope.toast('Error', result.error,'error');
                                    }
                                });
                            }
                        }else{
                            contractService.reviewActionItemUpdate(params).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    var obj = {};
                                    obj.action_name = 'save';
                                    obj.action_description = 'save$$action$$item$$('+data.action_item+')';
                                    obj.module_type = $state.current.activeLink;
                                    obj.action_url = $location.$$absUrl;
                                    $rootScope.confirmNavigationForSubmit(obj);
                                    $scope.getActionItemById(data.id_contract_review_action_item);
                                    $scope.ActionItemsTable($scope.tableStateRef);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error,'error');
                                }
                            });
                        }
                    }
                    else if(data != 0 && data.hasOwnProperty('id_contract_review_action_item')){
                        delete data.comments;
                        params = data;
                        params.updated_by = $scope.user.id_user;
                        params.contract_id = params.id_contract =  row.contract_id;
                        contractService.addReviewActionItemList(params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                var obj = {};
                                obj.action_name = 'update';
                                obj.action_description = 'update$$action$$item$$('+data.action_item+')';
                                obj.module_type = $state.current.activeLink;
                                obj.action_url = $location.$$absUrl;
                                $rootScope.confirmNavigationForSubmit(obj);
                                $scope.getActionItemById(data.id_contract_review_action_item);
                                $scope.ActionItemsTable($scope.tableStateRef);
                                $scope.cancel();
                            } else {
                                $rootScope.toast('Error', result.error,'error');
                            }
                        });
                    }
                }
                $scope.cancel = function () {
                    $uibModalInstance.close();
                };
            },
            resolve: {
                item: function () {
                    if ($scope.selectedRow) {
                        return $scope.selectedRow;
                    }
                }
            }
        });
        modalInstance.result.then(function ($data) {
        }, function () {
        });
    }
    if($stateParams.id){
        var param ={};
        param.user_role_id  = $scope.user1.user_role_id;
        param.customer_id = $scope.user1.customer_id;
        param.id_user = $scope.user1.id_user;
        param.id_contract_review_action_item = decode($stateParams.id);
        contractService.getAllActionItems(param).then(function(result){
            $scope.data = result.data.data[0];
            $scope.updateContractReview($scope.data,'view');
        });
    }
    $scope.deleteContractActionItem = function(row){
        var r=confirm("Do you want to continue?");
        $scope.deleConfirm = r;
        if(r==true){
            var params ={};
            params.id_contract_review_action_item  = row.id_contract_review_action_item ;
            params.updated_by  = $scope.user1.id_user;
            contractService.deleteActionItem(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success', result.message);
                    var obj = {};
                    obj.action_name = 'delete';
                    obj.action_description = 'delete$$action$$item$$('+row.action_item+')';
                    obj.module_type = $state.current.activeLink;
                    obj.action_url = $location.$$absUrl;
                    $rootScope.confirmNavigationForSubmit(obj);
                    $scope.cancel();
                    $scope.ActionItemsTable($scope.tableStateRef);
                }else $rootScope.toast('Error', result.error, 'error',$scope.user);
            });
        }
    }

    $scope.getActionItems = function (row) {
        $scope.contract_id = '';
        $scope.ActionItemsTable($scope.tableStateRef,row.contract_id);
    }
    $scope.getAllActionItems = function () {
        $scope.contract_id = '';
        $scope.ActionItemsTable($scope.tableStateRef);
    }
})