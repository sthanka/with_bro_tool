/* ocLazyLoad config */

angular.module('app')
    .config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
            $ocLazyLoadProvider.config({
                debug: true,
                events: false,
                modules: [
                    {
                        serie: true,
                        name: 'ui.sortable',
                        files: ['bower_components/jquery-ui/jquery-ui.min.js','bower_components/angular-ui-sortable/sortable.min.js']
                    },
                    {
                        name: 'attachment',
                        files: ['views/components/attachment/attachment-directive.js']
                    },
                    {
                        serie: true,
                        name: 'ng-fusioncharts',
                        files: ['angular-fusionchart/src/fusioncharts.js', 'angular-fusionchart/src/fusioncharts.theme.fint.js','angular-fusionchart/src/angular-fusioncharts.js']
                    },
                    {
                        name: 'ckeditor',
                        files: ['ckeditor/ckeditor.js','ckeditor/angular-ckeditor.js']
                    }
                ]
            })
        }
    ]);