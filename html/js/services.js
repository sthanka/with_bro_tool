'use strict';
angular.module('app')
    .factory('httpLoader', function ($rootScope) {
        var pendingReqs = {};
        return {
            addPendingReq: function (config) {
                if (config.hasOwnProperty('loaderId')) {
                    if (config.loaderId)
                        $('#' + config.loaderId).fadeIn();
                } else {
                    pendingReqs[config.url] = true;
                }
            },
            subtractPendingReq: function (config) {
                if (config && config.hasOwnProperty('loaderId')) {
                    if (config.loaderId)
                        $('#' + config.loaderId).fadeOut();
                } else {
                    if (config) {
                        delete pendingReqs[config.url];
                    } else {
                        pendingReqs = {};
                    }
                }
            },
            getPendingReqs: function () {
                return sizeOf(pendingReqs);
            }
        }
        function sizeOf(obj) {
            var size = 0,
                key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    size++;
                }
            }
            return size;
        }

    })
    .factory('errorInterceptor', function ($q, $rootScope, httpLoader,toastr) {
        toastr.options = {
            showMethod: 'fadeIn',
            preventDuplicates: true,
            timeOut: 3000
        };
        return {
            request: function (config) {
                httpLoader.addPendingReq(config);
                var encrypt = DATA_ENCRYPT;

                if (config.hasOwnProperty('DATA_ENCRYPT'))
                    encrypt = config.DATA_ENCRYPT;
                if(encrypt)
                {
                    config.headers.User = btoa(config.headers.User+'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                }
                if (config.hasOwnProperty('params') && encrypt) {
                    var actualParams = config.params;
                    config.params = {};
                    config.params.requestData = GibberishAES.enc(JSON.stringify(actualParams), 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                }
                else if (config.hasOwnProperty('data') && encrypt) {
                    if (config.data) {
                        var actualParams = config.data;
                        config.data = {};
                        if (actualParams.hasOwnProperty('file')) {
                            config.data.file = actualParams.file;
                            delete actualParams.file;
                        }
                        config.data.requestData = GibberishAES.enc(JSON.stringify(actualParams), 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx');
                    }
                }
                return config || $q.when(config);
            },
            response: function (response) {
                httpLoader.subtractPendingReq(response.config);

                var encrypt = DATA_ENCRYPT;
                if (response.hasOwnProperty('DATA_ENCRYPT'))
                    encrypt = config.DATA_ENCRYPT

                if (response.data.hasOwnProperty('responseData') && encrypt) {
                    response.data = JSON.parse(GibberishAES.dec(response.data.responseData, 'JKj178jircAPx7h4CbGyYVV6u0A1JF7YN5GfWDWx'));
                }

                return response || $q.when(response);
            },
            responseError: function (response) {
                httpLoader.subtractPendingReq(response.config);

                if (response && response.status === 404) {
                    $rootScope.toast('404', 'Not Found', 'warning');
                }
                if (response && response.status === -1) {
                    $rootScope.toast('Service Connection Error', 'Error while fetching URL', 'warning');
                }
                if (response && response.status === 401) {
                    $rootScope.toast('401', 'session expired', 'warning');
                    $rootScope.$broadcast('loggedOut');
                }
                if (response && response.status >= 500) {
                    $rootScope.toast('Oops!!!', 'Something went wrong. Please try again.', 'warning');
                }
                return $q.reject(response);
            }
        };
    })
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.interceptors.push('errorInterceptor');
    }])
    .factory('masterService', function($http){
        return {
            getCountiresList: function (params){
                return $http.get(API_URL + 'Master/countryList',{'params' : params}).then(function (response){
                    return response.data;
                });
            },
            getUserRole : function (params){
                return $http.get(API_URL + 'Master/role',{'params':params}).then (function(response){
                   return response.data;
                });
            },
            currencyList : function(params) {
                return $http.get(API_URL + 'Master/currencyList' , {'params' : params}).then (function(response){
                    return response.data;
                });
            }
        }
    })
    .factory('userService',function($http){
        return {
            post: function(params){
                return $http.post(API_URL + 'Signup/login', params).then(function (response) {
                    return response.data;
                });
            },
            forgotPassword: function(param){
                return $http.post(API_URL + 'Signup/forgetPassword', param).then(function (response) {
                    return response.data;
                });
            },
            getUserProfile: function(param){
                return $http.get(API_URL + 'User/info', {'params':param}).then(function (response) {
                    return response.data;
                });
            },
            postUserProfile: function(param) {
                return $http.post(API_URL + 'User/update', param).then(function (response) {
                    return response.data;
                });
            },
            changePassword: function(params) {
                return $http.post(API_URL + 'User/changePassword' , params).then(function(response){
                    return response.data;
                });
            },
            accessLogs: function (params) {
                return $http.post(API_URL + 'User/accessLog', params).then(function(response){
                   return response.data;
                });
            },
            companyDetails : function (params){
                return $http.get(API_URL + 'Customer/details',{params:params}).then(function(response){
                    return response.data;
                });
            },
            updateCompany : function (params) {
                return $http.post(API_URL + 'Customer/update',params).then(function(response){
                   return response.data;
                });
            },
            loginAs : function (params) {
                return $http.get(API_URL + 'user/loginasuser',{'params' : params}).then(function(response){
                   return response.data;
                });
            },
            unBlock : function (params) {
                return $http.post(API_URL + 'user/unblock',params).then(function(response){
                    return response.data;
                });
            },
            accessEntry : function (params) {
                return  $http.post(API_URL + 'User/accessLog', params).then(function(response){
                    return response.data;
                });
            },
            signUp : function (params) {
                return  $http.post(API_URL + 'Signup/getEncryptionSettings', params).then(function(response){
                    return response.data;
                });
            },
            getAccess : function(params) {
                return $http.get(API_URL + 'User/access', {params:params}).then(function (response) {
                    return response.data;
                });
            }
        }
    })
    .factory('customerService',function($http){
        return {
            list: function(param){
                return $http.get(API_URL + 'Customer/list',{params:param}).then(function (response) {
                    return response.data;
                });
            },
            add: function(params){
                return $http.post(API_URL + 'Customer/add', params).then(function (response) {
                    return response.data;
                });
            },
            update: function(params){
                return $http.post(API_URL + 'Customer/update', params).then(function (response) {
                    return response.data;
                });
            },
            delete: function(params){
                return $http.delete(API_URL + 'Customer/delete', {params:params}).then(function (response) {
                    return response.data;
                });
            },
            getCustomer: function (param) {
                return $http.get(API_URL + 'Customer/info',{params:param}).then(function(response){
                   return response.data;
                });
            },
            getAdminList: function(param) {
              return $http.get(API_URL + 'Customer/adminList',{'params':param}).then(function(response){
                 return response.data;
              });
            },
            postAdmin: function (params){
                return $http.post(API_URL + 'Customer/admin',params).then(function(response){
                    return response.data;
                });
            },
            deleteAdmin: function(params){
                return $http.delete(API_URL + 'Customer/admin', {params:params}).then(function (response) {
                    return response.data;
                });
            },
            getAdminById: function (params){
                return $http.get(API_URL + 'Customer/admin',{'params':params}).then(function(response){
                   return response.data;
                }) ;
            },
            resetPassword: function (params) {
                return $http.post(API_URL + 'Customer/resetPassword', params).then (function(response){
                    return response.data;
                });
            },
            getUserList: function(param) {
                return $http.get(API_URL + 'Customer/userList',{'params':param}).then(function(response){
                    return response.data;
                });
            },
            postUser: function (params){
                return $http.post(API_URL + 'Customer/user',params).then(function(response){
                    return response.data;
                });
            },
            deleteUser: function(params){
                return $http.delete(API_URL + 'Customer/user', {params:params}).then(function (response) {
                    return response.data;
                });
            },
            getUserById: function (params){
                return $http.get(API_URL + 'Customer/user',{'params':params}).then(function(response){
                    return response.data;
                }) ;
            },
            getTemplates : function(params){
                return $http.get(API_URL + 'Template/details', {'params':params}).then (function(response){
                   return response.data;
                });
            },
            getLDAPCustomer: function (params){
                return $http.get(API_URL + 'User/ldapdata',{'params':params}).then(function(response){
                    return response.data;
                }) ;
            },
            saveLDAP: function (params){
                return $http.post(API_URL + 'User/ldap',params).then(function(response){
                    return response.data;
                });
            },
            testLDAP: function (params){
                return $http.post(API_URL + 'Customer/checkAD',params).then(function(response){
                    return response.data;
                });
            },
            linkTemplate: function (params) {
                console.log(params);
                return $http.post(API_URL + 'Template/linkTemplateCustomer',params).then(function(response){
                    return response.data;
                });
            }
        }
    })
    .factory('moduleService', function($http){
        return{
            list: function(params){
                return $http.get(API_URL + 'Module/list', {'params':params}).then(function (response) {
                    return response.data;
                });
            },
            add: function(params){
                return $http.post(API_URL + 'Module/add', params).then(function (response) {
                    return response.data;
                });
            },
            update: function(params){
                return $http.post(API_URL + 'Module/update', params).then(function (response) {
                    return response.data;
                });
            },
            delete: function(params){
                return $http.delete(API_URL + 'Module/delete', {'params':params}).then(function (response) {
                    return response.data;
                });
            },
        }
    })
    .factory('topicService', function($http){
        return{
            list: function(params){
                return $http.get(API_URL + 'Topic/list',{'params':params}).then(function (response) {
                    return response.data;
                });
            },
            add: function(params){
                return $http.post(API_URL + 'Topic/add', params).then(function (response) {
                    return response.data;
                });
            },
            update: function(params){
                return $http.post(API_URL + 'Topic/update', params).then(function (response) {
                    return response.data;
                });
            },
            delete: function(params){
                return $http.delete(API_URL + 'Topic/delete',{'params':params}).then(function (response) {
                    return response.data;
                });
            },
            getTopicTypes : function(params){
                return $http.get(API_URL + 'topic/types', {'params' : params}).then(function(response){
                   return response.data;
                });
            }
        }
    })
    .factory('questionsService', function($http){
        return {
            list : function (params){
                return $http.get(API_URL + 'Question/list', {'params':params}).then (function(response){
                   return response.data;
                });
            },
            getTopicQuestions : function(params) {
                return $http.get(API_URL + 'Question/topicQuestions', {'params' : params}).then (function(response){
                    return response.data;
                });
            },
            postQuestions : function(params) {
                return $http.post(API_URL + 'Question/add', params).then (function(response){
                    return response.data;
                })
            },
            getQuestionInfo : function (params) {
                return $http.get(API_URL + 'Question/info', {'params' : params}).then( function(response){
                  return response.data;
                })
            },
            updateQuestion : function (params){
                return $http.post(API_URL + 'Question/update' , params).then(function(response){
                    return response.data;
                })
            },
            updateQuestionStatus : function (params) {
                return $http.post(API_URL + 'Question/updateStatus' , params).then (function(response){
                    return response.data;
                })
            },
            questionCategory : function (params) {
                return $http.get(API_URL + 'Question/category', {'params':params}).then(function(response){
                    return response.data;
                })
            },
            sortQuestions : function (params){
                return $http.post(API_URL + 'Question/order',params).then(function(response){
                   return response.data;
                });
            },
            updateRelationship : function(params){
                return $http.post(API_URL + 'Question/updateRelationshipCategories', params).then(function(response){
                   return response.data;
                });
            },
            getQuestionOptions : function (params) {
                return $http.get(API_URL + 'Question/questionmasteroptions', {'params':params}).then(function(response){
                    return response.data;
                });
            }
        }
    })
    .factory('settingsService', function($http){
        return{
            list: function(params){
                return $http.get(API_URL + 'Settings/info',{'params':params}).then(function (response) {
                    return response.data;
                });
            },
            update: function(params){
                return $http.post(API_URL + 'Settings/update', params).then(function (response) {
                    return response.data;
                });
            }
        }
    })
    .factory('relationCategoryService', function($http){
        return{
            list: function(params){
                return $http.get(API_URL + 'Relationship_category/list',{'params':params}).then(function (response) {
                    return response.data;
                });
            },
            add: function(params){
                return $http.post(API_URL + 'Relationship_category/add', params).then(function (response) {
                    return response.data;
                });
            },
            update: function(params){
                return $http.post(API_URL + 'Relationship_category/update', params).then(function (response) {
                    return response.data;
                });
            },
            updateSettings : function (params){
                return $http.post(API_URL + 'Customer/relationshipCategoryRemainder' , params).then(function(response){
                   return response.data;
                });
            },
            getSettingsData : function(params){
                return $http.get(API_URL + 'Customer/relationshipCategoryRemainder', {'params': params}).then(function(response){
                   return response.data;
                });
            }
        }
    })
    .factory('relationshipClassificationService', function($http){
        return{
            list: function(params){
                return $http.get(API_URL + 'Relationship_category/classificationList',{'params':params}).then(function (response) {
                    return response.data;
                });
            },
            add: function(params){
                return $http.post(API_URL + 'Relationship_category/classificationAdd', params).then(function (response) {
                    return response.data;
                });
            },
            update: function(params){
                return $http.post(API_URL + 'Relationship_category/classificationUpdate', params).then(function (response) {
                    return response.data;
                });
            },
            saveClassification: function(params){
                return $http.post(API_URL + 'Relationship_category/classificationChildAdd', params).then(function (response) {
                    return response.data;
                });
            },
            listChildClassification: function(params){
                return $http.get(API_URL + 'Relationship_category/classificationChild',{'params':params}).then(function (response) {
                    return response.data;
                });
            },
            addChildClassification: function(params){
                return $http.post(API_URL + 'Relationship_category/classificationChildAdd',params).then(function (response) {
                    return response.data;
                });
            }

        }
    })
    .factory('templateService' , function($http){
        return {
            getModulesData: function(params) {
                return $http.get(API_URL + 'Template/alltemplates', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            list: function (params) {
                return $http.get(API_URL + 'Template/list', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            getCounts : function (params) {
                return $http.get(API_URL + 'Template/count', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            info: function (params) {
                return $http.get(API_URL + 'Template/info', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            add: function (params) {
                return $http.post(API_URL + 'Template/add', params).then(function (response) {
                    return response.data;
                });
            },
            update: function (params) {
                return $http.post(API_URL + 'Template/update', params).then(function (response) {
                    return response.data;
                });
            },
            templateList : function (params) {
              return $http.get(API_URL + 'Template/details', {'params':params}).then(function(response){
                 return response.data;
              });
            },
            moduleList : function (params) {
                return $http.get(API_URL + 'Template/moduleList', {'params': params}).then (function(response){
                    return response.data;
                });
            },
            addModule : function (params) {
                return $http.post(API_URL + 'Template/module',params).then(function(response){
                    return response.data;
                });
            },
            deleteModule :  function (params) {
                return $http.delete(API_URL + 'Template/module',{'params':params}).then(function(response){
                    return response.data;
                });
            },
            getModule : function(params) {
                return $http.get(API_URL + 'Template/module', {'params': params}).then(function(response){
                    return response.data;
                });
            },
            topicList : function(params) {
                return $http.get(API_URL + 'Template/topicList', {'params': params}).then (function(response){
                    return response.data;
                });
            },
            addTopic : function (params) {
                return $http.post(API_URL + 'Template/Topic',params).then(function(response){
                    return response.data;
                });
            },
            deleteTopic :  function (params) {
                return $http.delete(API_URL + 'Template/Topic',{'params':params}).then(function(response){
                    return response.data;
                });
            },
            getTopic : function(params) {
                return $http.get(API_URL + 'Template/topic', {'params': params}).then(function(response){
                    return response.data;
                });
            },
            questionList : function(params) {
                return $http.get(API_URL + 'Template/questionList', {'params': params}).then (function(response){
                    return response.data;
                });
            },
            postQuestion : function (params) {
                return $http.post(API_URL + 'Template/question',params).then(function(response){
                    return response.data;
                });
            },
            getAllModules : function (params) {
                return $http.get(API_URL + 'Template/allModules', {'params':params}).then (function(response){
                    return response.data;
                });
            },
            getAllTopics: function (params) {
                return $http.get(API_URL + 'Template/allTopics', {'params':params}).then (function(response){
                    return response.data;
                });
            },
            getAllQuestions : function (params) {
                return $http.get(API_URL + 'Template/allQuestions', {'params':params}).then (function(response){
                    return response.data;
                });
            },
            deleteQuestion : function (params) {
                return $http.delete(API_URL + 'Template/question',{'params':params}).then(function(response){
                    return response.data;
                });
            },
            sortModules : function(params) {
                return $http.post(API_URL + 'Template/moduleOrder',params).then(function(response){
                    return response.data;
                })
            },
            sortTopics : function(params) {
                return $http.post(API_URL + 'Template/topicOrder',params).then(function(response){
                    return response.data;
                })
            },
            sortQuestions: function(params) {
                return $http.post(API_URL + 'Template/questionOrder',params).then(function(response){
                    return response.data;
                })
            },
            previewTemplate : function(params) {
                return $http.get(API_URL + 'Template/templatePreview', {'params': params}).then(function(response){
                   return response.data;
                });
            },
            viewTemplate : function(params) {
                return $http.get(API_URL + 'Template/templateView', {'params': params}).then(function(response){
                   return response.data;
                });
            },
            sortAll : function(params) {
                return $http.post(API_URL + 'Template/templateOrder', params).then(function(response){
                   return response.data;
                });
            }
        }
    })
    .factory('businessUnitService' , function($http){
        return {
            list: function (params) {
                return $http.get(API_URL + 'Business_unit/list', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            bulist: function (params) {
                return $http.get(API_URL + 'Business_unit/bulist', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            get: function (params) {
                return $http.get(API_URL + 'Business_unit/details', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            add: function (params) {
                return $http.post(API_URL + 'Business_unit/add', params).then(function (response) {
                    return response.data;
                });
            },
            update: function (params) {
                return $http.post(API_URL + 'Business_unit/update', params).then(function (response) {
                    return response.data;
                });
            }
        }
    })
    .factory('dashboardService' , function($http){
        return {
            info : function (params) {
                return $http.get(API_URL + 'Customer/dashboard' , {'params' : params}).then(function (response){
                   return response.data;
                });
            },
            contractsList: function (params){
                return $http.get(API_URL + 'Contract/list', {'params': params}).then(function (response) {
                    return response.data;
                });
            }
        }
    })
    .factory('contractService' , function($http){
        return {
            list: function (params){
                return $http.get(API_URL + 'Contract/list', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            listDelete: function (params){
                return $http.get(API_URL + 'Contract/deletedList', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            delete: function (params){
                return $http.post(API_URL + 'Contract/delecteContract', params).then(function (response) {
                    return response.data;
                });
            },
            undoDelete: function (params){
                return $http.post(API_URL + 'Contract/undoDelContract', params).then(function (response) {
                    return response.data;
                });
            },
            add : function(params) {
                return $http.post(API_URL + 'Contract/add' , params).then(function(response){
                   return response.data;
                });
            },
            getContractStatus : function(params) {
                return $http.get(API_URL + 'Contract/contractStatus', {'params' : params}).then(function(response){
                    return response.data;
                });
            },
            getContractById : function (params) {
                return $http.get(API_URL + 'Contract/info' ,  {'params': params}).then(function(response){
                    return response.data;
                });
            },
            update : function(params) {
                return $http.post(API_URL + 'Contract/update' , params).then(function(response){
                    return response.data;
                });
            },
            getRelationshipCategory : function (params){
                return $http.get(API_URL + 'Contract/relationshipCategory', {'params' : params}).then(function(response){
                    return response.data;
                });
            },
            getRelationshipClassiffication : function (params){
                return $http.get(API_URL + 'Contract/relationshipClassification', {'params' : params}).then(function(response){
                    return response.data;
                });
            },
            getDelegates : function (params){
                return $http.get(API_URL + 'Contract/getdelegates', {'params' : params}).then(function(response){
                    return response.data;
                })
            },
            reviewActionItemList: function (params){
                return $http.get(API_URL + 'Contract/reviewActionItems', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            addReviewActionItemList: function (params){
                return $http.post(API_URL + 'Contract/ReviewActionItem', params).then(function (response) {
                    return response.data;
                });
            },
            reviewActionItemUpdate: function (params){
                return $http.post(API_URL + 'Contract/ReviewActionItemUpdate', params).then(function (response) {
                    return response.data;
                });
            },
            responsibleUserList: function (params){
                return $http.get(API_URL + 'Contract/users', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            getActionItemResponsibleUsers : function(params) {
              return $http.get(API_URL + 'Contract/actionitemresponsibleusers', {'params': params}).then(function(response){
                 return response.data;
              });
            },
            contractResponsibleUserList: function (params){
                return $http.get(API_URL + 'Contract/contractreviewusers', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            getbuOwnerUsers : function (params) {
                return $http.get(API_URL + 'Contract/users' , {'params': params}).then(function(response){
                   return response.data;
                });
            },
            initializeReview : function (params) {
                return $http.get(API_URL + 'Contract/initializeReview' , {'params': params}).then(function(response){
                    return response.data;
                });
            },
            contractModule : function(params){
                return $http.get(API_URL + 'Contract/module', {'params' : params}).then(function(response){
                   return response.data;
                });
            },
            getcontractReviewModules : function(params) {
                return $http.get(API_URL + 'Contract/contractReview' , {'params' : params}).then(function(response){
                   return response.data;
                });
            },
            addContributors : function(params) {
                return $http.post(API_URL + 'Contract/contractContributor' , params).then(function(response){
                    return response.data;
                });
            },
            getAttachments : function(params) {
                return $http.get(API_URL + 'Document/list' , {'params' : params}).then(function(response){
                    return response.data;
                });
            },
            deleteActionItem : function (params) {
                return $http.delete(API_URL + 'Contract/ReviewActionItemDelete' , {'params' : params}).then(function(response){
                    return response.data;
                });
            },
            finalizeReviewList: function (params){
                return $http.post(API_URL + 'Contract/finalize', params).then(function (response) {
                    return response.data;
                });
            },
            answerQuestion : function (params) {
                return $http.post(API_URL + 'Contract/questionAnswer' , params).then(function(response){
                   return response.data;
                });
            },
            getDashboard : function(params) {
                return $http.get(API_URL + 'Contract/dashboard', {'params' : params}).then(function(response){
                   return response.data;
                });
            },
            getAllActionItems : function(params) {
                return $http.get(API_URL + 'Contract/actionItems', {'params' : params}).then(function(response){
                   return response.data;
                });
            },
            contractProviders : function(params) {
                return $http.get(API_URL+ 'Contract/providers', {'params':params}).then(function(response){
                   return response.data;
                });
            },
            getActionItemDetails : function(params){
                return $http.get(API_URL + 'Contract/actionItemDetails' , {'params' : params}).then(function(response){
                    return response.data;
                });
            },
            contractOverallDetails: function (params){
                return $http.get(API_URL + 'Contract/contractDetails', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            getTopicQuestionsById : function(params) {
                return $http.get(API_URL + 'Contract/getTopic', {'params' : params}).then (function(response){
                    return response.data;
                });
            },
            getUrl : function(params) {
                return $http.get(API_URL + 'Contract/getDownloadedFile', {'params' : params}).then (function(response){
                    return response.data;
                });
            },
            getchangeLogs : function(params) {
                return $http.get(API_URL + 'Contract/contractReviewChangelog' ,{'params': params}).then(function(response){
                   return response.data;
                });
            },
            exportReviewData : function(params) {
                return $http.get(API_URL + 'Contract/export' ,{'params' : params}).then(function(response){
                    return response.data;
                })
            },
            exportDashboardData : function(params) {
                return $http.get(API_URL + 'Contract/dashboardexport' ,{'params' : params}).then(function(response){
                    return response.data;
                })
            },
            discussDetails : function(params) {
                return $http.get(API_URL + 'Contract/reviewdiscussion', {'params' : params}).then (function(response){
                   return response.data;
                });
            },
            postdiscussion : function (params) {
                return $http.post(API_URL + 'Contract/reviewdiscussion' ,  params).then (function(response){
                   return response.data;
                });
            },
            closediscussion : function (params) {
              return $http.post(API_URL+'Contract/reviewdiscussionclose', params).then(function(response){
                 return response.data;
              });
            },
            getLogs : function (params) {
                return $http.get(API_URL + 'Contract/contract_log', {'params' : params}).then(function(response){
                   return response.data;
                });
            },
            providerList: function (params){
                return $http.get(API_URL + 'Contract/listProviders', {'params': params}).then(function (response) {
                    return response.data;
                });
            },
            exportContracts : function(params) {
                return $http.get(API_URL + 'Contract/contractListExport', {'params': params}).then(function(response){
                    return response.data;
                })
            },
            reviewUsers : function (params) {
                return $http.get(API_URL + 'Contract/reviewlevelusers', {'params': params}).then(function(response){
                    return response.data;
                })
            },
            getBussinessUnitList : function (params) {
                return $http.get(API_URL + 'Business_unit/list', {'params': params}).then(function(response){
                    return response.data;
                })
            },
            getFileLogs : function (params) {
                return $http.get(API_URL + 'Document/list', {'params' : params}).then(function(response){
                   return response.data;
                });
            },
        }
    })
    .factory('actionItemsService' , function($http){
        return {
            getActionItemFilters : function(params) {
                return $http.get(API_URL + 'Contract/actionItemFilters' , {'params' : params}).then(function(response){
                    return response.data;
                });
            }
        }
    })
    .factory('attachmentService' , function($http){
        return {
            getAttachments : function(params) {
                return $http.get(API_URL + 'Document/list' , {'params' : params}).then(function(response){
                    return response.data;
                });
            },
            deleteAttachments : function(params) {
                return $http.delete(API_URL + 'Document/delete' , {'params' : params}).then(function(response){
                    return response.data;
                });
            }
        }
    })
    .factory('calenderService' , function($http){
        return {
            post : function(params) {
                return $http.post(API_URL + 'customer/calender' , params ).then(function(response){
                    return response.data;
                });
            },
            get : function(params) {
                return $http.get(API_URL + 'customer/calender' , {'params' : params}).then(function(response){
                    return response.data;
                });
            },
            yearly : function(params) {
                return $http.get(API_URL + 'customer/calenderYearView' , {'params' : params}).then(function(response){
                    return response.data;
                });
            }
        }
    })
    .factory('emailTempalteService', function($http){
        return {
            get : function(params) {
                return $http.get(API_URL + 'Contract/emailTemplateList', {'params' : params}).then(function(response){
                    return response.data;
                })
            },
            post : function(params) {
                return $http.post(API_URL+ 'Contract/emailTemplateUpdate', params).then(function(response){
                   return response.data;
                });
            },
            testTemplate : function(params) {
                return $http.post(API_URL + 'customer/testemailtemplate', params).then(function(response){
                   return response.data;
                });
            },
            delete : function(params) {
                return $http.post(API_URL+ 'Contract/emailTemplateUpdateStatus', params).then(function(response){
                    return response.data;
                });
            },
        }
    })
    .factory('reportsService' , function($http){
        return {
            reportsList : function(params) {
              return $http.get(API_URL + 'report/list', {'params':params}).then(function(response){
                  return response.data;
              })
            },
            getReportFilters : function(params) {
                return $http.get(API_URL + 'Report/criteria' , {'params' : params}).then(function(response){
                    return response.data;
                });
            },
            searchReports : function (params) {
                return $http.get(API_URL+ 'Report/search', {'params' : params}).then(function(response){
                   return response.data;
                });
            },
            saveReport : function(params) {
                return $http.post( API_URL + 'Report/saveReport', params).then(function(response){
                    return response.data;
                })
            },
            getReportDetails : function(params) {
                return $http.get(API_URL + 'report/report',{'params' : params}).then(function(response){
                    return response.data;
                });
            },
            deleteReport : function(params) {
                return $http.delete(API_URL + 'report/delete', {params:params}).then(function (response) {
                    return response.data;
                });
            },
            exportReport : function(params) {
                return $http.get(API_URL + 'Report/export',{params: params}).then(function(response){
                   return response.data;
                });
            }
        }
    })
    .factory('historyService', function($http){
        return {
            customersList : function(params) {
                return $http.get(API_URL + 'Customer/listCustomers' , {'params' : params}).then(function(response){
                   return response.data;
                });
            },
            getCustomerUsers : function(params){
                return $http.get(API_URL + 'Customer/userListHistory', {'params': params}).then(function(response){
                   return response.data;
                });
            },
            getSummary : function(params) {
                return $http.get(API_URL + 'Customer/userHistory', {'params': params}).then(function(response){
                   return response.data;
                });
            },
            getActionsList : function(params) {
                return $http.get(API_URL + 'Customer/actionList', {'params': params}).then(function(response){
                    return response.data;
                })
            }
        }
    })
    .factory('notificationService', function($http){
        return {
            getUpdates : function(params) {
                return $http.get(API_URL + 'Customer/dailyupdates' , {'params' : params}).then(function(response){
                    return response.data;
                });
            },
            getCount : function(params) {
                return $http.get(API_URL+ 'customer/dailynotificationcount', {'params' : params}).then(function(response){
                   return response.data;
                });
            },
            list : function(params) {
                return $http.get(API_URL+ 'customer/notification', {'params' : params}).then(function(response){
                    return response.data;
                });
            }
        }
    })