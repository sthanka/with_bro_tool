angular
    .module('app')
    .directive('a', preventClickDirective)
    .directive('a', bootstrapCollapseDirective)
    .directive('a', navigationDirective)
    .directive('nav', sidebarNavDynamicResizeDirective)
    .directive('button', layoutToggleDirective)
    .directive('a', layoutToggleDirective)
    .directive('button', collapseMenuTogglerDirective)
    .directive('div', bootstrapCarouselDirective)
    .directive('toggle', bootstrapTooltipsPopoversDirective)
    .directive('tab', bootstrapTabsDirective)
    .directive('button', cardCollapseDirective)
    .factory('AuthService', function ($rootScope,$http,$location,$localStorage,$state, userService) {
        return {
            login: function () {
                if($localStorage.curUser && !angular.equals({}, $localStorage.curUser)){
                    if($localStorage.curUser.status) {
                        $http.defaults.headers.common['Authorization'] = $localStorage.curUser.access_token;
                        if($localStorage.curUser.data.parent)
                            $http.defaults.headers.common['User'] = $localStorage.curUser.data.parent.id_user;
                        else $http.defaults.headers.common['User'] = $localStorage.curUser.data.data.id_user;
                        //$http.defaults.headers.common['lang'] = 'english';
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            },
            logout: function (user, pass) {

            },
            checkUrl:function(module_url){
                var param = {};
                param.module_url = module_url.trim();
                if($localStorage.curUser.data.data.user_role_id)param.user_role_id = $localStorage.curUser.data.data.user_role_id;
                var data = [];
               /* return $http.get(API_URL + 'User/access', {params:param}).then(function (response) {
                    angular.forEach(response.data.data, function (value, key) {
                        angular.forEach(value, function (value, key) {
                            data[key] = value;
                        })
                    })
                    return data;
                });*/
                return userService.getAccess(param).then(function(result){
                    angular.forEach(result.data, function (value, key) {
                        angular.forEach(value, function (value, key) {
                            data[key] = value;
                        })
                    })
                    return data;
                });
            },
            isLoggedIn: function () {
                if ($localStorage.curUser && !angular.equals({}, $localStorage.curUser))
                    return true;
                return false;
                // Check auth token here from localStorage
            },
            getFields: function(){
                return  $localStorage.curUser;
            }
        }
    })
    .factory('decode',function(){
        return function(text){
            if(text!=undefined){
                return window.atob(text);
            }
        }
    })
    .factory('encode',function(){
        return function(text){
            return window.btoa(text);
        }
    })
    .filter('decode',function(){
        return function(text){
            if(text!=undefined){
                return window.atob(text);
            }
        }
    })
    .filter('unique', function() {
        return function (arr, targetField) {
            var values = [],
                i,
                unique,
                l = arr.length,
                results = [],
                obj;
            for( i = 0; i < l; i++ ) {
                obj = arr[i];
                unique = true;
                for( v = 0; v < values.length; v++ ){
                    if( obj[targetField] == values[v] ){
                        unique = false;
                    }
                }
                if( unique ){
                    values.push( obj[targetField] );
                    results.push( obj );
                }
            }
            return results;
        };
    })
    .filter('encode',function(){
        return function(text){
            return window.btoa(text);
        }
    })
    .filter('isUndefinedOrNull', function() {
        return function(value) {
            if(value === null || value === undefined)
                return '---';
            else
                return value;
        };
    })
    .directive('activeLink',['$location','$state','$window',function(location,$state,$window){
        return {
            restrict: 'AE',
            link: function(scope,element,attrs,controller){
                var clazz = attrs.activeLink;
                element.removeClass(clazz);
                var path='';
                if(attrs.ngHref)
                    path = attrs.ngHref;
                if(attrs.href)
                    path = attrs.href;
                if(path!=undefined){
                    path=decodeURIComponent(path.substring(1));
                }else{
                    path='#';
                }//hack because path does not return including hashbang
                scope.location='';
                scope.location=location;
                scope.$watch('location.path()',function(newPath,oldPath){
                    oldPath = oldPath.replace('/','/');
                    newPath = newPath.replace('/','#/');
                    var arr= [];
                    arr=newPath.split('/');
                    angular.forEach(arr,function(i,o){
                        if(o==1) {
                            if (element.attr('href') == '#/' + arr[o]) {
                                element.removeClass(clazz);
                                element.addClass(clazz);
                            } else {
                                //console.log('element', element.attr('href'));
                                //console.log('arr','#/' + arr[o]);
                                element.removeClass(clazz);
                            }
                        }
                    })
                   /* if($.trim('#'+newPath) == $.trim(attrs.href)){
                         element.removeClass(clazz);
                         element.addClass(clazz);
                    }else{
                        element.removeClass(clazz);
                        if($state.$current.activeLink === $.trim(attrs.id)){
                            element.removeClass(clazz);
                            element.addClass(clazz);
                            element.removeClass(clazz);
                        }
                        if($state.$current.parent.activeLink == $.trim(attrs.id)){
                            var id1 ='';
                            id1=$.trim($state.$current.parent.activeLink);
                            console.log('$state.$current.parent',$state.$current.parent);
                            $('#'+id1).removeClass(clazz);
                            console.log('id1',id1);
                            $('#'+id1).addClass(clazz);
                        }
                    }*/
                   /* else{
                        if($state.$current.parent.activeLink){
                            var id1 ='';
                            var id1=$.trim($state.$current.parent.activeLink);
                            //$('#'+id1).removeClass(clazz);
                            $('#'+id1).addClass(clazz);
                        }else if($state.$current.parent.activeLink == undefined){
                            var id = '';
                            var id=$.trim($state.$current.parent.activeLink);
                        }

                    }*/
                });
            }
        };
    }])
    .filter('underscoreadd',function(lowercaseFilter){
        return function(input){
            return lowercaseFilter(input.replace(/ /g,'_'));
        };
    })
    .filter('underscoreless',function(lowercaseFilter){
        return function(input){
            return lowercaseFilter(input.replace(/_/g,' '));
        };
    })
    .filter('capitalize',function(){
        return function(input){
            return (!!input)?input.charAt(0).toUpperCase()+input.substr(1).toLowerCase():'';
        }
    })
    .filter('checkEmpty',function(){
        return function(input,string){
            if(input==''||input == null||input === undefined){
                if(string)
                    return string;
                return '---';
            }else{
                return input;
            }
        };
    })
    .filter('replaceChar', function(){
        return function(input){
            var temp = input.split('$$');
            input = input.replace('$$',' ');
            return temp.join(' ');
        };
    })
    .filter('replaceUrl', function(){
        return function(input){
            var temp = input.split('#');
            return temp[1];
        };
    })
    .directive('textLine',function(){
        return {
            link: function(scope,element){
                element.addClass('text-line');
                element.bind('click',function(){
                    element.toggleClass('text-line');
                });
            },
        };
    })
    .filter('trusted',function($sce){
        return function(html){
            return $sce.trustAsHtml(html)
        }
    })
    .directive('providerActive',function(){
        return {
            link: function(scope,element){
                //element.addClass('provider-active');
                //element.parent().find('.provider-active').removeClass('provider-active');
                element.bind('click',function(){
                    element.parent().find('.provider-active').removeClass('provider-active');
                    element.addClass('provider-active');
                });
            },
        };
    })
    .directive("datePicker",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                jQuery('#appointment').datetimepicker({
                    format: 'DD/MM/YYYY',
                    //pickTime: false,
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    }
                });
            }
        }
    }])
    .filter('utcToLocal',function($filter,moment){
        return function(dateString,format){

            if(!format||format=='')
                format='medium';

            switch(format){
                case 'date':
                    format='MMM d, y';
                    break;
                case 'time':
                    format='h:mm:ss a';
                    break;
                case 'datetime':
                    format='MMM d, y  HH:mm:ss';
                    break;
                case 'iso':
                    format='y-MM-dd';
                    break;
                default:
                    break;
            }
            if(!dateString||dateString==''){
                return dateString;
            }else{
                var utcDate=moment.utc(dateString).toDate();
                var date=$filter('date')(utcDate,format);
                if(date == 'Invalid Date')return null;
                else return date;
            }
        };
    })
    .directive('nT',['$location','AuthService','userService',function($location,AuthService,userService,$rootScope,$state){
        return {
            restrict: 'A',
            link: function($scope,elem,attr){
                elem.on('click',function(){
                        var attrData=angular.fromJson(attr.nT);
                        var data={
                            'action_name': attrData.a_n,
                            'module_type': attrData.m_t,
                            'action_description': attrData.a_d,
                        }
                        var val = attrData.valid;
                        if(AuthService.getFields().data.parent){
                            data.user_id = AuthService.getFields().data.parent.id_user;
                            data.acting_user_id = AuthService.getFields().data.data.id_user;
                        }
                        else data.user_id = AuthService.getFields().data.data.id_user;
                        data.action_url= $location.absUrl();
                        if(AuthService.getFields().access_token != undefined){
                            var s = AuthService.getFields().access_token.split(' ');
                            data.access_token = s[1];
                        }
                        else data.access_token = '';
                        if(val){
                            //console.log('data',data);
                            userService.accessLogs(data).then(function(result){
                                if(result.status){}
                            });
                        }
                    });
            }
        };
    }])
    .directive('onlyDigits',function(){
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope,element,attr,ctrl){
                function inputValue(val){
                    if(val||val!=' '){
                        var digits=val.replace(/[^0-9]/g,'');
                        if(digits!==val){
                            ctrl.$setViewValue(digits);
                            ctrl.$render();
                        }
                        return parseInt(digits,10);
                    }
                    return undefined;
                }

                ctrl.$parsers.push(inputValue);
            }
        };
    })
    .directive('alphaNumeric',function(){
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope,element,attr,ctrl){
                function inputValue(val){
                    if(val||val!=' '){
                        var digits=val.replace(/[^a-zA-Z0-9]+$/g,'').replace(' ','');
                        if(digits!==val){
                            ctrl.$setViewValue(digits);
                            ctrl.$render();
                        }
                        return digits;
                    }
                    return undefined;
                }
                ctrl.$parsers.push(inputValue);
            }
        };
    })
    .directive('myDate',function(dateFilter){
        return {
            restrict: 'EAC',
            require: '?ngModel',
            link: function(scope,element,attrs,ngModel){
                ngModel.$parsers.push(function(viewValue){
                    return dateFilter(viewValue,'yyyy-MM-dd');
                });
            }
        };

    })
    .directive('httpResponse',['$http','httpLoader',function($http,httpLoader){
        return {
            restrict: 'A',
            link: function(scope,elm,attrs){
                scope.isLoading=function(){
                    //console.log('httpLoader.getPendingReqs',httpLoader.getPendingReqs());
                    return httpLoader.getPendingReqs()>0;
                };
                scope.$watch(scope.isLoading,function(v){
                    if(v){
                        $(elm).fadeIn();
                    }else{
                        $(elm).fadeOut();
                    }
                });
            }
        };
    }])
    .filter('nlToArray', function() {
        var span = document.createElement('span');
        return function(text) {
            var lines = text.split(',');
            for (var i = 0; i < lines.length; i++) {
                span.innerText = lines[i];
                span.textContent = lines[i];
                lines[i] = span.innerHTML;
            }
            //console.log('lines',lines.join('\n'));
            return lines.join('\n');
        };
    })
    .filter('splitText',function($sce){
        return function(data,strLength){
            if(data.trim().length>0){
                var d = data.match(new RegExp('.{1,' + strLength + '}', 'g'));
                if(d.length==1){
                    return $sce.trustAsHtml(d[0]);
                }
                else if(d.length>2){
                    d[1] = d[1].substring(0, (strLength-3));
                    return $sce.trustAsHtml(d[0]+'<br/>'+d[1]+'...');
                }else{
                    return $sce.trustAsHtml(d[0]+'...');
                }
            }
        };
    })
    .filter('currencyFormat',function($filter){
        return function(numberData,currencyFormat){
            if(numberData){
                console.log('data',numberData, currencyFormat);
                console.log('data output;', $filter('number')(numberData, fractionSize));
                var fractionSize = '';
                var returnData = '';
                if (currencyFormat === 'EUR') {
                    fractionSize = '2';
                    returnData = $filter('number')(numberData, fractionSize);
                    console.log('returnData',returnData);
                    var splitData = returnData.split('.');
                    splitData[0] = (splitData[0].split(',')).join('.');
                    console.log('splitData.join()',splitData.join());
                    return splitData.join();
                } else {
                    return numberData;
                }
                //var d = data.match(new RegExp('.{1,' + strLength + '}', 'g'));
                /*if(d.length==1){
                    return $sce.trustAsHtml(d[0]);
                }
                else if(d.length>2){
                    d[1] = d[1].substring(0, (strLength-3));
                    return $sce.trustAsHtml(d[0]+'<br/>'+d[1]+'...');
                }else{
                    return $sce.trustAsHtml(d[0]+'...');
                }*/
                //return data;
            }
        };
    })
    .directive('changeColor',function(){
        return {
            restrict: 'AE',
            scope: {
                ngModel: '='
            },
            link : function(scope,element,attr,ngModel){
                element.bind('click',function(){

                });
            }
        }
    })
//Window.prototype.btoa = function(decodedData) {};
//Window.prototype.atob = function(encodedData) {};
//Prevent click if href="#"
function preventClickDirective() {
    var directive = {
        restrict: 'E',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {
        if (attrs.href === '#'){
            element.on('click', function(event){
                event.preventDefault();
            });
        }
    }
}

//Bootstrap Collapse
function bootstrapCollapseDirective() {
    var directive = {
        restrict: 'E',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {
        if (attrs.toggle=='collapse'){
            element.attr('href','javascript;;').attr('data-target',attrs.href.replace('index.html',''));
        }
    }
}

/**
* @desc Genesis main navigation - Siedebar menu
* @example <li class="nav-item nav-dropdown"></li>
*/
function navigationDirective() {
    var directive = {
        restrict: 'E',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {
        if(element.hasClass('nav-dropdown-toggle') && angular.element('body').hasClass('sidebar-nav') && angular.element('body').width() > 782) {
            element.on('click', function(){
                if(!angular.element('body').hasClass('compact-nav')) {
                    element.parent().toggleClass('open').find('.open').removeClass('open');
                }
            });
        } else if (element.hasClass('nav-dropdown-toggle') && angular.element('body').width() < 783) {
            element.on('click', function(){
                element.parent().toggleClass('open').find('.open').removeClass('open');
            });
        }
    }
}

//Dynamic resize .sidebar-nav
sidebarNavDynamicResizeDirective.$inject = ['$window', '$timeout'];
function sidebarNavDynamicResizeDirective($window, $timeout) {
    var directive = {
        restrict: 'E',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {

        if (element.hasClass('sidebar-nav') && angular.element('body').hasClass('fixed-nav')) {
            var bodyHeight = angular.element(window).height();
            scope.$watch(function(){
                var headerHeight = angular.element('header').outerHeight();

                if (angular.element('body').hasClass('sidebar-off-canvas')) {
                    element.css('height', bodyHeight);
                } else {
                    element.css('height', bodyHeight - headerHeight);
                }
            })

            angular.element($window).bind('resize', function(){
                var bodyHeight = angular.element(window).height();
                var headerHeight = angular.element('header').outerHeight();
                var sidebarHeaderHeight = angular.element('.sidebar-header').outerHeight();
                var sidebarFooterHeight = angular.element('.sidebar-footer').outerHeight();

                if (angular.element('body').hasClass('sidebar-off-canvas')) {
                    element.css('height', bodyHeight - sidebarHeaderHeight - sidebarFooterHeight);
                } else {
                    element.css('height', bodyHeight - headerHeight - sidebarHeaderHeight - sidebarFooterHeight);
                }
            });
        }
    }
}

//LayoutToggle
layoutToggleDirective.$inject = ['$interval'];
function layoutToggleDirective($interval) {
    var directive = {
        restrict: 'E',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {
        element.on('click', function(){

            var bodyClass = localStorage.getItem('body-class');

            if ((element.hasClass('layout-toggler') || element.hasClass('sidebar-close')) && angular.element('body').hasClass('sidebar-off-canvas')) {
                angular.element('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');

                $interval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 100, 5)

            } else if (element.hasClass('layout-toggler') && (angular.element('body').hasClass('sidebar-nav') || bodyClass == 'sidebar-nav')) {
                angular.element('body').toggleClass('sidebar-nav');
                localStorage.setItem('body-class', 'sidebar-nav');
                if (bodyClass == 'sidebar-nav') {
                    localStorage.clear();
                }

                $interval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 100, 5)
            }

            if (element.hasClass('aside-toggle')) {
                angular.element('body').toggleClass('aside-menu-open');

                $interval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 100, 5)
            }
        });
    }
}

//Collapse menu toggler
function collapseMenuTogglerDirective() {
    var directive = {
        restrict: 'E',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {
        element.on('click', function(){
            if (element.hasClass('navbar-toggler') && !element.hasClass('layout-toggler')) {
                angular.element('body').toggleClass('mobile-open')
            }
        })
    }
}

//Bootstrap Carousel
function bootstrapCarouselDirective() {
    var directive = {
        restrict: 'E',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {
        if (attrs.ride=='carousel'){
            element.find('a').each(function(){
                $(this).attr('data-target',$(this).attr('href').replace('index.html','')).attr('href','javascript;;')
            });
        }
    }
}

//Bootstrap Tooltips & Popovers
function bootstrapTooltipsPopoversDirective() {
    var directive = {
        restrict: 'A',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {
        if (attrs.toggle=='tooltip'){
            angular.element(element).tooltip();
        }
        if (attrs.toggle=='popover'){
            angular.element(element).popover();
        }
    }
}

//Bootstrap Tabs
function bootstrapTabsDirective() {
    var directive = {
        restrict: 'A',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {
        element.click(function(e) {
            e.preventDefault();
            angular.element(element).tab('show');
        });
    }
}

//Card Collapse
function cardCollapseDirective() {
    var directive = {
        restrict: 'E',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {
        if (attrs.toggle=='collapse' && element.parent().hasClass('card-actions')){

            if (element.parent().parent().parent().find('.card-block').hasClass('in')) {
                element.find('i').addClass('r180');
            }

            var id = 'collapse-' + Math.floor((Math.random() * 1000000000) + 1);
            element.attr('data-target','#'+id)
            element.parent().parent().parent().find('.card-block').attr('id',id);

            element.on('click', function(){
                element.find('i').toggleClass('r180');
            })
        }
    }
}
