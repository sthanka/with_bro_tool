angular
    .module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', '$httpProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider, $httpProvider, $locationProvider) {
        $locationProvider.hashPrefix('');
        //$urlRouterProvider.otherwise('/404');
        $urlRouterProvider.otherwise(function ($injector, $location) {
            $injector.invoke(['$state', function ($state) {
                $state.go('app.404', null, {location: false});
            }]);
        });
        $urlRouterProvider.when('', '/');
        // $urlRouterProvider.when('/', '/dashboard');
        //if null or '/' empty url there ,redirect to first menu url
        $urlRouterProvider.when('/', ['$state', '$localStorage', '$location', function ($state, $localStorage, $location) {
            if ($localStorage.curUser && !angular.equals({}, $localStorage.curUser)) {
                var menuObj = $localStorage.curUser.data.menu;
                if (Array.isArray(menuObj) && menuObj.length > 0 && menuObj[0].module_url) {
                    $location.path(menuObj[0].module_url);
                } else {
                    $state.go('app.404', null, {location: false});
                }
            } else {
                $state.go('appSimple.login');
            }

        }]);

        $ocLazyLoadProvider.config({
            // Set to true if you want to see what and when is dynamically loaded
            debug: false
        });
        //console.log(' $breadcrumbProvider.get()', $breadcrumbProvider);
        $breadcrumbProvider.setOptions({
            prefixStateName: 'app',
            includeAbstract: true,
            template: '<li class="breadcrumb-item" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a class="f16"  ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a class="f16"><span ng-switch-when="true" >{{step.ncyBreadcrumbLabel}}</span></li>'
        });
        $stateProvider
            .state('appSimple', {
                abstract: false,
                templateUrl: 'views/common/layouts/simple.html',
                resolve: {
                    loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            serie: true,
                            name: 'Font Awesome',
                            files: ['css/font-awesome.min.css']
                        }, {
                            serie: true,
                            name: 'Simple Line Icons',
                            files: ['css/simple-line-icons.css']
                        }, {
                            serie: true,
                            name: 'Login Styles',
                            files: ['views/pages/login.css']
                        }]);
                    }],
                }
            })
            .state('appSimple.login', {
                url: '/login',
                controller: 'loginCtrl',
                templateUrl: 'views/pages/login.html'
            })
            .state('appSimple.forgotPassword', {
                url: '/forgot-password',
                controller: 'loginCtrl',
                templateUrl: 'views/user/forgot-password.html',
            })
            .state('appSimple.invitation-expire', {
                url: '/invitation-expire',
                templateUrl: 'views/user/invitation-expire.html',
            })
            .state('appSimple.welcome-user', {
                url: '/welcome-user',
                templateUrl: 'views/user/welcome-user.html',
            })
            .state('appSimple.error-messages', {
                url: '/error-messages',
                templateUrl: 'views/user/error-messages.html',
            })

            .state('app', {
                abstract: true,
                templateUrl: 'views/common/layouts/full.html',
                ncyBreadcrumb: {
                    label: 'Home',
                    skip: true
                },
                controller: 'fullLayoutCtrl',
                resolve: {
                    authenticate: ['AuthService', '$state', '$q', '$timeout', '$rootScope', function (AuthService, $state, $q, $timeout, $rootScope) {
                        var deferred = $q.defer();
                        if (AuthService.login()) {
                            var temp = angular.fromJson(AuthService.getFields());
                            $rootScope.id_user = temp.data.data.id_user;
                            $rootScope.user_name = temp.data.data.first_name +" "+temp.data.data.last_name;
                            $rootScope.first_name = temp.data.data.first_name;
                            $rootScope.last_name = temp.data.data.last_name;
                            $rootScope.email = temp.data.data.email;
                            $rootScope.user_role_id = temp.data.data.user_role_id;
                            $rootScope.user_role_name = temp.data.data.user_role_name;
                            $rootScope.profile_image = temp.data.data.profile_image;
                            $rootScope.profile_image_medium = temp.data.data.profile_image_medium;
                            $rootScope.profile_image_small = temp.data.data.profile_image_small;
                            $rootScope.access = temp.data.data.access;
                            deferred.resolve();
                        } else {
                            $timeout(function() {
                                $state.go('appSimple.login');
                            },0)
                            return $q.reject();
                        }
                        return deferred.promise;
                    }],
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            serie: true,
                            name: 'Font Awesome',
                            files: ['css/font-awesome.min.css']
                        }, {
                            serie: true,
                            name: 'Simple Line Icons',
                            files: ['css/simple-line-icons.css']
                        }, {
                            serie: true,
                            name: 'Icomoon Icons',
                            files: ['css/icomoon-icons.css']
                        }]);
                    }]
                }
            })
            .state('app.404', {
                url: '/404',
                templateUrl: 'views/pages/404.html',
                ncyBreadcrumb: {
                    label: '404'
                }
            })
            .state('app.notifiation',{
                url: '/notifications/:date',
                templateUrl :'views/user/notifications-details.html',
                controller: 'notificationCtrl',
                activeLink: 'Notifications',
                ncyBreadcrumb: {
                    parent: 'app.notifiationList',
                    label: 'Notifications Details'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'Notifications JS',
                            files: ['views/user/notificationController.js']
                        });
                    }]
                }
            })
            .state('app.notifiationList',{
                url: '/notification/list',
                templateUrl :'views/user/notifications-list.html',
                controller: 'notificationListCtrl',
                activeLink: 'Notifications',
                ncyBreadcrumb: {
                    label: 'Notifications'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'Notifications JS',
                            files: ['views/user/notificationController.js']
                        });
                    }]
                }
            })
            .state('app.dashboard', {
                url: '/dashboard',
                templateUrl: 'views/dashboard/user-dashboard.html',
                controller: 'dashboardCtrl',
                activeLink: 'dashboard',
                ncyBreadcrumb: {
                    label: 'Dashboard'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'Dashboard JS',
                            files: ['views/dashboard/dashboardController.js']
                        });
                    }]
                }
            })
            .state('app.dashboard2', {
                url: '/dashboard',
                templateUrl: 'views/dashboard/user-dashboard.html',
                controller: 'dashboardCtrl',
                activeLink: 'dashboard',
                ncyBreadcrumb: {
                    label: 'Dashboard'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'Dashboard JS',
                            files: ['views/dashboard/dashboardController.js']
                        });
                    }]
                }
            })
            .state('app.myProfile', {
                url: '/my-account/my-profile?:id',
                controller: 'profileCtrl',
                activeLink: 'My Profile',
                ncyBreadcrumb: {
                    label: 'My Profile'
                },
                templateUrl: 'views/user/user-profile.html',
                resolve: {
                    checkPermission: checkPermission,
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load controllers
                        return $ocLazyLoad.load({
                            files: ['js/controllers/main.js']
                        });
                    }]
                }
            })
            .state('app.changePassword', {
                url: '/my-account/change-password?:id',
                controller: 'profileCtrl',
                activeLink: 'Change Password',
                ncyBreadcrumb: {
                    label: 'Change Password'
                },
                templateUrl: 'views/user/change-password.html',
                resolve: {
                    checkPermission: checkPermission,
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load controllers
                        return $ocLazyLoad.load({
                            files: ['js/controllers/main.js']
                        });
                    }]
                }
            })
            .state('app.companySetup',{
                url: '/my-account/company-setup',
                controller: 'companySetupCtrl',
                templateUrl: 'views/user/change-company-details.html',
                activeLink: 'Company Setup',
                ncyBreadcrumb: {
                    label: 'Company Setup'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load controllers
                        return $ocLazyLoad.load({
                            files: ['js/controllers/main.js']
                        });
                    }]
                }
            })
            .state('app.contractDeleted',{
                template: '<ui-view></ui-view>',
                controller: 'deletedContractListCtrl',
                activeLink : 'Deleted Contract List',
                ncyBreadcrumb: {
                    label: 'deleted Contracts',
                    skip: true
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Contract JS',
                            files: ['views/Manage-Users/contracts/contractsController.js']
                        }]);
                    }],
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['attachment']);
                    }
                }
            })
            .state('app.contractDeleted.deleted-contracts', {
                url: '/deleted-contracts',
                templateUrl: 'views/Manage-Users/contracts/deleted-contracts.html',
                activeLink: 'Deleted Contract List',
                ncyBreadcrumb: {
                    label: 'Deleted Contracts'
                }
            })
            .state('app.contract',{
                template: '<ui-view></ui-view>',
                controller: 'contractOverviewCtrl',
                activeLink : 'contracts',
                ncyBreadcrumb: {
                    label: 'Contracts',
                    skip: true
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Contract JS',
                            files: ['views/Manage-Users/contracts/contractsController.js']
                        }]);
                    }],
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['attachment']);
                    }
                }
            })
            .state('app.contract.contract-overview', {
                url: '/contract?:pname?:status',
                templateUrl: 'views/Manage-Users/contracts/contract-overview.html',
                controller:'contractListCtrl',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Contracts Overview'
                }
            })
            .state('app.contract.view', {
                url: '/contract/view/:name/:id',
                templateUrl: 'views/Manage-Users/contracts/contract-details.html',
                controller:'contractViewCtrl',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Contract Details',
                    parent: 'app.contract.contract-overview'
                }
            })
            .state('app.contract.create-contract', {
                url: '/contract/create-contract',
                templateUrl: 'views/Manage-Users/contracts/create-edit-contract.html',
                controller:'createContractCtrl',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Create Contract',
                    parent: 'app.contract.contract-overview'
                }
            })
            .state('app.contract.edit-contract', {
                url: '/contract/edit/:name/:id',
                templateUrl: 'views/Manage-Users/contracts/create-edit-contract.html',
                controller:'createContractCtrl',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Edit Contract',
                    parent: 'app.contract.contract-overview'
                }
            })
            .state('app.contract.create-sub-contract', {
                url: '/contract/sub-create/:name/:id',
                templateUrl: 'views/Manage-Users/contracts/create-sub-contract.html',
                controller:'subContractCreateCtrl',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Create Sub-Contract',
                    parent: 'app.contract.contract-overview'
                }
            })
            .state('app.contract.review-action-item', {
                url: '/contract/review-action-item/:name/:id',
                templateUrl: 'views/Manage-Users/contracts/contract-review-list.html',
                controller:'contractReviewActionItemCtrl',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Review Action Item',
                    parent: 'app.contract.contract-overview'
                }
            })
            .state('app.contract.contract-review', {
                url: '/contract/contract-review/:name/:id/:rId',
                templateUrl: 'views/Manage-Users/contracts/contract-review.html',
                controller : 'contractReviewCtrl',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Contract Review',
                    parent: 'app.contract.contract-overview'
                }
            })
            .state('app.contract.review-change-log', {
                url: '/contract/contract-review-change-log/:name/:id/:rId',
                controller:'ReviewChangeLogCtrl',
                templateUrl: 'views/Manage-Users/contracts/contract-review-change-log.html',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Change Log',
                    parent: 'app.contract.contract-review'
                }
            })
            .state('app.contract.review-design', {
                url: '/contract/contract-review-design/:name/:id/:rId',
                controller:'ReviewDesign',
                templateUrl: 'views/Manage-Users/contracts/contract-review-discussion.html',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Review Discussion',
                    parent: 'app.contract.contract-review'
                }
            })
            .state('app.contract.contract-module-review', {
                url: '/contract/contract-module-review/:name/:id/:rId/:mName/:moduleId/:tName/:tId',
                templateUrl: 'views/Manage-Users/contracts/contract-module-review.html',
                controller: 'contractModuleReviewCtrl',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Contract Module',
                    parent: 'app.contract.contract-review'
                }
            })
            .state('app.contract.contract-dashboard', {
                url: '/contract/contract-dashboard/:name/:id?:rId',
                templateUrl: 'views/Manage-Users/contracts/contract-dashboard.html',
                controller: 'contractDashboardCtrl',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Contract Dashboard',
                    parent: 'app.contract.view'
                }
            })
            .state('app.contract.contract-log',{
                url: '/contract/contract-logs/:name/:id',
                templateUrl: 'views/Manage-Users/contracts/contract-logs.html',
                controller : 'contractLogCtrl',
                activeLink: 'Contracts',
                ncyBreadcrumb: {
                    label: 'Contract Logs',
                    parent: 'app.contract.view'
                },
            })
            .state('app.actionItems', {
                url: '/action-items?:id?:cId?:status',
                templateUrl: 'views/Manage-Users/action-items/action-items-overview.html',
                controller:'actionItemCtrl',
                activeLink : 'Action Items',
                ncyBreadcrumb: {
                    label: 'Action Items'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Contract JS',
                            files: ['views/Manage-Users/action-items/actionItemController.js']
                        }]);
                    }]
                }
            })
            .state('app.customer', {
                template: '<ui-view></ui-view>',
                controller: 'customerCtrl',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Customer',
                    skip: true
                },
            })
            .state('app.customer.customer-list', {
                url: '/customers',
                controller: 'customerListCtrl',
                templateUrl: 'views/customers/customer-list.html',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Customers',
                    parent:'app.customer'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer JS',
                            files: ['views/customers/customerCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.customer.edit-customer', {
                url: '/customers/edit/:name/:id',
                controller: 'addCustomerCtrl',
                templateUrl: 'views/customers/create-edit-customer.html',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Edit Customer',
                    parent:'app.customer.customer-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer JS',
                            files: ['views/customers/customerCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.customer.create-customer', {
                url: '/customers/add',
                controller: 'addCustomerCtrl',
                templateUrl: 'views/customers/create-edit-customer.html',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Create Customer',
                    parent:'app.customer.customer-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer JS',
                            files: ['views/customers/customerCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.customer.manage-templates', {
                url: '/customers/manage-templates/:name/:id',
                controller: 'ManageTemplatesCtrl',
                templateUrl: 'views/customers/customer-manage-templates.html',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Manage Template',
                    parent:'app.customer.customer-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer JS',
                            files: ['views/customers/customerCtrl.js']
                        }]);
                    }]
                }
            })

            .state('app.manage-admin', {
                template: '<ui-view></ui-view>',
                controller: 'customerCtrl',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Admin',
                    parent:'app.customer.customer-list',
                    skip : true
                },
            })
            .state('app.manage-admin.admin-list', {
                url: '/customers/admin/list/:name/:id',
                controller: 'customerAdminListCtrl',
                templateUrl: 'views/customers/manage-admin-list.html',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Admin',
                    parent:'app.customer.customer-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer JS',
                            files: ['views/customers/customerCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.manage-admin.edit-admin', {
                url: '/customers/admin/edit/:id/:name/:userId',
                controller: 'addAdminCtrl',
                templateUrl: 'views/customers/create-edit-customer-admin.html',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Edit Admin',
                    parent:'app.manage-admin.admin-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer JS',
                            files: ['views/customers/customerCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.manage-admin.create-admin', {
                url: '/customers/admin/add/:name/:id',
                controller: 'addAdminCtrl',
                templateUrl: 'views/customers/create-edit-customer-admin.html',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Create Admin',
                    parent:'app.manage-admin.admin-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer JS',
                            files: ['views/customers/customerCtrl.js']
                        }]);
                    }]
                }
            })

            .state('app.manage-user', {
                template: '<ui-view></ui-view>',
                controller: 'customerUserCtrl',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'User',
                    parent:'app.customer.customer-list',
                    skip : true
                },
            })
            .state('app.manage-user.user-list', {
                url: '/customers/user/list/:name/:id',
                controller: 'customerUserListCtrl',
                templateUrl: 'views/customers/manage-user-list.html',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'User',
                    parent:'app.customer.customer-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer JS',
                            files: ['views/customers/customerCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.manage-user.edit-user', {
                url: '/customers/user/edit/:id/:name/:userId',
                controller: 'addUserCtrl',
                templateUrl: 'views/customers/create-edit-customer-user.html',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Edit User',
                    parent:'app.manage-user.user-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer JS',
                            files: ['views/customers/customerCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.manage-user.create-user', {
                url: '/customers/user/add/:name/:id',
                controller: 'addUserCtrl',
                templateUrl: 'views/customers/create-edit-customer-user.html',
                activeLink : 'customers',
                ncyBreadcrumb: {
                    label: 'Create User',
                    parent:'app.manage-user.user-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer JS',
                            files: ['views/customers/customerCtrl.js']
                        }]);
                    }]
                }
            })

            .state('app.module', {
                template: '<ui-view></ui-view>',
                controller: 'moduleCtrl',
                activeLink : 'modules',
                ncyBreadcrumb: {
                    label: 'Module',
                    skip : true
                },
            })
            .state('app.module.module-list', {
                url: '/modules',
                controller: 'moduleListCtrl',
                templateUrl: 'views/modules/module-list.html',
                activeLink : 'modules',
                ncyBreadcrumb: {
                    label: 'Modules'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Module JS',
                            files: ['views/modules/moduleController.js']
                        }]);
                    }]
                }
            })
            .state('app.module.module-topic-list', {
                url: '/modules/topics/:name/:id',
                controller: 'moduleTopicController',
                templateUrl: 'views/modules/manage-topic.html',
                activeLink : 'modules',
                ncyBreadcrumb: {
                    label: 'Topics',
                    parent:'app.module.module-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Module JS',
                            files: ['views/modules/moduleController.js']
                        }]);
                    }]
                }
            })
            /**/
            .state('app.settings', {
                url: '/settings',
                controller: 'settingsCtrl',
                templateUrl: 'views/settings/settings-list.html',
                activeLink : 'settings',
                ncyBreadcrumb: {
                    label: 'Settings'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Module JS',
                            files: ['views/settings/settingsCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.relationship_category', {
                template: '<ui-view></ui-view>',
                controller: 'relationCategoryCtrl',
                activeLink : 'relationship categories',
                ncyBreadcrumb: {
                    label: 'Relationship Categories',
                    skip: true
                },
            })
            .state('app.relationship_category.list', {
                url: '/relationship_category',
                controller: 'relationCategoryCtrl',
                templateUrl: 'views/relation-category/relation-category-list.html',
                activeLink : 'relationship categories',
                ncyBreadcrumb: {
                    label: 'Relationship Categories'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Category relation JS',
                            files: ['views/relation-category/relationCategoryCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.relationship_category.relationship_classification', {
                url: '/relationship_category/relationship_classification/list',
                controller: 'relationshipClassificationCtrl',
                templateUrl: 'views/relationship_classification/relationship_classification_list.html',
                activeLink : 'relationship categories',
                ncyBreadcrumb: {
                    label: 'Relationship Classification'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Relationship ClassificationJS',
                            files: ['views/relationship_classification/relationshipClassificationCtrl.js']
                        }]);
                    }]
                }
            })

            .state('app.calender',{
                template: '<ui-view></ui-view>',
                controller: 'calenderCtrl',
                ncyBreadcrumb: {
                    label: 'Calendar',
                    skip: true
                },
                activeLink : 'Calendar',
                resolve : {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'Calender JS',
                            files: ['views/calender/calenderCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.calender.view', {
                url: '/calendar',
                controller: 'calenderCtrl',
                templateUrl: 'views/calender/calender.html',
                activeLink : 'calendar',
                ncyBreadcrumb: {
                    label: 'Calendar'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Calender JS',
                            files: ['views/calender/calenderCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.calender.year', {
                url: '/calendar/full-calendar/:year',
                controller: 'fullcalenderCtrl',
                templateUrl: 'views/calender/fullCalender.html',
                activeLink : 'calendar',
                ncyBreadcrumb: {
                    label: 'Year Calendar',
                    parent: 'app.calender.view'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Calender JS',
                            files: ['views/calender/calenderCtrl.js']
                        }]);
                    }]
                }
            })
           /* .state('app.fullcalender', {
                url: '/full-calendar/:year',
                controller: 'fullcalenderCtrl',
                templateUrl: 'views/calender/fullCalender.html',
                activeLink : 'calendar',
                ncyBreadcrumb: {
                    label: 'Calendar'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Calender JS',
                            files: ['views/calender/calenderCtrl.js']
                        }]);
                    }]
                }
            })*/
            .state('app.email-templates',{
                template: '<ui-view></ui-view>',
                controller: 'emailTempaltesCtrl',
                ncyBreadcrumb: {
                    label: 'Email Templates',
                    skip: true
                },
                activeLink : 'Email Templates',
                resolve : {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Email Tempaltes JS',
                            files: ['views/email-templates/emailTemplatesCtrl.js']
                        }]);
                    }],
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['ckeditor']);
                    }
                }
            })
            .state('app.email-templates.list',{
                url: '/email-templates',
                controller:"emailTempaltesCtrl",
                templateUrl: 'views/email-templates/email-templates.html',
                activeLink : 'Email Templates',
                ncyBreadcrumb: {
                    label: 'Email Templates'
                },
                resolve : {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Email Tempaltes JS',
                            files: ['views/email-templates/emailTemplatesCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.email-templates.edit',{
                url: '/email-templates/edit/:name/:id',
                controller:"emailTempaltesCtrl",
                templateUrl: 'views/email-templates/edit-email-templates.html',
                activeLink : 'Email Templates',
                ncyBreadcrumb: {
                    label: 'Edit Email Template',
                    parent: 'app.email-templates.list'
                }
            })

            .state('app.questions',{
                template: '<ui-view></ui-view>',
                controller: 'questionsCtrl',
                activeLink : 'questions',
                ncyBreadcrumb: {
                    label: 'Questions',
                    skip: true
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['ui.sortable']);
                    }
                }
            })
            .state('app.questions.questions-list',{
                url: '/questions',
                controller: 'questionsListCtrl',
                templateUrl: 'views/questions/questions-talbe-list.html',
                activeLink : 'questions',
                ncyBreadcrumb: {
                    label: 'Questions',
                    parent:'app.questions'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Question JS',
                            files: ['views/questions/questionsCtrl.js']
                        }]);
                    }]
                }
            })
            /*.state('app.questions.topic-questions',{
                url : '/question/topic/:name/:id',
                templateUrl : '',
                controller:'',
                ncyBreadcrumb: {
                    label: 'Topic Questions',
                    parent:'app.questions.questions-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Question JS',
                            files: ['views/questions/questionsCtrl.js']
                        }]);
                    }]
                }
            })*/
            .state('app.questions.questions-view', {
                url: '/questions/view/:mName/:name/:id',
                templateUrl: 'views/questions/questions-view.html',
                controller:"questionsView",
                activeLink : 'questions',
                ncyBreadcrumb: {
                    label: 'Topic Questions',
                    parent:'app.questions.questions-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Question JS',
                            files: ['views/questions/questionsCtrl.js']
                        }]);
                    }]
                }
            })

            .state('app.co-workers', {
                url: '/co-workers',
                templateUrl: 'views/co-workers.html',
                ncyBreadcrumb: {
                    label: 'Co Workers'
                }
            })
            .state('app.contract-review2', {
                url: '/contract-review2',
                templateUrl: 'views/contract-module-review.html',
                ncyBreadcrumb: {
                    label: 'Contract Review2'
                }
            })

            .state('app.templates', {
                template: '<ui-view></ui-view>',
                controller: 'templatesCtrl',
                activeLink : 'templates',
                ncyBreadcrumb: {
                    label: 'Templates',
                    skip: true
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Templates JS',
                            files: ['views/templates/templateCtrl.js']
                        }]);
                    }],
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['ui.sortable']);
                    }
                }
            })
            .state('app.templates.templates-list', {
                url: '/templates',
                templateUrl: 'views/templates/templates-list.html',
                controller:"templatesListCtrl",
                activeLink : 'templates',
                ncyBreadcrumb: {
                    label: 'Templates',
                    parent : 'app.templates'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Templates JS',
                            files: ['views/templates/templateCtrl.js']
                        }]);
                    }],
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['ui.sortable']);
                    }
                }
            })
            .state('app.templates.templates-preview', {
                url: '/templates/preview/:name/:id',
                templateUrl: 'views/templates/templates-preview.html',
                controller:"templatesView",
                activeLink : 'templates',
                ncyBreadcrumb: {
                    label: 'Template Preview',
                    parent : 'app.templates.templates-list'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Templates JS',
                            files: ['views/templates/templateCtrl.js']
                        }]);
                    }],
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['ui.sortable']);
                    }
                }
            })
            .state('app.templates.templates-view', {
                url: '/templates/view/:name/:id',
                templateUrl: 'views/templates/template-tree-view.html',
                controller:"templatesTreeView",
                activeLink : 'templates',
                ncyBreadcrumb: {
                    label: 'Manage Template',
                    parent : 'app.templates.templates-list'
                }
            })
             /*.state('app.templates.templates-view', {
                templateUrl: 'views/templates/tempalte-view.html',
                controller:"manageTemplateCtrl",
                activeLink : 'templates',
                ncyBreadcrumb: {
                    label: 'Templates',
                    parent : 'app.templates'
                }
            })
            .state('app.templates.templates-view.module', {
                url: '/templates/module/:name/:id',
                templateUrl: 'views/templates/templates-view-module.html',
                controller:"manageModuleTemplateCtrl",
                activeLink : 'Templates',
                ncyBreadcrumb: {
                    label: 'Modules',
                    parent : 'app.templates.templates-list'
                }
            })
            .state('app.templates.templates-view.topic', {
                url: '/templates/topic/:name/:id',
                templateUrl: 'views/templates/template-topic-view.html',
                activeLink : 'Templates',
                controller:"manageTopicTemplateCtrl",
                ncyBreadcrumb: {
                    label: 'Topics',
                    parent : 'app.templates.templates-list'
                }
            })
            .state('app.templates.templates-view.questions', {
                url: '/templates/question/:name/:id',
                templateUrl: 'views/templates/template-view-questions.html',
                controller:"manageQuestionsTemplateCtrl",
                activeLink : 'Templates',
                ncyBreadcrumb: {
                    label : 'Questions',
                    parent: 'app.templates.templates-list'
                }
            })*/

            .state('app.bussiness_unit', {
                template: '<ui-view></ui-view>',
                controller: 'bussinessUnitCtrl',
                activeLink: 'business unit',
                ncyBreadcrumb: {
                    label: 'Business Unit',
                    skip: true
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Bussiness Unit JS',
                            files: ['views/bussiness_unit/bussinessCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.bussiness_unit.list', {
                url: '/business-unit',
                controller: 'bussinessUnitCtrl',
                templateUrl: 'views/bussiness_unit/bussiness_unit_list.html',
                activeLink: 'business unit',
                ncyBreadcrumb: {
                    label: 'Business Unit',
                    parent : 'app.bussiness_unit'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Bussiness Unit JS',
                            files: ['views/bussiness_unit/bussinessCtrl.js']
                        }]);
                    }]
                }
            })
            .state('app.bussiness_unit.create', {
                url: '/business-unit/create',
                controller: 'bussinessUnitCreateCtrl',
                templateUrl: 'views/bussiness_unit/create-edit-bussiness-unit.html',
                activeLink: 'business unit',
                ncyBreadcrumb: {
                    label: 'Add Business Unit',
                    parent : 'app.bussiness_unit.list'
                }
            })
            .state('app.bussiness_unit.edit', {
                url: '/business-unit/edit?:id',
                controller: 'bussinessUnitEditCtrl',
                templateUrl: 'views/bussiness_unit/create-edit-bussiness-unit.html',
                activeLink: 'business unit',
                ncyBreadcrumb: {
                    label: 'Edit Business Unit',
                    parent : 'app.bussiness_unit.list'
                }
            })

            .state('app.customer-user', {
                template: '<ui-view></ui-view>',
                controller: 'manageUserCtrl',
                activeLink : 'users',
                ncyBreadcrumb: {
                    label: 'User',
                    skip : true
                },
                resolve : {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Manage Customer User JS',
                            files: ['views/Manage-Users/customer-users/customerUsersController.js']
                        }]);
                    }]
                }
            })
            .state('app.customer-user.list',{
                url: '/users?:buId',
                templateUrl: 'views/Manage-Users/customer-users/customer-users-list.html',
                activeLink : 'users',
                controller:"UserCtrl",
                ncyBreadcrumb: {
                    label: 'Users'
                },
                resolve : {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Manage Customer User JS',
                            files: ['views/Manage-Users/customer-users/customerUsersController.js']
                        }]);
                    }]
                }
            })

            .state('app.customer-user.create-customer-user',{
                url : '/users/create/:id',
                templateUrl:'views/Manage-Users/customer-users/create-edit-customer-user.html',
                controller:'addCustomUserCtrl',
                activeLink : 'users',
                ncyBreadcrumb:{
                    label: 'Create User',
                    parent:'app.customer-user.list'
                }
            })
            .state('app.customer-user.edit-customer-user',{
                url : '/users/edit/:id/:userId',
                templateUrl:'views/Manage-Users/customer-users/create-edit-customer-user.html',
                controller:'addCustomUserCtrl',
                activeLink : 'users',
                ncyBreadcrumb:{
                    label: 'Edit User',
                    parent:'app.customer-user.list'
                }
            })

            .state('app.customer-relationship_category', {
                template: '<ui-view></ui-view>',
                controller: 'customerRelationCategoryCtrl',
                activeLink: 'relationship categories',
                ncyBreadcrumb: {
                    label: 'Relationship Categories',
                    skip: true
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer Category relation JS',
                            files: ['views/Manage-Users/relationship-category/relationshipCategoryController.js']
                        }]);
                    }]
                }
            })
            .state('app.customer-relationship_category.list', {
                url: '/customer_relationship_category',
                controller: 'customerRelationCategoryCtrl',
                templateUrl: 'views/Manage-Users/relationship-category/customer-relation-category-list.html',
                activeLink: 'relationship categories',
                ncyBreadcrumb: {
                    label: 'Relationship Categories'
                },
                resolve: {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer Category relation JS',
                            files: ['views/Manage-Users/relationship-category/relationshipCategoryController.js']
                        }]);
                    }]
                }
            })
            .state('app.customer-relationship_category.customer-relationship_classification', {
                url: '/customer_relationship_category/relationship_classification/list',
                controller: 'customerRelationshipClassificationCtrl',
                templateUrl: 'views/Manage-Users/relationship-category/relationship_classification/customer-relationship_classification_list.html',
                activeLink: 'relationship categories',
                ncyBreadcrumb: {
                    label: 'Relationship Classification'
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Customer Relationship Classification JS',
                            files: ['views/Manage-Users/relationship-category/relationship_classification/relationshipClassificationController.js']
                        }]);
                    }]
                }
            })

            .state('app.reports',{
                template: '<ui-view></ui-view>',
                ncyBreadcrumb: {
                    label: 'Reporting',
                    skip : true
                },
                resolve : {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            name: 'Reporting',
                            files: ['views/reports/reportsController.js']
                        }]);
                    }],
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['ui.sortable']);
                    }
                }
            })
            .state('app.reports.reporting',{
                url: '/reports',
                templateUrl: 'views/reports/reporting.html',
                controller:"reportsCtrl",
                activeLink: 'Reports',
                ncyBreadcrumb: {
                    label: 'Reports'
                },
                resolve: {
                    checkPermission: checkPermission
                }
            })
            .state('app.reports.create-report',{
                url: '/reports/create-report?:bu?:cl?:st?:con?:id?:name',
                templateUrl: 'views/reports/create-report.html',
                controller:"createReportsCtrl",
                activeLink: 'Reports',
                ncyBreadcrumb: {
                    label: 'Create Report',
                    parent : 'app.reports.reporting'
                }
            })
            .state('app.reports.report-view',{
                url: '/reports/report-view/:bu/:cl/:st?:con?:id?:old',
                templateUrl: 'views/reports/report-view.html',
                controller:"generateReportsCtrl",
                activeLink: 'Reports',
                ncyBreadcrumb: {
                    label: 'View Report',
                    parent : 'app.reports.reporting'
                }
            })
            .state('app.reports.report-edit',{
                url: '/reports/report-edit?:bu?:cl?:st?:con?:id?:old?:name',
                templateUrl: 'views/reports/report-edit.html',
                controller:"generateReportsCtrl",
                activeLink: 'Reports',
                ncyBreadcrumb: {
                    label: 'Edit Report',
                    parent : 'app.reports.reporting'
                }
            })

            .state('app.admin-logs', {
                template: '<ui-view></ui-view>',
                activeLink : 'Customer Usage',
                ncyBreadcrumb: {
                    label: 'Customers',
                    skip:true
                }
            })
            .state('app.admin-logs.customers-list', {
                url:'/customer-usage',
                templateUrl: 'views/History/admin/log-customers-list.html',
                controller:'withAdminHistoryCtrl',
                activeLink: 'Customer Usage',
                ncyBreadcrumb: {
                    label: 'Customers'
                },
                resolve : {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'Customers',
                            files: ['views/History/admin/historyController.js']
                        }]);
                    }]
                }
            })
            .state('app.admin-logs.customer-user-list', {
                url:'/customer-usage/users/:cName/:cId',
                templateUrl: 'views/History/admin/log-customer-users-list.html',
                controller:'withAdminUsersCtrl',
                activeLink: 'Customer Usage',
                ncyBreadcrumb: {
                    label: 'Users',
                    parent : 'app.admin-logs.customers-list'
                },
                resolve : {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'Customers',
                            files: ['views/History/admin/historyController.js']
                        }]);
                    }]
                }
            })
            .state('app.admin-logs.customer-user-logs', {
                url:'/customer-usage/user/logs/:cName/:cId/:uName/:id?:from?:to',
                templateUrl: 'views/History/admin/logs-users-list.html',
                controller:'withAdminUserLogsCtrl',
                activeLink: 'Customer Usage',
                ncyBreadcrumb: {
                    label: 'Login History',
                    parent : 'app.admin-logs.customer-user-list'
                },
                resolve : {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'Customers',
                            files: ['views/History/admin/historyController.js']
                        }]);
                    }]
                }
            })
            .state('app.admin-logs.customer-user-actions', {
                url:'/customer-usage/user/actions/:cName/:cId/:uName/:id/:from/:to/:token',
                templateUrl: 'views/History/admin/log-customer-user-actions.html',
                controller:'withAdminUserActionsCtrl',
                activeLink: 'Customer Usage',
                ncyBreadcrumb: {
                    label: 'Actions',
                    parent : 'app.admin-logs.customer-user-logs'
                },
                resolve : {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'Customers',
                            files: ['views/History/admin/historyController.js']
                        }]);
                    }]
                }
            })

            .state('app.customerAdmin-logs', {
                template: '<ui-view></ui-view>',
                activeLink : 'User Usage',
                ncyBreadcrumb: {
                    label: 'Users',
                    skip:true
                }
            })
            .state('app.customerAdmin-logs.list', {
                url:'/user-usage',
                templateUrl: 'views/History/user/logs-users-list.html',
                controller:'adminHistoryCtrl',
                activeLink: 'User Usage',
                ncyBreadcrumb: {
                    label: 'Users'
                },
                resolve : {
                    checkPermission: checkPermission,
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'Users',
                            files: ['views/History/user/userHistoryController.js']
                        }]);
                    }]
                }
            })
            .state('app.customerAdmin-logs.user-logs', {
                url:'/user-usage/logs/:name/:id?:from?:to',
                templateUrl: 'views/History/user/user-log-history.html',
                controller:'userLogsCtrl',
                activeLink: 'User Usage',
                ncyBreadcrumb: {
                    label: 'Login History',
                    parent : 'app.customerAdmin-logs.list'
                },
                resolve : {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'Users',
                            files: ['views/History/user/userHistoryController.js']
                        }]);
                    }]
                }
            })
            .state('app.customerAdmin-logs.actions', {
                url:'/user-usage/actions/:name/:id/:from/:to/:token',
                templateUrl: 'views/History/user/user-actions-log.html',
                controller:'userActionsCtrl',
                activeLink: 'User Usage',
                ncyBreadcrumb: {
                    label: 'Actions',
                    parent : 'app.customerAdmin-logs.user-logs'
                },
                resolve : {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'Users',
                            files: ['views/History/user/userHistoryController.js']
                        }]);
                    }]
                }
            });
        function checkPermission($state, AuthService, $q, $rootScope,$location,userService) {
            var deferred = $q.defer();
            $rootScope.permission = [];
          //  userService.signUp().then(function(result){
          //      result = window.atob(result);
           //     result = JSON.parse(result);
           //     if(result.status){
                  //  AES_KEY = result.data.AES_KEY;
                  //  DATA_ENCRYPT = result.data.DATA_ENCRYPT;
                    AuthService.checkUrl($rootScope.currentUrl).then(function (result1) {
                        $rootScope.permission = result1;
                        if ($rootScope.permission) {
                            deferred.resolve();
                        }else{
                            $state.go("app.404");
                            deferred.reject();
                        }
                    });
                    return deferred.promise;
             //   }
            //})
        }
    }]);
